﻿using System.Text.RegularExpressions;

namespace Framework.Utility
{
    public static class FormatPhoneNumberHelper
    {
        public static string RemoveFormatPhone(this string maskPhoneNumber)
        {
            if (!string.IsNullOrEmpty(maskPhoneNumber))
            {
                return maskPhoneNumber.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "");
            }
            return maskPhoneNumber;
        }
        public static string ApplyFormatPhone(this string phoneNumber)
        {
            var result = "";
            
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                phoneNumber = RemoveFormatPhone(phoneNumber);

                if (!string.IsNullOrEmpty(phoneNumber) && phoneNumber.Length == 10)
                {
                    phoneNumber = phoneNumber.Replace(" ", "");
                    result = "(" + phoneNumber.Substring(0, 3) + ") " + phoneNumber.Substring(3, 3) + "-" + phoneNumber.Substring(6, 4);
                }
                else
                {
                    result = phoneNumber;
                }

            }
            return result;
        }

        public static bool IsFormatPhone(this string phoneNumber)
        {
            var match = Regex.Match(phoneNumber, @"(^\s*$)|(^\d{10}$)|(^[(]\d{3}[)]\s\d{3}[-]\d{4}$)");
            return match.Success;
        }

        public static int ParseToInt(this string number)
        {
            int result = 0;
            int.TryParse(number, out result);
            return result;
        }
    }
}
