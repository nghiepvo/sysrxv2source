﻿using System;
using System.Text.RegularExpressions;

namespace Framework.Utility
{
    public static class HtmlRemoval
    {
       /// <summary>
        /// Extension remove HTML from string with Regex.
       /// </summary>
       /// <param name="content">html content</param>
       /// <returns></returns>
        public static string RemoveHtml(this string content)
       {
           return StripTagsRegexCompiled(content);
       }

        /// <summary>
        /// Remove HTML from string with Regex.
        /// </summary>
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static readonly Regex HtmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string StripTagsRegexCompiled(string source)
        {
            return HtmlRegex.Replace(source, string.Empty);
        }

        /// <summary>
        /// Remove HTML tags from string using char array.
        /// </summary>
        public static string StripTagsCharArray(string source)
        {
            var array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            foreach (char @let in source)
            {
                if (@let == '<')
                {
                    inside = true;
                    continue;
                }
                if (@let == '>')
                {
                    inside = false;
                    continue;
                }
                if (inside) continue;
                array[arrayIndex] = @let;
                arrayIndex++;
            }
            return new string(array, 0, arrayIndex);
        }

        public static string ReplaceSpecialChar(this string s)
        {
            const string specialChar = @"/\()><=+-*~!#$%^&;";

            foreach (char c in specialChar)
            {
                string r = string.Format("\\{0}", c);

                s = s.Replace(c.ToString(), r);
            }

            return s;
        }
    }
}
