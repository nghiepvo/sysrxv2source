﻿using System;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Text.RegularExpressions;

namespace Framework.Utility
{
    public static class DataTypeProtectExtension
    {
        public static string ProtectYesAndEmpty(this int value)
        {
            return value > 0 ? "Yes" : string.Empty;
        }
        public static int ProtectInt32(object data)
        {
            return ProtectInt32(data, -1);
        }

        public static object ProtectDataType(object data, Type type, object defaultValue)
        {
            return ProtectDataType(data, type, defaultValue, CultureInfo.CurrentCulture);
        }

        public static object ProtectDataType(object data, Type type, object defaultValue, IFormatProvider provider)
        {
            if (data == null)
                return defaultValue;
            try
            {
                return Convert.ChangeType(data, type, provider);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static string ProtectString(object data)
        {
            return ProtectDataType(data, typeof(string), string.Empty).ToString();
        }

        public static short ProtectInt16(object data, short defaultValue)
        {
            return (short)ProtectDataType(data, typeof(short), defaultValue);
        }

        public static int ProtectInt32(object data, int defaultValue)
        {
            return (int)ProtectDataType(data, typeof(int), defaultValue);
        }

        public static long ProtectInt64(object data, long defaultValue)
        {
            return (long)ProtectDataType(data, typeof(long), defaultValue);
        }

        public static bool ProtectBoolean(object data, bool defaultValue)
        {
            return (bool)ProtectDataType(data, typeof(bool), defaultValue);
        }

        public static double ProtectDouble(object data, double defaultValue)
        {
            return (double)ProtectDataType(data, typeof(double), defaultValue);
        }

        public static decimal? ProtectDecimal(object data, decimal? defaultValue)
        {
            return (decimal?)ProtectDataType(data, typeof(decimal), defaultValue);
        }

        public static decimal ProtectDecimal(object data, decimal defaultValue)
        {
            return (decimal)ProtectDataType(data, typeof(decimal), defaultValue);
        }

        public static DateTime ProtectDateTime(object data, string format, DateTime defaultValue)
        {
            try
            {
                return DateTime.ParseExact(data.ToString(), format, CultureInfo.CurrentCulture);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static DateTime ProtectDateTime(object data, DateTime defaultValue)
        {
            return (DateTime)ProtectDataType(data, typeof(DateTime), defaultValue);
        }

        public static DateTime ProtectDateTime(DateTime baseDateTime, TimeSpan offset)
        {
            DateTime dt = baseDateTime;
            dt = dt.AddMilliseconds(offset.TotalMilliseconds);

            return dt;
        }

        public static DateTime ProtectDateTimeVi(string datetime)
        {
            var provider = new CultureInfo("vi-VN");

            return DateTime.ParseExact(datetime, "d", provider);
        }

        public static byte ProtectByte(object data, byte defaultValue)
        {
            return (byte)ProtectDataType(data, typeof(byte), defaultValue);
        }

        private static bool IsEmpty(object data)
        {
            if (data == null || data == DBNull.Value)
                return true;
            if (data.ToString().Length <= 0)
                return true;
            return false;
        }

        public static bool IsEmpty(params object[] data)
        {
            return data.All(IsEmpty);
        }

        public static bool IsNotEmpty(params object[] data)
        {
            return data.All(var => !IsEmpty(var));
        }

        public static bool ValidateString(string str, string expression)
        {
            var reg = new Regex(expression);
            var m = reg.Match(str);
            return m.Value == str;
        }

        public static string FormatData(object data, string format)
        {
            if (data == null) return string.Empty;
            var formattable = data as IFormattable;
            return formattable != null ? formattable.ToString(format, null) : data.ToString();
        }

        public static string ShortenLongString(string inputString, int maxLength, bool insertThreeDots)
        {
            if (insertThreeDots)
                maxLength -= 3;

            if (inputString.Length <= maxLength)
                return inputString;

            string[] tmp = ShortenLongString(inputString, maxLength, 0);
            return tmp[0] + (insertThreeDots ? "..." : "");
        }

        public static string[] ShortenLongString(string inputString, int leftLength, int rightLength)
        {
            var arrRes = new string[2];

            leftLength = Math.Min(leftLength, inputString.Length);
            rightLength = Math.Min(rightLength, inputString.Length - leftLength);

            arrRes[0] = inputString.Substring(0, leftLength);
            if (leftLength > 0)
            {
                if (inputString[leftLength] != ' ' && inputString[leftLength] != '\n')
                {
                    int trueIndex = Math.Max(inputString.LastIndexOf(" ", leftLength, StringComparison.Ordinal), inputString.LastIndexOf("\n", leftLength, StringComparison.Ordinal));
                    if (trueIndex > 0)
                        arrRes[0] = inputString.Substring(0, trueIndex);
                }
                inputString = inputString.Remove(0, arrRes[0].Length);
            }

            arrRes[1] = inputString.Remove(0, inputString.Length - rightLength);
            if (rightLength > 0)
            {
                int beginIndex = inputString.Length - rightLength;
                if (inputString[beginIndex - 1] != ' ' && inputString[beginIndex - 1] != '\n')
                {
                    int trueIndex = Math.Max(inputString.LastIndexOf(" ", beginIndex, StringComparison.Ordinal), inputString.IndexOf("\n", beginIndex, StringComparison.Ordinal));
                    if (trueIndex > 0)
                        arrRes[1] = inputString.Remove(0, trueIndex);
                }
            }

            return arrRes;
        }

        public static string WrapText(string inputText, int lineLength)
        {
            string[] texts = inputText.Split(new[] { '\n' });
            var bld = new StringBuilder();
            foreach (string text in texts)
            {
                string tmp = text;
                while (tmp.Length > 0)
                {
                    string shorten = ShortenLongString(tmp, lineLength, false);
                    bld.Append(shorten + "\n");
                    tmp = tmp.Remove(0, shorten.Length);
                }
            }

            return bld.ToString();
        }
    }
}
