﻿using RazorEngine;
using RazorEngine.Templating;

namespace Framework.Utility
{
    public static class RenderRazorEngineExtension
    {
        public static string Render<T>(this string template, T model) where T : new()
        {
            using (var serviceRazorEngine = new TemplateService())
            {
                Razor.SetTemplateService(serviceRazorEngine);
                return Razor.Parse(template, model);
            }
        }
    }
}
