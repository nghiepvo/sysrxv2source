﻿using System;
using System.Runtime.Serialization;

namespace Framework.Exceptions
{
    public class WorkCompException: Exception
    {

        public string WorkCompUserName { get; set; }
        /// <summary>
        ///     Initializes a new instance of the <see cref="WorkCompException" /> class.
        /// </summary>
        public WorkCompException()
        {
         
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="WorkCompException" /> class.
        /// </summary>
        /// <param name="message">
        ///     The message.
        /// </param>
        public WorkCompException(string message)
            : base(message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="WorkCompException" /> class.
        /// </summary>
        /// <param name="message">
        ///     The message.
        /// </param>
        /// <param name="inner">
        ///     The root cause.
        /// </param>
        public WorkCompException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="WorkCompException" /> class.
        /// </summary>
        /// <param name="info">
        ///     The serialization information.
        /// </param>
        /// <param name="context">
        ///     The context.
        /// </param>
        public WorkCompException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}