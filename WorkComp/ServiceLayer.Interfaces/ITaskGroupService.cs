﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface ITaskGroupService : IMasterFileService<TaskGroup>
    {
        List<TaskTemplate> GetListTaskTemplate(int taskGroupId, int? referralId);
    }
}