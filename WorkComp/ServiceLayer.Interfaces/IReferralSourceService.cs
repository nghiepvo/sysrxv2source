﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IReferralSourceService : IMasterFileService<ReferralSource>
    {
        InfoWhenChangeReferralSourceInReferral GetInfoWhenChangeReferralSourceInReferral(int idReferral);
    }
}