﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IClaimNumberService : IMasterFileService<ClaimNumber>
    {
        InfoWhenChangeClaimNumberInReferral GetInfoWhenChangeClaimNumberInReferral(int idClaimNumber);
    }
}