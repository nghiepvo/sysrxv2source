﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IZipService : IMasterFileService<Zip>
    {
    }
}