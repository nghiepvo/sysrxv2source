﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IDiaryService : IMasterFileService<Diary>
    {
        IList<Diary> GetDiaryWhenChangeReferral(int? referralId);
    }
}