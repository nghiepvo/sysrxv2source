﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface ICaseManagerService : IMasterFileService<CaseManager>
    {
        InfoWhenChangeCaseManagerInReferral GetInfoWhenChangeCaseManagerInReferral(int idCaseManager);
    }
}