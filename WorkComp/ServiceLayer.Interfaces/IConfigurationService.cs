﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IConfigurationService : IMasterFileService<Configuration>
    {
    }
}