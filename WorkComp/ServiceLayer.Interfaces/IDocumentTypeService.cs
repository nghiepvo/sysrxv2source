﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IDocumentTypeService : IMasterFileService<DocumentType>
    {
    }
}