﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface ISampleTestingTypeService : IMasterFileService<SampleTestingType>
    {
    }
}