﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;

namespace ServiceLayer.Interfaces
{
    public interface IReferralTaskService : IMasterFileService<ReferralTask>
    {
        int GetPreviousReferralTaskId(int id, int referralId);
        int GetNextReferralTaskId(int id, int referralId);
        dynamic GetDataParentForGrid(IQueryInfo queryInfo);
        void AddMultiple(List<ReferralTask> listEntity, int referralId);
        void CompletedTask(int id);
        void AssignToForReferralTask(List<int> listIdSelected, bool isSelectAll, int? assignToId, string typeWithUser);
        dynamic GetDataPrintToReferralTask(List<int> listIdSelected, ReferralQueryInfo queryInfo);
        bool UpdateComplateTask(ReferralTask referralTask);
    }
}