﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;

namespace ServiceLayer.Interfaces
{
    public interface IUserService : IMasterFileService<User>
    {
        void Active(int id);
        dynamic GetListEmailUser(IQueryInfo queryInfo);
    }
}