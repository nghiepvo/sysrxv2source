﻿
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IClaimantService : IMasterFileService<Claimant>
    {
        InfoWhenChangeClaimantInReferral GetInfoWhenChangeClaimantInReferral(int idClaimant);
    }
}
