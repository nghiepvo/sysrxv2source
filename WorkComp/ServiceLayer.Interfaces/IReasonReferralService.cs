﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IReasonReferralService : IMasterFileService<ReasonReferral>
    {
        IList<ReasonReferralParrentVo> GetReasonReferral(int? parentId);
        IList<ReasonReferralWithReferralVo> GetSelectedReasonReferralbyReferral(int referralId);
        IList<ReasonReferralWithReferralVo> GetUnselectedReasonReferralByReferral(int referralId);
        List<LookupItemVo> GetListReasonReferral();
        bool UpdateActiveReasonReferral(int id);
    }
}