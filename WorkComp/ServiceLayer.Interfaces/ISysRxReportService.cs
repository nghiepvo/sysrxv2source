﻿using System.Collections.Generic;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ReportValueObject;

namespace ServiceLayer.Interfaces
{
    public interface ISysRxReportService
    {
        IList<UtilizationByInjuredWorkerVo> GetDataUtilizationByInjuredWorkerReport(SysRxReportQueryInfo queryInfo, ref int totalRow);

        IList<TestResultsDetailVo> GetDataTestResultdetailReport(SysRxReportQueryInfo queryInfo, ref int totalRow);

        IList<CancellationDetailVo> GetCancellationDetailtReport(SysRxReportQueryInfo queryInfo, ref int totalRow);

        IList<TestResultsSummaryVo> GetTestResultsSummaryReport(SysRxReportQueryInfo queryInfo, ref int totalRow);

        IList<MGMTStatusSummaryReportVo> GetMGMTStatusSummaryReport(SysRxReportQueryInfo queryInfo, ref int totalRow);

        IList<MGMTUserProductivityVo> GetMGMTUserProductivityReport(SysRxReportQueryInfo queryInfo, ref int totalRow);
    }
}