﻿using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IReportService
    {
        dynamic GetDataForGrid(QueryInfo queryInfo,ReferralSearchViewModel searchCondition );
    }
}