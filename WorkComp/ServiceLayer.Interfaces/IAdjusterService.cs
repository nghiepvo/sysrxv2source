﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IAdjusterService : IMasterFileService<Adjuster>
    {
        InfoWhenChangeAdjusterInReferral GetInfoWhenChangeAdjusterInReferral(int idAdjuster);
    }
}