﻿using System;
using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IAlertService : IMasterFileService<Alert>
    {
        dynamic GetAlertPartial(int idCurrentUser, bool isAdminRole, int startIndex, int countItem);
        dynamic GetDataAlertForGrid(int idCurrentUser, bool isAdminRole, string searchId, DateTime? startDate, DateTime? endDate, int startIndex, int countItem);
    }
}