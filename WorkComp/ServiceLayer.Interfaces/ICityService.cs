﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface ICityService : IMasterFileService<City>
    {
    }
}