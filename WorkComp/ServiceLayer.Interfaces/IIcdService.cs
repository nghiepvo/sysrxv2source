﻿using System;
using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IIcdService : IMasterFileService<Icd>
    {
        List<LookupItemVo> GetListIcdType();
        List<Icd> GetIcdsByReferral(int referralId);
    }
}