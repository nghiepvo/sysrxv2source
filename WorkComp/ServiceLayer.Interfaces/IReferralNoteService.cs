﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IReferralNoteService : IMasterFileService<ReferralNote>
    {
        IList<ReferralNote> GetReferralNoteWhenChangeReferral(int? referralId);
    }
}