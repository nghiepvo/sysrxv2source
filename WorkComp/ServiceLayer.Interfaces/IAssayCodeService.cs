﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IAssayCodeService : IMasterFileService<AssayCode>
    {
    }
}