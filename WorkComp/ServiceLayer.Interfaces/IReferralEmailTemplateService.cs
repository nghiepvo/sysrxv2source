﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IReferralEmailTemplateService : IMasterFileService<ReferralEmailTemplate>
    {
        ReferralEmailTemplate GetIncludeAttachment(int referralId, int emailTemplateId);
    }
}