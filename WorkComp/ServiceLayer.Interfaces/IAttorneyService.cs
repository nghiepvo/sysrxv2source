﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IAttorneyService : IMasterFileService<Attorney>
    {
        InfoWhenChangeAttorneyInReferral GetInfoWhenChangeAttorneyInReferral(int idAttorney);
    }
}