﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IAssayCodeDescriptionService : IMasterFileService<AssayCodeDescription>
    {
    }
}