﻿using System.Linq;
using System.Transactions;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace ServiceLayer
{
    public class AdjusterService : MasterFileService<Adjuster>, IAdjusterService
    {
        private readonly IAdjusterRepository _adjusterRepository;
        private readonly ISolrReferralService _referralSolrService;
        public AdjusterService(IAdjusterRepository adjusterRepository,
                                 ISolrReferralService referralSolrService,                    
                                    IBusinessRuleSet<Adjuster> businessRuleSet = null)
            : base(adjusterRepository, adjusterRepository, businessRuleSet)
        {
            _adjusterRepository = adjusterRepository;
            _referralSolrService = referralSolrService;
        }

        public InfoWhenChangeAdjusterInReferral GetInfoWhenChangeAdjusterInReferral(int idAdjuster)
        {
            return _adjusterRepository.GetInfoWhenChangeAdjusterInReferral(idAdjuster);
        }

        public override Adjuster Update(Adjuster model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var id = model.Id;
                var oldData = _adjusterRepository.FirstOrDefault(o => o.Id == id);
                if (oldData == null)
                {
                    return model;
                }
                var oldName = oldData.FirstName + " " + (oldData.MiddleName ?? "") + " " + (oldData.LastName ?? "");
                var newName = model.FirstName + " " + (model.MiddleName ?? "") + " " + (model.LastName ?? "");
                base.Update(model);
                //Write to solr
                if (newName.Trim() != oldName.Trim())
                {
                    var listReferral =
                        _referralSolrService.GetByField(new SolrQueryByField("adjusterid", model.Id.ToString()))
                            .ToList();
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.AdjusterName = newName;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                }
                objTransaction.Complete();
                return model;
            }
        }
    }
}