﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class CollectionSiteService : MasterFileService<CollectionSite>, ICollectionSiteService
    {
        private readonly ICollectionSiteRepository _collectionSiteRepository;

        public CollectionSiteService(ICollectionSiteRepository collectionSiteRepository, IBusinessRuleSet<CollectionSite> businessRuleSet = null)
            : base(collectionSiteRepository, collectionSiteRepository, businessRuleSet)
        {
            _collectionSiteRepository = collectionSiteRepository;
        }

        public InfoWhenChangeCollectionSiteInReferral GetInfoWhenChangeCollectionSiteInReferral(int idCollectionSite)
        {
            return _collectionSiteRepository.GetInfoWhenChangeCollectionSiteInReferral(idCollectionSite);
        }
    }
}