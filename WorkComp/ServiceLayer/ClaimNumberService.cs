﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace ServiceLayer
{
    public class ClaimNumberService : MasterFileService<ClaimNumber>, IClaimNumberService
    {
        private readonly IClaimNumberRepository _claimNumberRepository;
        private readonly ISolrClaimNumberService _claimNumberSolrService;
        private readonly IStateRepository _stateRepository;
        private readonly ISolrReferralService _referralSolrService;
        private readonly ISolrReferralTaskService _referralTaskSolrService;
        private readonly IStateService _stateService;
        private readonly IPayerService _payerService;
        private readonly IAdjusterService _adjusterService;
        private readonly IBranchService _branchService;
        private readonly IClaimantService _claimantService;
        private readonly IEmployerService _employerService;
        
        public ClaimNumberService(IClaimNumberRepository claimNumberRepository, ISolrClaimNumberService claimNumberSolrService,
                                    ISolrReferralService referralSolrService, ISolrReferralTaskService referralTaskSolrService,
                                    IStateRepository stateRepository, IStateService stateService, IPayerService payerService,
                                    IAdjusterService adjusterService, IBranchService branchService, IClaimantService claimantService, 
                                    IEmployerService employerService, IBusinessRuleSet<ClaimNumber> businessRuleSet = null)
            : base(claimNumberRepository, claimNumberRepository, businessRuleSet)
        {
            _claimNumberRepository = claimNumberRepository;
            _claimNumberSolrService = claimNumberSolrService;
            _referralSolrService = referralSolrService;
            _stateRepository = stateRepository;
            _stateService = stateService;
            _payerService = payerService;
            _adjusterService = adjusterService;
            _branchService = branchService;
            _claimantService = claimantService;
            _employerService = employerService;
            _referralTaskSolrService = referralTaskSolrService;
        }

        public InfoWhenChangeClaimNumberInReferral GetInfoWhenChangeClaimNumberInReferral(int idClaimNumber)
        {
            return _claimNumberRepository.GetInfoWhenChangeClaimNumberInReferral(idClaimNumber);
        }
        public override ClaimNumber Add(ClaimNumber model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var objAddItem = base.Add(model);
                if (objAddItem.Id > 0)
                {
                    var objState = _stateService.FirstOrDefault(o => o.Id == objAddItem.StateId);
                    if (objState!= null)
                    {
                        objAddItem.State = objState;
                    }

                    var objAdjuster = _adjusterService.FirstOrDefault(o => o.Id == objAddItem.AdjusterId);
                    if (objAdjuster != null)
                    {
                        objAddItem.Adjuster = objAdjuster;
                    }

                    var objPayer = _payerService.FirstOrDefault(o => o.Id == objAddItem.PayerId);
                    if (objPayer != null)
                    {
                        objAddItem.Payer = objPayer;
                    }

                    if (objAddItem.BranchId != null)
                    {
                        var objBranch = _branchService.FirstOrDefault(o => o.Id == objAddItem.BranchId);
                        if (objBranch != null)
                        {
                            objAddItem.Branch = objBranch;
                        }
                    }

                    var objEmployee = _employerService.FirstOrDefault(o => o.Id == objAddItem.EmployerId);
                    if (objEmployee != null)
                    {
                        objAddItem.Employer = objEmployee;
                    }

                    var objClaimant = _claimantService.FirstOrDefault(o => o.Id == objAddItem.ClaimantId);
                    if (objClaimant != null)
                    {
                        objAddItem.Claimant = objClaimant;
                    }
                    //Write to solr
                    var objSolrItem = objAddItem.MapTo<SolrClaimNumber>();
                    // Delete all item which have same code
                    _claimNumberSolrService.Add(objSolrItem);
                    _claimNumberSolrService.Commit();
                }
                objTransaction.Complete();
                return objAddItem;
            }
        }

        public override ClaimNumber Update(ClaimNumber model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var id = model.Id;
                var oldData = _claimNumberRepository.FirstOrDefault(o => o.Id == id);
                if (oldData == null)
                {
                    return model;
                }
                var newName = model.Name;
                var oldName = oldData.Name;
                var newDoi = model.Doi;
                var oldDoi = oldData.Doi;
                // get new state
                var oldState = oldData.State == null ? "" : oldData.State.Name;
                var objNewState = _stateRepository.GetById(model.StateId.GetValueOrDefault());
                var newState = objNewState == null ? "" : objNewState.Name;
                base.Update(model);
                //Write to solr
                var objSolrItem = model.MapTo<SolrClaimNumber>();
                // Delete all item which have same code
                _claimNumberSolrService.Update(objSolrItem);
                _claimNumberSolrService.Commit();
                var listReferral = new List<SolrReferral>();
                var listReferralTask = new List<SolrReferralTask>();
                if (newName.Trim() != oldName.Trim())
                {
                    listReferral =
                   _referralSolrService.GetByField(new SolrQueryByField("claimnumberid", model.Id.ToString())).ToList();
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.ClaimNumber = newName;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                }
                if (newDoi.ToString("MMddyyyy") != oldDoi.ToString("MMddyyyy"))
                {
                    if (listReferral.Count == 0)
                    {
                        listReferral =
                            _referralSolrService.GetByField(new SolrQueryByField("claimnumberid", model.Id.ToString())).ToList();
                    }
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.ClaimNumberDoi = newDoi;
                            solrReferral.Doi = newDoi;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                }
                if (newState.Trim() != oldState.Trim())
                {
                    if (listReferral.Count == 0)
                    {
                        listReferral =
                            _referralSolrService.GetByField(new SolrQueryByField("claimnumberid", model.Id.ToString())).ToList();
                    }
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.Jurisdiction = newState;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                    if (listReferralTask.Count == 0)
                    {
                        listReferralTask =
                            _referralTaskSolrService.GetByField(new SolrQueryByField("claimnumberid", model.Id.ToString())).ToList();
                    }
                    if (listReferralTask.Count > 0)
                    {
                        foreach (var solrReferralTask in listReferralTask)
                        {
                            solrReferralTask.Jurisdiction = newState;
                            _referralTaskSolrService.Update(solrReferralTask);
                        }
                        _referralTaskSolrService.Commit();
                    }
                }
                objTransaction.Complete();
                return model;
            }
        }

        public override void DeleteById(int id)
        {
            using (var objTransaction = new TransactionScope())
            {
                base.DeleteById(id);
                //Write to solr
                _claimNumberSolrService.DeleteAllByField(new SolrQueryByField("id", id.ToString()));
                _claimNumberSolrService.Commit();
                objTransaction.Complete();
            }
        }

        public override void DeleteAll(Expression<Func<ClaimNumber, bool>> @where = null)
        {
            using (var objTransaction = new TransactionScope())
            {
                // Get listId from where
                var listItemDelete = base.Get(@where);
                base.DeleteAll(@where);
                //Write to solr
                foreach (var itemDelete in listItemDelete)
                {
                    _claimNumberSolrService.DeleteAllByField(new SolrQueryByField("id", itemDelete.Id.ToString()));
                }
                _claimNumberSolrService.Commit();
                objTransaction.Complete();
            }
        }

        public override List<LookupItemVo> GetLookup(LookupQuery query, Func<ClaimNumber, LookupItemVo> selector)
        {
            var objData = _claimNumberSolrService.GetLookup(query);
            var result = objData ?? base.GetLookup(query, selector);
            return result;
        }

        public override dynamic GetDataForGridMasterfile(IQueryInfo queryInfo)
        {
            var objData = _claimNumberSolrService.GetDataForGridMasterfile(queryInfo);
            var result = objData ?? base.GetDataForGridMasterfile(queryInfo);
            return result;
        }
    }
}