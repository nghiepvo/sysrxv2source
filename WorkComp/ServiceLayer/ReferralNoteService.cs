﻿using System.Transactions;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using Solr.ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class ReferralNoteService : MasterFileService<ReferralNote>, IReferralNoteService
    {
        private readonly IReferralNoteRepository _noteRepository;
        private readonly ISolrReferralService _referralSolrService;
        public ReferralNoteService(IReferralNoteRepository noteRepository, ISolrReferralService referralSolrService, IBusinessRuleSet<ReferralNote> businessRuleSet = null)
            : base(noteRepository, noteRepository, businessRuleSet)
        {
            _noteRepository = noteRepository;
            _referralSolrService = referralSolrService;
        }

        public System.Collections.Generic.IList<ReferralNote> GetReferralNoteWhenChangeReferral(int? referralId)
        {
            return _noteRepository.GetReferralNoteWhenChangeReferral(referralId);
        }

        public override ReferralNote Add(ReferralNote model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var objAddItem = base.Add(model);
                if (objAddItem.Id > 0)
                {
                    //Write to solr to update total note
                    var objSolrItem = _referralSolrService.GetById(objAddItem.ReferralId);
                    if (objSolrItem != null)
                    {
                        objSolrItem.TotalNote = objSolrItem.TotalNote + 1;
                        _referralSolrService.Update(objSolrItem);
                        _referralSolrService.Commit();
                    }
                }
                objTransaction.Complete();
                return objAddItem;
            }
        }
    }
}