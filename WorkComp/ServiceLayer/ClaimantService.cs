﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using Framework.BusinessRule;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace ServiceLayer
{
    public class ClaimantService : MasterFileService<Claimant>, IClaimantService
    {
        private readonly IClaimantRepository _claimantRepository;
        private readonly ISolrClaimantService _claimantSolrService;
        private readonly ISolrClaimNumberService _claimNumberSolrService;
        private readonly ISolrReferralService _referralSolrService;
        private readonly IStateRepository _stateRepository;
        private readonly ISolrReferralTaskService _referralTaskSolrService;
        public ClaimantService(IClaimantRepository claimantRepository, ISolrClaimantService claimantSolrService,
                                ISolrReferralService referralSolrService, IStateRepository stateRepository,
                                ISolrReferralTaskService referralTaskSolrService, ISolrClaimNumberService claimNumberSolrService, IBusinessRuleSet<Claimant> businessRuleSet = null)
            : base(claimantRepository, claimantRepository, businessRuleSet)
        {
            _claimantRepository = claimantRepository;
            _claimantSolrService = claimantSolrService;
            _referralSolrService = referralSolrService;
            _stateRepository = stateRepository;
            _referralTaskSolrService = referralTaskSolrService;
            _claimNumberSolrService = claimNumberSolrService;
        }

        public InfoWhenChangeClaimantInReferral GetInfoWhenChangeClaimantInReferral(int idClaimant)
        {
            return _claimantRepository.GetInfoWhenChangeClaimantInReferral(idClaimant);
        }

        public override Claimant Add(Claimant model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var objAddItem = base.Add(model);
                if (objAddItem.Id > 0)
                {
                    //Write to solr
                    var objSolrItem = objAddItem.MapTo<SolrClaimant>();
                    // Delete all item which have same code
                    _claimantSolrService.Add(objSolrItem);
                    _claimantSolrService.Commit();
                }
                objTransaction.Complete();
                return objAddItem;
            }
        }

        public override Claimant Update(Claimant model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var id = model.Id;
                var oldData = _claimantRepository.FirstOrDefault(o=>o.Id==id);
                if (oldData == null)
                {
                    return model;
                }
                var newName = (model.FirstName ?? "") + " " + (model.MiddleName ?? "") + " " + (model.LastName ?? "");
                var oldName = (oldData.FirstName ?? "") + " " + (oldData.MiddleName ?? "") + " " + (oldData.LastName ?? "");
                var newClaimantSsn = model.Ssn ?? string.Empty;
                var oldClaimantSsn = oldData.Ssn??string.Empty;
                var newClaimantDob = model.Birthday;
                var oldClaimantDob = oldData.Birthday;
                // get new state
                var oldClaimantState = oldData.State == null ? "" : oldData.State.Name;
                var objNewState = _stateRepository.GetById(model.StateId.GetValueOrDefault());
                var newClaimantState = objNewState == null ? "" : objNewState.Name;
                base.Update(model);
                //Write to solr
                var objSolrItem = model.MapTo<SolrClaimant>();
                // Delete all item which have same code
                _claimantSolrService.Update(objSolrItem);
                _claimantSolrService.Commit();
                var listReferral = new List<SolrReferral>();
                var listReferralTask = new List<SolrReferralTask>();
                var listClaimNumber = new List<SolrClaimNumber>();
                if (newName.Trim() != oldName.Trim())
                {
                    listReferral =
                   _referralSolrService.GetByField(new SolrQueryByField("claimantid", model.Id.ToString())).ToList();
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.ClaimantName = newName;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                    if (listReferralTask.Count == 0)
                    {
                        listReferralTask =
                            _referralTaskSolrService.GetByField(new SolrQueryByField("claimantid", model.Id.ToString())).ToList();
                    }
                    if (listReferralTask.Count > 0)
                    {
                        foreach (var solrReferralTask in listReferralTask)
                        {
                            solrReferralTask.ClaimantName = newName;
                            _referralTaskSolrService.Update(solrReferralTask);
                        }
                        _referralTaskSolrService.Commit();
                    }

                    if (listClaimNumber.Count == 0)
                    {
                        listClaimNumber =
                            _claimNumberSolrService.GetByField(new SolrQueryByField("claimantid", model.Id.ToString())).ToList();
                    }
                    if (listClaimNumber.Count > 0)
                    {
                        foreach (var solrClaimNumber in listClaimNumber)
                        {
                            solrClaimNumber.ClaimantName = newName;
                            _claimNumberSolrService.Update(solrClaimNumber);
                        }
                        _claimNumberSolrService.Commit();
                    }
                }
                if (newClaimantSsn.Trim() != oldClaimantSsn.Trim())
                {
                    if (listReferral.Count==0)
                    {
                        listReferral =
                            _referralSolrService.GetByField(new SolrQueryByField("claimantid", model.Id.ToString())).ToList();
                    }
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.ClaimantSsn = newClaimantSsn;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                    if (listClaimNumber.Count == 0)
                    {
                        listClaimNumber =
                            _claimNumberSolrService.GetByField(new SolrQueryByField("claimantid", model.Id.ToString())).ToList();
                    }
                    if (listClaimNumber.Count > 0)
                    {
                        foreach (var solrClaimNumber in listClaimNumber)
                        {
                            solrClaimNumber.ClaimantSsn = newClaimantSsn;
                            _claimNumberSolrService.Update(solrClaimNumber);
                        }
                        _claimNumberSolrService.Commit();
                    }
                }
                if (newClaimantDob.GetValueOrDefault().ToString("MMddyyyy") != oldClaimantDob.GetValueOrDefault().ToString("MMddyyyy"))
                {
                    if (listReferral.Count == 0)
                    {
                        listReferral =
                            _referralSolrService.GetByField(new SolrQueryByField("claimantid", model.Id.ToString())).ToList();
                    }
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.ClaimantDob = newClaimantDob;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                    if (listClaimNumber.Count == 0)
                    {
                        listClaimNumber =
                            _claimNumberSolrService.GetByField(new SolrQueryByField("claimantid", model.Id.ToString())).ToList();
                    }
                    if (listClaimNumber.Count > 0)
                    {
                        foreach (var solrClaimNumber in listClaimNumber)
                        {
                            solrClaimNumber.ClaimantDob = newClaimantDob;
                            _claimNumberSolrService.Update(solrClaimNumber);
                        }
                        _claimNumberSolrService.Commit();
                    }
                }
                if (newClaimantState.Trim() != oldClaimantState.Trim())
                {
                    if (listReferral.Count == 0)
                    {
                        listReferral =
                            _referralSolrService.GetByField(new SolrQueryByField("claimantid", model.Id.ToString())).ToList();
                    }
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.PatientState = newClaimantState;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                    if (listReferralTask.Count == 0)
                    {
                        listReferralTask =
                            _referralTaskSolrService.GetByField(new SolrQueryByField("claimantid", model.Id.ToString())).ToList();
                    }
                    if (listReferralTask.Count > 0)
                    {
                        foreach (var solrReferralTask in listReferralTask)
                        {
                            solrReferralTask.PatientState = newClaimantState;
                            _referralTaskSolrService.Update(solrReferralTask);
                        }
                        _referralTaskSolrService.Commit();
                    }
                    
                }
                objTransaction.Complete();
                return model;
            }
        }

        public override void DeleteById(int id)
        {
            using (var objTransaction = new TransactionScope())
            {
                base.DeleteById(id);
                //Write to solr
                _claimantSolrService.DeleteAllByField(new SolrQueryByField("id", id.ToString()));
                _claimantSolrService.Commit();
                objTransaction.Complete();
            }
        }

        public override void DeleteAll(Expression<Func<Claimant, bool>> @where = null)
        {
            using (var objTransaction = new TransactionScope())
            {
                // Get listId from where
                var listItemDelete = base.Get(@where);
                base.DeleteAll(@where);
                //Write to solr
                foreach (var itemDelete in listItemDelete)
                {
                    _claimantSolrService.DeleteAllByField(new SolrQueryByField("id", itemDelete.Id.ToString()));
                }
                _claimantSolrService.Commit();
                objTransaction.Complete();
            }
        }

        public override List<LookupItemVo> GetLookup(LookupQuery query, Func<Claimant, LookupItemVo> selector)
        {
            var objData = _claimantSolrService.GetLookup(query);
            var result = objData ?? base.GetLookup(query, selector);
            return result;
        }

        public override dynamic GetDataForGridMasterfile(IQueryInfo queryInfo)
        {
            var objData = _claimantSolrService.GetDataForGridMasterfile(queryInfo);
            var result = objData ?? base.GetDataForGridMasterfile(queryInfo);
            return result;
        }
    }
}
