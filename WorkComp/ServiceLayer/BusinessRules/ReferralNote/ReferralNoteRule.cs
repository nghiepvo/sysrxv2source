﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.ReferralNote
{
    public class ReferralNoteRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IReferralNoteRepository _noteRepository;

        public ReferralNoteRule(IReferralNoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var note = instance as Framework.DomainModel.Entities.ReferralNote;
            var validationResult = new List<ValidationResult>();

            if (note == null) return new BusinessRuleResult();

            if (note.ReferralId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Referral");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (note.AssignToId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Assign To");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (string.IsNullOrEmpty(note.Comment))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Comment");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "ReferralNoteRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}