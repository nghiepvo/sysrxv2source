﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;

namespace ServiceLayer.BusinessRules.Zip
{
    public class ZipRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var zip = instance as Framework.DomainModel.Entities.Zip;
            var validationResult = new List<ValidationResult>();
            if (zip != null)
            {
                if (zip.CityId == 0)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "City");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "ZipRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
