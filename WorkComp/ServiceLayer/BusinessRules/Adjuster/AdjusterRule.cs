﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Adjuster
{
    public class AdjusterRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IAdjusterRepository _adjusterRepository;

        public AdjusterRule(IAdjusterRepository adjusterRepository)
        {
            _adjusterRepository = adjusterRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var adjuster = instance as Framework.DomainModel.Entities.Adjuster;
            var validationResult = new List<ValidationResult>();

            if (adjuster == null) return new BusinessRuleResult();
            if (!string.IsNullOrEmpty(adjuster.Phone) && _adjusterRepository.CheckExist(o => o.Phone.Trim().ToLower() == adjuster.Phone.Trim().ToLower() && o.Id != adjuster.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Phone");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (!string.IsNullOrEmpty(adjuster.Email) && _adjusterRepository.CheckExist(o => o.Email.Trim().ToLower() == adjuster.Email.Trim().ToLower() && o.Id != adjuster.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Email");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (adjuster.StateId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "State");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (adjuster.CityId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "City");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (adjuster.ZipId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Zip");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "AdjusterRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
