﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Drug
{
     public class DrugRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
         private readonly IDrugRepository _drugRepository;

        public DrugRule(IDrugRepository drugRepository)
        {
            _drugRepository = drugRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var drug = instance as Framework.DomainModel.Entities.Drug;
            var validationResult = new List<ValidationResult>();

            if (drug == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(drug.Name) && _drugRepository.CheckExist(o => o.Name.Trim().ToLower() == drug.Name.Trim().ToLower() && o.Id != drug.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "DrugRule"; }
        }

         public string[] PropertyNames { get; set; }
    }
}
