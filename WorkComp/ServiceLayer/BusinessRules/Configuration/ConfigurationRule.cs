﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Configuration
{
    public class ConfigurationRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IConfigurationRepository _configurationRepository;

        public ConfigurationRule(IConfigurationRepository configurationRepository)
        {
            _configurationRepository = configurationRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var configuration = instance as Framework.DomainModel.Entities.Configuration;
            var validationResult = new List<ValidationResult>();
            if (configuration != null)
            {
                if (!string.IsNullOrEmpty(configuration.Name) && _configurationRepository.CheckExist(o => o.Name.Trim().ToLower() == configuration.Name.Trim().ToLower() && o.Id != configuration.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "ConfigurationRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}