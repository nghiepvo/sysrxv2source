﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.User
{
    public class UserRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IUserRepository _userRepository;

        public UserRule(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var user = instance as Framework.DomainModel.Entities.User;
            var validationResult = new List<ValidationResult>();
            if (user != null)
            {
                if (!string.IsNullOrEmpty(user.UserName) && _userRepository.CheckExist(o => o.UserName.Trim().ToLower() == user.UserName.Trim().ToLower() && o.Id != user.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "User name");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                if (!string.IsNullOrEmpty(user.Email) && _userRepository.CheckExist(o => o.Email.Trim().ToLower() == user.Email.Trim().ToLower() && o.Id != user.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Email");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                if (user.UserRoleId == 0)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Role");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                if (user.StateId == 0)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "State");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                if (user.CityId == 0)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "City");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                if (user.ZipId == 0)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Zip");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "UserRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
