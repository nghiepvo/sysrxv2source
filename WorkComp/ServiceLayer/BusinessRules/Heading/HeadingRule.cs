﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Heading
{
    public class HeadingRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
         private readonly IHeadingRepository _headingRepository;

         public HeadingRule(IHeadingRepository headingRepository)
        {
            _headingRepository = headingRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var heading = instance as Framework.DomainModel.Entities.Heading;
            var validationResult = new List<ValidationResult>();

            if (heading == null) return new BusinessRuleResult();

            if (string.IsNullOrEmpty(heading.Name))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "DiaryRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
