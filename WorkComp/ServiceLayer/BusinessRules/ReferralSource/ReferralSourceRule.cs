﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.ReferralSource
{
    public class ReferralSourceRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IReferralSourceRepository _referralSourceRepository;

        public ReferralSourceRule(IReferralSourceRepository referralSourceRepository)
        {
            _referralSourceRepository = referralSourceRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var referralSource = instance as Framework.DomainModel.Entities.ReferralSource;
            var validationResult = new List<ValidationResult>();

            if (referralSource == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(referralSource.Phone) && _referralSourceRepository.CheckExist(o => o.Phone.ToLower().Equals(referralSource.Phone.ToLower()) && o.Id != referralSource.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Phone");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (!string.IsNullOrEmpty(referralSource.Email) && _referralSourceRepository.CheckExist(o => o.Email.ToLower().Equals(referralSource.Email.ToLower()) && o.Id != referralSource.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Email");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }


            if (referralSource.ReferralTypeId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Type");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (referralSource.StateId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "State");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referralSource.CityId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "City");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referralSource.ZipId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Zip");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (referralSource.AuthorizationFrom == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "Start Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (referralSource.AuthorizationTo == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "End Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (referralSource.IsAuthorization)
            {
                if (referralSource.AuthorizationFrom == null)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "Start Date");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }

                if (referralSource.AuthorizationTo == null)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "End Date");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
            }

            if (failed)
            {
                var result = new BusinessRuleResult(true, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }
            return new BusinessRuleResult();
            
        }

        public string Name
        {
            get { return "ReferralSourceRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
