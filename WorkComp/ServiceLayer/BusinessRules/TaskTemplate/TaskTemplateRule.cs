﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.TaskTemplate
{
    public class TaskTemplateRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly ITaskTemplateRepository _taskTemplateRepository;

        public TaskTemplateRule(ITaskTemplateRepository taskTemplateRepository)
        {
            _taskTemplateRepository = taskTemplateRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var taskTemplate = instance as Framework.DomainModel.Entities.TaskTemplate;
            var validationResult = new List<ValidationResult>();
            if (taskTemplate != null)
            {
                if (!string.IsNullOrEmpty(taskTemplate.Title) && _taskTemplateRepository.CheckExist(o => o.Title.Trim().ToLower() == taskTemplate.Title.Trim().ToLower() && o.Id != taskTemplate.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Title");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                if (taskTemplate.StatusId == 0)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Status");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "TaskTemplateRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
