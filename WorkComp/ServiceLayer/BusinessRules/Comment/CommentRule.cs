﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Comment
{
    public class CommentRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
         private readonly ICommentRepository _commentRepository;

         public CommentRule(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var comment = instance as Framework.DomainModel.Entities.Comment;
            var validationResult = new List<ValidationResult>();

            if (comment == null) return new BusinessRuleResult();

            if (string.IsNullOrEmpty(comment.CommentContent))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Comment");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "CommentRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
