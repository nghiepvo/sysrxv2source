﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Branch
{
    public class BranchRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IBranchRepository _branchRepository;

        public BranchRule(IBranchRepository branchRepository)
        {
            _branchRepository = branchRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var branch = instance as Framework.DomainModel.Entities.Branch;
            var validationResult = new List<ValidationResult>();

            if (branch == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(branch.Phone) && _branchRepository.CheckExist(o => o.Phone.Trim().ToLower() == branch.Phone.Trim().ToLower() && o.Id != branch.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Phone");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (!string.IsNullOrEmpty(branch.Name) && _branchRepository.CheckExist(o => o.Name.Trim().ToLower() == branch.Name.Trim().ToLower() && o.PayerId == branch.PayerId && o.Id != branch.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ItemExistsWithParentItem"),"branch", branch.Name,"payer");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (branch.PayerId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Payer");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (branch.StateId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "State");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (branch.CityId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "City");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (branch.ZipId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Zip");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "BranchRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
