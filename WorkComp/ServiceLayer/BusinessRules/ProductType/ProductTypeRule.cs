﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.ProductType
{
    public class ProductTypeRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IProductTypeRepository _productTypeRepository;

        public ProductTypeRule(IProductTypeRepository productTypeRepository)
        {
            _productTypeRepository = productTypeRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var productType = instance as Framework.DomainModel.Entities.ProductType;
            var validationResult = new List<ValidationResult>();
            if (productType != null)
            {
                if (!string.IsNullOrEmpty(productType.Name) && _productTypeRepository.CheckExist(o => o.Name.Trim().ToLower() == productType.Name.Trim().ToLower() && o.Id != productType.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }

                if (productType.PayerId.GetValueOrDefault() == 0)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Payer");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }

                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;


            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "ProductTypeRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}