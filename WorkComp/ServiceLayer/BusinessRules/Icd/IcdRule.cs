﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Icd
{
    public class IcdRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IIcdRepository _icdRepository;

        public IcdRule(IIcdRepository icdRepository)
        {
            _icdRepository = icdRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var icd = instance as Framework.DomainModel.Entities.Icd;
            var validationResult = new List<ValidationResult>();
            if (icd != null)
            {
                if (!string.IsNullOrEmpty(icd.Code)&&_icdRepository.CheckExist(o => o.Code.Trim().ToLower() == icd.Code.Trim().ToLower() && o.Id != icd.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Code");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "IcdRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
