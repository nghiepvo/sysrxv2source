﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Diary
{
    public class DiaryRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
         private readonly IDiaryRepository _diaryRepository;

         public DiaryRule(IDiaryRepository diaryRepository)
        {
            _diaryRepository = diaryRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var diary = instance as Framework.DomainModel.Entities.Diary;
            var validationResult = new List<ValidationResult>();

            if (diary == null) return new BusinessRuleResult();

            if (string.IsNullOrEmpty(diary.Heading))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Heading");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (string.IsNullOrEmpty(diary.Reason))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Reason");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (diary.ReferralId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Referral");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (string.IsNullOrEmpty(diary.Comment))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Comment");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "DiaryRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
