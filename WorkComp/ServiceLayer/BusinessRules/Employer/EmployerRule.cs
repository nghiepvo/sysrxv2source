﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Employer
{
    public class EmployerRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IEmployerRepository _employerRepository;

        public EmployerRule(IEmployerRepository employerRepository)
        {
            _employerRepository = employerRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var employer = instance as Framework.DomainModel.Entities.Employer;
            var validationResult = new List<ValidationResult>();

            if (employer == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(employer.Phone) && _employerRepository.CheckExist(o => o.Phone.ToLower().Equals(employer.Phone.ToLower()) && o.Id != employer.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Phone");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
            }

            if (!string.IsNullOrEmpty(employer.Name) && _employerRepository.CheckExist(o => o.Name.ToLower().Equals(employer.Name.ToLower()) && o.Id != employer.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "EmployerRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
