﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.ClaimNumber
{
    public class ClaimNumberRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IClaimNumberRepository _claimNumberRepository;
        public ClaimNumberRule(IClaimNumberRepository claimNumberRepository)
        {
            _claimNumberRepository = claimNumberRepository;
        } 
        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var claimNumber = instance as Framework.DomainModel.Entities.ClaimNumber;
            var validationResult = new List<ValidationResult>();

            if (claimNumber == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(claimNumber.Name) && _claimNumberRepository.CheckExist(o => o.Name.Trim().ToLower() == claimNumber.Name.Trim().ToLower() && o.Id != claimNumber.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimNumber.PayerId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Payer");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimNumber.StateId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Jurisdiction");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (claimNumber.AdjusterId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Adjuster");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (claimNumber.ClaimantId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Claimant");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimNumber.EmployerId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Employer");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimNumber.Doi == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "DOI");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimNumber.OpenDate == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "Open Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimNumber.CloseDate == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "Close Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimNumber.ReClosedDate == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "ReClosed Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimNumber.ReOpenDate == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "ReOpen Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimNumber.ReportDate == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Report Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "ClaimNumber"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
