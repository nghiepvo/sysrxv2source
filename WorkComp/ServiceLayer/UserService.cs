﻿using System.Linq;
using System.Transactions;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace ServiceLayer
{
    public class UserService : MasterFileService<User>, IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ISolrReferralService _referralSolrService;
        private readonly ISolrReferralTaskService _referralTaskSolrService;
        public UserService(IUserRepository userRepository,
                            ISolrReferralService referralSolrService, 
                            ISolrReferralTaskService referralTaskSolrService,
                            IBusinessRuleSet<User> businessRuleSet = null)
            : base(userRepository, userRepository, businessRuleSet)
        {
            _userRepository = userRepository;
            _referralSolrService = referralSolrService;
            _referralTaskSolrService = referralTaskSolrService;
        }

        public void Active(int id)
        {
            _userRepository.Active(id);
            _userRepository.Commit();
        }

        public override User Update(User model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var id = model.Id;
                var oldData = _userRepository.FirstOrDefault(o => o.Id == id);
                if (oldData == null)
                {
                    return model;
                }
                var oldName = oldData.FirstName + " " + (oldData.MiddleName ?? "") + " " + (oldData.LastName ?? "");
                var newName = model.FirstName + " " + (model.MiddleName ?? "") + " " + (model.LastName ?? "");
                base.Update(model);
                //Write to solr
                if (newName.Trim() != oldName.Trim())
                {
                    var listReferral =
                        _referralSolrService.GetByField(new SolrQueryByField("assigntoid", model.Id.ToString()))
                            .ToList();
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.AssignToName = newName;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                    var listReferralTask =
                        _referralTaskSolrService.GetByField(new SolrQueryByField("assigntoid", model.Id.ToString()))
                            .ToList();
                    if (listReferralTask.Count > 0)
                    {
                        foreach (var solrReferralTask in listReferralTask)
                        {
                            solrReferralTask.AssignToName = newName;
                            _referralTaskSolrService.Update(solrReferralTask);
                        }
                        _referralTaskSolrService.Commit();
                    }
                }
                objTransaction.Complete();
                return model;
            }
        }

        public dynamic GetListEmailUser(IQueryInfo queryInfo)
        {
            return _userRepository.GetListEmailUser(queryInfo);
        }
    }
}