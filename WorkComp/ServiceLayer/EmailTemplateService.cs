﻿
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class EmailTemplateService:MasterFileService<EmailTemplate>,IEmailTemplateService
    {
        private readonly IEmailTemplateRepository _emailTemplateRepository;
        public EmailTemplateService(IEmailTemplateRepository emailTemplateRepository, IBusinessRuleSet<EmailTemplate> businessRuleSet = null)
            : base(emailTemplateRepository, emailTemplateRepository, businessRuleSet)
        {
            _emailTemplateRepository = emailTemplateRepository;
        }

        public dynamic GetDataForGridReferralDetail()
        {
            return _emailTemplateRepository.GetDataForGridReferralDetail();
        }

        public ReferralEmailTemplate GetOrderEmailTemplate(int referralId, int emailId)
        {
            return _emailTemplateRepository.GetOrderEmailTemplate(referralId, emailId);
        }

        public DataRequestForAuthorizationEmailTemplate GetDataRequestForAuthorization(int referralId)
        {
            return _emailTemplateRepository.GetDataRequestForAuthorization(referralId);
        }

        public AllEmailForReferral GetListEmailInReferral(int referralId)
        {
            return _emailTemplateRepository.GetListEmailInReferral(referralId);
        }

        public DataRequestForCollectionEmailTemplate GetDataRequestForCollection(int referralId)
        {
            return _emailTemplateRepository.GetDataRequestForCollection(referralId);
        }

        public CollectionServiceRequestEmail GetDataRequestForCollectionServiceRequest(int referralId)
        {
            return _emailTemplateRepository.GetDataRequestForCollectionServiceRequest(referralId);
        }

        public DataMdNotificationLetterTemplate GetDataMdNotificationLetterTemplate(int referralId)
        {
            return _emailTemplateRepository.GetDataMdNotificationLetterTemplate(referralId);
        }

        public DataCustomCommunicationTemplate GetDataCustomCommunicationTemplate(int referralId)
        {
            return _emailTemplateRepository.GetDataCustomCommunicationTemplate(referralId);
        }

        #region EmailTemplateVo

        public string ContentHtmlRequestForAuthorization(int referralId, EmailTemplateType emailTemplateType)
        {
            return _emailTemplateRepository.ContentHtmlRequestForAuthorization(referralId, emailTemplateType);
        }
        #endregion
    }
}
