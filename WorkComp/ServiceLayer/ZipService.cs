﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class ZipService : MasterFileService<Zip>, IZipService
    {
        private readonly IZipRepository _zipRepository;
        public ZipService(IZipRepository zipRepository, IBusinessRuleSet<Zip> businessRuleSet = null)
            : base(zipRepository, zipRepository, businessRuleSet)
        {
            _zipRepository = zipRepository;
        }

    }
}