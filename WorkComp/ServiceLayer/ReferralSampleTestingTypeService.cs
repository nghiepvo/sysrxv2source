﻿using System.Collections.Generic;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class ReferralSampleTestingTypeService : MasterFileService<ReferralSampleTestingType>, IReferralSampleTestingTypeService
    {
        private readonly IReferralSampleTestingTypeRepository _referralSampleTestingTypeRepository;

        public ReferralSampleTestingTypeService(IReferralSampleTestingTypeRepository referralSampleTestingTypeRepository, IBusinessRuleSet<ReferralSampleTestingType> businessRuleSet = null)
            : base(referralSampleTestingTypeRepository, referralSampleTestingTypeRepository, businessRuleSet)
        {
            _referralSampleTestingTypeRepository = referralSampleTestingTypeRepository;
        }

        public List<string> GetListTestingTypeName(int referralId)
        {
            return _referralSampleTestingTypeRepository.GetListTestingTypeName(referralId);
        }
    }
}