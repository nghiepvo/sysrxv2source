﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class CityService : MasterFileService<City>, ICityService
    {
        private readonly ICityRepository _cityRepository;
        public CityService(ICityRepository cityRepository,  IBusinessRuleSet<City> businessRuleSet = null)
            : base(cityRepository, cityRepository, businessRuleSet)
        {
            _cityRepository = cityRepository;
        }
    }
}