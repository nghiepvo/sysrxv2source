﻿using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace ServiceLayer
{
    public class ProductTypeService : MasterFileService<ProductType>, IProductTypeService
    {
        private readonly ISolrReferralService _referralSolrService;
        private readonly IProductTypeRepository _productTypeRepository;
        public ProductTypeService(IProductTypeRepository productTypeRepository,
                                    ISolrReferralService referralSolrService,
                                    IBusinessRuleSet<ProductType> businessRuleSet = null)
            : base(productTypeRepository, productTypeRepository, businessRuleSet)
        {
            _productTypeRepository = productTypeRepository;
            _referralSolrService = referralSolrService;
        }

        public override ProductType Update(ProductType model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var id = model.Id;
                var oldData = _productTypeRepository.FirstOrDefault(o => o.Id == id);
                if (oldData == null)
                {
                    return model;
                }
                var oldName = oldData.Name;
                base.Update(model);
                //Write to solr
                if (oldName.Trim() != model.Name.Trim())
                {
                    var listReferral =
                        _referralSolrService.GetByField(new SolrQueryByField("producttypeid", model.Id.ToString()))
                            .ToList();
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.ProductType = model.Name;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                }
                objTransaction.Complete();
                return model;
            }
        }
    }
}