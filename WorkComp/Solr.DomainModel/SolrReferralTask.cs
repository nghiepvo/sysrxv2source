﻿using System;
using SolrNet.Attributes;

namespace Solr.DomainModel
{
    public class SolrReferralTask : SolrEntity
    {
        [SolrField("referraltaskid")]
        public int? ReferralTaskId { get; set; }
        [SolrField("referralid")]
        public int? ReferralId { get; set; }
        [SolrField("title")]
        public string Title { get; set; }
        [SolrField("assigntoname")]
        public string AssignToName { get; set; }
        [SolrField("status")]
        public string Status { get; set; }
        [SolrField("jurisdiction")]
        public string Jurisdiction { get; set; }
        [SolrField("duedate")]
        public DateTime? DueDate { get; set; }
        [SolrField("claimantname")]
        public string ClaimantName { get; set; }
        [SolrField("patientstate")]
        public string PatientState { get; set; }
        [SolrField("createddate")]
        public DateTime? CreatedDate { get; set; }
        [SolrField("startdate")]
        public DateTime? StartDate { get; set; }
        [SolrField("assigntoid")]
        public int? AssignToId { get; set; }
        [SolrField("claimnumberid")]
        public int? ClaimNumberId { get; set; }
        [SolrField("claimantid")]
        public int? ClaimantId { get; set; }
        [SolrField("canceldate")]
        public DateTime? CancelDate { get; set; }
        [SolrField("completeddate")]
        public DateTime? CompletedDate { get; set; }
    }
}