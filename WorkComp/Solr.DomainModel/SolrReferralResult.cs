﻿using SolrNet.Attributes;

namespace Solr.DomainModel
{
    public class SolrReferralResult : SolrEntity
    {
        [SolrField("controlnumber")]
        public string ControlNumber { get; set; }
        [SolrField("consistencycmt")]
        public string Consistencycmt { get; set; }
        [SolrField("resultstatus")]
        public string ResultStatus { get; set; }
    }
}