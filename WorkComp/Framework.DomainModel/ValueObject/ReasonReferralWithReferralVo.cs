﻿using System.Collections.Generic;

namespace Framework.DomainModel.ValueObject
{
    public class ReasonReferralWithReferralVo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ReferralId { get; set; }
        public int? ParentId { get; set; }
        public IList<ReasonReferralWithReferralVo> Childrent { get; set; }
    }
}