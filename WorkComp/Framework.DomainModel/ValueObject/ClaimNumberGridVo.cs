﻿using System;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class ClaimNumberGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public string ClaimantFirstName { get; set; }
        public string ClaimantMiddleName { get; set; }
        public string ClaimantLastName { get; set; }

        private string _claimant;
        public string Claimant
        {
            get
            {
                if (string.IsNullOrEmpty(_claimant))
                {
                    _claimant=ClaimantFirstName + " " + ClaimantMiddleName + " " + ClaimantLastName;
                }
                return _claimant;
            }
            set
            {
                _claimant = value;
            }
        }


        public string AdjusterFirstName { get; set; }
        public string AdjusterMiddleName { get; set; }
        public string AdjusterLastName { get; set; }

        private string _adjuster;
        public string Adjuster
        {
            get
            {
                if (string.IsNullOrEmpty(_adjuster))
                {
                    _adjuster = AdjusterFirstName + " " + AdjusterMiddleName + " " + AdjusterLastName;
                }
                return _adjuster;
            }
            set
            {
                _adjuster = value;
            }
        }

        public string Payer { get; set; }
        public string Branch { get; set; }
        public DateTime? DoiDateTime { get; set; }
        private string _doi;
        public string Doi
        {
            get
            {
                if (string.IsNullOrEmpty(_doi))
                {
                    _doi = DoiDateTime != null ? DoiDateTime.GetValueOrDefault().ToShortDateString() : "";
                }
                return _doi;
            }
            set
            {
                _doi = value;
            }

        }
        public string State { get; set; }
        public string SpecialInstructions { get; set; }

        public int Status { get; set; }
        private string _statusName;
        public string StatusName
        {
            get
            {
                if (string.IsNullOrEmpty(_statusName))
                {
                    _statusName = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ClaimNumberStatus.ToString(), Status.ToString());
                }
                return _statusName;
            }
            set
            {
                _statusName = value;
            }
        }
    }
}
