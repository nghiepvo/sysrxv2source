﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class DataRequestForAuthorizationEmailTemplate
    {
        public DateTime? NextMdVisit { get; set; }
        public string AdjusterEmail { get; set; }
        public string AdjusterName { get; set; }
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public string PayerCity { get; set; }
        public string PayerState { get; set; }
        public string PayerZip { get; set; }
        public string Claimant { get; set; }
        public string PatientPhone { get; set; }
        public string PatientAddr { get; set; }
        public string PatientCity { get; set; }
        public string PatientState { get; set; }
        public string PatientZip { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime? Doi { get; set; }
        public string ClaimJurisdiction { get; set; }
        public DateTime? Dob { get; set; }
        public string PanelType { get; set; }
        public string AssignTo { get; set; }
        public string OrderTitle { get; set; }
        public string AssignEmail { get; set; }
        public string CollectionSiteOrPhysicianName { get; set; }
    }

    public class AllEmailForReferral
    {
        public string EmailReferral { get; set; }
        public string NameReferral { get; set; }
        public string EmailAdjuster { get; set; }
        public string NameAdjuster { get; set; }
        public string EmailClaimant { get; set; }
        public string NameClaimant { get; set; }
        public string EmailCaseManager { get; set; }
        public string NameCaseManager { get; set; }
        public string EmailAttorney { get; set; }
        public string NameAttorney { get; set; }
        public bool? IsCollectionSite { get; set; }
        public string CollectionSiteOrPhysicianEmail { get; set; }
        public string CollectionSiteOrPhysicianName { get; set; }
        
    }

    public class DataRequestForCollectionEmailTemplate
    {
        public string Claimant { get; set; }
        public string PayerName { get; set; }
        public string PatientPhone { get; set; }
        public string PatientAddr { get; set; }
        public string PatientEmail { get; set; }
        public string PatientCity { get; set; }
        public string PatientState { get; set; }
        public string PatientZip { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime? Doi { get; set; }
        public string ClaimJurisdiction { get; set; }
        public DateTime? Dob { get; set; }
        public string PanelType { get; set; }
        public string AssignTo { get; set; }
        public string AssignEmail { get; set; }
        public string PanelTypeSimpleTest { get; set; }
        public DateTime? NextMdVisit { get; set; }
        public string PhysicianName { get; set; }
        public string PhysicianAddress { get; set; }
        public string PhysicianCity { get; set; }
        public string PhysicianState { get; set; }
        public string PhysicianZip { get; set; }
        public string PhysicianPhone { get; set; }
        public string PhysicianFax { get; set; }
        public string ProviderCredentialText { get; set; }
        public string CollectionSiteOrPhysicianPhone { get; set; }
        public string CollectionSiteOrPhysicianFax { get; set; }
        public string CollectionSiteOrPhysicianName { get; set; }
        public string CollectionSiteOrPhysicianAddress { get; set; }
        public string CollectionSiteOrPhysicianCity { get; set; }
        public string CollectionSiteOrPhysicianState { get; set; }
        public string CollectionSiteOrPhysicianZip { get; set; }
        public bool? HasCollectionSite { get; set; }
    }

    public class CollectionServiceRequestEmail
    {
        public string ReferralNo { get; set; }
        public string ControlNo { get; set; }
        public string ClaimantName { get; set; }
        public string CarrierName { get; set; }
        public string CarrierAddr { get; set; }
        public string ClaimNo { get; set; }
        public DateTime? DateOfInjury { get; set; }
        public string RequestedProduct { get; set; }
        public List<MedicationHistoryEmail> MedicationHistoryEmails { get; set; }
        public string ReferralTitle { get; set; }
        public bool? HasCollectionSite { get; set; }
        public string CollectionSiteOrPhysicianFax { get; set; }
        public string CollectionSiteOrPhysicianPhone { get; set; }
        public string CollectionSiteOrPhysicianName { get; set; }
        public string CollectionSiteOrPhysicianAddress { get; set; }
        public string CollectionSiteOrPhysicianCity { get; set; }
        public string CollectionSiteOrPhysicianState { get; set; }
        public string CollectionSiteOrPhysicianZip { get; set; }
        public string AssignTo { get; set; }
        public string AssignEmail { get; set; }
    }

    public class MedicationHistoryEmail
    {
        public string DrugName { get; set; }
        public string Class { get; set; }
        public int? DaySupply { get; set; }
        public string Dosage { get; set; }
        public string MedicationHist { get; set; }
        public DateTime? StartDay { get; set; }
    }

    public class DataMdNotificationLetterTemplate
    {
        public DataMdNotificationLetterTemplate()
        {
            ReasonReferrals = new List<string>();
        }
        public string Claimant { get; set; }
        public string PayerName { get; set; }
        public string PatientPhone { get; set; }
        public string PatientAddr { get; set; }
        public string PatientEmail { get; set; }
        public string PatientCity { get; set; }
        public string PatientState { get; set; }
        public string PatientZip { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime? Doi { get; set; }
        public string ClaimJurisdiction { get; set; }
        public DateTime? Dob { get; set; }
        public string PanelType { get; set; }
        public string SampleTestingType { get; set; }
        public string AssignTo { get; set; }
        public string ProductTypeTitle { get; set; }

        public string PhysicianName { get; set; }
        public string PhysicianAddress { get; set; }
        public string PhysicianCity { get; set; }
        public string PhysicianState { get; set; }
        public string PhysicianZip { get; set; }
        public string PhysicianCityStateZip { get; set; }

        private string _physicianFax;

        public string PhysicianFax
        {
            get { return _physicianFax.ApplyFormatPhone(); }
            set { _physicianFax = value; }
        }


        public List<string> ReasonReferrals { get; set; }

        public string HtmlListReasonReferrals
        {
            get
            {
                var result = "";
                result += "<ul>";
                result = ReasonReferrals.Aggregate(result, (current, reasonReferral) => current + ("<li>" + reasonReferral + "</li>"));
                result += "</ul>";
                return result;
            }
        }
    }


    public class DataCustomCommunicationTemplate
    {
        public string Claimant { get; set; }
        public string PatientPhone { get; set; }
        public string PatientAddr { get; set; }
        public string PatientEmail { get; set; }
        public string PatientCity { get; set; }
        public string PatientState { get; set; }
        public string PatientZip { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime? Doi { get; set; }
        public string ClaimJurisdiction { get; set; }
        public DateTime? Dob { get; set; }
        public DateTime? NextMdVisit { get; set; }
        //service_request
        public string PanelType { get; set; }
        public string AssignTo { get; set; }
        public string PhysicianName { get; set; }
        public string PhysicianAddress { get; set; }
        private string _physicianPhone;

        public string PhysicianPhone
        {
            get { return _physicianPhone.ApplyFormatPhone(); }
            set { _physicianPhone = value; }
        }
    }
    
}
