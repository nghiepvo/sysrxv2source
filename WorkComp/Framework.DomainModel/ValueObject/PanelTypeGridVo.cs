﻿namespace Framework.DomainModel.ValueObject
{
    public class PanelTypeGridVo : ReadOnlyGridVo
    {
        public string PayerName { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool IsBasic { get; set; }
    }
}