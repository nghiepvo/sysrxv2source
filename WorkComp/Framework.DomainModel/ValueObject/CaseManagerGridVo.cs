﻿using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class CaseManagerGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public string Agency { get; set; }
        public string ClaimNumber { get; set; }
        public string Email { get; set; }

        private string _phone;
        public string Phone
        {
            get
            {
                return _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }

        private string _fax;
        public string Fax
        {
            get
            {
                return _fax.ApplyFormatPhone();
            }
            set
            {
                _fax = value;
            }
        }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }

        public string FullAddress
        {
            get { return CaculatorHelper.CaculateFormatFullAddress(Address, State, City, Zip); }
        }

    }
}
