﻿using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class EmployerGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }

        private string _phone;
        public string Phone
        {
            get
            {
                return _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }

        public string FederalTaxId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
    }
}
