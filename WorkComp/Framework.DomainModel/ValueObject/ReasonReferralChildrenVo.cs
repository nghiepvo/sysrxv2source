﻿using System.Collections.Generic;

namespace Framework.DomainModel.ValueObject
{
    public class ReasonReferralChildrenVo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}