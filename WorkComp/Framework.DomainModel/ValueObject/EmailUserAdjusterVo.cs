﻿namespace Framework.DomainModel.ValueObject
{
    public class EmailUserAdjusterVo: ReadOnlyGridVo
    {
        public string TypeName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
