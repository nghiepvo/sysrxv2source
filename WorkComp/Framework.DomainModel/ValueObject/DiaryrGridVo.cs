﻿using System;

namespace Framework.DomainModel.ValueObject
{
    public class DiaryrGridVo : ReadOnlyGridVo
    {
        //public string CreatedByFirstName { get; set; }
        //public string CreatedByMiddleName { get; set; }
        //public string CreatedByLastName { get; set; }

        //public string CreateBy
        //{
        //    get
        //    {
        //        return CreatedByFirstName + " " + CreatedByMiddleName + " " + CreatedByLastName;
        //    }
        //}
        public string CreatedBy { get; set; }
        public string Heading { get; set; }
        public string Reason { get; set; }
        public string CommentResult { get; set; }

        public string CreatedDateStr
        {
            get { return CreatedDate.ToString("g"); }
        }

        public DateTime CreatedDate { get; set; }
    }
}