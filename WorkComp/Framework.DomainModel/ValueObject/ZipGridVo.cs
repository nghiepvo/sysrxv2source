﻿namespace Framework.DomainModel.ValueObject
{
    public class ZipGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public string State { get; set; }
        public string City { get; set; }
    }
}