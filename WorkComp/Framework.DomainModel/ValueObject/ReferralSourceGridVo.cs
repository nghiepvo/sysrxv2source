﻿using System;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class ReferralSourceGridVo : ReadOnlyGridVo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string Name
        {
            get
            {
                return FirstName + " " + (string.IsNullOrEmpty(MiddleName)?"": MiddleName + " ") + LastName;
            }
        }

        public string Type { get; set; }

        public string Company { get; set; }
        public string Email { get; set; }
        private string _phone;
        public string Phone
        {
            get
            {
                return _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }
        public string Address { get; set; }
        public DateTime? AuthorizationFrom { get; set; }
        public DateTime? AuthorizationTo { get; set; }
        public bool IsAuthorization { get; set; }

        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }

        public string Authorization
        {
            get
            {
                if (!IsAuthorization) return "No";
                var temp = "";
                temp += AuthorizationFrom != null
                    ? AuthorizationFrom.GetValueOrDefault().ToShortDateString() + " - "
                    : "";
                temp += AuthorizationTo != null ? AuthorizationTo.GetValueOrDefault().ToShortDateString() : "";
                return temp;
            }
        }

    }
}
