﻿namespace Framework.DomainModel.ValueObject
{
    public class HeadingGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
    }
}