﻿namespace Framework.DomainModel.ValueObject
{
    public class RoleGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
    }
}