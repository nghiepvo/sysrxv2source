﻿namespace Framework.DomainModel.ValueObject
{
    public class DualListBoxItemVo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}