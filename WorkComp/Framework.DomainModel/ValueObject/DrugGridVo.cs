﻿namespace Framework.DomainModel.ValueObject
{
   public class DrugGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public string Description { get; set; }
    }
}
