﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    

    public class ReferralDetailVo
    {
        #region referral info
        public int ReferralInfoId { get; set; }
        public string ReferralInfoControlNumber { get; set; }
        public string ReferralInfoProductType { get; set; }
        public DateTime ReferralInfoDateEntered { get; set; }
        public DateTime ReferralInfoDateReceived { get; set; }
        public DateTime ReferralInfoDueDate { get; set; }
        public int ReferralInfoStatusId { get; set; }
        public int ReferralInfoAssignToId { get; set; }
        public string ReferralInfoAssignToFirstName { get; set; }
        public string ReferralInfoAssignToMiddleName { get; set; }
        public string ReferralInfoAssignToLastName { get; set; }
        public int? ReferralInfoReferralMethodId { get; set; }
        public string ReferralInfoSpecialInstruction { get; set; }
        public string ReferralInfoCreatedByFirstName { get; set; }
        public string ReferralInfoCreatedByMiddleName { get; set; }
        public string ReferralInfoCreatedByLastName { get; set; }
        public DateTime ReferralInfoCreatedDate { get; set; }
        public int? ReferralInfoPanelTypeId { get; set; }
        #endregion

        #region ReferralSourceInfo
        public string ReferralSourceInfoFirstName { get; set; }
        public string ReferralSourceInfoMiddleName { get; set; }
        public string ReferralSourceInfoLastName { get; set; }
        public string ReferralSourceInfoType { get; set; }
        public string ReferralSourceInfoCompany { get; set; }
        public string ReferralSourceInfoPhone { get; set; }
        public string ReferralSourceInfoFax { get; set; }
        public string ReferralSourceInfoEmail { get; set; }
        public string ReferralSourceInfoState { get; set; }
        public string ReferralSourceInfoCity { get; set; }
        public string ReferralSourceInfoZip { get; set; }
        public string ReferralSourceInfoAddress { get; set; }
        public DateTime? ReferralSourceInfoAuthorizationFrom { get; set; }
        public DateTime? ReferralSourceInfoAuthorizationTo { get; set; }
        public bool ReferralSourceInfoIsAuthorization { get; set; }
        #endregion

        #region PayorInfo
        public string PayorInfoPayer { get; set; }
        public string PayorInfoBranch { get; set; }
        public string PayorInfoSpecialInstruction { get; set; }
        #endregion

        #region AdjusterInfo
        public bool? AdjusterIsReferral { get; set; }
        public string AdjusterInfoFirstName { get; set; }
        public string AdjusterInfoLastName { get; set; }
        public string AdjusterInfoMiddleName { get; set; }
        public string AdjusterInfoEmail { get; set; }
        public string AdjusterInfoPhone { get; set; }
        public string AdjusterInfoExternalId { get; set; }
        public string AdjusterInfoState { get; set; }
        public string AdjusterInfoCity { get; set; }
        public string AdjusterInfoZip { get; set; }
        public string AdjusterInfoAddress { get; set; }
        #endregion

        #region ClaimantInfo
        public int ClaimantInfoId { get; set; }
        public string ClaimantInfoFirstName { get; set; }
        public string ClaimantInfoMiddleName { get; set; }
        public string ClaimantInfoLastName { get; set; }
        public string ClaimantInfoPayerPatientId { get; set; }
        public string ClaimantInfoHomePhone { get; set; }
        public string ClaimantInfoCellPhone { get; set; }
        public string ClaimantInfoGender { get; set; }
        public DateTime? ClaimantInfoDateOfBirth { get; set; }
        public string ClaimantInfoSsnId { get; set; }
        public string ClaimantInfoClaimantLanguage { get; set; }
        public string ClaimantInfoEmail { get; set; }
        public string ClaimantInfoAddress1 { get; set; }
        public string ClaimantInfoAddress2 { get; set; }
        public string ClaimantInfoState { get; set; }
        public string ClaimantInfoCity { get; set; }
        public string ClaimantInfoZip { get; set; }
        
        #endregion


        #region ClaimNumberInfo

        public string ClaimNumberInfoClaimNumber { get; set; }
        public DateTime ClaimNumberInfoDoi { get; set; }
        public string ClaimNumberInfoClaimJurisdiction { get; set; }
        public string ClaimNumberInfoSpecialInstruction { get; set; }

        public int ClaimNumberInfoStatusId { get; set; }

        public string ClaimNumberInfoStatus
        {
            get
            {
                return ClaimNumberInfoStatusId > 0
                    ? XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ClaimNumberStatus.ToString(), ClaimNumberInfoStatusId.ToString())
                    : "";
            }
        }

        public DateTime? ClaimNumberInfoOpenDate { get; set; }
        public DateTime? ClaimNumberInfoCloseDate { get; set; }
        public DateTime? ClaimNumberInfoReopenDate { get; set; }
        public DateTime? ClaimNumberInfoRecloseDate { get; set; }

        #endregion

        #region CaseManagerInfo
        public string CaseManagerInfoName { get; set; }
        public string CaseManagerInfoEmail { get; set; }
        public string CaseManagerInfoPhone { get; set; }
        public string CaseManagerInfoFax { get; set; }
        public string CaseManagerInfoAgency { get; set; }
        public string CaseManagerInfoSpecialInstruction { get; set; }
        public string CaseManagerInfoState { get; set; }
        public string CaseManagerInfoCity { get; set; }
        public string CaseManagerInfoZip { get; set; }
        public string CaseManagerInfoAddress { get; set; }
        #endregion

        #region  AttorneyInfo
        public string AttorneyInfoFirstName { get; set; }
        public string AttorneyInfoMiddleName { get; set; }
        public string AttorneyInfoLastName { get; set; }
        public string AttorneyInfoEmail { get; set; }
        public string AttorneyInfoPhone { get; set; }
        public string AttorneyInfoState { get; set; }
        public string AttorneyInfoCity { get; set; }
        public string AttorneyInfoZip { get; set; }
        public string AttorneyInfoAddress { get; set; }
        #endregion

        #region EmployerInfo
        public string EmployerInfoName { get; set; }
        public string EmployerInfoPhone { get; set; }
        public string EmployerInfoState { get; set; }
        public string EmployerInfoCity { get; set; }
        public string EmployerInfoZip { get; set; }
        public string EmployerInfoAddress { get; set; }
        #endregion

        #region CollectionMethodInfo
        #region Referral
        public bool? IsCollectionSite { get; set; }
        public bool? ReferralRush { get; set; }
        #endregion 

        #region ReferralColectionSite
        public string ReferralColectionSite { get; set; }
        public string ReferralColectionSiteSpecialHandling { get; set; }
        public string ReferralColectionSitePhone { get; set; }
        public string ReferralColectionSiteFax { get; set; }
        public string ReferralColectionSiteEmail { get; set; }
        public string ReferralColectionSiteAddress { get; set; }
        public DateTime? ReferralColectionSiteCollectionDate { get; set; }
        #endregion

        #region ReferralNpiNumber
        public string ReferralNpiNumberTreatingPhysicianFirstName { get; set; }
        public string ReferralNpiNumberTreatingPhysicianMiddleName { get; set; }
        public string ReferralNpiNumberTreatingPhysicianLastName { get; set; }
        public string ReferralNpiNumberTreatingPhysicianOrganizationName { get; set; }
        public DateTime? ReferralNpiNumberNextMdVisit { get; set; }
        public string ReferralNpiNumberNpiNumber { get; set; }
        public string ReferralNpiNumberSpecialHandling { get; set; }
        public string ReferralNpiNumberPhone { get; set; }
        public string ReferralNpiNumberFax { get; set; }
        public string ReferralNpiNumberEmail { get; set; }
        public string ReferralNpiNumberAddress { get; set; }
        #endregion
        #endregion
        public int TotalAttachment { get; set; }

    }

    public class ReasonReferralInReferralDetailVo
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string NameParent { get; set; }
        public string Name { get; set; }
    }

    

}
