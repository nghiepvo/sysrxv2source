﻿using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class UserGridVo : ReadOnlyGridVo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + (string.IsNullOrEmpty(MiddleName) ? "" : MiddleName + " ") + LastName;
            }
        }

        public string UserName { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        private string _phone;

        public string Phone {
            get
            {
                return _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value.RemoveFormatPhone();
            } 
        }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public bool IsActive { get; set; }
    }
}