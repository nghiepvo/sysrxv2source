﻿namespace Framework.DomainModel.ValueObject
{
    public class ReferralTypeGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
    }
}
