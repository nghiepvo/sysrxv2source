﻿using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class CollectionSiteGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }

        private string _phone;
        public string Phone
        {
            get
            {
                return _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }

        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
    }
}
