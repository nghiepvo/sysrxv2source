﻿namespace Framework.DomainModel.ValueObject
{
    public class ReferralReportGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public string Payer { get; set; }
    }
}