﻿using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class NpiNumberGridVo : ReadOnlyGridVo
    {
        public string Npi { set; get; }
        public string OrganizationName { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string MiddleName { get; set; }

        private string _name;
        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                {
                    
                    _name = FirstName + " " + (string.IsNullOrEmpty(MiddleName) ? "" : MiddleName + " ") + LastName;
                }
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string LicenseNumber { set; get; }
        public string ProviderCredentialText { set; get; }

        private string _phone;
        public string Phone
        {
            get
            {
                return _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }

        private string _fax;
        public string Fax
        {
            get
            {
                return _fax.ApplyFormatPhone();
            }
            set
            {
                _fax = value;
            }
        }
        public string Email { set; get; }

        public string Address { set; get; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }

        public string FullAddress
        {
            get
            {
                return CaculatorHelper.CaculateFormatFullAddress(Address, State, City, Zip);
            }
        }
    }
}
