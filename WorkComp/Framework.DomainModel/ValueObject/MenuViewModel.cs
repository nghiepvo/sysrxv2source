﻿namespace Framework.DomainModel.ValueObject
{
    public class MenuViewModel
    {
        public bool CanViewReferral { get; set; }
        public bool CanViewTask { get; set; }
        public bool CanViewSysRxReport { get; set; }
        public bool CanCreateReferral { get; set; }
        public bool CanViewIcd { get; set; }
        public bool CanViewCity { get; set; }
        public bool CanViewState { get; set; }
        public bool CanViewZip { get; set; }
        public bool CanViewDrug { get; set; }
        public bool CanViewEmployer { get; set; }
        public bool CanViewClaimantLanguage { get; set; }
        public bool CanViewNpiNumber { get; set; }
        public bool CanViewAttorney { get; set; }
        public bool CanViewTaskTemplate { get; set; }
        public bool CanViewPayer { get; set; }
        public bool CanViewTaskGroup { get; set; }
        public bool CanViewCollectionSite { get; set; }
        public bool CanViewReferralType { get; set; }
        public bool CanViewReferralSource { get; set; }
        public bool CanViewUser { get; set; }
        public bool CanViewUserRole { get; set; }
        public bool CanViewAssayCode { get; set; }
        public bool CanViewAssayCodeDescription { get; set; }
        public bool CanViewBranch { get; set; }
        public bool CanViewAdjuster { get; set; }
        public bool CanViewClaimNumber { get; set; }
        public bool CanViewClaimant { get; set; }
        public bool CanViewPanelType { get; set; }
        public bool CanViewCaseManager { get; set; }
        public bool CanViewReasonReferral { get; set; }
        public bool CanViewEmailTemplate { get; set; }
        public bool CanViewProductType { get; set; }
        public bool CanViewConfiguration { get; set; }
        public bool CanViewReferralReport { get; set; }
        public bool CanViewRequisitionPrint { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }

        public bool IsAdministrator
        {
            get
            {
                switch (ControllerName.ToLower())
                {
                    case "referralsource":
                    case "adjuster":
                    case "referraltype":
                    case "reasonreferral":
                    case "claimant":
                    case "attorney":
                    case "employer":
                    case "icd":
                    case "collectionsite":
                    case "drug":
                    case "tasktemplate":
                    case "npinumber":
                    case "paneltype":
                    case "state":
                    case "city":
                    case "zip":
                    case "emailtemplate":
                    case "payer":
                    case "branch":
                    case "user":
                    case "userrole":
                    case "claimnumber":
                    case "configuration":
                    case "claimantlanguage":
                    case "taskgroup":
                    case "assaycodedescription":
                    case "assaycode":
                    case "casemanager":
                    case "producttype":
                        return true;
                    default:
                        return false;
                }
            }
        }

    }
}