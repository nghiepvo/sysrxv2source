﻿namespace Framework.DomainModel.ValueObject
{
   public class StateGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public string AbbreviationName { get; set; }
    }
}
