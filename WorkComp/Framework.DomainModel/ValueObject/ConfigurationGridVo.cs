﻿using System.Net;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class ConfigurationGridVo : ReadOnlyGridVo
    {
        public int TypeId { get; set; }

        public string TypeName
        {
            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ConfigurationType.ToString(), TypeId.ToString());
            }
        }

        public string Name { get; set; }
        private string _value;
        public string Value {
            get
            {
                if (this.IsHtml ?? false)
                {
                    return WebUtility.HtmlDecode(_value);
                }
                return _value;
            }
            set
            {
                _value = value;
            } 
        }

        public bool? Configurable { get; set; }
        public bool? IsHtml { get; set; }
    }
}