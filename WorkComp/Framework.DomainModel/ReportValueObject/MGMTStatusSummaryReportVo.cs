﻿using System;

namespace Framework.DomainModel.ReportValueObject
{
    public class MGMTStatusSummaryReportVo
    {
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public string ProductTypeName { get; set; }
        public int? New { get; set; }
        public int? Open { get; set; }
        public int? Completed { get; set; }
        public int? Cancelled { get; set; }
        public int? Reopen { get; set; }
        public int? TotalNew { get; set; }
        public int? TotalOpen { get; set; }
        public int? TotalComplete { get; set; }
        public int? TotalCancelled { get; set; }
        public int? TotalReopen { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? TotalPage { get; set; }
        public int? TotalRow { get; set; }
    }
}
