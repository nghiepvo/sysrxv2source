﻿using System;

namespace Framework.DomainModel.ReportValueObject
{
    public class OpenReferralDetailVo
    {
        public int? ClaimantId { get; set; }
        public int? ReferralId { get; set; }
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public string AdjusterName { get; set; }
        public string ReferralSourceName { get; set; }
        public string EmployerName { get; set; }
        public string EmployerAddress { get; set; }
        public string ClaimantFirstName { get; set; }
        public string ClaimantLastName { get; set; }
        public string ClaimNumber { get; set; }
        public int? ClaimNumberId { get; set; }
        public string Jurisdiction { get; set; }
        public string Doi { get; set; }
        public string Icd { get; set; }
        public string DateRequestReceived { get; set; }
        public string Product { get; set; }
        public string SampleType { get; set; }
        public string PanelRequested { get; set; }
        public string RequestReason { get; set; }
        public string Criteria { get; set; }
        public string Accession { get; set; }
        public string DateCollected { get; set; }
        public string CollectionSite { get; set; }
        public string CollectionLocation { get; set; }
        public string TreatingPhysicianNpi { get; set; }
        public string TreatingPhysicianName { get; set; }
        public string TreatingPhysicianAddress { get; set; }
        public string TreatingPhysicianCity { get; set; }
        public string TreatingPhysicianState { get; set; }
        public string TreatingPhysicianZip { get; set; }
        public string TreatingPhysicianPhone { get; set; }
        public string ReviewStatus { get; set; }
        public string SpecimenReceivedDate { get; set; }
        public string CompletedTestDate { get; set; }
        public string PrescribedDrugs { get; set; }
        public string TestResult { get; set; }
        public int NumReportedDetected { get; set; }
        public int NumReportedNotDetected { get; set; }
        public int NumNotReportedDetected { get; set; }
        public int NumAlcoholDetected { get; set; }
        public int NumIllicitsDetected { get; set; }
        public int NumInvalidSample { get; set; }
        public int NumNoMedication { get; set; }

        public string ReportedDetected {
            get
            {
                return NumReportedDetected > 0 ? "Yes" : "";
            }
        }
        public string ReportedNotDetected
        {
            get
            {
                return NumReportedNotDetected > 0  ? "Yes" : "";
            }
        }
        public string NotReportedDetected
        {
            get
            {
                return NumNotReportedDetected > 0  ? "Yes" : "";
            }
        }
        public string AlcoholDetected
        {
            get
            {
                return NumAlcoholDetected > 0  ? "Yes" : "";
            }
        }
        public string IllicitsDetected
        {
            get
            {
                return NumIllicitsDetected > 0  ? "Yes" : "";
            }
        }
        public string InvalidSample
        {
            get
            {
                return NumInvalidSample > 0 ? "Yes" : "";
            }
        }

        public string NoMedication
        {
            get
            {
                return NumNoMedication > 0 ? "Yes" : "";
            }
        }
        public string TestingSchedule { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? TotalPage { get; set; }
        public int? TotalRow { get; set; }
    }
}
