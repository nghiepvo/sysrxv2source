﻿using System.Security.Principal;

namespace Framework.DomainModel.Interfaces
{
    public interface IWorkcompIdentity : IIdentity
    {
        int UserIdentityId { get; }
    }
}