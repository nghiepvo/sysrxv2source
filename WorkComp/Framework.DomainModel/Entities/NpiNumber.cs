﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class NpiNumber :Entity
    {
        public NpiNumber()
        {
            ReferralNpiNumbers = new Collection<ReferralNpiNumber>();
        }
        public string OrganizationName {set; get;}
        public string FirstName {set; get;}
        public string LastName {set; get;}
        public string MiddleName {set; get;}
        [LocalizeRequired]
        public string Npi {set; get;}
        [LocalizePhone]
        public string Phone {set; get;}
        public string Fax {set; get;}
        public string LicenseNumber {set; get;}
        public string ProviderCredentialText {set; get;}
        [LocalizeEmailAddress]
        public string Email	{set; get;}
        [LocalizeRequired]
        public string Address {set; get;}

        public int? StateId { set; get; }
        public virtual State State { get; set; }
        public int? CityId { set; get; }
        public virtual City City { get; set; }
        public int? ZipId { set; get; }
        public virtual Zip Zip { get; set; }

        public bool? OptedOutSendMail { get; set; }

        public bool? IsUserUpdate {set; get;}
        public virtual ICollection<ReferralNpiNumber> ReferralNpiNumbers { get; set; }

    }
}
