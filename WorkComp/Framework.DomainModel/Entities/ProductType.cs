using System.Collections.Generic;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class ProductType : Entity
    {
        public ProductType()
        {
            Referrals = new List<Referral>();
        }
        [LocalizeRequired]
        [LocalizeMaxLength(100)]
        public string Name { get; set; }
        public int? PayerId { get; set; }
        public virtual Payer Payer { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}