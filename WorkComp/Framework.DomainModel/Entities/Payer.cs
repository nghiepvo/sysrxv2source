﻿using System.Collections.ObjectModel;
using Framework.DataAnnotations;
using System.Collections.Generic;

namespace Framework.DomainModel.Entities
{
    public class Payer : Entity
    {
        public Payer()
        {
            Branchs = new Collection<Branch>();
            Adjusters = new Collection<Adjuster>();
            ClaimNumbers = new Collection<ClaimNumber>();
            PanelTypes = new Collection<PanelType>();
            ProductTypes = new List<ProductType>();
        }

        [LocalizeRequired]
        public string Name { get; set; }
        public string ControlNumberPrefix { get; set; }
        public string SpecialInstructions { get; set; }
        public string Address { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public int? ZipId { get; set; }
        public virtual State State { get; set; }
        public virtual City City { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<Branch> Branchs { get; set; }
        public virtual ICollection<Adjuster> Adjusters { get; set; }
        public virtual ICollection<ClaimNumber> ClaimNumbers { get; set; }
        public virtual ICollection<PanelType> PanelTypes { get; set; }
        public virtual ICollection<ProductType> ProductTypes { get; set; }
    }
}
