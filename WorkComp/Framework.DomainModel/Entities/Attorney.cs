﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class Attorney: Entity
    {
        public Attorney()
        {
            Referrals = new Collection<Referral>();
        }
        [LocalizeMaxLength(50)]
        [LocalizeRequired]
        public string FirstName { get; set; }
        [LocalizeMaxLength(50)]
        [LocalizeRequired]
        public string LastName { get; set; }
        [LocalizeMaxLength(50)]
        public string MiddleName { get; set; }
        [LocalizeMaxLength(500)]
        public string Lawfirm { get; set; }

        [LocalizeRequired, LocalizePhone]
        public string Phone { get; set; }
        [LocalizeEmailAddress]
        public string Email { get; set; }
        [LocalizeRequired]
        public string Address { get; set; }

        public int? StateId { get; set; }
        public virtual State State { get; set; }
        public int? CityId { get; set; }
        public virtual City City { get; set; }
        public int? ZipId { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
