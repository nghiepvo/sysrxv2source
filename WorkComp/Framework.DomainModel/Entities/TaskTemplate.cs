﻿using System.Collections.Generic;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class TaskTemplate:Entity
    {
        public TaskTemplate()
        {
            TaskGroupTaskTemplates = new List<TaskGroupTaskTemplate>();
        }
        [LocalizeRequired]
        [LocalizeMaxLength(200)]
        public string Title { get; set; }
        public string Description { get; set; }
        public int? NumDueDate { get; set; }
        public double? NumDueHour { get; set; }
        public int StatusId { get; set; }
        public int? AssignToId { get; set; }
        public int? TaskTypeId { get; set; }
        public bool? IsSystem { get; set; }
        public virtual ICollection<TaskGroupTaskTemplate> TaskGroupTaskTemplates { get; set; }
        public virtual User AssignTo { get; set; }
    }
}