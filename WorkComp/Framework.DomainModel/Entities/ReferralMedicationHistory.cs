using System;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class ReferralMedicationHistory : Entity
    {
        public int DrugId { get; set; }
        public int ReferralId { get; set; }
        public int? DaysSupply { get; set; }
        [LocalizeMaxLength(50)]
        public string Dosage { get; set; }
        public int? DosageUnit { get; set; }
        public int? ProvidedById { get; set; }
        public DateTime? FillDate { get; set; }
        public virtual Drug Drug { get; set; }
        public virtual Referral Referral { get; set; }
        public virtual ReferralType ProvidedBy { get; set; }
    }
}