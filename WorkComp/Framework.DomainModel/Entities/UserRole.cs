using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class UserRole : Entity
    {
        public UserRole()
        {
            Users = new Collection<User>();
            UserRoleFunctions = new Collection<UserRoleFunction>();
        }
        [LocalizeRequired]
        [LocalizeMaxLength(50)]
        public string Name { get; set; }
        public bool IsEnable { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<UserRoleFunction> UserRoleFunctions { get; set; }
    }
}
