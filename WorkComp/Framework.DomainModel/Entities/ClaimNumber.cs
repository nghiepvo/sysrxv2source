﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class ClaimNumber : Entity
    {
        public ClaimNumber()
        {
            Referrals = new Collection<Referral>();
            CaseManagers = new Collection<CaseManager>();
        }

        public int? ClaimantId { get; set; }
        public virtual Claimant Claimant { get; set; }
        public int? AdjusterId { get; set; }
        public virtual Adjuster Adjuster { get; set; }

        public int? PayerId { get; set; }
        public virtual Payer Payer { get; set; }
        public int? BranchId { get; set; }
        public virtual Branch Branch { get; set; }

        public int? EmployerId { get; set; }
        public virtual Employer Employer { get; set; }

        [LocalizeRequired]
        public DateTime Doi { get; set; }

        [LocalizeMaxLength(50)]
        public string Name { get; set; }

        public int? StateId { get; set; }
        public virtual State State { get; set; }
        public string SpecialInstructions { get; set; }

        public DateTime? CloseDate { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? ReClosedDate { get; set; }
        public DateTime? ReOpenDate { get; set; }
        public DateTime? ReportDate { get; set; }
        public int Status { get; set; }
        public bool? OptedOutSendMail { get; set; }

        public bool IsUserUpdate { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
        public virtual ICollection<CaseManager> CaseManagers { get; set; }

    }
}