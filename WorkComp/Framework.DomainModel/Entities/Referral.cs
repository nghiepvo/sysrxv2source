using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Framework.DomainModel.Entities
{
    public class Referral : Entity
    {
        public Referral()
        {
            ReferralCollectionSites = new Collection<ReferralCollectionSite>();
            ReferralIcds = new Collection<ReferralIcd>();
            ReferralMedicationHistories = new Collection<ReferralMedicationHistory>();
            ReferralNpiNumbers = new Collection<ReferralNpiNumber>();
            ReferralReasonReferrals = new Collection<ReferralReasonReferral>();
            ReferralTasks = new Collection<ReferralTask>();
            ReferralAttachments = new Collection<ReferralAttachment>();
            ReferralNotes = new Collection<ReferralNote>();
            Diaries = new Collection<Diary>();
            ReferralEmailTemplates = new Collection<ReferralEmailTemplate>();
            ReferralSampleTestingTypes = new Collection<ReferralSampleTestingType>();
            ReferralCancels = new Collection<ReferralCancel>();
        }

        public string ControlNumber { get; set; }
        public int ReferralSourceId { get; set; }
        public int ClaimantNumberId { get; set; }
        public bool? AdjusterIsReferral { get; set; }
        public int ProductTypeId { get; set; }
        public DateTime EnteredDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime DueDate { get; set; }
        public int AssignToId { get; set; }
        public int StatusId { get; set; }
        public int? ReferralMethodId { get; set; }
        public string SpecialInstruction { get; set; }
        public int? AttorneyId { get; set; }
        public int? CaseManagerId { get; set; }
        public bool? IsCollectionSite { get; set; }
        public bool? NoMedicationHistory { get; set; }
        public int? PanelTypeId { get; set; }
        public bool? IsSendMailTreatingPhysician { get; set; }
        public bool? Rush { get; set; }
        public string FileResult { get; set; }
        public string InsertFrom { get; set; }
        [NotMapped]
        public int TaskGroupIdSelected { get; set; }
        public DateTime? CreatedDateNov { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public DateTime? PendingDate { get; set; }
        public DateTime? ReopenDate { get; set; }
        public int? TestResultId { get; set; }
        public virtual Attorney Attorney { get; set; }
        public virtual CaseManager CaseManager { get; set; }
        public virtual ClaimNumber ClaimNumber { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual ReferralSource ReferralSource { get; set; }
        public virtual PanelType PanelType { get; set; }
        public virtual User AssignTo { get; set; }
        public virtual TestResult TestResult { get; set; }
        public virtual ICollection<ReferralCollectionSite> ReferralCollectionSites { get; set; }
        public virtual ICollection<ReferralIcd> ReferralIcds { get; set; }
        public virtual ICollection<ReferralMedicationHistory> ReferralMedicationHistories { get; set; }
        public virtual ICollection<ReferralNpiNumber> ReferralNpiNumbers { get; set; }
        public virtual ICollection<ReferralReasonReferral> ReferralReasonReferrals { get; set; }
        public virtual ICollection<ReferralTask> ReferralTasks { get; set; }
        public virtual ICollection<ReferralNote> ReferralNotes { get; set; }
        public virtual ICollection<ReferralAttachment> ReferralAttachments { get; set; }
        public virtual ICollection<Diary> Diaries { get; set; }
        public virtual ICollection<ReferralEmailTemplate> ReferralEmailTemplates { get; set; }
        public virtual ICollection<ReferralSampleTestingType> ReferralSampleTestingTypes { get; set; }
        public virtual ICollection<ReferralCancel> ReferralCancels { get; set; }
    }
}