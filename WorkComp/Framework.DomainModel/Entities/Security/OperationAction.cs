﻿namespace Framework.DomainModel.Entities.Security
{
    public enum OperationAction
    {
        None = 0,
        View = 1,
        Print = 2,
        Add = 3,
        Update = 4,
        Delete = 5
    }
}
