using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class SampleTestingType : Entity
    {
        public SampleTestingType()
        {
            AssayCodes = new Collection<AssayCode>();
            PanelCodes = new List<PanelCode>();
            ReferralSampleTestingTypes=new Collection<ReferralSampleTestingType>();
        }

        [LocalizeRequired]
        [LocalizeMaxLength(50)]
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? Checked { get; set; }

        public virtual ICollection<AssayCode> AssayCodes { get; set; }
        public virtual ICollection<PanelCode> PanelCodes { get; set; }
        public virtual ICollection<ReferralSampleTestingType> ReferralSampleTestingTypes { get; set; }
    }
}