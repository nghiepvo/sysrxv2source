﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class Branch : Entity
    {
        public Branch()
        {
            Adjusters = new Collection<Adjuster>();
            ClaimNumbers = new Collection<ClaimNumber>();
        }
        [LocalizeRequired]
        public string Name { get; set; }
        public string ManagerName { get; set; }
        [LocalizePhone]
        public string Fax { get; set; }
        [LocalizePhone]
        [LocalizeRequired]
        public string Phone { get; set; }
        [LocalizeRequired]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        
        public int PayerId { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public int? ZipId { get; set; }

        public virtual Payer Payer { get; set; }
        public virtual State State { get; set; }
        public virtual City City { get; set; }
        public virtual Zip Zip { get; set; }

        public bool IsUserUpdate { get; set; }

        public virtual ICollection<Adjuster> Adjusters { get; set; }
        public virtual ICollection<ClaimNumber> ClaimNumbers { get; set; }
    }
}
