using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class State : Entity
    {
        public State()
        {
            Cities = new List<City>();
            NpiNumbers = new Collection<NpiNumber>();
            Attorneys = new Collection<Attorney>();
            Payers = new List<Payer>();
            CollectionSites = new Collection<CollectionSite>();
            ReferralSources = new Collection<ReferralSource>();
            Branchs = new List<Branch>();
            Adjusters = new Collection<Adjuster>();
            Users = new Collection<User>(); 
            Claimants = new Collection<Claimant>();
            ClaimNumbers = new Collection<ClaimNumber>();
            CaseManagers = new Collection<CaseManager>();
        }
        [LocalizeMaxLength(100)]
        [LocalizeRequired]
        public string Name { get; set; }
        [LocalizeRequired]
        [LocalizeMaxLength(5)]
        public string AbbreviationName { get; set; }
        public virtual ICollection<City> Cities { get; set; }
        public virtual ICollection<Employer> Employers { get; set; }
        public virtual ICollection<NpiNumber> NpiNumbers { get; set; }
        public virtual ICollection<Attorney> Attorneys { get; set; }
        public virtual ICollection<Payer> Payers { get; set; }
        public virtual ICollection<CollectionSite> CollectionSites { get; set; }
        public virtual ICollection<ReferralSource> ReferralSources { get; set; }
        public virtual ICollection<Branch> Branchs { get; set; }
        public virtual ICollection<Adjuster> Adjusters { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Claimant> Claimants { get; set; }
        public virtual ICollection<ClaimNumber> ClaimNumbers { get; set; }
        public virtual ICollection<CaseManager> CaseManagers { get; set; }
    }
}