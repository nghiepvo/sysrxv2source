namespace Framework.DomainModel.Entities
{
    public class Comment : Entity
    {
        public int? ReferralTaskId { get; set; }
        public string CommentContent { get; set; }
        public virtual ReferralTask ReferralTask { get; set; }
    }
}