using System.Collections.Generic;

namespace Framework.DomainModel.Entities
{
    public class Heading : Entity
    {
        public string Name { get; set; }
    }
}