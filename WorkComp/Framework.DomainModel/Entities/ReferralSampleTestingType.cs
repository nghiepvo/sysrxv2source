namespace Framework.DomainModel.Entities
{
    public class ReferralSampleTestingType : Entity
    {
        public int? ReferralId { get; set; }
        public int? SampleTestingTypeId { get; set; }
        public virtual SampleTestingType SampleTestingType { get; set; }
        public virtual Referral Referral { get; set; }
    }
}