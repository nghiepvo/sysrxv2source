﻿using System;

namespace Framework.DomainModel.Entities
{
    public class ReferralCollectionSite:Entity
    {
        public int? CollectionSiteId { get; set; }
        public int ReferralId { get; set; }
        public string SpecialInstructions { get; set; }
        public DateTime? CollectionDate { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public virtual CollectionSite CollectionSite { get; set; }
        public virtual Referral Referral { get; set; }
    }
}