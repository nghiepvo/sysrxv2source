﻿namespace Framework.DomainModel.Entities
{
    public class ReferralReasonReferral : Entity
    {
        public int? ReferralId { get; set; }
        public int? ReasonReferralId { get; set; }
        public virtual ReasonReferral ReasonReferral { get; set; }
        public virtual Referral Referral { get; set; }
    }
}