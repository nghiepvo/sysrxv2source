namespace Framework.DomainModel.Entities
{
    public class Diary : Entity
    {
        public string Heading { get; set; }
        public string Reason { get; set; }
        public string Comment { get; set; }
        public int ReferralId { get; set; }
        public virtual Referral Referral { get; set; }
    }
}