namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralNpiNumberMap : WorkCompEntityTypeConfiguration<ReferralNpiNumber>
    {
        public ReferralNpiNumberMap()
        {
            Property(t => t.Phone)
                .HasMaxLength(50);

            Property(t => t.Fax)
                .HasMaxLength(50);

            Property(t => t.Address)
                .HasMaxLength(200);

            Property(t => t.Email)
                .HasMaxLength(100);

            // Table & Column Mappings
            ToTable("ReferralNpiNumber");
            Property(t => t.NpiNumberId).HasColumnName("NpiNumberId");
            Property(t => t.ReferralId).HasColumnName("ReferralId");
            Property(t => t.NextMdVisitDate).HasColumnName("NextMdVisitDate");
            Property(t => t.SpecialHandling).HasColumnName("SpecialHandling");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Fax).HasColumnName("Fax");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.Email).HasColumnName("Email");

            // Relationships
            HasOptional(t => t.NpiNumber)
                .WithMany(t => t.ReferralNpiNumbers)
                .HasForeignKey(d => d.NpiNumberId);
            HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralNpiNumbers)
                .HasForeignKey(d => d.ReferralId);
        }
    }
}