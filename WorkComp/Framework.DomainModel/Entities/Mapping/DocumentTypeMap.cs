namespace Framework.DomainModel.Entities.Mapping
{
    public class DocumentTypeMap : WorkCompEntityTypeConfiguration<DocumentType>
    {
        public DocumentTypeMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(255);
            // Properties
            Property(t => t.Title)
                .HasMaxLength(1000);

            // Table & Column Mappings
            ToTable("DocumentType");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.Title).HasColumnName("Title");
            Property(t => t.Order).HasColumnName("Order");
        }
    }
}