namespace Framework.DomainModel.Entities.Mapping
{
    public class PanelTypeMap : WorkCompEntityTypeConfiguration<PanelType>
    {
        public PanelTypeMap()
        {

            
            // Table & Column Mappings
            ToTable("PanelType");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.Code).HasColumnName("Code");
            Property(t => t.PayerId).HasColumnName("PayerId");
            Property(t => t.IsBasic).HasColumnName("IsBasic");

            // Relationships
            HasOptional(t => t.Payer)
                .WithMany(t => t.PanelTypes)
                .HasForeignKey(d => d.PayerId);
        }
    }
}