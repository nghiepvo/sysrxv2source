namespace Framework.DomainModel.Entities.Mapping
{
    public class IcdMap : WorkCompEntityTypeConfiguration<Icd>
    {
        public IcdMap()
        {
            // Properties
            Property(t => t.Code)
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("Icd");
            Property(t => t.IcdTypeId).HasColumnName("IcdTypeId");
            Property(t => t.Code).HasColumnName("Code");
            Property(t => t.Description).HasColumnName("Description");

            // Relationships
            HasOptional(t => t.IcdType)
                .WithMany(t => t.Icds)
                .HasForeignKey(d => d.IcdTypeId);
        }
    }
}