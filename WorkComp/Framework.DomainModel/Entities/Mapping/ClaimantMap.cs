﻿
namespace Framework.DomainModel.Entities.Mapping
{
    public class ClaimantMap: WorkCompEntityTypeConfiguration<Claimant>
    {
        public ClaimantMap()
        {
            // Properties
            Property(t => t.PayerPatientId)
                .HasMaxLength(50);
            Property(t => t.FirstName).IsRequired()
                .HasMaxLength(50);
            Property(t => t.LastName).IsRequired()
                .HasMaxLength(50);
            Property(t => t.MiddleName)
                .HasMaxLength(50);
            Property(t => t.Suffix)
                .HasMaxLength(50);
            Property(t => t.Gender).IsRequired()
                .HasMaxLength(1);
            Property(t => t.Email)
                .HasMaxLength(50);
            Property(t => t.Birthday);
            Property(t => t.PatientAbbr)
                .HasMaxLength(50);
            Property(t => t.Ssn)
                .HasMaxLength(50);
            Property(t => t.WorkPhone)
                .HasMaxLength(50);
            Property(t => t.WorkPhoneExtension)
                .HasMaxLength(50);
            Property(t => t.HomePhone)
                .HasMaxLength(50);
            Property(t => t.HomePhoneExtension)
                .HasMaxLength(50);
            Property(t => t.CellPhone)
                .HasMaxLength(50);
            Property(t => t.Address1)
                .HasMaxLength(100);
            Property(t => t.Address2)
                .HasMaxLength(100);
            Property(t => t.IsUserUpdate).IsRequired();

            // Table & Column Mappings
            ToTable("Claimant");
            Property(t => t.PayerPatientId).HasColumnName("PayerPatientId");
            Property(t => t.FirstName).HasColumnName("FirstName");
            Property(t => t.LastName).HasColumnName("LastName");
            Property(t => t.MiddleName).HasColumnName("MiddleName");
            Property(t => t.Suffix).HasColumnName("Suffix");
            Property(t => t.Birthday).HasColumnName("Birthday");
            Property(t => t.Gender).HasColumnName("Gender");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.PatientAbbr).HasColumnName("PatientAbbr");
            Property(t => t.Ssn).HasColumnName("Ssn");
            Property(t => t.WorkPhone).HasColumnName("WorkPhone");
            Property(t => t.WorkPhoneExtension).HasColumnName("WorkPhoneExtension");
            Property(t => t.HomePhone).HasColumnName("HomePhone");
            Property(t => t.HomePhoneExtension).HasColumnName("HomePhoneExtension");
            Property(t => t.CellPhone).HasColumnName("CellPhone");
            Property(t => t.BestContactNumber).HasColumnName("BestContactNumber");
            Property(t => t.ClaimantLanguageId).HasColumnName("ClaimantLanguageId");
            Property(t => t.ExpirationDate).HasColumnName("ExpirationDate");
            Property(t => t.Address1).HasColumnName("Address1");
            Property(t => t.Address2).HasColumnName("Address2");
            Property(t => t.CityId).HasColumnName("CityId");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.ZipId).HasColumnName("ZipId");

            HasOptional(o => o.ClaimantLanguage).WithMany(o => o.Claimants).HasForeignKey(o => o.ClaimantLanguageId);

            HasOptional(t => t.State)
                .WithMany(t => t.Claimants)
                .HasForeignKey(d => d.StateId);
            HasOptional(t => t.City)
                .WithMany(t => t.Claimants)
                .HasForeignKey(d => d.CityId);
            HasOptional(t => t.Zip)
                .WithMany(t => t.Claimants)
                .HasForeignKey(d => d.ZipId);
        }
    }
}
