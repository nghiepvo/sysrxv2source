namespace Framework.DomainModel.Entities.Mapping
{
    public class CollectionSiteMap : WorkCompEntityTypeConfiguration<CollectionSite>
    {
        public CollectionSiteMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(500).IsRequired();
            Property(t => t.Phone)
                .HasMaxLength(50);
            Property(t => t.Fax)
                .HasMaxLength(50);
            Property(t => t.Email)
               .HasMaxLength(100);
            Property(t => t.CollectionHour)
                .HasMaxLength(500);
            Property(t => t.LunchHour)
                .HasMaxLength(500);
            Property(t => t.LocationIdentified)
                .HasMaxLength(500);
            Property(t => t.CostInformation)
                .IsMaxLength();
            Property(t => t.ContactName)
                .HasMaxLength(500);
             Property(t => t.ContactEmail)
                .HasMaxLength(50);
            Property(t => t.Address)
               .HasMaxLength(100).IsRequired();

            //Name	nvarchar(500)	Unchecked
            //Phone	nvarchar(50)	Checked
            //Fax	nvarchar(50)	Checked
            //Email	nvarchar(100)	Checked
            //CollectionHour	nvarchar(500)	Checked
            //LunchHour	nvarchar(500)	Checked
            //LocationIdentified	nvarchar(500)	Checked
            //CostInformation	nvarchar(MAX)	Checked
            //ContactName	nvarchar(500)	Checked
            //ContactEmail	nvarchar(50)	Checked
            //Address	nvarchar(100)	Unchecked
            //StateId	int	Unchecked
            //CityId	int	Unchecked
            //ZipId	int	Unchecked
            //Lat	float	Checked
            //Lng	float	Checked
            //Contracted	bit	Checked

            // Table & Column Mappings
            ToTable("CollectionSite");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Fax).HasColumnName("Fax");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.CollectionHour).HasColumnName("CollectionHour");
            Property(t => t.LunchHour).HasColumnName("LunchHour");
            Property(t => t.LocationIdentified).HasColumnName("LocationIdentified");
            Property(t => t.CostInformation).HasColumnName("CostInformation");
            Property(t => t.ContactName).HasColumnName("ContactName");
            Property(t => t.ContactEmail).HasColumnName("ContactEmail");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.CityId).HasColumnName("CityId");
            Property(t => t.ZipId).HasColumnName("ZipId");
            Property(t => t.Lat).HasColumnName("Lat");
            Property(t => t.Lng).HasColumnName("Lng");
            Property(t => t.Contracted).HasColumnName("Contracted");

            // Relationship
            HasOptional(o => o.State).WithMany(o => o.CollectionSites).HasForeignKey(o => o.StateId);
            HasOptional(o => o.City).WithMany(o => o.CollectionSites).HasForeignKey(o => o.CityId);
            HasOptional(o => o.Zip).WithMany(o => o.CollectionSites).HasForeignKey(o => o.ZipId);
        }
    }
}