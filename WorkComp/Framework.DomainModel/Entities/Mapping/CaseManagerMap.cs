﻿namespace Framework.DomainModel.Entities.Mapping
{
    public class CaseManagerMap: WorkCompEntityTypeConfiguration<CaseManager>
    {
        public CaseManagerMap()
        {
            // Properties
            Property(o => o.Name).HasMaxLength(100).IsRequired();
            Property(o => o.Agency).HasMaxLength(100);
            Property(o => o.Phone).HasMaxLength(50);
            Property(o => o.Email).HasMaxLength(50);
            Property(o => o.Fax).HasMaxLength(50);
            Property(o => o.SpecialInstructions).IsMaxLength();
            Property(o => o.Address).HasMaxLength(200).IsRequired();

            // Table & Column Mappings
            ToTable("CaseManager");
            Property(o => o.ClaimNumberId).HasColumnName("ClaimNumberId");
            Property(o => o.Name).HasColumnName("Name");
            Property(o => o.Agency).HasColumnName("Agency");
            Property(o => o.Phone).HasColumnName("Phone");
            Property(o => o.Email).HasColumnName("Email");
            Property(o => o.Fax).HasColumnName("Fax");
            Property(o => o.SpecialInstructions).HasColumnName("SpecialInstructions");
            Property(o => o.Address).HasColumnName("Address");
            Property(o => o.CityId).HasColumnName("CityId");
            Property(o => o.StateId).HasColumnName("StateId");
            Property(o => o.ZipId).HasColumnName("ZipId");

            //Relationship
            HasRequired(o => o.ClaimNumber).WithMany(o => o.CaseManagers).HasForeignKey(o => o.ClaimNumberId);
            HasOptional(o => o.State).WithMany(o => o.CaseManagers).HasForeignKey(o => o.StateId);
            HasOptional(o => o.City).WithMany(o => o.CaseManagers).HasForeignKey(o => o.CityId);
            HasOptional(o => o.Zip).WithMany(o => o.CaseManagers).HasForeignKey(o => o.ZipId);
        }
        
        
    }
}
