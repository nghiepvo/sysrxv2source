namespace Framework.DomainModel.Entities.Mapping
{
    public class AssayCodeMap : WorkCompEntityTypeConfiguration<AssayCode>
    {
        public AssayCodeMap()
        {
            //AssayCodeDescriptionId	int	Unchecked
            //SampleTestingTypeId	int	Unchecked
            //Code	nvarchar(10)	Unchecked

            // Properties
            Property(t => t.Code)
                .HasMaxLength(50);
            Property(t => t.SampleTestingTypeId)
                .IsRequired();
            Property(t => t.AssayCodeDescriptionId)
                 .IsRequired();

            // Table & Column Mappings
            ToTable("AssayCode");
            Property(t => t.Code).HasColumnName("Code");
            Property(t => t.SampleTestingTypeId).HasColumnName("SampleTestingTypeId");
            Property(t => t.AssayCodeDescriptionId).HasColumnName("AssayCodeDescriptionId");

            //Relationship
            HasRequired(o => o.AssayCodeDescription).WithMany(o => o.AssayCodes).HasForeignKey(o => o.AssayCodeDescriptionId);
            HasRequired(o => o.SampleTestingType).WithMany(o => o.AssayCodes).HasForeignKey(o => o.SampleTestingTypeId);
        }
    }
}