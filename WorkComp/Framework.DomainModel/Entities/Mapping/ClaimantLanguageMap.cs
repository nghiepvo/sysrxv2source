﻿namespace Framework.DomainModel.Entities.Mapping
{
    public class ClaimantLanguageMap : WorkCompEntityTypeConfiguration<ClaimantLanguage>
    {
        public ClaimantLanguageMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(100);

            // Table & Column Mappings
            ToTable("ClaimantLanguage");
            Property(t => t.Name).HasColumnName("Name");
        }
    }
}
