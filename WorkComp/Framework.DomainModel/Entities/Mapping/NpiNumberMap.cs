namespace Framework.DomainModel.Entities.Mapping
{
    public class NpiNumberMap : WorkCompEntityTypeConfiguration<NpiNumber>
    {
        public NpiNumberMap()
        {
            // Properties
            Property(t => t.OrganizationName)
                .HasMaxLength(500);
            Property(t => t.FirstName)
                .HasMaxLength(100).IsRequired();
            Property(t => t.LastName)
                .HasMaxLength(100).IsRequired();
            Property(t => t.MiddleName)
                .HasMaxLength(50);
            Property(t => t.Npi)
                .HasMaxLength(50).IsRequired();
            Property(t => t.Phone)
                .HasMaxLength(50);
            Property(t => t.Fax)
                .HasMaxLength(50);
            Property(t => t.LicenseNumber)
                .HasMaxLength(50);
            Property(t => t.ProviderCredentialText)
                .HasMaxLength(50);
            Property(t => t.Email)
                .HasMaxLength(50);
            Property(t => t.Address)
                .HasMaxLength(500).IsRequired();
            //Property(t => t.StateId).IsRequired();
            //Property(t => t.CityId).IsRequired();
            //Property(t => t.ZipId).IsRequired();


            // Table & Column Mappings
            ToTable("NpiNumber");
            Property(t => t.OrganizationName).HasColumnName("OrganizationName");
            Property(t => t.FirstName).HasColumnName("FirstName");
            Property(t => t.LastName).HasColumnName("LastName");
            Property(t => t.MiddleName).HasColumnName("MiddleName");
            Property(t => t.Npi).HasColumnName("Npi");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Fax).HasColumnName("Fax");
            Property(t => t.LicenseNumber).HasColumnName("LicenseNumber");
            Property(t => t.ProviderCredentialText).HasColumnName("ProviderCredentialText");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.CityId).HasColumnName("CityId");
            Property(t => t.ZipId).HasColumnName("ZipId");
            Property(t => t.IsUserUpdate).HasColumnName("IsUserUpdate");

            //Relationship
            HasOptional(o => o.State).WithMany(s => s.NpiNumbers).HasForeignKey(s => s.StateId);
            HasOptional(o => o.City).WithMany(s => s.NpiNumbers).HasForeignKey(s => s.CityId);
            HasOptional(o => o.Zip).WithMany(s => s.NpiNumbers).HasForeignKey(s => s.ZipId);
            
        }
    }
}