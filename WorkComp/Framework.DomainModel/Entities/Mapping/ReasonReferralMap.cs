namespace Framework.DomainModel.Entities.Mapping
{
    public class ReasonReferralMap : WorkCompEntityTypeConfiguration<ReasonReferral>
    {
        public ReasonReferralMap()
        {
            // Properties
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            ToTable("ReasonReferral");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.ParentId).HasColumnName("ParentId");

            // Relationships
            HasOptional(t => t.Parent)
                .WithMany(t => t.Childrent)
                .HasForeignKey(d => d.ParentId);
        }
    }
}