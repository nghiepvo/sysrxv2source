namespace Framework.DomainModel.Entities.Mapping
{
    public class DrugMap : WorkCompEntityTypeConfiguration<Drug>
    {
        public DrugMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(100);
            Property(t => t.Class)
                .HasMaxLength(100).IsRequired();
            Property(t => t.Description)
                .HasMaxLength(1000);

            // Table & Column Mappings
            ToTable("Drug");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.Class).HasColumnName("Class");
            Property(t => t.Description).HasColumnName("Description");
        }
    }
}