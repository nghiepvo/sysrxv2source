namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralCollectionSiteMap : WorkCompEntityTypeConfiguration<ReferralCollectionSite>
    {
        public ReferralCollectionSiteMap()
        {
            Property(t => t.Phone)
                .HasMaxLength(50);

            Property(t => t.Fax)
                .HasMaxLength(50);

            Property(t => t.Address)
                .HasMaxLength(200);

            // Table & Column Mappings
            ToTable("ReferralCollectionSite");
            Property(t => t.CollectionSiteId).HasColumnName("CollectionSiteId");
            Property(t => t.ReferralId).HasColumnName("ReferralId");
            Property(t => t.SpecialInstructions).HasColumnName("SpecialInstructions");
            Property(t => t.CollectionDate).HasColumnName("CollectionDate");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Fax).HasColumnName("Fax");
            Property(t => t.Address).HasColumnName("Address");

            // Relationships
            HasOptional(t => t.CollectionSite)
                .WithMany(t => t.ReferralCollectionSites)
                .HasForeignKey(d => d.CollectionSiteId);
            HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralCollectionSites)
                .HasForeignKey(d => d.ReferralId);
        }
    }
}