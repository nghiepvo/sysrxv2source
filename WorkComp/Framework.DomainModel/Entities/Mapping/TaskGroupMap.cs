namespace Framework.DomainModel.Entities.Mapping
{
    public class TaskGroupMap : WorkCompEntityTypeConfiguration<TaskGroup>
    {
        public TaskGroupMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(200);
            // Table & Column Mappings
            ToTable("TaskGroup");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.IsDefault).HasColumnName("IsDefault");
        }
    }
}