namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralReasonReferralMap : WorkCompEntityTypeConfiguration<ReferralReasonReferral>
    {
        public ReferralReasonReferralMap()
        {
            // Table & Column Mappings
            ToTable("ReferralReasonReferral");
            Property(t => t.ReferralId).HasColumnName("ReferralId");
            Property(t => t.ReasonReferralId).HasColumnName("ReasonReferralId");

            // Relationships
            HasOptional(t => t.ReasonReferral)
                .WithMany(t => t.ReferralReasonReferrals)
                .HasForeignKey(d => d.ReasonReferralId);
            HasOptional(t => t.Referral)
                .WithMany(t => t.ReferralReasonReferrals)
                .HasForeignKey(d => d.ReferralId);
        }
    }
}