namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralEmailTemplateMap : WorkCompEntityTypeConfiguration<ReferralEmailTemplate>
    {
        public ReferralEmailTemplateMap()
        {
            // Properties
            Property(t => t.Content).IsRequired();
            Property(t => t.Subject).IsRequired();
            Property(t => t.ReferralId).IsRequired();
            Property(t => t.EmailTemplateId).IsRequired();

            // Table & Column Mappings
            ToTable("ReferralEmailTemplate");
            Property(t => t.ReferralId).HasColumnName("ReferralId");
            Property(t => t.EmailTemplateId).HasColumnName("EmailTemplateId");
            Property(t => t.Content).HasColumnName("Content");
            Property(t => t.Subject).HasColumnName("Subject");
            Property(t => t.EmailAddress).HasColumnName("EmailAddress");
            Property(t => t.DateToSendMail).HasColumnName("DateToSendMail");
            Property(t => t.FaxNumber).HasColumnName("FaxNumber");
            Property(t => t.DateToSendFax).HasColumnName("DateToSendFax");

            // Relationships
            HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralEmailTemplates)
                .HasForeignKey(d => d.ReferralId);

            HasRequired(t => t.EmailTemplate)
                .WithMany(t => t.ReferralEmailTemplates)
                .HasForeignKey(d => d.EmailTemplateId);
        }
    }
}