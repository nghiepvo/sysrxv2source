﻿
namespace Framework.DomainModel.Entities.Mapping
{
    public class PayerMap : WorkCompEntityTypeConfiguration<Payer>
    {
        public PayerMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(50);

            Property(t => t.ControlNumberPrefix)
                .HasMaxLength(50);

            Property(t => t.SpecialInstructions)
                .IsMaxLength();

            Property(t => t.Address)
                .HasMaxLength(200);

            // Table & Column Mappings
            ToTable("Payer");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.ControlNumberPrefix).HasColumnName("ControlNumberPrefix");
            Property(t => t.SpecialInstructions).HasColumnName("SpecialInstructions");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.CityId).HasColumnName("CityId");
            Property(t => t.ZipId).HasColumnName("ZipId");

            // Relationships
            HasOptional(t => t.State)
                .WithMany(t => t.Payers)
                .HasForeignKey(d => d.StateId);
            HasOptional(t => t.City)
                .WithMany(t => t.Payers)
                .HasForeignKey(d => d.CityId);
            HasOptional(t => t.Zip)
                .WithMany(t => t.Payers)
                .HasForeignKey(d => d.ZipId);
        }
    }
}
