using System;
using System.Collections.Generic;

namespace Framework.DomainModel.Entities
{
    public class ReferralTask : Entity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int? AssignToId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime DueDate { get; set; }
        public int? TaskTypeId { get; set; }
        public int ReferralId { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual Referral Referral { get; set; }
        public virtual User AssignTo { get; set; }
        public int StatusId { get; set; }
        public bool? Rush { get; set; }
        public bool? IsCollectionServiceRequestLetter { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public DateTime? PendingDate { get; set; }
        public DateTime? ReopenDate { get; set; }
    }
}