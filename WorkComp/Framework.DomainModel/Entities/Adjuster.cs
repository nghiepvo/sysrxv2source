﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class Adjuster : Entity
    {
        public Adjuster()
        {
            ClaimNumbers = new Collection<ClaimNumber>();
        }
        public int? BranchId { get; set; }
        public virtual Branch Branch { get; set; }
        

        [LocalizeRequired, LocalizeMaxLength(50)]
        public string FirstName { get; set; }
        
        [LocalizeMaxLength(50)]
        public string MiddleName { get; set; }
        [LocalizeRequired]
        [LocalizeMaxLength(50)]
        public string LastName { get; set; }

        [LocalizeRequired, LocalizePhone]
        public string Phone { get; set; }

        [LocalizeMaxLength(50)]
        public string Extension { get; set; }

        [LocalizeRequired, LocalizeEmailAddress]
        public string Email { get; set; }

        public DateTime? AssignedDate { get; set; }
        [LocalizeRequired]
        [LocalizeMaxLength(100)]
        public string Address { get; set; }
        public int? StateId { get; set; }
        public virtual State State { get; set; }
        public int? CityId { get; set; }
        public virtual City City { get; set; }
        public int? ZipId { get; set; }
        public virtual Zip Zip { get; set; }
        public bool IsUserUpdate { get; set; }

        public string ExternalId { get; set; }
        public bool? AllowCreateReferral { get; set; }
        public bool? OptedOutSendMail { get; set; }

        public virtual ICollection<ClaimNumber> ClaimNumbers { get; set; }
    }
}
