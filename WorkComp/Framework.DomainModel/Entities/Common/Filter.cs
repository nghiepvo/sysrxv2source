﻿using System;
using System.Collections.Generic;

namespace Framework.DomainModel.Entities.Common
{
    public enum Operator
    {
        None = 0,
        Equals = 1,
        NotEquals = 2,
        GreaterThan = 3,
        GreaterThanOrEqual = 4,
        LessThan = 5,
        LessThanOrEqual = 6,
        Contains = 7,
        NotContains = 8,
        StartsWith = 9,
        EndsWith = 10
    }

    public enum TypeDataKendo
    {
        None = 0,
        String = 1,
        Number = 2,
        Date = 3,
        DateTime = 4,
        Boolean = 5
    }

    public enum Logic
    {
        None = 0,
        And = 1,
        Or = 2
    
    }

    [Serializable]
    public class TestFilter
    {
        public string Field { get; set; }
        public string FieldTypeString { get; set; }
    }

    [Serializable]
    public class Filter
    {
        public Filter()
        {
            FieldFilters = new List<FieldFilter>();
        }
        public List<FieldFilter> FieldFilters { get; set; }
        public string Field { get; set; }
        public string FieldTypeString { get; set; }

        public TypeDataKendo FieldType
        {
            get
            {
                if (string.IsNullOrEmpty(FieldTypeString))
                {
                    return TypeDataKendo.None;
                }
                switch (FieldTypeString.ToLower())
                {
                    case "string":
                        return TypeDataKendo.String;
                    case "number":
                        return TypeDataKendo.Number;
                    case "date":
                        return TypeDataKendo.Date;
                    case "datetime":
                        return TypeDataKendo.DateTime;
                    case "boolean":
                        return TypeDataKendo.Boolean;

                    default :
                        return TypeDataKendo.None;
                }
            }
        }

        public string LogicString { get; set; }

        public Logic Logical
        {
            get
            {
                if (string.IsNullOrEmpty(LogicString))
                {
                    return Logic.None;
                }
                switch (LogicString.ToLower())
                {
                    case "and":
                        return Logic.And;
                    case "or":
                        return Logic.Or;
                    default:
                        return Logic.None;
                }
            }
        }
    }

    [Serializable]
    public class FieldFilter
    {
        
        public string Value { get; set; }
        public string OperatorString { get; set; }

        public Operator Operation
        {
            get
            {
                switch (OperatorString.ToLower())
                {
                    case "eq":
                        return Operator.Equals;
                    case "neq":
                        return Operator.NotEquals;
                    case "startswith":
                        return Operator.StartsWith;
                    case "endswith":
                        return Operator.EndsWith;
                    case "contains":
                        return Operator.Contains;
                    case "doesnotcontain":
                        return Operator.NotContains;
                    case "gt":
                        return Operator.GreaterThan;
                    case "gte":
                        return Operator.GreaterThanOrEqual;
                    case "lt":
                        return Operator.LessThan;
                    case "lte":
                        return Operator.LessThanOrEqual;
                    default:
                        return Operator.None;
                }
            }
        }
    }
    

    
}
