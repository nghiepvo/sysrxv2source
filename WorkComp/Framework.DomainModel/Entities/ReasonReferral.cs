﻿using System.Collections.Generic;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class ReasonReferral:Entity
    {
        public ReasonReferral()
        {
            Childrent = new List<ReasonReferral>();
            ReferralReasonReferrals = new List<ReferralReasonReferral>();
        }

        [LocalizeRequired]
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<ReasonReferral> Childrent { get; set; }
        public virtual ReasonReferral Parent { get; set; }
        public virtual ICollection<ReferralReasonReferral> ReferralReasonReferrals { get; set; }
    }
}