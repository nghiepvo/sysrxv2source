using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class AssayCodeDescription : Entity
    {
        public AssayCodeDescription()
        {
            AssayCodes = new Collection<AssayCode>();
        }

        [LocalizeRequired]
        [LocalizeMaxLength(200)]
        public string Description { get; set; }
        public virtual ICollection<AssayCode> AssayCodes { get; set; } 
    }
}