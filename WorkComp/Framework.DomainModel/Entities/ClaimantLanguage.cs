﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;
namespace Framework.DomainModel.Entities
{
    public class ClaimantLanguage : Entity
    {
        public ClaimantLanguage()
        {
            Claimants = new Collection<Claimant>();
        }
        [LocalizeRequired]
        public string Name { get; set; }

        public virtual ICollection<Claimant> Claimants { get; set; }
    }
}
