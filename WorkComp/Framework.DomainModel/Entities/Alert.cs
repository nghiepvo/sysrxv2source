﻿namespace Framework.DomainModel.Entities
{
    public class Alert: Entity
    {
        public int StatusId { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public int LinkId { get; set; }
        public int AssignToId { get; set; }
        public User AssignTo { get; set; }
        public bool IsRush { get; set; }
    }
}
