using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Icd
    {
        public Icd()
        {
            this.ReferralIcds = new List<ReferralIcd>();
        }

        public int Id { get; set; }
        public Nullable<int> IcdTypeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual IcdType IcdType { get; set; }
        public virtual ICollection<ReferralIcd> ReferralIcds { get; set; }
    }
}
