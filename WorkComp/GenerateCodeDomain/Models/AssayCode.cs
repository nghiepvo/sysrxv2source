using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class AssayCode
    {
        public int Id { get; set; }
        public int AssayCodeDescriptionId { get; set; }
        public int SampleTestingTypeId { get; set; }
        public string Code { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual AssayCodeDescription AssayCodeDescription { get; set; }
        public virtual SampleTestingType SampleTestingType { get; set; }
    }
}
