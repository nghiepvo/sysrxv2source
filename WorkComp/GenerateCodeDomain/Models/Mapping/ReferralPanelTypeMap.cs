using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ReferralPanelTypeMap : EntityTypeConfiguration<ReferralPanelType>
    {
        public ReferralPanelTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("ReferralPanelType");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.ReferralId).HasColumnName("ReferralId");
            this.Property(t => t.PanelTypeId).HasColumnName("PanelTypeId");

            // Relationships
            this.HasOptional(t => t.PanelType)
                .WithMany(t => t.ReferralPanelTypes)
                .HasForeignKey(d => d.PanelTypeId);
            this.HasOptional(t => t.Referral)
                .WithMany(t => t.ReferralPanelTypes)
                .HasForeignKey(d => d.ReferralId);

        }
    }
}
