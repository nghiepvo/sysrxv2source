using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class TaskTemplateMap : EntityTypeConfiguration<TaskTemplate>
    {
        public TaskTemplateMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(200);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("TaskTemplate");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.NumDueDate).HasColumnName("NumDueDate");
            this.Property(t => t.NumDueHour).HasColumnName("NumDueHour");
            this.Property(t => t.StatusId).HasColumnName("StatusId");
            this.Property(t => t.AssignToId).HasColumnName("AssignToId");
            this.Property(t => t.TaskTypeId).HasColumnName("TaskTypeId");
            this.Property(t => t.IsSystem).HasColumnName("IsSystem");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.TaskTemplates)
                .HasForeignKey(d => d.AssignToId);

        }
    }
}
