using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class PanelCodeMap : EntityTypeConfiguration<PanelCode>
    {
        public PanelCodeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CptCode)
                .HasMaxLength(50);

            this.Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("PanelCode");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CptCode).HasColumnName("CptCode");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.SampleTestingTypeId).HasColumnName("SampleTestingTypeId");
            this.Property(t => t.PanelId).HasColumnName("PanelId");
            this.Property(t => t.PanelTypeId).HasColumnName("PanelTypeId");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasOptional(t => t.Panel)
                .WithMany(t => t.PanelCodes)
                .HasForeignKey(d => d.PanelId);
            this.HasRequired(t => t.PanelType)
                .WithMany(t => t.PanelCodes)
                .HasForeignKey(d => d.PanelTypeId);
            this.HasRequired(t => t.SampleTestingType)
                .WithMany(t => t.PanelCodes)
                .HasForeignKey(d => d.SampleTestingTypeId);

        }
    }
}
