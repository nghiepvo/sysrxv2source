using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class FunctionMap : EntityTypeConfiguration<Function>
    {
        public FunctionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(200);

            this.Property(t => t.Controller)
                .HasMaxLength(200);

            this.Property(t => t.ViewName)
                .HasMaxLength(200);

            this.Property(t => t.Url)
                .HasMaxLength(200);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Function");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdParent).HasColumnName("IdParent");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Controller).HasColumnName("Controller");
            this.Property(t => t.ViewName).HasColumnName("ViewName");
            this.Property(t => t.Url).HasColumnName("Url");
            this.Property(t => t.Order).HasColumnName("Order");
            this.Property(t => t.IsSetParamUrl).HasColumnName("IsSetParamUrl");
            this.Property(t => t.IsMenu).HasColumnName("IsMenu");
            this.Property(t => t.IsSecurity).HasColumnName("IsSecurity");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasOptional(t => t.Function2)
                .WithMany(t => t.Function1)
                .HasForeignKey(d => d.IdParent);

        }
    }
}
