using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class EmployerMap : EntityTypeConfiguration<Employer>
    {
        public EmployerMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ExternalId)
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .HasMaxLength(50);

            this.Property(t => t.FederalTaxId)
                .HasMaxLength(50);

            this.Property(t => t.GeoAddress)
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .HasMaxLength(100);

            this.Property(t => t.Address1)
                .HasMaxLength(100);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Employer");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ExternalId).HasColumnName("ExternalId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.FederalTaxId).HasColumnName("FederalTaxId");
            this.Property(t => t.GeoAddress).HasColumnName("GeoAddress");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.CityId).HasColumnName("CityId");
            this.Property(t => t.ZipId).HasColumnName("ZipId");
            this.Property(t => t.Rank).HasColumnName("Rank");
            this.Property(t => t.IsUserUpdate).HasColumnName("IsUserUpdate");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasOptional(t => t.City)
                .WithMany(t => t.Employers)
                .HasForeignKey(d => d.CityId);
            this.HasOptional(t => t.State)
                .WithMany(t => t.Employers)
                .HasForeignKey(d => d.StateId);
            this.HasOptional(t => t.Zip)
                .WithMany(t => t.Employers)
                .HasForeignKey(d => d.ZipId);

        }
    }
}
