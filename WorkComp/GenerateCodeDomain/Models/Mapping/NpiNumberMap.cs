using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class NpiNumberMap : EntityTypeConfiguration<NpiNumber>
    {
        public NpiNumberMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.OrganizationName)
                .HasMaxLength(50);

            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MiddleName)
                .HasMaxLength(50);

            this.Property(t => t.Npi)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .HasMaxLength(50);

            this.Property(t => t.Fax)
                .HasMaxLength(50);

            this.Property(t => t.LicenseNumber)
                .HasMaxLength(50);

            this.Property(t => t.ProviderCredentialText)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .HasMaxLength(200);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("NpiNumber");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.OrganizationName).HasColumnName("OrganizationName");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.MiddleName).HasColumnName("MiddleName");
            this.Property(t => t.Npi).HasColumnName("Npi");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.LicenseNumber).HasColumnName("LicenseNumber");
            this.Property(t => t.ProviderCredentialText).HasColumnName("ProviderCredentialText");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.CityId).HasColumnName("CityId");
            this.Property(t => t.ZipId).HasColumnName("ZipId");
            this.Property(t => t.IsUserUpdate).HasColumnName("IsUserUpdate");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasRequired(t => t.City)
                .WithMany(t => t.NpiNumbers)
                .HasForeignKey(d => d.CityId);
            this.HasRequired(t => t.State)
                .WithMany(t => t.NpiNumbers)
                .HasForeignKey(d => d.StateId);
            this.HasRequired(t => t.Zip)
                .WithMany(t => t.NpiNumbers)
                .HasForeignKey(d => d.ZipId);

        }
    }
}
