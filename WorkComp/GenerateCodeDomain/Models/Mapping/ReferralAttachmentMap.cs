using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ReferralAttachmentMap : EntityTypeConfiguration<ReferralAttachment>
    {
        public ReferralAttachmentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.AttachedFileName)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("ReferralAttachment");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ReferralId).HasColumnName("ReferralId");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.RowGUID).HasColumnName("RowGUID");
            this.Property(t => t.AttachedFileSize).HasColumnName("AttachedFileSize");
            this.Property(t => t.AttachedFileName).HasColumnName("AttachedFileName");
            this.Property(t => t.AttachedFileContent).HasColumnName("AttachedFileContent");

            // Relationships
            this.HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralAttachments)
                .HasForeignKey(d => d.ReferralId);

        }
    }
}
