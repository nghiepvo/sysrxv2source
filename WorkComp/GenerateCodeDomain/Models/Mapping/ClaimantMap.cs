using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ClaimantMap : EntityTypeConfiguration<Claimant>
    {
        public ClaimantMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.PayerPatientId)
                .HasMaxLength(50);

            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MiddleName)
                .HasMaxLength(50);

            this.Property(t => t.Suffix)
                .HasMaxLength(50);

            this.Property(t => t.Gender)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.PatientAbbr)
                .HasMaxLength(50);

            this.Property(t => t.Ssn)
                .HasMaxLength(50);

            this.Property(t => t.WorkPhone)
                .HasMaxLength(50);

            this.Property(t => t.WorkPhoneExtension)
                .HasMaxLength(50);

            this.Property(t => t.HomePhone)
                .HasMaxLength(50);

            this.Property(t => t.HomePhoneExtension)
                .HasMaxLength(50);

            this.Property(t => t.CellPhone)
                .HasMaxLength(50);

            this.Property(t => t.ContactPhone)
                .HasMaxLength(50);

            this.Property(t => t.BestContactNumber)
                .HasMaxLength(50);

            this.Property(t => t.Address1)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Address2)
                .HasMaxLength(100);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Claimant");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.PayerPatientId).HasColumnName("PayerPatientId");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.MiddleName).HasColumnName("MiddleName");
            this.Property(t => t.Suffix).HasColumnName("Suffix");
            this.Property(t => t.Birthday).HasColumnName("Birthday");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.PatientAbbr).HasColumnName("PatientAbbr");
            this.Property(t => t.Ssn).HasColumnName("Ssn");
            this.Property(t => t.WorkPhone).HasColumnName("WorkPhone");
            this.Property(t => t.WorkPhoneExtension).HasColumnName("WorkPhoneExtension");
            this.Property(t => t.HomePhone).HasColumnName("HomePhone");
            this.Property(t => t.HomePhoneExtension).HasColumnName("HomePhoneExtension");
            this.Property(t => t.CellPhone).HasColumnName("CellPhone");
            this.Property(t => t.ContactPhone).HasColumnName("ContactPhone");
            this.Property(t => t.BestContactNumber).HasColumnName("BestContactNumber");
            this.Property(t => t.ClaimantLanguageId).HasColumnName("ClaimantLanguageId");
            this.Property(t => t.ExpirationDate).HasColumnName("ExpirationDate");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.CityId).HasColumnName("CityId");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.ZipId).HasColumnName("ZipId");
            this.Property(t => t.Lat).HasColumnName("Lat");
            this.Property(t => t.Lng).HasColumnName("Lng");
            this.Property(t => t.IsUserUpdate).HasColumnName("IsUserUpdate");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasRequired(t => t.City)
                .WithMany(t => t.Claimants)
                .HasForeignKey(d => d.CityId);
            this.HasOptional(t => t.ClaimantLanguage)
                .WithMany(t => t.Claimants)
                .HasForeignKey(d => d.ClaimantLanguageId);
            this.HasRequired(t => t.State)
                .WithMany(t => t.Claimants)
                .HasForeignKey(d => d.StateId);
            this.HasRequired(t => t.Zip)
                .WithMany(t => t.Claimants)
                .HasForeignKey(d => d.ZipId);

        }
    }
}
