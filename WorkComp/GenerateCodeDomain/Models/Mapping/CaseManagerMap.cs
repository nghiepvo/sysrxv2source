using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class CaseManagerMap : EntityTypeConfiguration<CaseManager>
    {
        public CaseManagerMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Agency)
                .HasMaxLength(100);

            this.Property(t => t.Phone)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Fax)
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("CaseManager");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ClaimNumberId).HasColumnName("ClaimNumberId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Agency).HasColumnName("Agency");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.SpecialInstructions).HasColumnName("SpecialInstructions");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.CityId).HasColumnName("CityId");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.ZipId).HasColumnName("ZipId");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasRequired(t => t.City)
                .WithMany(t => t.CaseManagers)
                .HasForeignKey(d => d.CityId);
            this.HasRequired(t => t.ClaimNumber)
                .WithMany(t => t.CaseManagers)
                .HasForeignKey(d => d.ClaimNumberId);
            this.HasRequired(t => t.State)
                .WithMany(t => t.CaseManagers)
                .HasForeignKey(d => d.StateId);
            this.HasRequired(t => t.Zip)
                .WithMany(t => t.CaseManagers)
                .HasForeignKey(d => d.ZipId);

        }
    }
}
