using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class TaskStatu
    {
        public TaskStatu()
        {
            this.Referrals = new List<Referral>();
            this.TaskTemplates = new List<TaskTemplate>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
        public virtual ICollection<TaskTemplate> TaskTemplates { get; set; }
    }
}
