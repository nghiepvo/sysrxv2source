using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class TaskTemplate
    {
        public TaskTemplate()
        {
            this.TaskGroupTaskTemplates = new List<TaskGroupTaskTemplate>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<int> NumDueDate { get; set; }
        public Nullable<double> NumDueHour { get; set; }
        public int StatusId { get; set; }
        public Nullable<int> AssignToId { get; set; }
        public Nullable<int> TaskTypeId { get; set; }
        public Nullable<bool> IsSystem { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<TaskGroupTaskTemplate> TaskGroupTaskTemplates { get; set; }
        public virtual User User { get; set; }
    }
}
