using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Employer
    {
        public Employer()
        {
            this.ClaimNumbers = new List<ClaimNumber>();
            this.Referrals = new List<Referral>();
        }

        public int Id { get; set; }
        public string ExternalId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string FederalTaxId { get; set; }
        public string GeoAddress { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public Nullable<int> StateId { get; set; }
        public Nullable<int> CityId { get; set; }
        public Nullable<int> ZipId { get; set; }
        public Nullable<int> Rank { get; set; }
        public Nullable<bool> IsUserUpdate { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual City City { get; set; }
        public virtual ICollection<ClaimNumber> ClaimNumbers { get; set; }
        public virtual State State { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
