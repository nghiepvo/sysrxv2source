using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class SampleTestingType
    {
        public SampleTestingType()
        {
            this.AssayCodes = new List<AssayCode>();
            this.PanelCodes = new List<PanelCode>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<bool> Checked { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<AssayCode> AssayCodes { get; set; }
        public virtual ICollection<PanelCode> PanelCodes { get; set; }
    }
}
