using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class ReferralCollectionSite
    {
        public int Id { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public Nullable<int> CollectionSiteId { get; set; }
        public int ReferralId { get; set; }
        public string SpecialInstructions { get; set; }
        public Nullable<System.DateTime> CollectionDate { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public Nullable<bool> IsRush { get; set; }
        public virtual CollectionSite CollectionSite { get; set; }
        public virtual Referral Referral { get; set; }
    }
}
