using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class UserRoleFunction
    {
        public int Id { get; set; }
        public Nullable<int> UserRoleId { get; set; }
        public Nullable<int> SecurityOperationId { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public Nullable<System.DateTime> LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public Nullable<int> DocumentTypeId { get; set; }
        public virtual DocumentType DocumentType { get; set; }
        public virtual SecurityOperation SecurityOperation { get; set; }
        public virtual UserRole UserRole { get; set; }
    }
}
