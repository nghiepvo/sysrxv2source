using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class ReferralMedicationHistory
    {
        public int Id { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public int DrugId { get; set; }
        public int ReferralId { get; set; }
        public Nullable<int> DaysSupply { get; set; }
        public string Dosage { get; set; }
        public Nullable<int> DosageUnit { get; set; }
        public Nullable<int> ProvidedBy { get; set; }
        public Nullable<System.DateTime> FillDate { get; set; }
        public virtual Drug Drug { get; set; }
        public virtual Referral Referral { get; set; }
    }
}
