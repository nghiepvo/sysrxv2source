using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class ReferralIcd
    {
        public int Id { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public Nullable<int> ReferralId { get; set; }
        public Nullable<int> IcdId { get; set; }
        public virtual Icd Icd { get; set; }
        public virtual Referral Referral { get; set; }
    }
}
