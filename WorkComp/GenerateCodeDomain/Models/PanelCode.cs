using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class PanelCode
    {
        public int Id { get; set; }
        public string CptCode { get; set; }
        public string Code { get; set; }
        public int SampleTestingTypeId { get; set; }
        public Nullable<int> PanelId { get; set; }
        public int PanelTypeId { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual Panel Panel { get; set; }
        public virtual PanelType PanelType { get; set; }
        public virtual SampleTestingType SampleTestingType { get; set; }
    }
}
