using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Attorney
    {
        public Attorney()
        {
            this.Referrals = new List<Referral>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Lawfirm { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public Nullable<int> StateId { get; set; }
        public Nullable<int> CityId { get; set; }
        public Nullable<int> ZipId { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual City City { get; set; }
        public virtual State State { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
