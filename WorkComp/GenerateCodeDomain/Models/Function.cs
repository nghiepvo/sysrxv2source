using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Function
    {
        public Function()
        {
            this.Function1 = new List<Function>();
            this.GridConfigs = new List<GridConfig>();
        }

        public int Id { get; set; }
        public Nullable<int> IdParent { get; set; }
        public string Name { get; set; }
        public string Controller { get; set; }
        public string ViewName { get; set; }
        public string Url { get; set; }
        public Nullable<int> Order { get; set; }
        public Nullable<bool> IsSetParamUrl { get; set; }
        public Nullable<bool> IsMenu { get; set; }
        public Nullable<bool> IsSecurity { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<Function> Function1 { get; set; }
        public virtual Function Function2 { get; set; }
        public virtual ICollection<GridConfig> GridConfigs { get; set; }
    }
}
