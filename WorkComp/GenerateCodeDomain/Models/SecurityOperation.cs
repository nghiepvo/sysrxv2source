using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class SecurityOperation
    {
        public SecurityOperation()
        {
            this.UserRoleFunctions = new List<UserRoleFunction>();
        }

        public int Id { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public Nullable<System.DateTime> LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public string Name { get; set; }
        public virtual ICollection<UserRoleFunction> UserRoleFunctions { get; set; }
    }
}
