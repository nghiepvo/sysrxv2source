using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class UserRole
    {
        public UserRole()
        {
            this.Users = new List<User>();
            this.UserRoleFunctions = new List<UserRoleFunction>();
        }

        public int Id { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public string Name { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<UserRoleFunction> UserRoleFunctions { get; set; }
    }
}
