using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Configuration
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public Nullable<bool> Configurable { get; set; }
        public Nullable<bool> IsHtml { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
    }
}
