﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Utility;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace Solr.ServiceLayer
{
    public class SolrClaimantService : SolrMasterFileService<SolrClaimant>, ISolrClaimantService
    {
        public SolrClaimantService()
        {
            SearchColumns = new Collection<string> { "fullname", "gender", "email", "bestcontactnumber", "ssn", "language", "address" };
            DisplayColumnForCombobox = "fullname";
        }
        public override IEnumerable<ReadOnlyGridVo> GetDataForGridWithEntity(IList<SolrClaimant> listData)
        {
            var finalResult = listData.Select(x => new ClaimantGridVo
            {
                Name = x.FullName,
                GenderName = x.Gender,
                Dob=x.Dob==null?"":x.Dob.Value.ToString("MM/dd/yyyy"),
                Language = x.Language,
                Ssn = x.Ssn,
                BestContactNumber = x.BestContactNumber.ApplyFormatPhone(),
                Email = x.Email,
                FullAddress=x.Address,
                Id = x.Id.ParseToInt()
            });

            return finalResult;
        }

        public override IEnumerable<LookupItemVo> GetDataForComboBox(IList<SolrClaimant> listData)
        {
            var finalResult = listData.Select(x => new LookupItemVo
            {
                KeyId = x.Id.ParseToInt(),
                DisplayName = x.FullName,
            });

            return finalResult;
        }

        protected override List<SortOrder> BuildSortExpression(IQueryInfo queryInfo)
        {
            //if (queryInfo.Sort == null || queryInfo.Sort.Count == 0)
            //{
            //    queryInfo.Sort = new List<Sort> { new Sort { Field = "idclaimant", Dir = "desc" } };
            //}
            var result = new List<SortOrder>();
            //foreach (var sort in queryInfo.Sort)
            //{
            //    switch (sort.Field)
            //    {
            //        case "Name":
            //            sort.Field = "FullName";
            //            break;
            //        case "GenderName":
            //            sort.Field = "Gender";
            //            break;
            //    }
            //    if (sort.Dir == "desc")
            //    {
            //        var objAdd = new SortOrder(sort.Field.ToLower(), Order.DESC);
            //        result.Add(objAdd);
            //    }
            //    else
            //    {
            //        var objAdd = new SortOrder(sort.Field.ToLower(), Order.ASC);
            //        result.Add(objAdd);
            //    }
            //}
            var objAdd = new SortOrder("idclaimant", Order.DESC);
            result.Add(objAdd);
            return result;
        }

        protected override List<SortOrder> BuildSortOrders()
        {
            //new List<SortOrder>{new SortOrder("id", Order.ASC)} ,
            return new List<SortOrder>() { new SortOrder(DisplayColumnForCombobox, Order.ASC) };
        }
    }
}