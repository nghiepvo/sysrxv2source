﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Utility;
using Microsoft.Practices.ServiceLocation;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Exceptions;

namespace Solr.ServiceLayer
{
    public class SolrNpiNumberService : SolrMasterFileService<SolrNpiNumber>, ISolrNpiNumberService
    {
        private readonly ISolrOperations<SolrNpiNumber> _npiNumberSolrService;
        public SolrNpiNumberService()
        {
            _npiNumberSolrService = ServiceLocator.Current.GetInstance<ISolrOperations<SolrNpiNumber>>();
            SearchColumns = new Collection<string> { "npi", "organization", "fullname", "providercredential", "licensenumber", "phone", "fax", "email", "address" };
            DisplayColumnForCombobox = "npi";
        }
        public override IEnumerable<ReadOnlyGridVo> GetDataForGridWithEntity(IList<SolrNpiNumber> listData)
        {
            var finalResult = listData.Select(x => new NpiNumberGridVo
            {
                Npi = x.Npi,
                OrganizationName = x.Organization,
                Name = x.FullName,
                LicenseNumber = x.LicenseNumber,
                ProviderCredentialText = x.ProviderCredential,
                Phone = x.Phone.ApplyFormatPhone(),
                Fax = x.Fax.ApplyFormatPhone(),
                Email = x.Email,
                Address = x.Address,
                Id = x.Id.ParseToInt()
            });

            return finalResult;
        }

        public override IEnumerable<LookupItemVo> GetDataForComboBox(IList<SolrNpiNumber> listData)
        {
            var finalResult = listData.Select(x => new LookupItemVo
            {
                KeyId = x.Id.ParseToInt(),
                DisplayName = x.Npi,
            });

            return finalResult;
        }

        protected override List<SortOrder> BuildSortExpression(IQueryInfo queryInfo)
        {
            //if (queryInfo.Sort == null || queryInfo.Sort.Count == 0)
            //{
            //    queryInfo.Sort = new List<Sort> { new Sort { Field = "idNpiNumber", Dir = "desc" } };
            //}
            var result = new List<SortOrder>();
            //foreach (var sort in queryInfo.Sort)
            //{
            //    switch (sort.Field)
            //    {
            //        case "Name":
            //            sort.Field = "FullName";
            //            break;
            //        case "GenderName":
            //            sort.Field = "Gender";
            //            break;
            //    }
            //    if (sort.Dir == "desc")
            //    {
            //        var objAdd = new SortOrder(sort.Field.ToLower(), Order.DESC);
            //        result.Add(objAdd);
            //    }
            //    else
            //    {
            //        var objAdd = new SortOrder(sort.Field.ToLower(), Order.ASC);
            //        result.Add(objAdd);
            //    }
            //}
            var objAdd = new SortOrder("npinumberid", Order.DESC);
            result.Add(objAdd);
            return result;
        }

        public List<LookupItemVo> GetLookupTreatingPhysician(LookupQuery query)
        {
            if (_npiNumberSolrService == null)
            {
                return null;
            }
            var filterQuery = BuildLookupTreatingPhysicianCondition(query);
            try
            {
                var data = _npiNumberSolrService.Query(SolrQuery.All, new QueryOptions
                {
                    //OrderBy = new List<SortOrder>{new SortOrder("id", Order.ASC)} ,
                    Start = 0,
                    Rows = query.Take,
                    FilterQueries = filterQuery
                });
                var dataGrid = GetDataForComboBoxForTreatingPhysician(data);
                return dataGrid.ToList();
            }
            catch (SolrConnectionException ex)
            {
                return null;
            }
        }
        private static IEnumerable<LookupItemVo> GetDataForComboBoxForTreatingPhysician(IEnumerable<SolrNpiNumber> listData)
        {
            var finalResult = listData.Select(x => new LookupItemVo
            {
                KeyId = x.Id.ParseToInt(),
                DisplayName = x.FullName,
            });

            return finalResult;
        }
        private static List<ISolrQuery> BuildLookupTreatingPhysicianCondition(LookupQuery query)
        {
            var listFilterChild = new List<ISolrQuery>();
            if (!string.IsNullOrEmpty(query.Query))
            {
                var keyword = query.Query;
                var listKeySearch = keyword.Split(' ');
                var keySolr = "";
                var countKeySearch = listKeySearch.Count();
                for (var i = 0; i < countKeySearch; i++)
                {
                    var key = listKeySearch[i];
                    keySolr += "fullname:*" + key + "*";
                    if (i < (countKeySearch - 1))
                    {
                        keySolr += " AND ";
                    }
                }
                listFilterChild.Add(new SolrQuery(keySolr));
            }
            return listFilterChild;
        }
    }
}