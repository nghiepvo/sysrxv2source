﻿using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IZipRepository : IRepository<Zip>, IQueryableRepository<Zip>
    {
    }
}