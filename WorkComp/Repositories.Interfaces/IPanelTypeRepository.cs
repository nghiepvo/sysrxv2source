﻿using System;
using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IPanelTypeRepository : IRepository<PanelType>, IQueryableRepository<PanelType>
    {
        List<LookupItemVo> GetListPanelTypeStandard();
        List<PanelCode> GetListPanelCode(int panelTypeId);
        InfoWhenChangePanelTypeInReferral GetInfoWhenChangePanelTypeInReferral(int idPanelType);
        List<LookupItemVo> GetLookupForReferral(LookupQuery queryInfo, Func<PanelType, LookupItemVo> selector);
    }
}