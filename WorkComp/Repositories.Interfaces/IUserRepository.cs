﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>, IQueryableRepository<User>
    {
        User GetUserByUserNameAndPass(string username, string password);
        void Active(int id);
        dynamic GetListEmailUser(IQueryInfo queryInfo);
    }
}
