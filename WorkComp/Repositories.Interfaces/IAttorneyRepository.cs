﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IAttorneyRepository : IRepository<Attorney>, IQueryableRepository<Attorney>
    {
        InfoWhenChangeAttorneyInReferral GetInfoWhenChangeAttorneyInReferral(int idAttorney);
    }
}
