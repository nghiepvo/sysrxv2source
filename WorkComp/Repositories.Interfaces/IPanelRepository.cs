﻿using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IPanelRepository : IRepository<Panel>, IQueryableRepository<Panel>
    {
    }
}