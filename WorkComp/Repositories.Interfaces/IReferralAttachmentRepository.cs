﻿using System;
using System.Collections.Generic;
using Framework.DomainModel.Entities;

namespace Repositories.Interfaces
{
    public interface IReferralAttachmentRepository
    {
        IList<TResult> GetAttachments<TResult>(int referralId,Func<ReferralAttachment, TResult> selector);
        byte[] GetAttachmentByGuid(Guid rowGuid);
    }
}
