﻿using System;
using System.Collections.Generic;
using Framework.DomainModel.Entities;

namespace Repositories.Interfaces
{
    public interface IReferralEmailTemplateAttachmentRepository
    {
        IList<TResult> GetAttachments<TResult>(int referralEmailTemplateId, Func<ReferralEmailTemplateAttachment, TResult> selector);
        byte[] GetAttachmentByGuid(Guid rowGuid);
    }
}
