﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IIcdRepository : IRepository<Icd>,IQueryableRepository<Icd>
    {
        List<LookupItemVo> GetListIcdType();
        List<Icd> GetIcdsByReferral(int referralId);
    }
}