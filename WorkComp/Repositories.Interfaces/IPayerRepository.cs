﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IPayerRepository : IRepository<Payer>, IQueryableRepository<Payer>
    {
        LookupItemVo GetDefaulDataSource(int id);
    }
}
