﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IReferralSourceRepository : IRepository<ReferralSource>, IQueryableRepository<ReferralSource>
    {
        InfoWhenChangeReferralSourceInReferral GetInfoWhenChangeReferralSourceInReferral(int idReferral);
    }
}
