﻿using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IConfigurationRepository : IRepository<Configuration>, IQueryableRepository<Configuration>
    {
    }
}