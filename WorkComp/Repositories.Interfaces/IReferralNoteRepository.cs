﻿using System.Collections;
using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IReferralNoteRepository : IRepository<ReferralNote>, IQueryableRepository<ReferralNote>
    {
        IList<ReferralNote> GetReferralNoteWhenChangeReferral(int? referralId);
    }
}