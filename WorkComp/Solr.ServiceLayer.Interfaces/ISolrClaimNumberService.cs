﻿using Solr.DomainModel;

namespace Solr.ServiceLayer.Interfaces
{
    public interface ISolrClaimNumberService : ISolrMasterFileService<SolrClaimNumber>
    {
    }
}