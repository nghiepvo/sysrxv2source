﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using Framework.DomainModel.Entities;

namespace Repositories
{
    public class EntityFrameworkClaimantRepository : EntityFrameworkRepositoryBase<Claimant>, IClaimantRepository
    {
        public EntityFrameworkClaimantRepository()
        {
            SearchColumns.Add("FirstName");
            SearchColumns.Add("MiddleName");
            SearchColumns.Add("LastName");
            SearchColumns.Add("Email");
            SearchColumns.Add("Ssn");
            SearchColumns.Add("BestContactNumber");
            SearchColumns.Add("Address1");
            SearchColumns.Add("Address2");
            SearchColumns.Add("State");
            SearchColumns.Add("City");
            SearchColumns.Add("Zip");
            SearchColumns.Add("String.Concat(String.Concat(FirstName,\" \"),String.Concat(MiddleName,\" \"),LastName)");
            SearchColumns.Add("String.Concat(String.Concat(Address1,\",\",\" \"),String.Concat(State,\",\",\" \"),String.Concat(City,\",\",\" \"),Zip)");
            SearchColumns.Add("String.Concat(String.Concat(Address1,\"-\",Address2,\",\"),String.Concat(\" \",State,\",\",\" \"),String.Concat(City,\",\",\" \"),Zip)");
            DisplayColumnForCombobox = "FirstName";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in GetAll()
                               join s in WorkCompDb.Set<State>() on entity.StateId equals s.Id into states
                               from state in states.DefaultIfEmpty()
                               join c in WorkCompDb.Set<City>() on entity.CityId equals c.Id into cities
                               from city in cities.DefaultIfEmpty()
                               join z in WorkCompDb.Set<Zip>() on entity.ZipId equals z.Id into zips
                               from zip in zips.DefaultIfEmpty()
                               join cl in WorkCompDb.Set<ClaimantLanguage>() on entity.ClaimantLanguageId equals cl.Id into claimantLanguages
                               from claimantLanguage in claimantLanguages.DefaultIfEmpty()
                               select new { entity, state, zip, city, claimantLanguage })
                               .OrderBy(queryInfo.SortString)
                                .Select(o => new ClaimantGridVo
                                {
                                    Id = o.entity.Id,
                                    FirstName = o.entity.FirstName,
                                    MiddleName = o.entity.MiddleName,
                                    LastName = o.entity.LastName,
                                    Gender = o.entity.Gender,
                                    Birthday = o.entity.Birthday,
                                    Language = o.claimantLanguage.Name,
                                    Ssn = o.entity.Ssn,
                                    BestContactNumber = o.entity.BestContactNumber,
                                    Email = o.entity.Email,
                                    Address1 = o.entity.Address1,
                                    Address2 = o.entity.Address2,
                                    State = o.state.Name,
                                    City = o.city.Name,
                                    Zip = o.zip.Name
                                })
                                ;
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            bool flagAddress = false, flagName = false;

            var dir = "";
            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.FullAddress"))
            {
                sort.Field = "entity.Address1";
                dir = sort.Dir;
                flagAddress = true;
            }
            if (flagAddress)
            {
                queryInfo.Sort.Add(new Sort { Field = "entity.Address2", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(entity.State != null) ? entity.State.Name : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(entity.City != null) ? entity.City.Name : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(entity.Zip != null) ? entity.Zip.Name : string.Empty", Dir = dir });
            }

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Name"))
            {
                sort.Field = "entity.FirstName";
                dir = sort.Dir;
                flagName = true;
            }

            if (flagName)
            {
                queryInfo.Sort.Add(new Sort { Field = "entity.MiddleName", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "entity.LastName", Dir = dir });
            }
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("Language"))
                {
                    x.Field = "(entity.ClaimantLanguage != null) ? entity.ClaimantLanguage.Name : string.Empty";
                }
                else if (x.Field.Contains("GenderName"))
                {
                    x.Field = "entity.Gender";
                }
                else if (x.Field.Contains("Dob"))
                {
                    x.Field = "entity.Birthday";
                }
            });
        }

        protected override string BuildLookupCondition(LookupQuery query)
        {
            var where = new StringBuilder();
            @where.Append("(");
            var innerWhere = new List<string>();
            var queryDisplayName = String.Format("FirstName.Contains(\"{0}\") OR LastName.Contains(\"{0}\") OR MiddleName.Contains(\"{0}\")", query.Query);
            innerWhere.Add(queryDisplayName);
            @where.Append(String.Join(" OR ", innerWhere.ToArray()));
            @where.Append(")");
            return @where.ToString();
        }

        public InfoWhenChangeClaimantInReferral GetInfoWhenChangeClaimantInReferral(int idClaimant)
        {
            var query = from entity in GetAll()
                        join l in WorkCompDb.ClaimantLanguages on entity.ClaimantLanguageId equals l.Id into claimantLanguages
                        from claimantLanguage in claimantLanguages.DefaultIfEmpty()
                        join s in WorkCompDb.States on entity.StateId equals s.Id into states
                        from state in states.DefaultIfEmpty()
                        join c in WorkCompDb.Cities on entity.CityId equals c.Id into cities
                        from city in cities.DefaultIfEmpty()
                        join z in WorkCompDb.Zips on entity.ZipId equals z.Id into zips
                        from zip in zips.DefaultIfEmpty()
                        where entity.Id == idClaimant
                        select new InfoWhenChangeClaimantInReferral
                        {
                            ClaimantAddress1 = entity.Address1,
                            ClaimantAddress2 = entity.Address2,
                            CellPhone = entity.CellPhone,
                            ClaimantCity = city == null ? "" : city.Name,
                            HomePhone = entity.HomePhone,
                            ClaimantId = entity.Id,
                            ClaimantLanguage = claimantLanguage == null ? "" : claimantLanguage.Name,
                            ClaimantPayerPatientId = entity.PayerPatientId,
                            ClaimantSsn = entity.Ssn,
                            ClaimantState = state == null ? "" : state.Name,
                            ClaimantZip = zip == null ? "" : zip.Name,
                            Dob = entity.Birthday,
                            Gender = entity.Gender,
                            ExpirationDate = entity.ExpirationDate

                        };
            return query.FirstOrDefault();
        }
    }
}
