﻿using Framework.DomainModel.Entities;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkDocumentTypeRepository : EntityFrameworkRepositoryBase<DocumentType>, IDocumentTypeRepository
    {
       
    }
}