﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkUserRepository : EntityFrameworkRepositoryBase<User>, IUserRepository
    {
        public EntityFrameworkUserRepository()
            : base()
        {
            SearchColumns.Add("FirstName");
            SearchColumns.Add("LastName");
            SearchColumns.Add("MiddleName");
            SearchColumns.Add("UserName");
            SearchColumns.Add("Role");
            SearchColumns.Add("Email");
            SearchColumns.Add("Address");
            SearchColumns.Add("City");
            SearchColumns.Add("State");
            SearchColumns.Add("Zip");
            SearchColumns.Add("Phone");
            DisplayColumnForCombobox = "FirstName";

        }
        public User GetUserByUserNameAndPass(string username, string password)
        {
            return GetAll().SingleOrDefault(o => o.UserName == username && o.Password == password);
        }

        public void Active(int id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.IsActive = !entity.IsActive.GetValueOrDefault();
            }
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in GetAll() where entity.IsEnable == true
                               join r in WorkCompDb.Set<UserRole>().AsNoTracking() on entity.UserRoleId equals r.Id into roles
                               from role in roles.DefaultIfEmpty()
                               join c in WorkCompDb.Set<City>().AsNoTracking() on entity.CityId equals c.Id into cities
                               from city in cities.DefaultIfEmpty()
                               join st in WorkCompDb.Set<State>().AsNoTracking() on entity.StateId equals st.Id into states
                               from state in states.DefaultIfEmpty()
                               join z in WorkCompDb.Set<Zip>().AsNoTracking() on entity.ZipId equals z.Id into zips
                               from zip in zips.DefaultIfEmpty()
                               select new { entity, role,city,state,zip }).OrderBy(queryInfo.SortString).Select(s => new UserGridVo()
                               {
                                   Id = s.entity.Id,
                                   FirstName = s.entity.FirstName,
                                   LastName = s.entity.LastName,
                                   MiddleName = s.entity.MiddleName,
                                   UserName = s.entity.UserName,
                                   Role = s.role == null ? "" : s.role.Name,
                                   Email = s.entity.Email,
                                   Phone = s.entity.Phone,
                                   Address = s.entity.Address,
                                   State = s.state == null ? "" : s.state.Name,
                                   City = s.city == null ? "" : s.city.Name,
                                   Zip = s.zip == null ? "" : s.zip.Name,
                                   IsActive = s.entity.IsActive ?? false
                               });
            return queryResult;
        }

        protected override string BuildLookupCondition(LookupQuery query)
        {
            var where = new StringBuilder();

            @where.Append("(");
            var innerWhere = new List<string>();
            var queryDisplayName = String.Format("FirstName.Contains(\"{0}\") OR LastName.Contains(\"{0}\") OR MiddleName.Contains(\"{0}\")", query.Query);
            innerWhere.Add(queryDisplayName);
            @where.Append(String.Join(" OR ", innerWhere.ToArray()));
            @where.Append(")");

            if (query.HierachyItems != null)
            {
                foreach (var parentItem in query.HierachyItems.Where(parentItem => parentItem.Value != string.Empty
                    && parentItem.Value != "-1"
                    && parentItem.Value != "0"
                    && !parentItem.IgnoredFilter))
                {
                    var filterValue = parentItem.Value.Replace(",", string.Format(" OR {0} = ", parentItem.Name));
                    @where.Append(string.Format(" AND ( {0} = {1})", parentItem.Name, filterValue));
                }
            }
            return @where.ToString();
        }


        public dynamic GetListEmailUser(IQueryInfo queryInfo)
        {
            // Caculate for search string
            if (!string.IsNullOrEmpty(queryInfo.SearchString))
            {
                queryInfo.SearchString = queryInfo.SearchString.Replace(' ', '+');
                queryInfo.SearchString = Encoding.UTF8.GetString(Convert.FromBase64String(queryInfo.SearchString));
                queryInfo.ParseParameters(queryInfo.SearchString);
            }
            var cmd = WorkCompDb.Database.Connection.CreateCommand();
            if (queryInfo.ParentId.GetValueOrDefault() == 0)
            {
                cmd.CommandText = "[dbo].[udsGetListUserAdjusterEmail]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SearchEmail", string.IsNullOrEmpty(queryInfo.SearchTerms) ? null : queryInfo.SearchTerms));
                cmd.Parameters.Add(new SqlParameter("@StartIndex", queryInfo.Skip));
                cmd.Parameters.Add(new SqlParameter("@CountItem", queryInfo.Take));
                cmd.Parameters.Add(new SqlParameter("@RowCount", SqlDbType.Int)
                {
                    Direction = ParameterDirection.ReturnValue
                });
            }
            else
            {
                cmd.CommandText = "[dbo].[GetListEmailByReferralId]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ReferralId", queryInfo.ParentId.GetValueOrDefault()));
            }
            try
            {

                WorkCompDb.Database.Connection.Open();
                // Run the sproc  
                var reader = cmd.ExecuteReader();
                var table = new DataTable();
                table.Load(reader);
                var totalCount = 0;
                
                var data = table.AsEnumerable().Select(row => new EmailUserAdjusterVo()
                {
                    Id = int.Parse(row["RowNumber"].ToString()),
                    TypeName = row["TypeName"].ToString(),
                    Name = row["Name"].ToString(),
                    Email = row["Email"].ToString(),
                }).ToList();

                if (queryInfo.ParentId.GetValueOrDefault() == 0)
                {
                    int.TryParse(cmd.Parameters["@RowCount"].Value.ToString(), out totalCount);
                }
                else
                {
                    totalCount = data.Count;
                }

                var totalRecords = totalCount;
                return new { Data = data, TotalRowCount = totalRecords };
            }
            finally
            {
                WorkCompDb.Database.Connection.Close();
            }
        }
    }
}