﻿using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Linq;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkReferralSampleTestingTypeRepository : EntityFrameworkRepositoryBase<ReferralSampleTestingType>, IReferralSampleTestingTypeRepository
    {
        public List<string> GetListTestingTypeName(int referralId)
        {
            var query = (from entity in GetAll().Where(o => o.ReferralId == referralId && o.SampleTestingTypeId != null)
                join s in WorkCompDb.Set<SampleTestingType>() on entity.SampleTestingTypeId equals s.Id into
                    intoSampleTestingType
                from sampleTestingType in intoSampleTestingType.DefaultIfEmpty()
                select new {entity, sampleTestingType})
                .Select(o => o.sampleTestingType.Name);
            return query.ToList();
        }
    }
}