﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using System.Linq.Dynamic;
namespace Repositories
{
    public class EntityFrameworkTaskGroupRepository : EntityFrameworkRepositoryBase<TaskGroup>, ITaskGroupRepository
    {
        public EntityFrameworkTaskGroupRepository()
        {
            SearchColumns.Add("Name");
            DisplayColumnForCombobox = "Name";
        }
        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var finalResult = (from entity in GetAll()
                               select new { entity}).OrderBy(queryInfo.SortString).Select(x => new TaskGroupGridVo
                               {
                                   Name = x.entity.Name,
                                   Id = x.entity.Id,
                                   IsDefault = x.entity.IsDefault
                               });
            return finalResult;
        }

        public List<TaskTemplate> GetListTaskTemplate(int taskGroupId, int? referralId)
        {
            var data =  (from entity in WorkCompDb.TaskTemplates
                join tg in WorkCompDb.Set<TaskGroupTaskTemplate>() on entity.Id equals tg.TaskTemplateId into
                    taskGroupTaskTemplates
                from taskGroupTaskTemplate in taskGroupTaskTemplates
                where taskGroupTaskTemplate.TaskGroupId == taskGroupId
                select entity).Include(o=>o.AssignTo).ToList();
            if (referralId.GetValueOrDefault() == 0)
            {
                return data;
            }
            
            var objReferral = WorkCompDb.Set<Referral>().Include(o=>o.AssignTo).FirstOrDefault(o => o.Id == referralId);
            if (objReferral != null)
            {
                var copyData = data;
                for (var i = 0; i < copyData.Count; i++)
                {
                    if (copyData[i].AssignTo == null)
                    {
                        data[i].AssignTo = objReferral.AssignTo;
                    }
                }
            }
            return data;
            
            
        }
    }
}