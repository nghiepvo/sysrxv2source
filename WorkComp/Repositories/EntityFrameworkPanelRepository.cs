﻿using Framework.DomainModel.Entities;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkPanelRepository : EntityFrameworkRepositoryBase<Panel>, IPanelRepository
    {
        public EntityFrameworkPanelRepository()
            : base()
        {
            DisplayColumnForCombobox = "Name";
        }
    }
}