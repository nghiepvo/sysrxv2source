﻿using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkClaimNumberRepository : EntityFrameworkRepositoryBase<ClaimNumber>, IClaimNumberRepository
    {
        public EntityFrameworkClaimNumberRepository()
        {
            SearchColumns.Add("Name");
            SearchColumns.Add("Payer");
            SearchColumns.Add("Branch");
            SearchColumns.Add("State");
            SearchColumns.Add("SpecialInstructions");
            SearchColumns.Add("String.Concat(String.Concat(ClaimantFirstName,\" \"),String.Concat(ClaimantMiddleName,\" \"), ClaimantLastName)");
            SearchColumns.Add("String.Concat(String.Concat(ClaimantFirstName,\" \"), ClaimantLastName)");
            SearchColumns.Add("String.Concat(String.Concat(AdjusterFirstName,\" \"),String.Concat(AdjusterMiddleName,\" \"), AdjusterLastName)");
            SearchColumns.Add("String.Concat(String.Concat(AdjusterFirstName,\" \"), AdjusterLastName)");

            DisplayColumnForCombobox = "Name";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {

            var queryResult = (from entity in GetAll()
                               join a in WorkCompDb.Set<Adjuster>() on entity.AdjusterId equals a.Id into intoAdjuster 
                               from adjuster in intoAdjuster.DefaultIfEmpty()
                               join c in WorkCompDb.Set<Claimant>() on entity.ClaimantId equals c.Id into intoClaimant 
                               from claimant in intoClaimant.DefaultIfEmpty()
                               join p in WorkCompDb.Set<Payer>() on entity.PayerId equals p.Id into intoPayer 
                               from payer in intoPayer.DefaultIfEmpty()
                               join b in WorkCompDb.Set<Branch>() on entity.BranchId equals b.Id into intoBranch
                               from branch in intoBranch.DefaultIfEmpty()
                               join s in WorkCompDb.Set<State>() on entity.StateId equals s.Id into intoState 
                               from state in intoState.DefaultIfEmpty()
                               select new { entity, adjuster, claimant, payer, branch, state }).OrderBy(queryInfo.SortString)
                            .Select(o => new ClaimNumberGridVo
                {
                                Id = o.entity.Id,
                                ClaimantFirstName = o.claimant.FirstName,
                                ClaimantMiddleName = o.claimant.MiddleName,
                                ClaimantLastName = o.claimant.LastName,
                                AdjusterFirstName = o.adjuster.FirstName,
                                AdjusterMiddleName = o.adjuster.MiddleName,
                                AdjusterLastName = o.adjuster.LastName,
                                Payer = o.payer.Name,
                                Branch = o.branch.Name,
                                Name = o.entity.Name,
                                DoiDateTime = o.entity.Doi,
                                State = o.state.AbbreviationName,
                                
                                SpecialInstructions = o.entity.SpecialInstructions,
                                Status = o.entity.Status
                            });
            
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("entity.State"))
                {
                    x.Field = "(state != null) ? state.AbbreviationName : string.Empty";
                }

                if (x.Field.Contains("entity.Payer"))
                {
                    x.Field = "(payer != null) ? payer.Name : string.Empty";
                }
                if (x.Field.Contains("entity.Branch"))
                {
                    x.Field = "(branch != null) ? branch.Name : string.Empty";

                }
            });
            bool flagClaimant = false, flagAdjuster = false;

            var dir = "";
            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;

            

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Claimant"))
            {
                sort.Field = "claimant.FirstName";
                dir = sort.Dir;
                flagClaimant = true;
            }

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Adjuster"))
            {
                sort.Field = "adjuster.FirstName";
                dir = sort.Dir;
                flagAdjuster = true;
            }

            if (flagAdjuster)
            {
                queryInfo.Sort.Add(new Sort { Field = "adjuster.MiddleName", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "adjuster.LastName", Dir = dir });
            }

            if (!flagClaimant) return;
            queryInfo.Sort.Add(new Sort { Field = "claimant.MiddleName", Dir = dir });
            queryInfo.Sort.Add(new Sort { Field = "claimant.LastName", Dir = dir });
        }

        
        public InfoWhenChangeClaimNumberInReferral GetInfoWhenChangeClaimNumberInReferral(int idClaimNumber)
        {
            var query = from entity in GetAll()
                        join s in WorkCompDb.States on entity.StateId equals s.Id into states
                        from state in states.DefaultIfEmpty()
                        where entity.Id==idClaimNumber
                        select new InfoWhenChangeClaimNumberInReferral
                        {
                            ClaimNumberId = entity.Id,
                            ClaimNumberJurisdiction = state.AbbreviationName,
                            ClaimNumberInstruction = entity.SpecialInstructions,
                            Doi = entity.Doi,
                            StatusId = entity.Status
                        };
            return query.FirstOrDefault();
        }

    }
}