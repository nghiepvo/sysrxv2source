﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkUserRoleRepository : EntityFrameworkRepositoryBase<UserRole>, IUserRoleRepository
    {
        public EntityFrameworkUserRoleRepository()
            : base()
        {
            SearchColumns.Add("Name");
            DisplayColumnForCombobox = "Name"; 
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in GetAll().AsNoTracking() where entity.IsEnable
                              select new {entity}).OrderBy(queryInfo.SortString).Select(s => new RoleGridVo()
            {
                Id = s.entity.Id,
                Name = s.entity.Name
            });
            return queryResult;
        }

        

        public override List<LookupItemVo> GetLookup(LookupQuery query, Func<UserRole, LookupItemVo> selector)
        {
            var queryResult = GetAll().AsNoTracking().Where(o => o.IsEnable).Select(selector);
            return queryResult.ToList();
        }

        public dynamic GetRoleFunction(int idRole)
        {
            var objListResult = new List<UserRoleFunctionGridVo>();
            var objListDocumentType =
                WorkCompDb.Set<DocumentType>().AsNoTracking().OrderBy(o => o.Order).ThenByDescending(o=>o.Id).ToList();
            var objListUserRoleFunction = new List<UserRoleFunction>();
            if (idRole > 0)
            {
                objListUserRoleFunction = WorkCompDb.Set<UserRoleFunction>().AsNoTracking().Where(o=>o.UserRoleId==idRole).ToList();
            }
            foreach (var documentType in objListDocumentType)
            {
                var objAdd = new UserRoleFunctionGridVo {Id = documentType.Id, Name = documentType.Title};
                // Check for isView
                var isView =
                    objListUserRoleFunction.Any(
                        o => o.SecurityOperationId == (int) OperationAction.View && o.DocumentTypeId == documentType.Id);
                objAdd.IsView = isView;
                // Check for isDelete
                var isDelete =
                    objListUserRoleFunction.Any(
                        o => o.SecurityOperationId == (int)OperationAction.Delete && o.DocumentTypeId == documentType.Id);
                objAdd.IsDelete = isDelete;
                // Check for isUpdate
                var isUpdate =
                    objListUserRoleFunction.Any(
                        o => o.SecurityOperationId == (int)OperationAction.Update && o.DocumentTypeId == documentType.Id);
                objAdd.IsUpdate = isUpdate;
                // Check for isAdd
                var isAdd =
                    objListUserRoleFunction.Any(
                        o => o.SecurityOperationId == (int)OperationAction.Add && o.DocumentTypeId == documentType.Id);
                objAdd.IsInsert = isAdd;
                objListResult.Add(objAdd);
            }
            return new { Data = objListResult, TotalRowCount = objListResult.Count };
        }

        public List<DocumentType> GetAllDocumentType()
        {
            return WorkCompDb.Set<DocumentType>().ToList();
        }
    }
}