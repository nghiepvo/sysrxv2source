﻿using Framework.DomainModel.Entities;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkSampleTestingTypeRepository : EntityFrameworkRepositoryBase<SampleTestingType>, ISampleTestingTypeRepository
    {
        
    }
}