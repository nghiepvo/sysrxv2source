﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkDiaryRepository : EntityFrameworkRepositoryBase<Diary>, IDiaryRepository
    {
        public override dynamic GetDataForGridMasterfile(IQueryInfo queryInfo)
        {
            List<DiaryrGridVo> result;
            // Create a SQL command to execute the sproc 
            var cmd = WorkCompDb.Database.Connection.CreateCommand();
            cmd.CommandText = "[dbo].[GetDiaryForReferral]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ReferralId", queryInfo.ParentId));
            cmd.Parameters.Add(new SqlParameter("@StartIndex", queryInfo.Skip));
            cmd.Parameters.Add(new SqlParameter("@CountItem", queryInfo.Take));
            cmd.Parameters.Add(new SqlParameter("@TotalItem", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output 
            });
            try
            {

                WorkCompDb.Database.Connection.Open();
                // Run the sproc  
                var reader = cmd.ExecuteReader();
                var table = new DataTable();
                table.Load(reader);
                var totalCount = 0;
                int.TryParse(cmd.Parameters["@TotalItem"].Value.ToString(), out totalCount);
                queryInfo.TotalRecords = totalCount;
                result = table.AsEnumerable().Select(row => new DiaryrGridVo
                {
                    CommentResult = row["CommentResult"].ToString(),
                    CreatedBy = row["CreatedBy"].ToString(),
                    Heading = row["Heading"].ToString(),
                    Reason = row["Reason"].ToString(),
                    CreatedDate = DateTime.Parse(row["CreatedDate"].ToString())
                }).ToList();


            }
            finally
            {
                WorkCompDb.Database.Connection.Close();
            }
            return new { Data = result, TotalRowCount = queryInfo.TotalRecords };
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            if (queryInfo.Sort == null || (queryInfo.Sort != null && queryInfo.Sort.Count == 0))
            {
                queryInfo.Sort = new List<Sort> { new Sort { Field = "CreatedOn", Dir = "desc" } };
            }
        }

        public IList<Diary> GetDiaryWhenChangeReferral(int? referralId)
        {
            return WorkCompDb.Diaries.Where(p => p.ReferralId == referralId).ToList();
        }

    }
}