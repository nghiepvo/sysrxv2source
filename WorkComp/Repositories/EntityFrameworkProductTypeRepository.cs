﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkProductTypeRepository : EntityFrameworkRepositoryBase<ProductType>, IProductTypeRepository
    {
        public EntityFrameworkProductTypeRepository()
            : base()
        {
            //Includes.Add("States");
            SearchColumns.Add("Name");
            SearchColumns.Add("Payer");
            DisplayColumnForCombobox = "Name";
        }
        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var finalResult = (from entity in GetAll()
                               join p in WorkCompDb.Set<Payer>() on entity.PayerId equals p.Id into payers
                               from payer in payers.DefaultIfEmpty()
                               select new { entity, payer }).OrderBy(queryInfo.SortString).Select(x => new ProductTypeGridVo()
                                {
                                    Name = x.entity.Name,
                                    Id = x.entity.Id,
                                    Payer = x.payer == null ? "" : x.payer.Name
                                });
            return finalResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("Payer"))
                {
                    x.Field = "(entity.Payer != null) ? entity.Payer.Name : string.Empty";
                }
            });
        }

        /// <summary>
        /// Lookup for product type, it depend on payer, if there is any product type for payer => only load list product type of payer.
        /// But if no record for payer or payer is null => only load basic product type
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        protected override string BuildLookupCondition(LookupQuery query)
        {
            var where = new StringBuilder();

            @where.Append("(");
            var innerWhere = new List<string>();
            var queryDisplayName = String.Format("{0}.Contains(\"{1}\") ", DisplayColumnForCombobox, query.Query);
            innerWhere.Add(queryDisplayName);
            @where.Append(String.Join(" OR ", innerWhere.ToArray()));
            @where.Append(")");
            if (query.HierachyItems != null)
            {

                foreach (var parentItem in query.HierachyItems.Where(parentItem => parentItem.Value != string.Empty
                    && parentItem.Value != "-1"
                    && parentItem.Value != "0"
                    && !parentItem.IgnoredFilter))
                {
                    var filterValue = parentItem.Value.Replace(",", string.Format(" OR {0} = ", parentItem.Name));
                    @where.Append(string.Format(" AND ( {0} = {1})", parentItem.Name, filterValue));
                }
            }
            if (query.HierachyItems == null)
            {
                @where.Append(" AND ( PayerId = NULL)");
            }
            else
            {
                var payerCondition = query.HierachyItems.FirstOrDefault(o => o.Name == "PayerId");
                if (payerCondition == null)
                {
                    @where.Append(" AND ( PayerId = NULL)");
                }
                else
                {
                    @where.Append(" OR ( PayerId = NULL)");
                }
            }
            return @where.ToString();
        }

        public override List<LookupItemVo> GetLookup(LookupQuery query, Func<ProductType, LookupItemVo> selector)
        {
            var lookupWhere = BuildLookupCondition(query);

            var lookupList = GetAll().AsNoTracking().Where(lookupWhere);
            var currentRecord = GetAll().AsNoTracking().Where(x => x.Id == query.Id);
            if (!query.IncludeCurrentRecord && currentRecord.SingleOrDefault() != null) 
            {
                return currentRecord.Select(selector).ToList();
            }

            if (!string.IsNullOrEmpty(query.Query) || !query.IncludeCurrentRecord)
            {
                currentRecord = Enumerable.Empty<ProductType>().AsQueryable();
            }
            var listData = lookupList
                        .Union(currentRecord)
                        .OrderBy(DisplayColumnForCombobox).ToList();
            // Lookup for product type, it depend on payer, if there is any product type for payer => only load list product type of payer.
            // But if no record for payer or payer is null => only load basic product type
            if (listData.Any(o => o.PayerId != null))
            {
                listData = listData.Where(o => o.PayerId != null).ToList();
            }
            var lookupAnonymous = listData.Skip(0)
                .Take(query.Take)
                .Select(selector);
            return lookupAnonymous.ToList();
        }
    }
}