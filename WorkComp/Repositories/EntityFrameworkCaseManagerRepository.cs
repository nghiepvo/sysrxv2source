﻿using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkCaseManagerRepository : EntityFrameworkRepositoryBase<CaseManager>, ICaseManagerRepository
    {
        public EntityFrameworkCaseManagerRepository()
        {
            SearchColumns.Add("Name");
            SearchColumns.Add("Agency");
            SearchColumns.Add("ClaimNumber");
            SearchColumns.Add("Email");
            SearchColumns.Add("Phone");
            SearchColumns.Add("Fax");
            SearchColumns.Add("Address");
            SearchColumns.Add("State");
            SearchColumns.Add("City");
            SearchColumns.Add("Zip");
            SearchColumns.Add("String.Concat(String.Concat(Address,\",\",\" \"),String.Concat(State,\",\",\" \"),String.Concat(City,\",\",\" \"),Zip)");
            DisplayColumnForCombobox = "Name";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in GetAll()
                               join s in WorkCompDb.Set<State>() on entity.StateId equals s.Id into states
                               from state in states.DefaultIfEmpty()
                               join c in WorkCompDb.Set<City>() on entity.CityId equals c.Id into cities
                               from city in cities.DefaultIfEmpty()
                               join z in WorkCompDb.Set<Zip>() on entity.ZipId equals z.Id into zips
                               from zip in zips.DefaultIfEmpty()
                               join cl in WorkCompDb.Set<ClaimNumber>() on entity.ClaimNumberId equals cl.Id into claimNumbers
                               from claimNumber in claimNumbers.DefaultIfEmpty()
                               select new { entity, state, zip, city, claimNumber })
                               .OrderBy(queryInfo.SortString)
                                .Select(o => new CaseManagerGridVo
                                {
                                    Id = o.entity.Id,
                                    Name = o.entity.Name,
                                    Agency = o.entity.Agency,
                                    ClaimNumber = o.claimNumber != null ? o.claimNumber.Name : string.Empty,
                                    Email = o.entity.Email,
                                    Phone = o.entity.Phone,
                                    Fax = o.entity.Fax,
                                    Address = o.entity.Address,
                                    State = o.state == null ? "" : o.state.Name,
                                    City = o.city == null ? "" : o.city.Name,
                                    Zip = o.zip == null ? "" : o.zip.Name,
                                });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            var flagAddress = false;

            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("entity.ClaimNumber"))
                {
                    x.Field = "(claimNumber != null) ? claimNumber.Name : string.Empty";
                }
            });

            var dir = "";
            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.FullAddress"))
            {
                sort.Field = "entity.Address";
                dir = sort.Dir;
                flagAddress = true;
            }
            if (flagAddress)
            {
                queryInfo.Sort.Add(new Sort { Field = "(state != null) ? state.Name : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(city != null) ? city.Name : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(zip != null) ? zip.Name : string.Empty", Dir = dir });
            }
        }

        public InfoWhenChangeCaseManagerInReferral GetInfoWhenChangeCaseManagerInReferral(int idCaseManager)
        {
            var query = from entity in GetAll()
                        join s in WorkCompDb.States on entity.StateId equals s.Id into states
                        from state in states.DefaultIfEmpty()
                        join c in WorkCompDb.Cities on entity.CityId equals c.Id into cities
                        from city in cities.DefaultIfEmpty()
                        join z in WorkCompDb.Zips on entity.ZipId equals z.Id into zips
                        from zip in zips.DefaultIfEmpty()
                        where entity.Id == idCaseManager
                        select new InfoWhenChangeCaseManagerInReferral
                        {
                            CaseManagerAddress = entity.Address,
                            CaseManagerAgency = entity.Agency,
                            CaseManagerCity = city == null ? "" : city.Name,
                            CaseManagerEmail = entity.Email,
                            CaseManagerId = entity.Id,
                            CaseManagerInstruction = entity.SpecialInstructions,
                            CaseManagerState = state == null ? "" : state.Name,
                            CaseManagerZip = zip == null ? "" : zip.Name,
                            Fax = entity.Fax,
                            Phone = entity.Phone
                        };
            return query.FirstOrDefault();
        }
    }
}