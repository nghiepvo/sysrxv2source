﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using Framework.DomainModel;

namespace Repositories.Utility
{
    public static class DbContextExtension
    {

        /// <summary>
        /// Include all object in entity. Using Super class patern
        /// </summary>
        /// <typeparam name="T">Entity of Entity Framework</typeparam>
        /// <param name="dbSet"></param>
        /// <returns>Query</returns>
        public static DbQuery<T> GetIncludeCollections<T>(this DbSet<T> dbSet) where T : Entity
        {
            var entity = typeof(T);
            var props = entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            if (entity.BaseType == null)
            {
                throw new Exception("Not inheritance Entity.(Super class patern)");
            }
            var propList = PropListWithSuperClassPatern(entity.BaseType.Name, props).Where(o => o.Contains("System.Collections.Generic")).ToList();
            return QueryBase(dbSet, propList);
        }

        /// <summary>
        /// Include all object in entity
        /// </summary>
        /// <typeparam name="T">Entity of Entity Framework</typeparam>
        /// <param name="dbSet"></param>
        /// <param name="namespaceName">Referrence namespace. ex:Framework.DomainModel.Entities </param>
        /// <returns>Query</returns>
        public static DbQuery<T> GetIncludeCollections<T>(this DbSet<T> dbSet, string namespaceName) where T : Entity
        {
            var entity = typeof(T);
            var props = entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            if (string.IsNullOrEmpty(namespaceName))
            {
                throw new Exception("namespaceName is required.");
            }
            var propList = PropListWithNamespace(namespaceName, props).Where(o => o.Contains("System.Collections.Generic")).ToList();
            return QueryBase(dbSet, propList);
        }


       

        /// <summary>
        /// Include all object in entity. Using Super class patern
        /// </summary>
        /// <typeparam name="T">Entity of Entity Framework</typeparam>
        /// <param name="dbSet"></param>
        /// <returns>Query</returns>
        public static DbQuery<T> GetIncludeAll<T>(this DbSet<T> dbSet) where T : Entity
        {
            var entity = typeof(T);
            var props = entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            if (entity.BaseType == null)
            {
                throw new Exception("Not inheritance Entity.(Super class patern)");
            }
            var propList = PropListWithSuperClassPatern(entity.BaseType.Name, props);
            return QueryBase(dbSet, propList);
        }

        /// <summary>
        /// Include all object in entity
        /// </summary>
        /// <typeparam name="T">Entity of Entity Framework</typeparam>
        /// <param name="dbSet"></param>
        /// <param name="namespaceName">Referrence namespace. ex:Framework.DomainModel.Entities </param>
        /// <returns>Query</returns>
        public static DbQuery<T> GetIncludeAll<T>(this DbSet<T> dbSet, string namespaceName) where T : Entity
        {
            var entity = typeof(T);
            var props = entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            if (string.IsNullOrEmpty(namespaceName))
            {
                throw  new Exception("namespaceName is required.");
            }
            var propList =  PropListWithNamespace(namespaceName, props);
            return QueryBase(dbSet, propList);
        }

        #region Behavior for Include all
        private static DbQuery<T> QueryBase<T>(this DbQuery<T> dbSet, IReadOnlyList<string> props) where T : Entity
        {
            DbQuery<T> query = null;
            for (var i = 0; i < props.Count; i++)
            {
                if (i == 0)
                {
                    query = dbSet.Include(props[i]);
                }
                else
                {
                    if (query == null)
                        throw new Exception("Preview Code. (tell for Nghiep.Vo)");
                    query = query.Include(props[i]);
                }
            }
            return query ?? dbSet;
        }

        private static List<string> PropListWithNamespace(string namespaceName, IEnumerable<PropertyInfo> props)
        {
            var propList = props.Where(o =>
                (o.PropertyType.FullName.Contains("System.Collections.Generic") && o.GetMethod.IsVirtual) ||
                (o.PropertyType.FullName.Contains(namespaceName) && o.GetMethod.IsVirtual))
                .Select(o => o.Name)
                .ToList();
            return propList;
        }

        private static List<string> PropListWithSuperClassPatern(string entityBaseName, IEnumerable<PropertyInfo> props)
        {
            var propList = props.Where(o =>
                (o.PropertyType.FullName.Contains("System.Collections.Generic") && o.GetMethod.IsVirtual) ||
                (o.PropertyType.BaseType != null && o.PropertyType.BaseType.Name.Contains(entityBaseName) &&
                 o.GetMethod.IsVirtual))
                .Select(o => o.Name)
                .ToList();
            return propList;
        }

        #endregion

    }
}
