﻿using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using System.Linq.Dynamic;

namespace Repositories
{
    public class EntityFrameworkCityRepository : EntityFrameworkRepositoryBase<City>, ICityRepository
    {
        public EntityFrameworkCityRepository()
            : base()
        {
            //Includes.Add("States");
            SearchColumns.Add("Name");
            SearchColumns.Add("State");
            DisplayColumnForCombobox = "Name";
        }
        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var finalResult = (from entity in GetAll()
                               join st in WorkCompDb.Set<State>() on entity.StateId equals st.Id into states
                               from state in states.DefaultIfEmpty()
                               select new { entity, state }).OrderBy(queryInfo.SortString).Select(x => new CityGridVo()
                               {
                                   Name = x.entity.Name,
                                   Id = x.entity.Id,
                                   State = x.state == null ? "" : x.state.Name
                               });
            return finalResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("State"))
                {
                    x.Field = "(entity.State != null) ? entity.State.Name : string.Empty";
                }
            });
        }
    }
}