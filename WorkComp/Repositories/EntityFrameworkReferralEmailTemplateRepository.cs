﻿using System.Linq;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkReferralEmailTemplateRepository : EntityFrameworkRepositoryBase<ReferralEmailTemplate>, IReferralEmailTemplateRepository
    {
        public ReferralEmailTemplate GetIncludeAttachment(int referralId, int emailTemplateId)
        {
            return
                WorkCompDb.Set<ReferralEmailTemplate>()
                    .Include("ReferralEmailTemplateAttachments")
                    .FirstOrDefault(o => o.ReferralId == referralId && o.EmailTemplateId == emailTemplateId);
        }
    }
}