﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ReportValueObject;
using Framework.Utility;
using Repositories.Interfaces;

namespace Repositories
{
    public class SysRxReportRepository : ISysRxReportRepository
    {
        private readonly WorkCompDbContext _workCompDb;
        public SysRxReportRepository()
        {
              _workCompDb =new WorkCompDbContext();
        }

        public IList<UtilizationByInjuredWorkerVo> GetDataUtilizationByInjuredWorkerReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return GetDataBase<UtilizationByInjuredWorkerVo>("[udsReportUtilizationByInjuredWorker]", queryInfo, ref totalRow);
        }

        public IList<TestResultsDetailVo> GetDataTestResultdetailReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return GetDataBase<TestResultsDetailVo>("[udsReportTestResultdetail]", queryInfo, ref totalRow);
        }

        private IList<T> GetDataBase<T>(string storeName, SysRxReportQueryInfo queryInfo, ref int totalRow) where T : new()
        {
            if (queryInfo.DateFrom < new DateTime(1753, 1, 1))
                queryInfo.DateFrom = null;
            if (queryInfo.DateTo < new DateTime(1753, 1, 1))
                queryInfo.DateTo = null;

            var data =
                _workCompDb.Database.QueryStore<T>
                    (
                        storeName, ref totalRow,
                        CommandType.StoredProcedure,
                        new[]
                        {
                            new SqlParameter("@PayerId", queryInfo.PayerId),
                            new SqlParameter("@DateFrom", queryInfo.DateFrom), 
                            new SqlParameter("@DateTo", queryInfo.DateTo), 
                            new SqlParameter("@StartIndex", queryInfo.CurrentPage > 1? (queryInfo.CurrentPage - 1) * queryInfo.PageSize: queryInfo.CurrentPage), 
                            new SqlParameter("@CountItem", queryInfo.PageSize), 
                        }
                    );
            return data ?? new List<T>();
        }

        public IList<CancellationDetailVo> GetCancellationDetailtReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return GetDataBase<CancellationDetailVo>("[udsReportCancellationDetail]", queryInfo, ref totalRow);
        }

        public IList<TestResultsSummaryVo> GetTestResultsSummaryReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return GetDataBase<TestResultsSummaryVo>("[udsReportTestResultsSummary]", queryInfo, ref totalRow);
        }

        public IList<MGMTStatusSummaryReportVo> GetMGMTStatusSummaryReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return GetDataBase<MGMTStatusSummaryReportVo>("[udsReportMGMTStatusSummary]", queryInfo, ref totalRow);
        }

        public IList<MGMTUserProductivityVo> GetMGMTUserProductivityReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return GetDataBase<MGMTUserProductivityVo>("[udsReportMGMTUserProductivity]", queryInfo, ref totalRow);
        }
    }
}