﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkReferralSourceRepository : EntityFrameworkRepositoryBase<ReferralSource>, IReferralSourceRepository
    {
        public EntityFrameworkReferralSourceRepository()
        {
            SearchColumns.Add("FirstName");
            SearchColumns.Add("MiddleName");
            SearchColumns.Add("LastName");
            SearchColumns.Add("Company");
            SearchColumns.Add("Phone");
            SearchColumns.Add("Email");
            SearchColumns.Add("Type");
            SearchColumns.Add("Address");
            SearchColumns.Add("State");
            SearchColumns.Add("City");
            SearchColumns.Add("Zip");
            SearchColumns.Add("String.Concat(String.Concat(FirstName,\" \"),String.Concat(MiddleName,\" \"),LastName)");
            DisplayColumnForCombobox = "FirstName";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in GetAll()
                               join city in WorkCompDb.Set<City>()
                                   on entity.CityId equals city.Id into jcity
                               from subcity in jcity.DefaultIfEmpty()
                               join state in WorkCompDb.Set<State>()
                                   on entity.StateId equals state.Id into jstate
                               from substate in jstate.DefaultIfEmpty()
                               join zip in WorkCompDb.Set<Zip>()
                                   on entity.ZipId equals zip.Id into jzip
                               from subzip in jzip.DefaultIfEmpty()
                               join referralType in WorkCompDb.Set<ReferralType>()
                                              on entity.ReferralTypeId equals referralType.Id into jReferralType
                               from subRefrallType in jReferralType.DefaultIfEmpty()
                               select new { entity, subcity, substate, subzip, subRefrallType })
                .OrderBy(queryInfo.SortString)
                .Select(s => new ReferralSourceGridVo
                {
                    Id = s.entity.Id,
                    FirstName = s.entity.FirstName,
                    LastName = s.entity.LastName,
                    MiddleName = s.entity.MiddleName,
                    Type = s.subRefrallType == null ? "" : s.subRefrallType.Name,
                    Company = s.entity.Company,
                    Phone = s.entity.Phone,
                    Email = s.entity.Email,
                    Address = s.entity.Address,
                    AuthorizationFrom = s.entity.AuthorizationFrom,
                    AuthorizationTo = s.entity.AuthorizationTo,
                    IsAuthorization = s.entity.IsAuthorization,
                    State = s.substate == null ? "" : s.substate.Name,
                    City = s.subcity == null ? "" : s.subcity.Name,
                    Zip = s.subzip == null ? "" : s.subzip.Name
                });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            var dir = "";
            bool flagName = false;
            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;
            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Name"))
            {
                sort.Field = "entity.FirstName";
                dir = sort.Dir;
                flagName = true;
            }

            if (flagName)
            {
                queryInfo.Sort.Add(new Sort { Field = "entity.MiddleName", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "entity.LastName", Dir = dir });
            }
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("Type"))
                {
                    x.Field = "(entity.ReferralType != null) ? entity.ReferralType.Name : string.Empty";
                }
                else if (x.Field.Contains("State"))
                {
                    x.Field = "(entity.State != null) ? entity.State.Name : string.Empty";
                }
                else if (x.Field.Contains("City"))
                {
                    x.Field = "(entity.City != null) ? entity.City.Name : string.Empty";
                }
                else if (x.Field.Contains("Zip"))
                {
                    x.Field = "(entity.Zip != null) ? entity.Zip.Name : string.Empty";
                }
            });
        }

        protected override string BuildLookupCondition(LookupQuery query)
        {
            var where = new StringBuilder();

            @where.Append("(");
            var innerWhere = new List<string>();
            var queryDisplayName = String.Format("FirstName.Contains(\"{0}\") OR LastName.Contains(\"{0}\") OR MiddleName.Contains(\"{0}\")", query.Query);
            innerWhere.Add(queryDisplayName);
            @where.Append(String.Join(" OR ", innerWhere.ToArray()));
            @where.Append(")");
            return @where.ToString();
        }

        public InfoWhenChangeReferralSourceInReferral GetInfoWhenChangeReferralSourceInReferral(int idReferral)
        {
            var query = from entity in GetAll()
                join t in WorkCompDb.ReferralTypes on entity.ReferralTypeId equals t.Id into referralTypes
                from referralType in referralTypes.DefaultIfEmpty()
                join s in WorkCompDb.States on entity.StateId equals s.Id into states
                from state in states.DefaultIfEmpty()
                join c in WorkCompDb.Cities on entity.CityId equals c.Id into cities
                from city in cities.DefaultIfEmpty()
                join z in WorkCompDb.Zips on entity.ZipId equals z.Id into zips
                from zip in zips.DefaultIfEmpty()
                where entity.Id==idReferral
                select new InfoWhenChangeReferralSourceInReferral
                {
                    ReferralSourceId = entity.Id,
                    ReferralAddress = entity.Address,
                    ReferralCity = city == null ? "" : city.Name,
                    ReferralState = state == null ? "" : state.Name,
                    ReferralZip = zip == null ? "" : zip.Name,
                    IsAuthorization = entity.IsAuthorization,
                    ReferralClaimDateFrom = entity.AuthorizationFrom,
                    ReferralClaimDateTo = entity.AuthorizationTo,
                    ReferralCompany = entity.Company,
                    ReferralEmail = entity.Email,
                    ReferralExtention = entity.Extension,
                    Fax = entity.Fax,
                    Phone = entity.Phone,
                    ReferralType = referralType == null ? "" : referralType.Name,
                    ReferralSourceName=entity.FirstName+" "+entity.MiddleName+" "+entity.LastName
                };
            return query.FirstOrDefault();
        }
    }
}