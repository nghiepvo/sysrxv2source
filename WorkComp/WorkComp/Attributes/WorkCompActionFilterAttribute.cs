﻿using System.Web.Mvc;
using Framework.Exceptions;

namespace WorkComp.Attributes
{
    public class WorkCompActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
        }

        public override void OnActionExecuted(ActionExecutedContext actionExecutedContext)
        {
            var modelState = ((Controller)actionExecutedContext.Controller).ModelState;
            if (actionExecutedContext.Exception != null)
            {
                if (actionExecutedContext.Exception is BusinessRuleException)
                {
                    var ex = actionExecutedContext.Exception as BusinessRuleException;
                    foreach (var error in ex.FailedRules)
                    {
                        //TO work with multiple field names for one erorr
                        if (error.ValidationResults == null || error.ValidationResults.Count == 0)
                        {
                            modelState.AddModelError((error.PropertyNames != null && error.PropertyNames.Length > 0) ? error.PropertyNames[0] : string.Empty, error.Message);
                        }
                        else
                        {
                            foreach (var dataError in error.ValidationResults)
                            {
                                modelState.AddModelError(dataError.MemberNames.ToString(), dataError.ErrorMessage);
                            }
                        }
                    }
                }
            }
        }
    }
}