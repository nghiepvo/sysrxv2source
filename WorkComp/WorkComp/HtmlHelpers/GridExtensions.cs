﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Framework.DataAnnotations;
using WorkComp.Models;

namespace WorkComp.HtmlHelpers
{
    public static class GridExtensions
    {
        public static MvcHtmlString GridColumns(this HtmlHelper htmlHelper, GridViewModel viewModel)
        {
            var gridColumns = new StringBuilder();

            var schemas = new List<string>();
            var columns = new List<string>();
            gridColumns.Append("var kendoCultureFormat = kendo.culture().calendar.patterns.d; \n");

            gridColumns.Append("var schemaFields = { \n");

            schemas.Add("Id: { editable: false }");
            schemas.Add("IsChecked: { editable: false }");
            if (viewModel.CheckIsReadOnlyRow)
            {
                schemas.Add("IsReadOnly: { editable: false }");
            }
            // Add checkbox column
            if (viewModel.UseCheckBoxColumn)
            {
                var chkColumnString = "{field: \"IsChecked\", title: \"\",mandatory:true, attributes:{style:'text-align:Left;'}, width: \"30px\",sortable:false, template:\"<input type='checkbox' class='check_row checkbox' onClick='checkRowInGrid(this,${Id});'/>\", headerTemplate:\"<input type='checkbox' class='check_all_row checkbox' id='chkAll-" + viewModel.GridId + "' onClick='checkAllRowInGrid(this);'/>\"}";
                columns.Add(chkColumnString);
            }
            foreach (var column in viewModel.ViewColumns)
            {
                #region Custom Template
                if (!string.IsNullOrEmpty(column.CustomTemplate))
                {
                    var columnWidthStr = column.ColumnWidth == 0 ? "" : string.Format(" width: {0}", column.ColumnWidth);
                    var customTemplateColumnString = "{field: \"" + column.Name + "\", title: \"" + column.Text + "\", mandatory: true, hidden: " + column.HideColumn.ToString().ToLower() + ", " + columnWidthStr + " , attributes:{style:'text-align:left;'},sortable:false, template: kendo.template($(\"#" + column.CustomTemplate + "\").html())}";
                    columns.Add(customTemplateColumnString);
                    continue;
                }
                #endregion
                //schemas.Add(column.Name + ": { editable: false }");
                var dataType = "";
                var formatString = string.IsNullOrEmpty(column.ColumnFormat)
                                         ? string.Empty
                                         : "format: '{" + column.ColumnFormat + "}', ";
                if (!string.IsNullOrEmpty(column.ColumnFormat)
                    && (column.ColumnFormat.Contains("yyyy") || column.ColumnFormat.ToLower().Contains("date")))
                {
                    dataType = ", type:'date'";
                    // To format kendo grid date column base on culture can use format
                    // or template: "#=kendo.format('{0:d}', ValidFrom)#"
                    formatString = "format: '{0:' + kendoCultureFormat + '}', ";

                }
                var isEncode = "";
                if (column.IsHtmlEncode)
                {
                    isEncode = ",encoded: false";
                }

                var isBool = "";
                if (column.IsBoolValue)
                {
                    isBool = string.Format(
                        ", template : \"<input name='{0}' type='checkbox' data-bind='checked: {0}' #= {0} ? checked='checked' : '' # disabled='disabled'/>\"",
                        column.Name);
                }
                //schemas.Add(column.Name + ": { editable: "+column.IsEditable.ToString().ToLower()+ dataType + " }");
                schemas.Add(column.Name + ": { editable: false " + dataType + " }");
                var columnWidthString = column.ColumnWidth == 0 ? "" : string.Format(", width: {0}", column.ColumnWidth);
                var columnString = "{ " + formatString + " field: \"" + column.Name + "\", title: \"" + column.Text + "\"" + isEncode + isBool +
                                columnWidthString +
                                ", attributes:{style:'text-align:" + column.ColumnJustification + ";'}, sortable: " + column.Sortable.ToString().ToLower()+", hidden: " + column.HideColumn.ToString().ToLower();
                columnString += "}";
                columns.Add(columnString);
            }
            // Check if user manager grid => add command reset pass and active
            if (viewModel.UseDeleteColumn)
            {
                // Add delete column
                const string delColumnString = "{ command: [{template: kendo.template($(\"#deleteTemplate\").html())}], title: \"&nbsp;\", width: \"100px\",mandatory:true,sortable:false, }";
                columns.Add(delColumnString);
            }
            else if (!string.IsNullOrEmpty(viewModel.UserCustomTemplate))
            {
                const string delColumnString = "{ command: [{template: kendo.template($(\"#customTemplate\").html())}], title: \"&nbsp;\", width: \"100px\",mandatory:true,sortable:false, }";
                columns.Add(delColumnString);
            }
            

            gridColumns.Append(string.Join(", \n", schemas.ToArray()));
            gridColumns.Append("}; \n");
            gridColumns.Append(" var columns = [ \n");
            gridColumns.Append(string.Join(", \n", columns.ToArray()));
            gridColumns.Append("];\n");

            return new MvcHtmlString(gridColumns.ToString());
        }
        public static MvcHtmlString GridColumnChilds(this HtmlHelper htmlHelper, GridViewModel viewModel)
        {
            var gridColumns = new StringBuilder();

            var schemas = new List<string>();
            var columns = new List<string>();
            gridColumns.Append("var kendoCultureFormat = kendo.culture().calendar.patterns.d; \n");

            gridColumns.Append("var schemaFields = { \n");

            schemas.Add("Id: { editable: false }");
            schemas.Add("IsChecked: { editable: false }");
            // Add checkbox column
            if (viewModel.UseCheckBoxColumn)
            {
                var chkColumnString = "{field: \"IsChecked\", title: \" \",mandatory:true, attributes:{style:'text-align:Left;'}, width: \"30px\",sortable:false, template:\"<input type='checkbox' class='check_row checkbox' onClick='checkRowInGrid(this,${Id});'/>\", headerTemplate:\"<input type='checkbox' class='check_all_row checkbox' id='chkAll-" + viewModel.GridId + "' onClick='checkAllRowInGrid(this);'/>\"}";
                columns.Add(chkColumnString);
            }
            foreach (var column in viewModel.ViewColumnChilds)
            {
                //schemas.Add(column.Name + ": { editable: false }");
                var dataType = "";
                var formatString = string.IsNullOrEmpty(column.ColumnFormat)
                                         ? string.Empty
                                         : "format: '{" + column.ColumnFormat + "}', ";
                if (!string.IsNullOrEmpty(column.ColumnFormat)
                    && (column.ColumnFormat.Contains("yyyy") || column.ColumnFormat.ToLower().Contains("date")))
                {
                    dataType = ", type:'date'";
                    // To format kendo grid date column base on culture can use format
                    // or template: "#=kendo.format('{0:d}', ValidFrom)#"
                    formatString = "format: '{0:' + kendoCultureFormat + '}', ";

                }
                //schemas.Add(column.Name + ": { editable: "+column.IsEditable.ToString().ToLower()+ dataType + " }");
                schemas.Add(column.Name + ": { editable: false " + dataType + " }");
                var columnWidthString = column.ColumnWidth == 0 ? "" : string.Format(", width: {0}", column.ColumnWidth);
                var columnString = "{ " + formatString + " field: \"" + column.Name + "\", title: \"" + column.Text + "\"" +
                                columnWidthString +
                                ", attributes:{style:'text-align:" + column.ColumnJustification + ";'}, hidden: " + column.HideColumn.ToString().ToLower();

                columnString += "}";
                columns.Add(columnString);
            }
            if (viewModel.UseDeleteColumn)
            {
                // Add delete column
                const string delColumnString = "{ command: [\"destroy\"], title: \"&nbsp;\", width: \"100px\",mandatory:true,sortable:false, }";
                columns.Add(delColumnString);
            }
            gridColumns.Append(string.Join(", \n", schemas.ToArray()));
            gridColumns.Append("}; \n");
            gridColumns.Append(" var columnChilds = [ \n");
            gridColumns.Append(string.Join(", \n", columns.ToArray()));
            gridColumns.Append("];\n");

            return new MvcHtmlString(gridColumns.ToString());
        }

        public static MvcHtmlString GetValidationForKendoEditor(Type type, string propertyName,string customMessage="")
        {

            var property = type.GetProperty(propertyName);
            var customAttributes = property.GetCustomAttributes(true).OfType<ValidationAttribute>();
            var validations = new List<string>();
            if (customAttributes.Count() > 0)
            {
                foreach (var attr in customAttributes)
                {
                    if (attr.GetType() == typeof(LocalizeRequiredAttribute))
                    {
                        var stringMessage = "";
                        stringMessage = !string.IsNullOrEmpty(customMessage) ? customMessage : attr.FormatErrorMessage(propertyName);
                        
                        validations.Add(string.Format(" required = \"required\" data-required-msg=\"{0}\" ", stringMessage));
                    }
                    else if (attr.GetType() == typeof(LocalizeMaxLengthAttribute) || attr.GetType() == typeof(MaxLengthAttribute))
                    {
                        validations.Add(string.Format("maxlength = {0}", ((MaxLengthAttribute)attr).Length));
                    }
                }

            }
            return new MvcHtmlString(String.Join(", ", validations.ToArray()));
        }
    }
}