﻿using System.Web.Mvc;
using System.Web.Mvc.Html;
using WorkComp.Models.DualListBox;
using WorkComp.Models.DualSelectListBox;

namespace WorkComp.HtmlHelpers
{
    public static class DualListBoxExtensions
    {
        public static MvcHtmlString GenericDualListBox(this HtmlHelper htmlHelper, string modelName, int entityId, string avaliableAction, string selectedAction,
                                                                                                                   string queryEntityName, string headerText = "")
        {
            var model = new DualListBoxViewModel
            {
                ControlId = modelName,
                ModelName = modelName,
                GetAllUrl = string.Format("/{0}/{1}", modelName, avaliableAction),
                GetSelectedUrl = string.Format("/{0}/{1}?{2}= {3}", modelName, selectedAction, queryEntityName, entityId),
                HeaderText = headerText

            };
            var mvcHtmlString = htmlHelper.Partial("~/Views/Shared/DualListBox/_DualListBoxViewModel.cshtml", model);
            return mvcHtmlString;
        }

        public static MvcHtmlString DualSelectListBox(this HtmlHelper htmlHelper, string controllerName, int entityId,
                                                    string parentSelectAction, string childSelectAction, string treeViewAction,
                                                    string parentSelectLabelText, string childSelectLabelText, string selectedTreeLabelText,
                                                    int? offsetHeight = null)
        {

            var advanceFilterModel = new DualSelectListBoxViewModel()
            {
                ControlId = string.Format("{0}DualSelectFilter", controllerName),
                ModelName = controllerName,
                ParentSelectUrl = string.Format("/{0}/{1}?{2}Id= {3}", controllerName, parentSelectAction, controllerName, entityId),
                ChildSelectUrl = string.Format("/{0}/{1}?{2}Id= {3}", controllerName, childSelectAction, controllerName, entityId),
                TreeViewUrl = string.Format("/{0}/{1}?{2}Id= {3}", controllerName, treeViewAction, controllerName, entityId),
                MasterfileId = entityId,
                ParentSelectLabelText = parentSelectLabelText,
                ChildSelectLabelText = childSelectLabelText,
                SelectedTreeLabelText = selectedTreeLabelText,
                OffsetHeight = offsetHeight
            };


            var mvcHtmlString = htmlHelper.Partial("~/Views/Shared/DualSelectListBox/DualSelectListBox.cshtml", advanceFilterModel);
            return mvcHtmlString;
        }

        public static MvcHtmlString DualSelectTreeView(this HtmlHelper htmlHelper, string controllerName,string modelEntityIdName, int entityId,
                                                     string treeViewAction,string selectedItemAction,
                                                    string treeViewLabelText, string selectItemLabelText,
                                                    int? offsetHeight = null)
        {

            var advanceFilterModel = new DualSelectTreeViewViewModel()
            {
                ControlId = string.Format("{0}DualSelectFilter", controllerName),
                ModelName = controllerName,
                TreeViewUrl = string.Format("/{0}/{1}?{2}= {3}", controllerName, treeViewAction, modelEntityIdName, entityId),
                SelectedUrl = string.Format("/{0}/{1}?{2}= {3}", controllerName, selectedItemAction, modelEntityIdName, entityId),
                MasterfileId = entityId,
                TreeViewLabelText = treeViewLabelText,
                SelectedItemText = selectItemLabelText,
                OffsetHeight = offsetHeight
            };


            var mvcHtmlString = htmlHelper.Partial("~/Views/Shared/DualSelectTreeView/DualSelectTreeView.cshtml", advanceFilterModel);
            return mvcHtmlString;
        }
    }
}