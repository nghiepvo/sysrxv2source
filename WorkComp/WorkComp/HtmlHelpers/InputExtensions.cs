﻿using System;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using WorkComp.Models.Date;
using WorkComp.Models.Editor;
using WorkComp.Models.Input;

namespace WorkComp.HtmlHelpers
{
    public static class InputExtensions
    {
        public static MvcHtmlString WorkcompTextBox(this HtmlHelper htmlHelper, string id, string label, string dataBindingValue, bool required = false, int length = Int32.MaxValue, bool isReadonly = false, string cssClass = "k-textbox", object htmlAttribute = null, string placeHolderText="")
        {
            var attribute = new RouteValueDictionary();
            if (htmlAttribute != null)
            {
                attribute = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttribute);
            }

            attribute.Add("id", id);

            var viewModel = new InputTextViewModel
            {
                ID = id,
                Label = label,
                HtmlAttributes = attribute,
                Required = required,
                Class = cssClass,
                DataBindingValue = dataBindingValue,
                Length = length,
                ReadOnly = isReadonly,
                PlaceHolderText=placeHolderText
            };

            return htmlHelper.Partial("~/Views/Shared/Input/InputText.cshtml", viewModel);
        }

        public static MvcHtmlString WorkcompCheckBox(this HtmlHelper htmlHelper, string id, string label, string dataBindingValue, bool isReadonly = false, string cssClass = "k-checkbox", object htmlAttribute = null)
        {
            var attribute = new RouteValueDictionary();
            if (htmlAttribute != null)
            {
                attribute = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttribute);
            }

            attribute.Add("id", id);

            var viewModel = new CheckBoxViewModel
            {
                ID = id,
                Label = label,
                HtmlAttributes = attribute,
                Class = cssClass,
                DataBindingValue = dataBindingValue,
                ReadOnly = isReadonly
            };

            return htmlHelper.Partial("~/Views/Shared/Input/CheckBox.cshtml", viewModel);
        }

        public static MvcHtmlString WorkcompTextArea(this HtmlHelper htmlHelper, string id, string label, string dataBindingValue, bool required = false, bool isReadonly = false, string cssClass = "k-textbox", object htmlAttribute = null, string placeHolderText = "", int cols = 2, int rows = 2, double widthPercentLable = 14, double widthPercentField = 85)
        {
            var attribute = new RouteValueDictionary();
            if (htmlAttribute != null)
            {
                attribute = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttribute);
            }

            attribute.Add("id", id);

            var viewModel = new AreaTextViewModel
            {
                ID = id,
                Label = label,
                HtmlAttributes = attribute,
                Required = required,
                Class = cssClass,
                DataBindingValue = dataBindingValue,
                ReadOnly = isReadonly,
                PlaceHolderText = placeHolderText,
                Cols = cols,
                Rows = rows,
                WidthPercentLable = widthPercentLable,
                WidthPercentField = widthPercentField
            };

            return htmlHelper.Partial("~/Views/Shared/Input/TextArea.cshtml", viewModel);
        }

        public static MvcHtmlString WorkcompUploadFile(this HtmlHelper htmlHelper, string id, string label, string dataBindingValue)
        {
            var viewModel = new FileUploadViewModel
            {
                ID = id,
                Label = label,
                DataBindingValue = dataBindingValue
            };

            return htmlHelper.Partial("~/Views/Shared/Input/UploadFile.cshtml", viewModel);
        }
        
        public static MvcHtmlString WorkcompDatePicker(this HtmlHelper htmlHelper, string id, string label, string dataBindingValue, string format,
            bool required = false, bool isReadonly = false, string cssClass = "k-datepicker", object htmlAttribute = null, bool isSetEmpty = false)
        {
            var attribute = new RouteValueDictionary();
            if (htmlAttribute != null)
            {
                attribute = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttribute);
            }

            attribute.Add("id", id);

            var viewModel = new DatePickerViewModel
            {
                ID = id,
                Label = label,
                Format = format,
                HtmlAttributes = attribute,
                Required = required,
                Class = cssClass,
                DataBindingValue = dataBindingValue,
                ReadOnly = isReadonly,
                IsSetEmpty = isSetEmpty,
            };

            return htmlHelper.Partial("~/Views/Shared/Date/DatePicker.cshtml", viewModel);
        }
        public static MvcHtmlString WorkcompDatetimePicker(this HtmlHelper htmlHelper, string id, string label, string dataBindingValue, string format, 
            bool required = false, bool isReadonly = false, string cssClass = "k-datetimepicker", object htmlAttribute = null, bool isSetEmpty = false
            )
        {
            var attribute = new RouteValueDictionary();
            if (htmlAttribute != null)
            {
                attribute = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttribute);
            }

            attribute.Add("id", id);

            var viewModel = new DatetimePickerViewModel
            {
                ID = id,
                Label = label,
                Format = format,
                HtmlAttributes = attribute,
                Required = required,
                Class = cssClass,
                DataBindingValue = dataBindingValue,
                ReadOnly = isReadonly,
                IsSetEmpty = isSetEmpty
            };

            return htmlHelper.Partial("~/Views/Shared/Date/DatetimePicker.cshtml", viewModel);
        }
        public static MvcHtmlString WorkcompDatetimeRangePicker(this HtmlHelper htmlHelper,
            string idStart, string labelStart, string dataBindingValueStart,  
             string idEnd, string labelEnd, string dataBindingValueEnd,  string format, bool hasTime = false, object htmlAttributeEnd = null, object htmlAttributeStart = null,
           bool required = false, bool isReadonly = false, string cssClass = "")
        {
            var attributeStart = new RouteValueDictionary();
            if (htmlAttributeStart != null)
            {
                attributeStart = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributeStart);
            }

            attributeStart.Add("id", idStart);

            var attributeEnd = new RouteValueDictionary();
            if (htmlAttributeEnd != null)
            {
                attributeEnd = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributeEnd);
            }

            attributeEnd.Add("id", idEnd);

            var viewModel = new DatetimeRangePickerViewModel
            {
                Class = cssClass,
                ReadOnly = isReadonly,
                Required = required,
                Format=format,

                IDStart = idStart,
                LabelStart = labelStart,
                HtmlAttributesStart = attributeStart,
                DataBindingValueStart = dataBindingValueStart,
                HasTime = hasTime,

                IDEnd = idEnd,
                LabelEnd = labelEnd,
                DataBindingValueEnd = dataBindingValueEnd
            };

            return htmlHelper.Partial("~/Views/Shared/Date/DatetimeRangePicker.cshtml", viewModel);
        }
        private static MvcHtmlString InputNumericBase(this HtmlHelper htmlHelper, string id, string label,
                                                      int length = 10, string dataBindingValue = "", int width = 200,
                                                      string format = "", object minimumValue = null,
                                                      object maximumValue = null, double stepValue = 1, bool readOnly = false, int decimals = 2, string placeHolderText = "", bool isRequired = false)
        {
            var viewModel = new InputNumericViewModel
            {
                ID = id,
                Label = label,
                Length = length,
                DataBindingValue = string.IsNullOrEmpty(dataBindingValue) ? "''" : dataBindingValue,
                Width = width,
                Format = format,
                MinimumValue = minimumValue,
                MaximumValue = maximumValue,
                StepValue = stepValue,
                PlaceHolderText = placeHolderText,
                ReadOnly = readOnly,
                Decimals = decimals,
                Required = isRequired
                
            };

            return htmlHelper.Partial("~/Views/Shared/Input/InputNumeric.cshtml", viewModel);
        }
        public static MvcHtmlString InputPositiveNumeric(this HtmlHelper htmlHelper, string id, string label,
                                                 string dataBindingValue = "", int width = 200, string format = "", int maximumValue = 999999999, bool readOnly = false)
        {
            var length = maximumValue.ToString(CultureInfo.InvariantCulture).Length;
            return InputNumericBase(htmlHelper, id, label, length, dataBindingValue, width, format, 0, maximumValue, 1, readOnly, 0);
        }

        public static MvcHtmlString InputNumeric(this HtmlHelper htmlHelper, string id, string label, int length = 10,
                                                 string dataBindingValue = "", int width = 200, string format = "",
                                                 double minimumValue = 0.1,
                                                 double maximumValue = 999999999, int stepValue = 1, bool readOnly = false, int decimals = 2, string placeHolderText = "", bool isRequired = false)
        {
            var decimalLength = decimals > 0 ? decimals + 1 : 0;
            var maxlength = maximumValue.ToString(CultureInfo.InvariantCulture).Length + decimalLength;
            if (decimals > 0 && string.IsNullOrEmpty(format))
            {
                format = string.Format("#,#.{0}", "0".PadRight(decimals, '0'));
            }
            return InputNumericBase(htmlHelper, id, label, maxlength, dataBindingValue, width, format, minimumValue,
                                    maximumValue, stepValue, readOnly, decimals, placeHolderText, isRequired);
        }
        public static MvcHtmlString InputMasked(this HtmlHelper htmlHelper, string id, string label,
                                                string dataBindingValue,  string format , bool isRequied = false, bool readOnly = false, 
            object htmlAttribute = null)
        {
            var attribute = new RouteValueDictionary();
            if (htmlAttribute != null)
            {
                attribute = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttribute);
            }

            attribute.Add("id", id);

            var viewModel = new InputMaskedViewModel
            {
                ID = id,
                Label = label,
                HtmlAttributes = attribute,
                Required = isRequied,
                DataBindingValue = dataBindingValue,
                Format =format
            };

            return htmlHelper.Partial("~/Views/Shared/Input/InputMasked.cshtml", viewModel);
        }

        public static MvcHtmlString WorkcompEditor(this HtmlHelper htmlHelper, string id, string label, string dataBindingValue, bool required = false,int width = 50, int height = 50, string urlRead = null, 
            string urlDestroy = null, string urlCreate = null, string urlThumb = null, string urlUpload = null, string urlImage = null,  bool isReadonly = false, string cssClass = "k-textbox", object htmlAttribute = null)
        {
            var attribute = new RouteValueDictionary();
            if (htmlAttribute != null)
            {
                attribute = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttribute);
            }

            attribute.Add("id", id);

            var viewModel = new EditorViewModel
            {
                ID = id,
                Label = label,
                HtmlAttributes = attribute,
                Required = required,
                Class = cssClass,
                DataBindingValue = dataBindingValue,
                ReadOnly = isReadonly,
                Width = width,
                Height = height,
                UrlRead = urlRead,
                UrlDestroy = urlDestroy,
                UrlCreate = urlCreate,
                UrlThumb = urlThumb,
                UrlUpload = urlUpload,
                UrlImage = urlImage
            };

            return htmlHelper.Partial("~/Views/Shared/Editor/Editor.cshtml", viewModel);
        }
        public static MvcHtmlString InputWithAttributes(this HtmlHelper helper, string id, object htmlAttributes = null)
        {
            var control = new TagBuilder("input");

            foreach (var attribute in (RouteValueDictionary)htmlAttributes)
                control.MergeAttribute(attribute.Key, attribute.Value.ToString());

            return new MvcHtmlString(control.ToString());
        }
    }
}