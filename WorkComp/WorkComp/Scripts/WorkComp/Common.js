﻿
function ClosePopupKendo(isUpdatedForCancelReferral) {
    parent.$("#window").data("kendoWindow").close();
    if (isUpdatedForCancelReferral) {
        parent.$('#isUpdatedCancelReferral').val(1);
    } else {
        // referesh grid
        parent.Postbox.messages.notifySubscribers(null, Postbox.REFRESH_READONLY_GRID);
    }
}
function ShowPopupKendo(w, h, title, url) {

    if ($("#window").length == undefined || $("#window").length == 0) {
        $("body").append("<div id='window'></div>");
    }
    var window = $("#window");
    window.kendoWindow({
        width: w + "px",
        height: h + "px",
        modal: true,
        title: title,
        actions: ["Pin", "Refresh", "Maximize", "Close"],
        content: {
            url: url
        },
        visible: false,
        iframe: true,
        close: onClosePopupKendoWindow,
        deactivate: function () {
            this.destroy();
        }
    });
    window.data("kendoWindow").open();
    window.data("kendoWindow").center();
   
}

function ShowPopupKendoNoIframe(title, w, h, modalConfig, actionConfig, resizableConfig, contentConfig, objDataJson) {
    //var elememtKendoWindow = elementWindow.indexOf('#') > -1 || elementWindow.indexOf('.') > -1 ? elementWindow : '#' + elementWindow;
    
    var height = _.isNumber(h) ? h + 'px' : h;
    var width = _.isNumber(w) ? w + 'px' : w;
    var action = !_.isUndefined(actionConfig) && _.isArray(actionConfig) ? actionConfig : ["Pin", "Refresh", "Minimize", "Maximize", "Close"];
    var resizable = !_.isUndefined(resizableConfig) && _.isBoolean(resizableConfig) ? resizableConfig : false;
    var modal = !_.isUndefined(resizableConfig) && _.isBoolean(resizableConfig) ? resizableConfig : false;
    var content = !_.isUndefined(contentConfig) ? contentConfig : null;
    var objectData = !_.isUndefined(objDataJson) ? objDataJson : null;

    //Check is exit elemnet
    var elementGenerate = '';
    var index = 0;
    while (true) {
        elementGenerate = "kendo-window-kn-" + index;
        if (!$('#' + elementGenerate).hasClass('k-window-content')) {
            elementGenerate = 'kendo-window-kn-' + (index);
            break;
        }
        index ++;
    }
    
   
    $('body').append('<div id="' + elementGenerate + '"></div>');
    
   
    if (objectData != null) {
        $('#' + elementGenerate).data('data-popup', objectData);
    }
    
    var window = $('#' + elementGenerate);
    //var window = $(elememtKendoWindow);
    var kendoWindow = null;
    if (_.isUndefined(window.data("kendoWindow"))) {
        kendoWindow = window.kendoWindow({
            title: title,
            width: width,
            height: height,
            modal: modal,
            actions: action,
            visible: true,
            resizable: resizable,
            content: content,
            close: onClosePopupKendoWindow,
            deactivate: function () {
                this.destroy();
            },
        }).data("kendoWindow");
        kendoWindow.center();
    } else {
        window.data("kendoWindow").open();
    }
    return kendoWindow;
}

function ClosePopup(returnValue) {
    //if (parent.$("#window1").data("kendoWindow")==undefined) {
    //    parent.$("#window").data("kendoWindow").close();
    //} else {
    //    parent.$("#window1").data("kendoWindow").close();
    //}
    window.close();
    window.returnValue = returnValue;
   
}
function ShowPopup(w_f, h_f, type, url,gridId) {
    var w = screen.width;
    var h = screen.height - 100;
    var l = (w - w_f) / 2;
    var t = (h - h_f) / 2;
   
    //var window = $("#window");
    //window.kendoWindow({
    //    width: w_f + "px",
    //    height:h_f+"px",
    //    title: type,
    //    content: url,
    //    modal: true,
    //    visible: false,
    //    actions: ["Pin", "Refresh", "Maximize", "Close"],
    //    activate: onActivate,
    //    close: onClose,
    //    iframe: true
    //}).data("kendoWindow").open();
    //window.data("kendoWindow").center();
    
   
    if (window.showModalDialog) {
        var result = window.showModalDialog(url, type.replaceAll("-", ""), "dialogWidth:" + w_f + "px;dialogHeight:" + h_f + "px");
        if (result != null && result != '') {
            result = result + "";
            if (result.indexOf("_") > -1) {
                var listReturnValue = result.split("_");
                if (listReturnValue.length == 2 && listReturnValue[0] == "CreateReferral" && listReturnValue[1] != "0") {
                    var idReferral = listReturnValue[1];
                    window.location.href = "/Referral/Detail/" + idReferral;
                }
            }
            
        }
       
        // refresh grid
        Postbox.messages.notifySubscribers(null, Postbox.REFRESH_READONLY_GRID);
    } else {

        var myWindow = window.open(url, type.replaceAll("-", ""), 'width=' + w_f + ',height=' + h_f + ',left=' + l + ',top=' + t + ',resizable=yes,location=no ,modal=yes');
        if (myWindow) myWindow.focus();
    }
}

function CreateLoading() {
    if ($("#loading").length == 0) {
        var html = "<div id='loading' style='z-index:999999 !important;'><div style='position:fixed;z-index:999999 !important;border:1px solid #D2D7DC;border-radius:5px;padding:5px;background:#fff;top:" + ($(window).height() + 31) / 2 + "px;left:" + ($(window).width() - 100) / 2 + "px'>";
        html += "<div style='line-height:31px;height:31px;float:left;margin-right:5px;'><img src='/Content/images/loading.gif' alt='loading' title='loading' /></div>";
        html += "<div style='line-height:31px;height:31px;float:left;'>Loading...</div>";
        html += "</div></div>";
        $("body").append(html);
    }
}
function DeleteLoading() {
    $("#loading").remove();
}

function HTMLDecode(s) {
    return jQuery('<div></div>').html(s).text();
}
function HTMLEncode(s) {
    return jQuery('<div></div>').text(s).html();
}
var onClosePopupKendoWindow = function () {
    //Postbox.messages.notifySubscribers(null, Postbox.REFRESH_READONLY_GRID);
    Postbox.messages.notifySubscribers(null, Postbox.CLOSE_PARENT_WINDOWS_KENDO);
    //Postbox.messages.notifySubscribers(null, Postbox.CLOSE_GRID_SEND_MAIL);
};
var onActivate = function () {
};
function ShowPopupWhenClickOnLookup(w_f, h_f, type,parentObject, url) {
    var w = screen.width;
    var h = screen.height - 100;
    var l = (w - w_f) / 2;
    var t = (h - h_f) / 2;
    //if ($("#window1").length == undefined || $("#window1").length == 0) {
    //    $("body").append("<div id='window1'></div>");
    //}
    //var window1 = $("#window1");
    //window1.kendoWindow({
    //    width: (w_f-20) + "px",
    //    height: (h_f-20) + "px",
    //    title: type,
    //    content: url,
    //    modal: true,
    //    visible: false,
    //    refresh: onRefresh,
    //    iframe: true
    //}).data("kendoWindow").open();
    //window1.data("kendoWindow").center();
    if (window.showModalDialog) {
        var result = window.showModalDialog(url, parentObject, "dialogWidth:" + w_f + "px;dialogHeight:" + h_f + "px");
        //Return value to combobox when create object
        Postbox.messages.notifySubscribers(result, Postbox.RETURN_VALUE_TO_COMBOBOX);

        if (!_.isUndefined(parentObject) ) {
            if (parentObject != null && !_.isUndefined(parentObject.Type) && !_.isEmpty(parentObject.Type)) {
                Postbox.messages.notifySubscribers(parentObject.Type, Postbox.RETURN_VALUE_FOR_GRID);
            } else {
                Postbox.messages.notifySubscribers(result, Postbox.RETURN_VALUE_FOR_GRID);
            }
        }
        
    } else {
        var myWindow = window.open(url, type.replaceAll("-", ""), 'width=' + w_f + ',height=' + h_f + ',left=' + l + ',top=' + t + ',resizable=yes,location=no ,modal=yes');
        if (myWindow) myWindow.focus();
    }
}

var onRefresh = function () {
    
    //$("#window1").parent().parent().css({ height: parseInt($("#window1").parent().parent().css("height").replace("px", "")) - 50 });
};
function ReturnViewModel(type, id,displayName) {
    var self = this;
    self.TypeId = type;
    self.KeyId = id;
    self.DisplayName = displayName;
}
function FormatPage() {
    $(".wrapper .main").css({ height: $(window).height() - 41 });
    $(".wrapper .main .content").css({ height: $(window).height() - 51 });
}

function FormatPageReport() {
    $(".wrapper .main").css({ height: $(window).height() - 41 });
    $(".wrapper .main .content").css({ height: $(window).height() - 41 });
    $(".wrapper .main .left .nav").css({ height: $(window).height() - 115 });
    $(".wrapper .main .right").css({ width: $(window).width() - 221 });
    $(".wrapper .main .right .report").css({ height: $(window).height() - 148 });

    $(".wrapper .main .left .nav").find("a").click(function () {
        $(".wrapper .main .left .nav").find("li").removeClass("active");
        $(this).parent().addClass("active");
    });

    var url = window.location.href;
    var hash = url.substring(url.indexOf('#') + 1);
    if (hash != undefined && hash != "" && parseInt(hash) > 0) {
        $(".wrapper .main .left .nav").find("li").removeClass("active");
        $(".wrapper .main .left .nav li:nth-child(" + hash + ")").addClass("active");
    }
}

function FormatForm() {
    $(".wrapper .content").css({ height: $(window).height() - 81 });
}

function EnableCreateFooterButton(isEnable) {
    if (isEnable) {
        $('button[data-function="Postbox.FOOTER_ACTION_SAVE"]').removeAttr('disabled');
        $('button[data-function="Postbox.FOOTER_ACTION_SAVE"]').removeClass('k-state-disabled');
        $('button[data-function="Postbox.FOOTER_ACTION_SAVE_AND_NEW"]').removeAttr('disabled');
        $('button[data-function="Postbox.FOOTER_ACTION_SAVE_AND_NEW"]').removeClass('k-state-disabled');
    } else {
        $('button[data-function="Postbox.FOOTER_ACTION_SAVE"]').attr('disabled', 'disabled');
        $('button[data-function="Postbox.FOOTER_ACTION_SAVE"]').addClass('k-state-disabled');
        $('button[data-function="Postbox.FOOTER_ACTION_SAVE_AND_NEW"]').attr('disabled', 'disabled');
        $('button[data-function="Postbox.FOOTER_ACTION_SAVE_AND_NEW"]').addClass('k-state-disabled');
    }
}

function ValidateEmail(str) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

    if (filter.test(str)) {
        return true;
    }
    else {
        return false;
    }

}
$(window).load(function () {
    $(".tab").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });
    //$('ul.tabs').each(function () {
    //    // For each set of tabs, we want to keep track of
    //    // which tab is active and it's associated content
    //    var $active, $content, $links = $(this).find('a');

    //    // If the location.hash matches one of the links, use that as the active tab.
    //    // If no match is found, use the first link as the initial active tab.
    //    $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
    //    $active.addClass('active');

    //    $content = $($active[0].hash);

    //    // Hide the remaining content
    //    $links.not($active).each(function () {
    //        $(this.hash).hide();
    //    });

    //    // Bind the click event handler
    //    $(this).on('click', 'a', function (e) {
    //        // Make the old tab inactive.
    //        $active.removeClass('active');
    //        $content.hide();

    //        // Update the variables with the new link and content
    //        $active = $(this);
    //        $content = $(this.hash);

    //        // Make the tab active.
    //        $active.addClass('active');
    //        $content.show();

    //        // Prevent the anchor's default click action
    //        e.preventDefault();
    //    });
    //});

});


function ShowHideContentCommunicationDiary(id) {
    if ($("#content-communication-diary-" + id).css("display") != "none") {
        $("#content-communication-diary-" + id).hide();
        $("#btnShowHideDiary-" + id).val("Show");
    } else {
        $("#content-communication-diary-" + id).show();
        $("#btnShowHideDiary-" + id).val("Hide");
    }
}