﻿function ReferralDetailExtensionData(/*elementWindow,*/ elementEditor, elementSearchEmail, elementGridSearchEmail, elementGridSelected, referralId, emailTitle, includeContentHtml, fileName, modelName) {
    var self = this;
    //self.ElementWindow = $(elementWindow);
    self.ModelName = modelName;
    self.ElementEditor = $(elementEditor);
    self.ElementSearchEmail = $(elementSearchEmail);
    self.ElementGridSearchEmail = $(elementGridSearchEmail);
    self.ElementGridSelected = $(elementGridSelected);
    self.ElementEditor.kendoEditor();
    self.EmailAddressSetup = ko.observable('');
    self.EmailAddressSetupCc = ko.observable('');
    self.GridCurrent = ko.observable('');
    self.CompareGrid = function () {
        return self.GridCurrent() == self.ElementGridSearchEmail.attr('id');
    };

    self.init = function() {
        self.Meassage('');
        self.To('');
        self.Cc('');
        self.ElementEditor.data('kendoEditor').value('');
        self.ArrayItemTo([]);
        self.ArrayItemCc([]);
        self.ArrayItemGridSearchEmailRemove([]);
        self.ArrayItemRemoveTemp([]);
        self.ArrayItemSelectedTemp([]);
        self.EmailAddressSetup('');
        self.EmailAddressSetupCc('');
        self.ArrayItemToAndCc([]);
        $('#listEmailTo').html('');
        $('#listEmailCc').html('');
        $('ul#list-email-to-manual span.email, ul#list-email-cc-manual span.email').filter(function (index) {
            $(this).parent('li').remove();
        });
    };


    self.ReferralId = ko.observable(referralId);
    self.To = ko.observable('');
    self.Cc = ko.observable('');
    self.SearchString = ko.observable('');
    self.SecureEmail = ko.observable(true);
    self.Title = ko.observable(emailTitle);
    self.Meassage = ko.observable('');
    self.IncludeContent = ko.observable($("<div/>").html(includeContentHtml).text());
    self.FileName = ko.observable(fileName);

    self.EncodeQuerySearch = function () {
        return Base64.encode('<AdvancedQueryParameters>' + self.CreateQueryString() + '</AdvancedQueryParameters>');
    };

    self.CreateQueryString = function () {
        // Create a xml string to parse to query info in server
        return '<SearchTerms>' + self.SearchString() + '</SearchTerms>';
    };

    self.ClickSearchString = function () {
        self.ElementGridSearchEmail.data('kendoGrid').dataSource.read();
    };

    self.KeyPressEnterSearch = function (data, event) {
        if (event.keyCode == 13) {
            self.SearchString = ko.observable($("#search-string").val());
            self.ElementGridSearchEmail.data('kendoGrid').dataSource.read();
        }
        return true;
    };

    self.ModelState = ko.observable();
    self.ModelState.subscribe(function (newValue) {
        Postbox.messages.notifySubscribers(newValue, Postbox.VALIDATION_MODEL_STATE);
    });
    self.ElementEditor.data('kendoEditor').bind('change', function () {
        self.Meassage(this.value());
    });



    self.TypeClick = '';
    self.ArrayItemTo = ko.observableArray(null);
    self.ClickWindowSearchTo = function () {
        self.TypeClick = 'To';
        self.ArrayItemGridSearchEmailRemove(self.ArrayItemTo());
        initData();

    };

    self.ArrayItemCc = ko.observableArray(null);
    self.ClickWindowSearchCc = function () {
        self.TypeClick = 'Cc';
        self.ArrayItemGridSearchEmailRemove(self.ArrayItemCc());
        initData();
    };

    self.ArrayItemToAndCc = ko.observableArray(null);

    function initData() {
        self.ElementGridSelected.data('kendoGrid').dataSource.data([]);
        self.ElementSearchEmail.data("kendoWindow").open();
        addToAndCc();
        self.AddDataSourceGridSelected();
        if (self.DataSourceGridSelected.total() > 0) {
            self.ElementGridSelected.data('kendoGrid').setDataSource(self.DataSourceGridSelected);
        }

        var kendoGrid = self.ElementGridSearchEmail.data('kendoGrid');
        var visibleRows = kendoGrid.tbody.children('tr');
        $(visibleRows).each(function () {
            row = $(this);
            if (row.find('input.check_row').is(':disabled')) {
                row.find('.check_row').removeAttr('disabled');
            }
        });

        for (var i = 0; i < self.ArrayItemToAndCc().length; i++) {
            var item = self.ArrayItemToAndCc()[i];
            var row = kendoGrid.dataSource.get(item.Id);
            if (!_.isUndefined(row) && row != null) {
                var element = kendoGrid.tbody.find("tr[data-uid='" + row.uid + "']");
                $(element).children('td:first-child').children('input:checkbox').attr('disabled', 'disabled');
            }
        }
    }

    self.KeyPressAddCharacter = function (data, event) {
        var id = $(event.currentTarget).attr('id');
        if (id == 'enter-key-cc') {
            self.Cc = ko.observable($("#enter-key-cc").val());
        } else if (id == 'enter-key-to') {
            self.To = ko.observable($("#enter-key-to").val());
        }
        return true;
    };



    function addToAndCc() {
        var data = new Array();
        for (var i = 0; i < self.ArrayItemCc().length; i++) {
            data.push(self.ArrayItemCc()[i]);
        }
        for (var j = 0; j < self.ArrayItemTo().length; j++) {
            data.push(self.ArrayItemTo()[j]);
        }
        self.ArrayItemToAndCc(data);
    }


    self.ClickCancelSearchEmail = function () {
        $(self.ElementSearchEmail).data("kendoWindow").close();
    };

    self.ClickCancel = function () {
        Postbox.messages.notifySubscribers(null, Postbox.CLOSE_GRID_SEND_MAIL);
        //$(self.ElementWindow).data("kendoWindow").close();
    };

    Postbox.messages.subscribe(function (value) {
        if (!_.isUndefined($('#referral-result-search-email').data("kendoWindow"))) {
            $('#referral-result-search-email').data("kendoWindow").close();
            $('#referral-result-search-email').data("kendoWindow").destroy();
        }
    }, self, Postbox.CLOSE_PARENT_WINDOWS_KENDO);

    self.ClickSelect = function () {
        $(self.ElementSearchEmail).data("kendoWindow").close();
        var data = '';
        var dataEmailStr = '';
        for (var i = 0; i < self.ArrayItemGridSearchEmailRemove().length; i++) {
            var item = self.ArrayItemGridSearchEmailRemove()[i];
            data += item.Email + ';';
            dataEmailStr += "<li><span class='email' title='" + item.Email + "' data-role='tooltip'>" + item.Email + "</span><a href='javascript:void(0);' onclick='DeleteEmail(this,\"" + item.Email + "\")' ><span class='k-icon k-delete'></span></a></li>";
        }

        if (self.TypeClick == 'To') {
            self.ArrayItemTo(self.ArrayItemGridSearchEmailRemove());
            self.To(data);
            $('#listEmailTo').html(dataEmailStr);
            $(".email").kendoTooltip({
                position: "top"
            });
            self.EmailAddressSetup(data);

        } else if (self.TypeClick == 'Cc') {
            self.ArrayItemCc(self.ArrayItemGridSearchEmailRemove());
            self.Cc(data);
            $('#listEmailCc').html(dataEmailStr);
            $(".email").kendoTooltip({
                position: "top"
            });
            self.EmailAddressSetupCc(data);
        }
    };


    self.ArrayItemSelectedTemp = ko.observableArray(null);
    self.ArrayItemRemoveTemp = ko.observableArray(null);
    self.ArrayItemGridSearchEmailRemove = ko.observableArray(null);

    self.DataSourceGridSelected = new kendo.data.DataSource({ data: null });
    self.AddDataSourceGridSelected = function () {
        if (self.ArrayItemGridSearchEmailRemove() != null && self.ArrayItemGridSearchEmailRemove().length > 0) {
            for (var j = 0; j < self.ArrayItemGridSearchEmailRemove().length; j++) {
                self.DataSourceGridSelected.add(self.ArrayItemGridSearchEmailRemove()[j]);
            }
        }
    };
    self.MoveSelected = function () {
        if (self.ArrayItemSelectedTemp() != null && self.ArrayItemSelectedTemp().length > 0) {
            if (self.ArrayItemGridSearchEmailRemove() == null || self.ArrayItemGridSearchEmailRemove().length < 1) {
                self.ArrayItemGridSearchEmailRemove(self.ArrayItemSelectedTemp());
            } else {
                for (var j = 0; j < self.ArrayItemSelectedTemp().length; j++) {
                    self.ArrayItemGridSearchEmailRemove.push(self.ArrayItemSelectedTemp()[j]);
                }
            }
            self.DataSourceGridSelected = new kendo.data.DataSource({ data: null });
            self.AddDataSourceGridSelected();
            self.ElementGridSelected.data('kendoGrid').setDataSource(self.DataSourceGridSelected);
            handleGridSearch(true);
            self.ArrayItemSelectedTemp(new Array());
        }
    };

    self.RemoveSelected = function () {
        if (self.ArrayItemRemoveTemp() != null && self.ArrayItemRemoveTemp().length > 0) {
            if (self.ArrayItemGridSearchEmailRemove() != null || self.ArrayItemGridSearchEmailRemove().length > 0) {
                for (var j = 0; j < self.ArrayItemRemoveTemp().length; j++) {
                    self.removeArrayWithIdValue(self.ArrayItemGridSearchEmailRemove(), self.ArrayItemRemoveTemp()[j].Id);
                }

                self.DataSourceGridSelected = new kendo.data.DataSource({ data: null });
                self.AddDataSourceGridSelected();
                if (self.DataSourceGridSelected.total() == 0) {
                    self.ElementGridSelected.data('kendoGrid').dataSource.data([]);
                } else {
                    self.ElementGridSelected.data('kendoGrid').setDataSource(self.DataSourceGridSelected);
                }

            }
            handleGridSearch(false);
            self.ArrayItemRemoveTemp(new Array());
        }
    };

    function handleGridSearch(isDisabled) {
        var kendoGrid = self.ElementGridSearchEmail.data('kendoGrid');
        if (!_.isUndefined(kendoGrid) && kendoGrid != null) {
            var visibleRows = kendoGrid.tbody.children('tr');
            var row;
            if (isDisabled) {
                $(visibleRows).each(function () {
                    row = $(this);
                    if (row.find('input.check_row').is(':checked')) {
                        row.find('.check_row').attr('disabled', 'disabled');
                    }
                    row.find('.check_row').attr('checked', false);
                });
            } else {
                $(visibleRows).each(function () {
                    row = $(this);
                    if (row.find('input.check_row').is(':disabled')) {
                        var data = kendoGrid.dataItem(row);
                        if (!_.isUndefined(data) && data != null) {
                            var dataItem = _.findWhere(self.ArrayItemRemoveTemp(), { Id: data.Id });
                            if (!_.isUndefined(dataItem) && dataItem != null) {
                                row.find('input.check_row').removeAttr('disabled');
                            }
                        }
                    }
                });
            }
        }
    }

    var urlSendMail = '/' + self.ModelName + '/SendMailResult';

    self.ClickSend = function () {
        var models = {
            ReferralId: self.ReferralId(),
            To: self.EmailAddressSetup(),
            Cc: self.EmailAddressSetupCc(),
            Title: self.Title(),
            Message: self.Meassage(),
            SecureEmail: self.SecureEmail(),
            FileName: self.FileName(),
            IncludeContent: self.IncludeContent(),
        };

        var data = Base64.encode(ko.toJSON(models));
        var request = $.ajax({ cache: false, async: false, url: urlSendMail, type: "Post", data: data , dataType: "json" });
        request.done(function (result) {
            if (_.isUndefined(result.Error) && _.isEmpty(result.Error)) {
                self.ModelState(new FeedbackViewModel('Success', result.Data, "", []));
                Postbox.messages.notifySubscribers(null, Postbox.CLOSE_GRID_SEND_MAIL);
                //$(self.ElementWindow).data("kendoWindow").close();
            } else {
                self.ModelState(new FeedbackViewModel(result.Status, result.Error, result.StackTrace, result.ModelStateErrors));
                Postbox.messages.notifySubscribers(null, Postbox.CLOSE_GRID_SEND_MAIL);
                //$(self.ElementWindow).data("kendoWindow").close();
            }
        });
    };

    self.removeArrayWithIdValue = function(arr, idVal) {
        if (_.isArray(arr)) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].Id == idVal) {
                    arr.splice(i, 1);
                }
            }
        }
    };
}