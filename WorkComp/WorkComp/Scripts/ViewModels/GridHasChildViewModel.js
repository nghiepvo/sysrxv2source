﻿function GridHasChildViewModel(id,
                        modelName,
                        schemaFields,
                        columns,
                        searchItem,
                        useCheckBoxColumn,   
                        emptyGridText,
                        userId,
                        documentTypeId,
                        gridInternalName,
                        showColumnMenu,
                        widthPopup,
                        heightPopup,
                        cannotCopyMess,
                        deleteAllConfirmMess,
                        gridChildName,
                        columnChilds
                        ) {
    var widthOfCommandColumn = 90;
    var widthOfCheckboxColumn = 30;
    var self = this;
    self.ID = ko.observable(id);
    self.ModelName = ko.observable(modelName);
    self.DataSource = ko.observable();
    self.SearchTerm = ko.observable(searchItem);
    self.UseCheckBoxColumn = ko.observable(useCheckBoxColumn);
    self.ShowColumnMenu = ko.observable(showColumnMenu);
    var gridConfigViewModel = new GridConfigViewModel(0, userId, documentTypeId, gridInternalName);
    // All validation messages to the postbox for the _ValidationSummary to display.
    self.ModelState = ko.observable();
    self.ModelState.subscribe(function (newValue) {
        Postbox.messages.notifySubscribers(newValue, Postbox.VALIDATION_MODEL_STATE);
    });
    var gridID = "#" + self.ID();

    var queryUrl = "/" + modelName + "/" + "GetDataForGrid";
    var deleteUrl = "/" + modelName + "/" + "Delete";
    var deleteMultiUrl = "/" + modelName + "/" + "DeleteMulti";
    var createUrl = "/" + modelName + "/" + "Create";
    var updateUrl = "/" + modelName + "/" + "Update";
    var copyUrl = "/" + modelName + "/" + "Copy";
    var exportExcelUrl = "/" + modelName + "/" + "ExportExcel";
    // Get grid config
    $.ajax({
        type: "GET",
        url: "/GridConfig/Get",
        data: { UserId: userId, DocumentTypeId: documentTypeId, GridInternalName: gridInternalName},
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (data) {
            if (_.isNull(data.ViewColumns) || _.isUndefined(data.ViewColumns)) {
                columns.forEach(function(column) {
                    gridConfigViewModel.addColumn(column);
                });
                return;
            }
            gridConfigViewModel.Id(data.Id);
            gridConfigViewModel.importColumnConfigs(data.ViewColumns);

            //Processing Grid Columns Config
            var totalWidth = 0;
            columns.forEach(function (column) {
                if (!_.isUndefined(column.field)&& column.field.trim() == "IsChecked") {
                    column.width = widthOfCheckboxColumn;
                }
                if (!_.isUndefined(column.command)) {
                    column.width = widthOfCommandColumn;
                }
                if (_.isString(column.width))
                    column.width = parseInt(column.width.replace("px", ""));

                var columnConfig = gridConfigViewModel.findViewColumn(column);
                //Get column config
                if (!_.isUndefined(columnConfig) && !_.isNull(columnConfig)) {
                    column.hidden = columnConfig.HideColumn();

                    if (columnConfig.ColumnWidth() > 0) {
                        column.width = columnConfig.ColumnWidth();
                    }
                }
                else {

                    column.hidden = false;
                    gridConfigViewModel.addColumn(column);
                }

                if (_.isUndefined(column.hidden) || !column.hidden) {
                    totalWidth += column.width;
                }
            });

            //sort column order
            columns = _.sortBy(columns, function (column) { return gridConfigViewModel.findViewColumnIndex(column); });

            //work around extend the last column to fit with grid width if total columns width less than grid width
            //prevent auto ajust column
            var gridWidth = $("#" + self.ID()).width() - 22; //remove width of vertical scrollbar
            if (totalWidth < gridWidth) {
                var visibleColumns = _.where(columns, { hidden: false });
                var lastColumn = _.last(visibleColumns);
                if (!_.isUndefined(lastColumn.command)) {
                    lastColumn.width = widthOfCommandColumn;
                    // Caculate width the pre column
                    var prePosition = _.size(visibleColumns) - 2;
                    var preColumn = visibleColumns[prePosition];
                    if (prePosition != 0) {
                        preColumn.width += (gridWidth - totalWidth - widthOfCommandColumn);
                    }
                } else {
                    lastColumn.width += (gridWidth - totalWidth);
                }
                
            }
        }
    });


    self.EditUrl = ko.observable();
    if (queryUrl == null || queryUrl == "")
        queryUrl = '/Query/ExecuteQuery';

    var getUrl = queryUrl;
    var delUrl = deleteUrl;

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: getUrl,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            },
            destroy: {
                url: delUrl,
                dataType: 'jsonp'
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                } else if (operation == "read") {

                    // Limitation of nested json object; temporarily have to modify the json to
                    // pass in sort information correctly.
                    var result = {
                        pageSize: options.pageSize,
                        skip: options.skip,
                        take: options.take
                    };

                    if (options.sort) {
                        for (var i = 0; i < options.sort.length; i++) {
                            result["sort[" + i + "].field"] = options.sort[i].field;
                            result["sort[" + i + "].dir"] = options.sort[i].dir;
                        }
                    }

                    return result;
                }
            }
        },
        serverPaging: true,
        serverSorting: true,
        pageSize: 50,
        batch: true,
        emptyMsg: emptyGridText,
        change: EmptyGridMessage,
        table: "#" + self.ID(),
        schema: {
            model: {
                id: "Id",
                fields: schemaFields
            },
            data: "Data",
            total: "TotalRowCount"
        }
    });

    self.DataSource(dataSource);
    self.CopyData = function () {
        // Get the item which select in grid
        var selectRowCount = 0;
        var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
        if (selectedRowIds != null) {
            var selectedRowIdArray = selectedRowIds.split(',');
            selectRowCount = selectedRowIdArray.length;
            if (selectRowCount > 1) {
                alert(cannotCopyMess);
                return;
            }
            if (selectRowCount <= 0) {
                return;
            }
            var url = copyUrl + "/" + selectedRowIdArray[0];
            ShowPopup(widthPopup, heightPopup, "Copy item", url, gridID);
        }
    };
    
    self.DeleteMulti = function () {
        if (confirm(deleteAllConfirmMess)) {
            var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
            var isDeleteAll= $(gridID).attr('data-kendo-grid-selected-all');
            var models = {
                selectedRowIdArray: selectedRowIds,
                isDeleteAll: isDeleteAll
            };
            var request = $.ajax({ cache: false, async: false, url: deleteMultiUrl, type: "POST", data: models, dataType: "json" });
            request.done(function (result) {
                if (result.Error === undefined || result.Error === '') {
                    // refresh grid
                    Postbox.messages.notifySubscribers(null, Postbox.REFRESH_READONLY_GRID);
                    
                } else {
                   //Show error with error in result
                }
            });
        }
    };

    self.NewItem = function() {
        ShowPopup(widthPopup, heightPopup, "New Item", createUrl,gridID);
    };

    self.ExportExcel = function () {
        var request = $.ajax({ cache: false, async: false, url: exportExcelUrl, type: "POST", data: {}, dataType: "json" });
        request.done(function (result) {
            if (result.Error === undefined || result.Error === '') {
                // refresh grid
                Postbox.messages.notifySubscribers(null, Postbox.REFRESH_READONLY_GRID);
            } else {
                self.ModelState(new FeedbackViewModel(result.Status, result.Error, result.StackTrace, result.ModelStateErrors));
            }
        });
    };

    self.SearchItem = function () {
        // Create a query for search
        Postbox.messages.notifySubscribers('searchString=' + Base64.encode('<AdvancedQueryParameters>' + self.CreateQueryString() + '</AdvancedQueryParameters>'), Postbox.SELECTED_QUERY_FILTER_APPLIED);
    };
    
    self.CreateQueryString = function () {
        // Create a xml string to parse to query info in server
        return '<SearchTerms>' + self.SearchTerm() + '</SearchTerms>';
    };

    Postbox.messages.subscribe(function (newValue) {
        var kendoGrid = $("#" + self.ID()).data("kendoGrid");
        var searchUrl = '';
        if (getUrl.indexOf("?", 0) == -1) {
            searchUrl =getUrl+ '?' + newValue;
        } else {
            searchUrl = getUrl + '&' + newValue;
        }
        //Set url property of the grid data source
        kendoGrid.dataSource.transport.options.read.url = searchUrl;

        //Read data source to update
        kendoGrid.dataSource.read();
    }, self, Postbox.SELECTED_QUERY_FILTER_APPLIED);
    
    $(gridID).kendoGrid({
        dataSource: dataSource,
        pageable: false,
        sortable: true,
        height: 500,
        columns: columns,
        selectable: "multiple",
        editable: "popup",
        change: onChange,
        scrollable: { virtual: true },
        navigatable: false,
        resizable: true,
        columnResize: changeColumnConfig,
        reorderable: true,
        detailTemplate: (gridChildName != null && gridChildName != "" ? kendo.template($("#template").html()) : null),
        detailInit: (gridChildName != null && gridChildName != "" ? detailInit : null),
        columnReorder: changeColumnPosition,
        columnMenu: self.ShowColumnMenu(),
        columnHide: changeColumnConfig,
        columnShow: changeColumnConfig,
        columnMenuInit: function (e) {
            e.container.find('li.k-item.k-sort-asc[role="menuitem"]').remove();
            e.container.find('li.k-item.k-sort-desc[role="menuitem"]').remove();
            e.container.find('.k-columns-item .k-group').css({ 'width': '200px', 'max-height': '400px' });
            mandatoryFields.forEach(function (field) {
                e.container.find(".k-columns-item[role='menuitem'] input[type='checkbox'][data-field='" + field.Name() + "']").parent().remove();
            });
        },
        dataBound: function (e) {
            if (self.UseCheckBoxColumn()) {
                bindCheckBox();
            }
            resizeGridToExtendLastRow();
        }
    });
    var dataSourceChild = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/" + gridChildName + "/" + "GetDataForGrid",
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                } else if (operation == "read") {

                    // Limitation of nested json object; temporarily have to modify the json to
                    // pass in sort information correctly.
                    var result = {
                        pageSize: options.pageSize,
                        skip: options.skip,
                        take: options.take
                    };

                    if (options.sort) {
                        for (var i = 0; i < options.sort.length; i++) {
                            result["sort[" + i + "].field"] = options.sort[i].field;
                            result["sort[" + i + "].dir"] = options.sort[i].dir;
                        }
                    }

                    return result;
                }
            }
        },
        serverPaging: true,
        serverSorting: true,
        pageSize: 5,
        batch: true,
        emptyMsg: emptyGridText,
        change: EmptyGridMessage,
        table: "#child-" + self.ID(),
        schema: {
            model: {
                id: "Id",
                fields: schemaFields
            },
            data: "Data",
            total: "TotalRowCount"
        }
    });

    function detailInit(e) {
        var detailRow = e.detailRow;

        detailRow.find(".tabstrip").kendoTabStrip({
            animation: {
                open: { effects: "fadeIn" }
            }
        });

        detailRow.find(".list-containers").kendoGrid({
            dataSource: dataSourceChild,
            scrollable: false,
            sortable: true,
            pageable: true,
            columns:columnChilds
        });
    }
    var mandatoryFields = gridConfigViewModel.getMandatoryColumns();
    mandatoryFields.forEach(function (field) {
        $("#" + self.ID() + ">.k-grid-header table>thead").find("[data-field='" + field.Name() + "']>.k-header-column-menu").remove();
    });
    
    // Prevent drag and drop for some mandatory field
    //$("th:nth(0)", gridID).data("kendoDropTarget").destroy();
    $(gridID).data("kendoDraggable").bind("dragstart", function (e) {
        mandatoryFields.forEach(function(field) {
            if (_.isUndefined(e.currentTarget.attr('data-field'))|| e.currentTarget.attr('data-field') === field.Name()) {
                e.preventDefault();
                return;
            }
        });
    });
    function bindCheckBox() {
        $(".k-grid-content tbody tr").each(function () {
            var $tr = $(this);
            var uid = $tr.attr("data-uid");
            var dataEntry;
            $.each(dataSource._data, function (index, item) {
                if (item.uid === uid) {
                    dataEntry = item;
                }
            });
            $tr.addClass('kendo-data-row').attr('data-kendo-grid-rowid', dataEntry.Id);
        });
        var chkAll = $(gridID).attr('data-kendo-grid-selected-all');
        var visibleRows = $(gridID).find('.kendo-data-row');
        if (chkAll == 1) {
            $(visibleRows).each(function () {
                var row = $(this);
                row.find('.check_row').attr('checked', 'checked');
            });
        } else {
            //Mark any selected rows as selected (persists selections from page to page)
            var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
            if (selectedRowIds != null) {
                var selectedRowIdArray = selectedRowIds.split(',');
                $(visibleRows).each(function () {
                    var row = $(this);
                    var rowId = row.attr('data-kendo-grid-rowid');
                    if ($.inArray(rowId, selectedRowIdArray) > -1) {
                        //row.addClass('k-state-selected');
                        row.find('.check_row').attr('checked', 'checked');
                    }
                });
            }
        }

    }
    function onChange(e) {
        e.preventDefault();
        var grid = $(gridID).data("kendoGrid");
        var dataItem = grid.dataItem(grid.select());
        var idRow = dataItem.Id;
        var url = updateUrl + "/" + idRow;
        ShowPopup(widthPopup, heightPopup, "Update item", url, gridID);
    }
    function resizeGridToExtendLastRow() {
        var gridHeight = $("#" + self.ID()).height();
        var insideGridHeight = $("#" + self.ID() + ">.k-grid-content table").height();
        if (gridHeight > insideGridHeight) {
            var numberOfCells = $("#" + self.ID() + ">.k-grid-content table>tbody>tr:last-of-type td").length;
            var row = $("#" + self.ID() + ">.k-grid-content table")[0].insertRow(-1);
            for (var i = 0; i < numberOfCells; i++) {
                row.insertCell(i);
            }
            $("#" + self.ID() + ">.k-grid-content table>tbody>tr:last-of-type td").css("height", gridHeight - insideGridHeight);//.css("vertical-align", "top");
            $(row).hover(
                function () {
                    $(this).css("background-color", "#FFFFFF");
                }
            );
        }
    }

    $(window).resize(function () {
        resizeGridToExtendLastRow();
    });

    function changeColumnConfig(e) {
        gridConfigViewModel.changeColumnConfig(e.column);
        saveGridConfig();
    }

    function changeColumnPosition(e) {
        var grid = $(gridID).data("kendoGrid");
        var direction = 1;
        var start = e.newIndex;
        var end = e.oldIndex;
        if (e.oldIndex < e.newIndex) {
            direction = -1;
            start = e.oldIndex;
            end = e.newIndex;
        }
        //re-order the column in range of effect
        for (var j = start; j <= end; j++) {
            var column = setColumnIndex(grid.columns[j], j + 1 * direction);
            gridConfigViewModel.changeColumnOrder(column);
        }
        //update order of the changed column
        var changedColumn = setColumnIndex(e.column, e.newIndex);
        gridConfigViewModel.changeColumnOrder(changedColumn);
        saveGridConfig();
    }

    function setColumnIndex(column, index) {
        column.index = index;
        return column;
    }
    function saveGridConfig() {

        var gridConfig = ko.toJSON(gridConfigViewModel);
        //Save grid's config
        $.ajax({
            type: "POST",
            url: "/GridConfig/Save",
            contentType: 'application/json; charset=utf-8',
            data: gridConfig,
            dataType: "json",
            success: function (result) {
                gridConfigViewModel.Id(result.Data.Id);
            }
        });
    }
}

function EmptyGridMessage() {
    $(this.options.table).find('#no-records-msg').remove();
    if (this.total() > 0) {
        return;
    }

    $(this.options.table).find('.k-virtual-scrollable-wrap').append('<div id="no-records-msg" style="color: #888; height: 100%; text-align: center; line-height: 100px; text-transform: uppercase;">' + this.options.emptyMsg + '</div>');
}
