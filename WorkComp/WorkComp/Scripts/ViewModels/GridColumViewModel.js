﻿
function GridColumnViewModel(text, name, hidden, width, columnOrder, mandatory) {
    var self = this;

    self.Text = ko.observable(text);
    self.Name = ko.observable(name);
    self.HideColumn = ko.observable(hidden);
    self.ColumnWidth = ko.observable(width);
    self.ColumnOrder = ko.observable(columnOrder);
    self.Mandatory = ko.observable(mandatory);
}