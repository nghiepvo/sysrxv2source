﻿function GridViewModel(id,
                        modelName,
                        schemaFields,
                        columns,
                        searchItem,
                        useCheckBoxColumn,   
                        emptyGridText,
                        userId,
                        documentTypeId,
                        gridInternalName,
                        showColumnMenu,
                        widthPopup,
                        heightPopup,
                        cannotCopyMess,
                        deleteAllConfirmMess,
                        checkIsReadOnlyRow,
                            exporViewModel, canDeleteMulti, canCreateItem, canCopyItem) {
    var widthOfCommandColumn = 90;
    var widthOfCheckboxColumn = 30;
    if (widthPopup == undefined || widthPopup == null) {
        widthPopup = 600;
    }
    if (heightPopup == undefined || heightPopup == null) {
        heightPopup = 800;
    }
    var self = this;
    self.ID = ko.observable(id);
    self.ModelName = ko.observable(modelName);
    self.DataSource = ko.observable();
    self.SearchTerm = ko.observable(searchItem);
    self.UseCheckBoxColumn = ko.observable(useCheckBoxColumn);
    self.ShowColumnMenu = ko.observable(showColumnMenu);
    self.CanDeleteMulti = ko.observable(canDeleteMulti);
    self.CanCreateItem = ko.observable(canCreateItem);
    self.CanCopyItem = ko.observable(canCopyItem);
    var gridConfigViewModel = new GridConfigViewModel(0, userId, documentTypeId, gridInternalName);
    var exportOptionViewModel = exporViewModel;
    exportOptionViewModel.Type('r1000');
    
    // All validation messages to the postbox for the _ValidationSummary to display.
    self.ModelState = ko.observable();
    self.ModelState.subscribe(function (newValue) {
        Postbox.messages.notifySubscribers(newValue, Postbox.VALIDATION_MODEL_STATE);
    });
    var gridID = "#" + self.ID();

    var queryUrl = "/" + modelName + "/" + "GetDataForGrid";
    
    var deleteMultiUrl = "/" + modelName + "/" + "DeleteMulti";
    var createUrl = "/" + modelName + "/" + "Create";
    var updateUrl = "/" + modelName + "/" + "Update";
    var copyUrl = "/" + modelName + "/" + "Copy";
    var exportExcelUrl = "/" + modelName + "/" + "ExportExcel";
    // Get grid config
    $.ajax({
        type: "GET",
        url: "/GridConfig/Get",
        data: { UserId: userId, DocumentTypeId: documentTypeId, GridInternalName: gridInternalName},
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (data) {
            if (_.isNull(data.ViewColumns) || _.isUndefined(data.ViewColumns)) {
                columns.forEach(function(column) {
                    gridConfigViewModel.addColumn(column);
                });
                return;
            }
            gridConfigViewModel.Id(data.Id);
            gridConfigViewModel.importColumnConfigs(data.ViewColumns);

            //Processing Grid Columns Config
            var totalWidth = 0;
            columns.forEach(function (column) {
               
                if (_.isString(column.width))
                    column.width = parseInt(column.width.replace("px", ""));

                var columnConfig = gridConfigViewModel.findViewColumn(column);
                //Get column config
                if (!_.isUndefined(columnConfig) && !_.isNull(columnConfig)) {
                    column.hidden = columnConfig.HideColumn();

                    if (columnConfig.ColumnWidth() > 0) {
                        if (!_.isUndefined(column.field) && column.field.trim() == "IsChecked") {
                            column.width = widthOfCheckboxColumn;
                        }
                        else if (!_.isUndefined(column.command)) {
                            column.width = widthOfCommandColumn;
                        } else {
                            column.width = columnConfig.ColumnWidth();
                        }
                    }
                }
                else {

                    column.hidden = false;
                    gridConfigViewModel.addColumn(column);
                }

                if (_.isUndefined(column.hidden) || !column.hidden) {
                    totalWidth += column.width;
                }
            });

            //sort column order
            columns = _.sortBy(columns, function (column) { return gridConfigViewModel.findViewColumnIndex(column); });

            //work around extend the last column to fit with grid width if total columns width less than grid width
            //prevent auto ajust column
            var gridWidth = $("#" + self.ID()).width() - 22; //remove width of vertical scrollbar
            if (totalWidth < gridWidth) {
                var visibleColumns = _.where(columns, { hidden: false });
                var lastColumn = _.last(visibleColumns);
                if (!_.isUndefined(lastColumn.command)) {
                    lastColumn.width = widthOfCommandColumn;
                    // Caculate width the pre column
                    var prePosition = _.size(visibleColumns) - 2;
                    var preColumn = visibleColumns[prePosition];
                    if (prePosition != 0) {
                        preColumn.width += (gridWidth - totalWidth);
                    }
                } else {
                    lastColumn.width += (gridWidth - totalWidth);
                }
                
            }
            
        }
    });


    self.EditUrl = ko.observable();
    if (queryUrl == null || queryUrl == "")
        queryUrl = '/Query/ExecuteQuery';

    var getUrl = queryUrl;

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: getUrl,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                } else if (operation == "read") {

                    // Limitation of nested json object; temporarily have to modify the json to
                    // pass in sort information correctly.
                    var result = {
                        pageSize: options.pageSize,
                        skip: options.skip,
                        take: options.take
                    };

                    if (options.sort) {
                        for (var i = 0; i < options.sort.length; i++) {
                            result["sort[" + i + "].field"] = options.sort[i].field;
                            result["sort[" + i + "].dir"] = options.sort[i].dir;
                        }
                    }
                    return result;
                }
            }
        },
        serverPaging: true,
        serverSorting: true,
        pageSize: 50,
        batch: true,
        emptyMsg: emptyGridText,
        change: EmptyGridMessage,
        table: "#" + self.ID(),
        schema: {
            model: {
                id: "Id",
                fields: schemaFields
            },
            data: "Data",
            total: "TotalRowCount"
        }
    });

    self.DataSource(dataSource);
    self.CopyData = function () {
        if (!self.CanCopyItem()) {
            return;
        }
        // Get the item which select in grid
        var selectRowCount = 0;
        var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
        if (selectedRowIds != null) {
            var selectedRowIdArray = selectedRowIds.split(',');
            selectRowCount = selectedRowIdArray.length;
            if (selectRowCount > 1) {
                alert(cannotCopyMess);
                return;
            }
            if (selectRowCount <= 0) {
                return;
            }
            var url = copyUrl + "/" + selectedRowIdArray[0];
            ShowPopup(widthPopup, heightPopup, "Copy item", url, gridID);
        }
    };
    
    self.DeleteMulti = function () {
        if (!self.CanDeleteMulti()) {
            return;
        }
        if (confirm(deleteAllConfirmMess)) {
            var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
            var isDeleteAll= $(gridID).attr('data-kendo-grid-selected-all');
            var models = {
                selectedRowIdArray: selectedRowIds,
                isDeleteAll: isDeleteAll
            };
            var request = $.ajax({ cache: false, async: false, url: deleteMultiUrl, type: "POST", data: models, dataType: "json" });
            request.done(function (result) {
                if (result.Error === undefined || result.Error === '') {
                    // refresh grid
                    Postbox.messages.notifySubscribers(null, Postbox.REFRESH_READONLY_GRID);
                    
                } else {
                    //Show error with error in result
                    self.ModelState(new FeedbackViewModel(result.Status, result.Error, result.StackTrace, result.ModelStateErrors));
                }
            });
        }
    };

    self.NewItem = function () {
        if (!self.CanCreateItem()) {
            return;
        }
        ShowPopup(widthPopup, heightPopup, "New Item", createUrl,gridID);
    };
    

    
    exportOptionViewModel.IsExportExcel.subscribe(function (newValue) {
        if (newValue == true) {
            var take = exportOptionViewModel.Take();
            var type = exportOptionViewModel.Type();
            
            if (take == 0) {
                take = 1;
            }
            if (type == 'all') {
                take = dataSource.total();
            }
            var queryInfo = {
                SearchString: self.EncodeQuerySearch(),
                Sort: dataSource.sort(),
                Take: take
            };
            var gridConfig = ko.toJSON(gridConfigViewModel);
            var query = ko.toJSON(queryInfo);

            $.ajax({
                type: "POST",
                url: exportExcelUrl,
                contentType: 'application/json; charset=utf-8',
                data: '{ "gridConfig":' + gridConfig + ', "queryInfo": ' + query + '}',
                dataType: "json",
                success: function (result) {
                    if (result.Error === undefined || result.Error === '') {
                        //exprot excel from html
                        var resultTemp = result.Item.replace(/(\r\n|\n|\r)/gm, "");

                        tableToExcel(resultTemp, "Data");
                        
                    } else {
                        // Show error
                        self.ModelState(new FeedbackViewModel(result.Status, result.Error, result.StackTrace, result.ModelStateErrors));
                    }
                }
            });
            exportOptionViewModel.IsExportExcel(false);
        }
        
    });
    self.ExportExcel = function () {
        
        // Save grid config
        //saveGridConfig();
       
        
    };

   
        
    var tableToExcel = (function() {
        var uri = 'data:application/vnd.ms-excel;base64,',
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body>{table}</body></html>',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g,
                    function(m, p) {
                        return c[p];
        });
    };
        return function(html, name, filename) {
            var ctx = { worksheet: name || 'Worksheet', table: html };
            window.location.href = uri + base64(format(template, ctx));
        };

    })();

    self.KeyPressSearchTerm = function (data, event) {
        if (event.keyCode == 13) {
            self.SearchTerm = ko.observable($("#txt-search").val());
            self.SearchItem();
        }
        return true;
    };
    self.SearchItem = function () {
        // Create a query for search
        Postbox.messages.notifySubscribers('searchString=' + self.EncodeQuerySearch(), Postbox.SELECTED_QUERY_FILTER_APPLIED);
    };
    
    self.EncodeQuerySearch = function () {
        return Base64.encode('<AdvancedQueryParameters>' + self.CreateQueryString() + '</AdvancedQueryParameters>');
    };
    
    self.CreateQueryString = function () {
        // Create a xml string to parse to query info in server
        return '<SearchTerms>' + self.SearchTerm() + '</SearchTerms>';
    };

    Postbox.messages.subscribe(function (newValue) {
        var kendoGrid = $("#" + self.ID()).data("kendoGrid");
        var searchUrl = '';
        if (getUrl.indexOf("?", 0) == -1) {
            searchUrl =getUrl+ '?' + newValue;
        } else {
            searchUrl = getUrl + '&' + newValue;
        }
        //Set url property of the grid data source
        kendoGrid.dataSource.transport.options.read.url = searchUrl;

        //Read data source to update
        kendoGrid.dataSource.read();
    }, self, Postbox.SELECTED_QUERY_FILTER_APPLIED);
    
    $(gridID).kendoGrid({
        dataSource: dataSource,
        sortable: true,
        height: 500,
        columns: columns,
        selectable: "multiple",
        
        //editable: { confirmation: false },
        //Nghiep Fix
        editable: false,
        change: onChange,
        scrollable: { virtual: false },

        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        navigatable: false,
        resizable: true,
        columnResize: changeColumnConfig,
        reorderable: true,
        columnReorder: changeColumnPosition,
        columnMenu: self.ShowColumnMenu(),
        columnHide: changeColumnConfig,
        columnShow: changeColumnConfig,
        columnMenuInit: function (e) {
            e.container.find('li.k-item.k-sort-asc[role="menuitem"]').remove();
            e.container.find('li.k-item.k-sort-desc[role="menuitem"]').remove();
            e.container.find('.k-columns-item .k-group').css({ 'width': '200px', 'max-height': '400px' });
            mandatoryFields.forEach(function (field) {
                e.container.find(".k-columns-item[role='menuitem'] input[type='checkbox'][data-field='" + field.Name() + "']").parent().remove();
            });

        },
        dataBound: function (e) {
            
            if (self.UseCheckBoxColumn()) {
                bindCheckBox();
            }
            resizeGridToExtendLastRow();
            var isHasHiddenColumn = false;
            var totalWidth = 0;
            columns.forEach(function(column) {
                if (_.isUndefined(column.hidden) || !column.hidden) {
                    totalWidth += column.width;
                } else {
                    isHasHiddenColumn = true;
                }
            });
            if (isHasHiddenColumn) {
                $("#" + self.ID() + ">.k-grid-header table").css("width", totalWidth);
                $("#" + self.ID() + ">.k-grid-content table").css("width", totalWidth);
            }

            if (self.SearchTerm() != '') {
                
            }
        }
        
    });
    var mandatoryFields = gridConfigViewModel.getMandatoryColumns();
    mandatoryFields.forEach(function (field) {
        $("#" + self.ID() + ">.k-grid-header table>thead").find("[data-field='" + field.Name() + "']>.k-header-column-menu").remove();
    });
    
    // Prevent drag and drop for some mandatory field
    $("th:nth(0)", gridID).data("kendoDropTarget").destroy();
    $(gridID).data("kendoDraggable").bind("dragstart", function (e) {
        mandatoryFields.forEach(function(field) {
            if (_.isUndefined(e.currentTarget.attr('data-field'))|| e.currentTarget.attr('data-field') === field.Name()) {
                e.preventDefault();
                return;
            }
        });
    });
    function bindCheckBox() {
        //Nghiep Fix Uncheck all when binding data
        $("#chkAll-" + $(gridID).attr("id")).removeAttr('checked');
        $(".k-grid-content tbody tr").each(function () {
            var $tr = $(this);
            var uid = $tr.attr("data-uid");
            var dataEntry;
            $.each(dataSource._data, function (index, item) {
                if (item.uid === uid) {
                    dataEntry = item;
                }
            });
            if (dataEntry != undefined && dataEntry.Id != undefined) {
                $tr.addClass('kendo-data-row').attr('data-kendo-grid-rowid', dataEntry.Id);
                if (checkIsReadOnlyRow == true && dataEntry.IsReadOnly == true) {
                    $tr.addClass('gray');
                    $tr.find('td>input.check_row')[0].remove();
                    $tr.find('td>a.k-button.k-button-icontext')[0].remove();

                }
                else if (dataEntry.IsActive == false) {
                    $tr.addClass('gray');
                }
            }
           
        });
        var chkAll = $(gridID).attr('data-kendo-grid-selected-all');
        
        var visibleRows = $(gridID).find('.kendo-data-row');
        if (chkAll == 1) {
            $(visibleRows).each(function () {
                var row = $(this);
                row.find('.check_row').attr('checked', 'checked');
            });
        } else {
            //Mark any selected rows as selected (persists selections from page to page)
            var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
            if (selectedRowIds != null) {
                var selectedRowIdArray = selectedRowIds.split(',');
                $(visibleRows).each(function () {
                    var row = $(this);
                    var rowId = row.attr('data-kendo-grid-rowid');
                    if ($.inArray(rowId, selectedRowIdArray) > -1) {
                        //row.addClass('k-state-selected');
                        row.find('.check_row').attr('checked', 'checked');
                    }
                });
            }
        }

    }
    function onChange(e) {
        e.preventDefault();
        var grid = $(gridID).data("kendoGrid");
        var dataItem = grid.dataItem(grid.select());
        if (dataItem === undefined || dataItem == null) {
            return;
        }
        if (checkIsReadOnlyRow == true && dataItem.IsReadOnly == true) {
            return;
        }
        var idRow = dataItem.Id;
        if (idRow != null && idRow != 0) {
            var url = updateUrl + "/" + idRow;
            ShowPopup(widthPopup, heightPopup, "Update item", url, gridID);
        }
    }
    function resizeGridToExtendLastRow() {
        
        var gridHeight = $("#" + self.ID()).height()-37;
        var insideGridHeight = $("#" + self.ID() + ">.k-grid-content table").height();
        if (gridHeight > insideGridHeight) {
            var numberOfCells = $("#" + self.ID() + ">.k-grid-content table>tbody>tr:last-of-type td").length;
            var row = $("#" + self.ID() + ">.k-grid-content table")[0].insertRow(-1);
            for (var i = 0; i < numberOfCells; i++) {
                row.insertCell(i);
            }
            $("#" + self.ID() + ">.k-grid-content table>tbody>tr:last-of-type td").css("height", gridHeight - insideGridHeight);//.css("vertical-align", "top");
            $(row).hover(
                function () {
                    $(this).css("background-color", "#FFFFFF");
                }
            );
        }
    }

    $(window).resize(function () {
        resizeGridToExtendLastRow();
    });

    function changeColumnConfig(e) {
        if (checkColums(e.sender.columns)) {
            gridConfigViewModel.changeColumnConfig(e.column);
            saveGridConfig();
        } else {
            location.reload();
        }
    }

    function checkColums(cols) {
        var colsTemp = [];
        for (var i = 0; i < cols.length; i++) {
            if (_.isUndefined(cols[i].hidden) || !cols[i].hidden) {
                colsTemp.push(cols[i]);
            }
        }
        if (colsTemp.length < 3) {
            alert('Can not hide column.');
            return false;
        }
        return true;
    }

    function changeColumnPosition(e) {
        var grid = $(gridID).data("kendoGrid");
        var direction = 1;
        var start = e.newIndex;
        var end = e.oldIndex;
        if (e.oldIndex < e.newIndex) {
            direction = -1;
            start = e.oldIndex;
            end = e.newIndex;
        }
        //re-order the column in range of effect
        for (var j = start; j <= end; j++) {
            var column = setColumnIndex(grid.columns[j], j + 1 * direction);
            gridConfigViewModel.changeColumnOrder(column);
        }
        //update order of the changed column
        var changedColumn = setColumnIndex(e.column, e.newIndex);
        gridConfigViewModel.changeColumnOrder(changedColumn);
        saveGridConfig();
    }

    function setColumnIndex(column, index) {
        column.index = index;
        return column;
    }
    function saveGridConfig() {

        var gridConfig = ko.toJSON(gridConfigViewModel);
        //Save grid's config
        $.ajax({
            type: "POST",
            url: "/GridConfig/Save",
            contentType: 'application/json; charset=utf-8',
            data: gridConfig,
            dataType: "json",
            success: function (result) {
                if (result.Error === undefined || result.Error === '') {
                    gridConfigViewModel.Id(result.Data.Id);
                } else {
                    // Show error
                    self.ModelState(new FeedbackViewModel(result.Status, result.Error, result.StackTrace, result.ModelStateErrors));
                }
            }
        });
    }

}

function EmptyGridMessage() {
    $(this.options.table).find('#no-records-msg').remove();
    if (this.total() > 0) {
        return;
    }

    $(this.options.table).find('.k-virtual-scrollable-wrap').append('<div id="no-records-msg" style="color: #888; height: 100%; text-align: center; line-height: 100px; text-transform: uppercase;">' + this.options.emptyMsg + '</div>');
}
