﻿

var DualSelectParentViewModel = function (modelId, parentSelectUrl, codeText) {

    this.schemaModel = {
        id: "RowGuid",
        fields: {
            KeyID: { editable: false, nullable: false },
            ParentDisplayName: { editable: false, nullable: false, validation: { required: true } },
            Active: { editable: false, nullable: false },
        }
    };
    this.columns = [
       {
           field: "ParentDisplayName",
           title: codeText,
           width: "40px",
           template: '#var displayName=ParentDisplayName;if(Active==false){displayName="<span style=\'color:Gainsboro\'>"+displayName+"</span>";}##= displayName #'
       }
    ];
    this.gridParentViewModel = new GridLookupViewModel("grid-container-available-parent-" + modelId, "", this.schemaModel, this.columns, parentSelectUrl);
    this.getAvailableParentGridControl = $("#grid-container-available-parent-" + modelId).data("kendoGrid");
};

DualSelectParentViewModel.prototype = function () {
    var selectParentAction = function (gridItems) {
        var gridViewDataSource = this.gridParentViewModel.DataSource();
        _.each(gridItems, function (value, key) {
            var dataItem = gridViewDataSource.get(value.KeyID);
            eval(gridViewDataSource.remove(dataItem));
        });
    },
    removeParentAction = function (seletedNodes) {
        var gridViewDataSource = this.gridParentViewModel.DataSource();
        _.each(seletedNodes, function (value, key) {
            eval(gridViewDataSource.add(value));
        });
        gridViewDataSource.sort({ field: "ParentDisplayName", dir: "asc" });
    };
    return {
        selectParentAction: selectParentAction,
        removeParentAction: removeParentAction
    }
}();