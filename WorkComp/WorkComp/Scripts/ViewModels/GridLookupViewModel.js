﻿/*
This view model is to support the mix combination between normal lookup but display in grid format
It also support searching and parentfilter.
*/
function GridLookupViewModel(id,
                        modelName,
                        schemaFields,
                        columns,                                              
                        queryUrl) {
    var self = this;

    self.ID = ko.observable(id);
    self.ModelName = ko.observable(modelName);
    self.DataSource = ko.observable();

    var parentItems = new Array();
    self.ParentItems = ko.observableArray(parentItems);    
    self.GetUrl = ko.observable(queryUrl);
    self.SearchTerm = ko.observable("");
    self.FilteredByParent = ko.observable(true);

    Postbox.messages.subscribe(function(newValue) {
        var kendoGrid = $("#" + self.ID()).data("kendoGrid");

        self.SearchTerm(newValue);        
        

        //Read data source to update
        kendoGrid.dataSource.read();        
    }, self, "GridLookupFilter_SearchTerm");

    Postbox.messages.subscribe(function (newValue) {
        var kendoGrid = $("#" + self.ID()).data("kendoGrid");

        self.FilteredByParent(newValue);      

        //Read data source to update
        kendoGrid.dataSource.read();
    }, self, "GridLookupFilter_FilteredByParent");

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: self.GetUrl(),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            },
            parameterMap: function (options, operation) {
                
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                } else if (operation == "read") {                   
                    
                    // Limitation of nested json object; temporarily have to modify the json to
                    // pass in sort information correctly.
                    var result = {
                        query: self.SearchTerm(),
                        ParentItems: (self.FilteredByParent()) ? JSON.stringify(self.ParentItems()): null,                       
                        take: 150
                    };
                  
                    return result;
                }
            }
        },
        serverPaging: false,
        serverSorting: false,       
        pageSize: 150,
        batch: true,        
        table: "#" + self.ID(),
        schema: {
            model: {
                id: "KeyID",
                fields: schemaFields
            },
            data: "Data",
            total: "TotalRowCount"
        }
    });

    self.DataSource(dataSource);

    $("#" + self.ID()).kendoGrid({
        dataSource: dataSource,
        selectable: "multiple",
        pageable: false,
        sortable: {
            mode: "multiple",
            allowUnsort: true
        },
        columns: columns,
        editable: false,        
        scrollable: {
            virtual: false
        },
        change:onChange
       
    });

    function onChange(arg) {
        var grid = $("#" + self.ID()).data("kendoGrid");        
        Postbox.messages.notifySubscribers(grid.select(), self.ID() + "_SELECTION");
    }
}
