function ShowPopup(w_f, h_f, type, url,gridId) {
    var w = screen.width;
    var h = screen.height - 100;
    var l = (w - w_f) / 2;
    var t = (h - h_f) / 2;
    if (window.showModalDialog) {
        var result = window.showModalDialog(url, type.replaceAll("-", ""), "dialogWidth:" + w_f + "px;dialogHeight:" + h_f + "px");
        if (result == 1) {
            $(gridId).data('kendoGrid').dataSource.read();
            $(gridId).data('kendoGrid').refresh();
        }
        ReturnValue(type, result);

    } else {

        var myWindow = window.open(url, type.replaceAll("-", ""), 'width=' + w_f + ',height=' + h_f + ',left=' + l + ',top=' + t + ',resizable=yes,location=no ,modal=yes');
        if (myWindow) myWindow.focus();
    }
}