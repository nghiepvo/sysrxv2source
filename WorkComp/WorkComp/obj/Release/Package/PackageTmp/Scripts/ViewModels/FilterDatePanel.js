﻿
function FilterDatePanel(type, isShow, element, elementStartDate, elementEndDate, customTime, elementPre, elementNex) {
    var self = this;
    var elementBinding = element,
        elementStartDateBinding = elementStartDate,
        elementEndDateBinding = elementEndDate,
            elementPrevious = elementPre, 
            elementNext = elementNex;
    self.filterItemResult = {
        Field: 'DueDate',
        FieldTypeString: "datetime",
        LogicString: 'And',
        FilterType:'all',
        FieldFilters: [{ OperatorString: '', Value: '' }]
    };

    self.SelectedLable = ko.observable('All Time');
    self.CustomStartDate = ko.observable('');
    self.StartDateForFilter = ko.observable('');
    self.CustomStartDate.subscribe(function (val) {
        self.filterItemResult = {
            Field: 'DueDate',
            FieldTypeString: "datetime",
            LogicString: 'And',
            FilterType:self.Type(),
            FieldFilters: [
                { OperatorString: '', Value: val + ' 00:00' },
                { OperatorString: '', Value: self.CustomEndDate() + ' 23:55' }
            ]
        };
        if (self.Type() == 'custom') {
            self.SelectedLable('Custom');
        }
        $(elementStartDateBinding).data('kendoDatePicker').value(val);

    });
    self.CustomEndDate = ko.observable('');
    self.CustomEndDate.subscribe(function (val) {
        self.filterItemResult = {
            Field: 'DueDate',
            FieldTypeString: "datetime",
            LogicString: 'And',
            FilterType: self.Type(),
            FieldFilters: [
                { OperatorString: '', Value: self.CustomStartDate() + ' 00:00' },
                { OperatorString: '', Value: val + ' 23:55' }
            ]
        };
        if (self.Type() == 'custom') {
            self.SelectedLable('Custom');
        }
        $(elementEndDateBinding).data('kendoDatePicker').value(val);
    });
    self.CurrentSelectedLable = ko.observable('All Time');

    self.SetCurrentSelectedLableWithType = function(t) {
        switch (t) {
        case 'all':
            self.CurrentSelectedLable('All Time');
            break;
        case 'today':
            self.CurrentSelectedLable('Today');
            break;
        case 'thisweek':
            self.CurrentSelectedLable('This Week');
            break;
        case 'lastweek':
            self.CurrentSelectedLable('Last Week');
            break;
        case 'thismonth':
            self.CurrentSelectedLable('This Month');
            break;
        case 'lastmonth':
            self.CurrentSelectedLable('Last Month');
            break;
        case 'custom':
            self.CurrentSelectedLable('Custom');
            break;
        default:
            self.CurrentSelectedLable('');
        }
    };

    self.CurrentType = ko.observable('all');
    self.Type = ko.observable(type);
    self.Type.subscribe(function (val) {
        var date = new Date();
        var curentDayOfWeek, dateFirstDayOfWeek;
        switch (val) {
            case 'all':
                self.SelectedLable('All Time');
                self.filterItemResult = {
                    Field: 'DueDate',
                    FieldTypeString: "datetime",
                    LogicString: 'And',
                    FilterType: self.Type(),
                    FieldFilters: [{OperatorString:'', Value: ''}]
                };
                break;
            case 'today':
                self.SelectedLable('Today');
                self.filterItemResult = {
                    Field: 'DueDate',
                    FieldTypeString: "datetime",
                    LogicString: 'And',
                    FilterType: self.Type(),
                    FieldFilters: [
                        { OperatorString: '', Value: date.toString('MM/dd/yyyy') + ' 00:00' },
                        { OperatorString: '', Value: date.toString('MM/dd/yyyy') + ' 23:55' }
                    ]
                };
                break;
            case 'thisweek':
                self.SelectedLable('This Week');
                curentDayOfWeek = date.getDay();
                if (curentDayOfWeek != 1) {
                    dateFirstDayOfWeek = date.addDays(-(curentDayOfWeek - 1));
                } else {
                    dateFirstDayOfWeek = date;
                }

                self.filterItemResult = {
                    Field: 'DueDate',
                    FieldTypeString: "datetime",
                    LogicString: 'And',
                    FilterType: self.Type(),
                    FieldFilters: [
                        { OperatorString: '', Value: dateFirstDayOfWeek.toString('MM/dd/yyyy') + ' 00:00' },
                        { OperatorString: '', Value: dateFirstDayOfWeek.addDays(6).toString('MM/dd/yyyy') + ' 23:55' }
                    ]
                };

                break;
            case 'lastweek':
                self.SelectedLable('Last Week');
                curentDayOfWeek = date.getDay();
                if (curentDayOfWeek != 1) {
                    dateFirstDayOfWeek = date.addDays(-(curentDayOfWeek +6));
                } else {
                    dateFirstDayOfWeek = date.addDays(-7);
                }
                self.filterItemResult = {
                    Field: 'DueDate',
                    FieldTypeString: "datetime",
                    LogicString: 'And',
                    FilterType: self.Type(),
                    FieldFilters: [
                        { OperatorString: '', Value: dateFirstDayOfWeek.toString('MM/dd/yyyy') + ' 00:00' },
                        { OperatorString: '', Value: dateFirstDayOfWeek.addDays(6).toString('MM/dd/yyyy') + ' 23:55' }
                    ]
                };
                break;
            case 'thismonth':
                self.SelectedLable('This Month');
                var startDateTemp = new Date(date.getFullYear(), date.getMonth(), 1);
                var endDateTemp = new Date(startDateTemp.getFullYear(), startDateTemp.getMonth(), startDateTemp.getDaysInMonth());
                self.filterItemResult = {
                    Field: 'DueDate',
                    FieldTypeString: "datetime",
                    LogicString: 'And',
                    FilterType: self.Type(),
                    FieldFilters: [
                        { OperatorString: '', Value: startDateTemp.toString('MM/dd/yyyy') + ' 00:00' },
                        { OperatorString: '', Value: endDateTemp.toString('MM/dd/yyyy') + ' 23:55' }
                    ]
                };
                break;
            case 'lastmonth':
                var startDateTemp = new Date(date.getFullYear(), date.getMonth()-1, 1);
                var endDateTemp = new Date(startDateTemp.getFullYear(), startDateTemp.getMonth(), startDateTemp.getDaysInMonth());
                self.SelectedLable('Last Month');
                self.filterItemResult = {
                    Field: 'DueDate',
                    FieldTypeString: "datetime",
                    LogicString: 'And',
                    FilterType: self.Type(),
                    FieldFilters: [
                        { OperatorString: '', Value: startDateTemp.toString('MM/dd/yyyy') + ' 00:00' },
                        { OperatorString: '', Value: endDateTemp.toString('MM/dd/yyyy')  + ' 23:55' }
                    ]
                };
                break;
            case 'custom':
                self.filterItemResult = {
                    Field: 'DueDate',
                    FieldTypeString: "datetime",
                    LogicString: 'And',
                    FilterType: self.Type(),
                    FieldFilters: [
                        { OperatorString: '', Value: self.CustomStartDate() + ' 00:00' },
                        { OperatorString: '', Value: self.CustomEndDate() + ' 23:55' }
                    ]
                };
                self.SelectedLable('Custom');
                break;
            default:
                break;
        }
    });
    self.ShowContent = ko.observable(isShow);
    self.ShowContent.subscribe(function () {
        self.SelectedLable(self.CurrentSelectedLable());
        self.Type(self.CurrentType());
    });


    self.ShowExportOption = function () {
        var temp = self.ShowContent();
        self.ShowContent(!temp);
    };

    
    
    self.init = function () {

        function startChange() {
            var startDate = start.value(),
            endDate = end.value();

            if (startDate) {
                startDate = new Date(startDate);
                startDate.setDate(startDate.getDate());
                end.min(startDate);
            } else if (endDate) {
                start.max(new Date(endDate));
            } else {
                endDate = new Date();
                start.max(endDate);
                end.min(endDate);
            }
            
            self.CustomStartDate(start.value().toString('MM/dd/yyyy'));
        }

        function endChange() {
            var endDate = end.value(),
            startDate = start.value();

            if (endDate) {
                endDate = new Date(endDate);
                endDate.setDate(endDate.getDate());
                start.max(endDate);
            } else if (startDate) {
                end.min(new Date(startDate));
            } else {
                endDate = new Date();
                start.max(endDate);
                end.min(endDate);
            }

            self.CustomEndDate(end.value().toString('MM/dd/yyyy'));
        }

        var start = $(elementStartDateBinding).kendoDatePicker({
            change: startChange
        }).data("kendoDatePicker");

        var end = $(elementEndDateBinding).kendoDatePicker({
            change: endChange
        }).data("kendoDatePicker");

        initRandDatePicker();

        start.max(end.value());
        end.min(start.value());
    };

    function initRandDatePicker() {
        var date = new Date();
        var dateToString = date.toString('MM/dd/yyyy');
        self.CustomEndDate(dateToString);
        var curentDate = date.getDay();
        if (curentDate != 1) {
            var dateAddDays = date.addDays(-(curentDate - 1));
            self.CustomStartDate(dateAddDays.toString('MM/dd/yyyy'));
        } else {
            self.CustomStartDate(dateToString);
        }
    }

    self.Cancel = function() {
        self.ShowContent(false);
    };
    self.FlagClickFilter = ko.observable(false);
    self.Filter = function () {
        self.SetDataForFilter();
        Postbox.messages.notifySubscribers(self.filterItemResult, Postbox.RETURN_VALUE_FOR_FILTER);
        self.CurrentSelectedLable(self.SelectedLable());
        self.CurrentType(self.Type());
        self.ShowContent(false);
        self.FlagClickFilter(true);
        self.HandleToolTip();
    };

    self.SetDataForFilter = function() {
        if (self.Type() == 'custom') {
            self.SelectedLable("Custom");
        } else if (self.Type() == 'all') {
            self.filterItemResult = {
                Field: 'DueDate',
                FieldTypeString: "datetime",
                LogicString: 'And',
                FilterType: self.Type(),
                FieldFilters: [{ OperatorString: '', Value: '' }]
            };
        }
        if (self.filterItemResult.FieldFilters.length == 2) {
            self.StartDateForFilter(self.filterItemResult.FieldFilters[0].Value.substring(0, 10));
        } else {
            self.StartDateForFilter('');
        }
        return self.filterItemResult;

    };

    self.HandleToolTip = function () {
        
        var content = '';
        var datakendoTooltip = $(elementNext).data('kendoTooltip');
        if (!_.isUndefined(datakendoTooltip) && datakendoTooltip != null) {
            if (self.filterItemResult.FieldFilters.length == 2) {
                content = self.filterItemResult.FieldFilters[1].Value.substring(0, 10);
            }
            datakendoTooltip.options.content = content == '' ? 'ALL' : content;
            datakendoTooltip.refresh();
        }
        
        datakendoTooltip = $(elementPrevious).data('kendoTooltip');
        if (!_.isUndefined(datakendoTooltip) && datakendoTooltip != null) {
            if (self.filterItemResult.FieldFilters.length == 2) {
                content = self.filterItemResult.FieldFilters[0].Value.substring(0, 10);
            }
            datakendoTooltip.options.content = content == '' ? 'ALL' : content;
            datakendoTooltip.refresh();
        }
    }

    function preOrNextFilter(number) {
        var date;
        var startDate = self.StartDateForFilter();
        if (startDate == '') {
            date = new Date();
        } else {
            var dayTemp = startDate.split("/");
            date = new Date(dayTemp[2], dayTemp[0] - 1, dayTemp[1]);
        }
        var dateFirstDayOfWeek;
        var curentDayOfWeek;
        if (self.Type() == 'today') {
            var dateTemp = date.addDays(number);
            self.filterItemResult = {
                Field: 'DueDate',
                FieldTypeString: "datetime",
                LogicString: 'And',
                FilterType: self.Type(),
                FieldFilters: [
                    { OperatorString: '', Value: dateTemp.toString('MM/dd/yyyy') + ' 00:00' },
                    { OperatorString: '', Value: dateTemp.toString('MM/dd/yyyy') + ' 23:55' }
                ]
            };
            self.Filter();
        }
        else if (self.Type() == 'thisweek' || self.Type() == 'lastweek') {
            curentDayOfWeek = date.getDay();
            if (curentDayOfWeek != 1) {
                dateFirstDayOfWeek = date.addDays(-(curentDayOfWeek - 1));
            } else {
                dateFirstDayOfWeek = date;
            }
            dateFirstDayOfWeek = dateFirstDayOfWeek.addWeeks(number);
            self.filterItemResult = {
                Field: 'DueDate',
                FieldTypeString: "datetime",
                LogicString: 'And',
                FilterType: self.Type(),
                FieldFilters: [
                    { OperatorString: '', Value: dateFirstDayOfWeek.toString('MM/dd/yyyy') + ' 00:00' },
                    { OperatorString: '', Value: dateFirstDayOfWeek.addDays(6).toString('MM/dd/yyyy') + ' 23:55' }
                ]
            };
            self.Filter();
        }
        else if (self.Type() == 'thismonth' || self.Type() == 'lastmonth') {
            var startDateTemp = new Date(date.getFullYear(), date.getMonth() + number, 1);
            var endDateTemp = new Date(startDateTemp.getFullYear(), startDateTemp.getMonth(), startDateTemp.getDaysInMonth());
            self.filterItemResult = {
                Field: 'DueDate',
                FieldTypeString: "datetime",
                LogicString: 'And',
                FilterType: self.Type(),
                FieldFilters: [
                    { OperatorString: '', Value: startDateTemp.toString('MM/dd/yyyy') + ' 00:00' },
                    { OperatorString: '', Value: endDateTemp.toString('MM/dd/yyyy') + ' 23:55' }
                ]
            };
            self.Filter();
        }
        else if (self.Type() == 'custom') {
            var startDateCustom = Date.parse(self.filterItemResult.FieldFilters[0].Value.substring(0, 10));
            var endDateCustom = Date.parse(self.filterItemResult.FieldFilters[1].Value.substring(0, 10));
            var rank = endDateCustom - startDateCustom;
            if (number == 1) {
                startDateCustom = startDateCustom.addMilliseconds(rank);
                endDateCustom = endDateCustom.addMilliseconds(rank);
            } else {
                startDateCustom = startDateCustom.addMilliseconds(-rank);
                endDateCustom = endDateCustom.addMilliseconds(-rank);
            }

            self.filterItemResult = {
                Field: 'DueDate',
                FieldTypeString: "datetime",
                LogicString: 'And',
                FilterType: self.Type(),
                FieldFilters: [
                    { OperatorString: '', Value: startDateCustom.toString('MM/dd/yyyy') + ' 00:00' },
                    { OperatorString: '', Value: endDateCustom.toString('MM/dd/yyyy') + ' 23:55' }
                ]
            };
            self.Filter();
        }
    }

    self.TitlePreFilter = ko.observable('All');
    self.PreFilter = function () {
        preOrNextFilter(-1);
    };
    self.TitleNextFilter = ko.observable('All');
    self.NextFilter = function() {
        preOrNextFilter(1);
    };
    $(customTime).bind('click', function() {
        self.Type('custom');
    });
   
    $(elementBinding).click(function (e) {
        e.stopPropagation(); // This is the preferred method.
    });
    $(elementNext).kendoTooltip({
        position: "top",
        width: 90
    });
    $(elementPrevious).kendoTooltip({
        position: "top",
        width: 90
    });
    
}