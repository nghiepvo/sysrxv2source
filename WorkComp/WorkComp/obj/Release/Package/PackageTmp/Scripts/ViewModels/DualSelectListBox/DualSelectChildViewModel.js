﻿

var DualSelectChildViewModel = function (modelId, childSelectUrl, codeText) {
    this.schemaModel = {
        id: "RowGuid",
        fields: {
            KeyID: { editable: false, nullable: false },
            ChildDisplayName: { editable: false, nullable: false, validation: { required: true } },
            Active: { editable: false, nullable: false },
        }
    };

    this.columns = [
       {
           field: "ChildDisplayName",
           title: codeText,
           width: "40px",
           template: '#var displayName=ChildDisplayName;##= displayName #'
       }
    ];
    this.gridChildViewModel = new GridLookupViewModel("grid-container-available-child-" + modelId, "", this.schemaModel, this.columns, childSelectUrl);
    this.getAvailableChildGridControl = $("#grid-container-available-child-" + modelId).data("kendoGrid");
};

DualSelectChildViewModel.prototype = function () {
    var selectChildAction = function (gridItems) {
        var gridChildViewModel = this.gridChildViewModel;
        var gridViewDataSource = gridChildViewModel.DataSource();
        _.each(gridItems, function (value, key) {
            var dataItem = gridViewDataSource.get(value.KeyID);
            eval(gridViewDataSource.remove(dataItem));
        });
    },
        removeChildAction = function (seletedNodes) {
            var gridChildViewModel = this.gridChildViewModel;
            var gridViewDataSource = gridChildViewModel.DataSource();
            _.each(seletedNodes, function (value, key) {

                var canAdd = true;
                var data = gridViewDataSource._data;

                for (item in data) {
                    try {
                        if (data[item].ChildDisplayName == value.ChildDisplayName) {
                            canAdd = false;
                        }
                    } catch (ex) {

                    }
                }

                if (canAdd) {
                    eval(gridViewDataSource.add(value));
                }
            });

            gridViewDataSource.sort({ field: "ChildDisplayName", dir: "asc" });
        };
    return {
        selectChildAction: selectChildAction,
        removeChildAction: removeChildAction
    };
}();