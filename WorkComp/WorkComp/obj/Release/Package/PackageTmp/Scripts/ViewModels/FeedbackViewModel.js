﻿
function FeedbackViewModel(status, message, description, modelStateArray) {
    var self = this;

    self.Status = ko.observable(status); // This is values such as "success", error", "critical"
    self.Message = ko.observable(message); // Primary Message that gets displayed
    self.Description = ko.observable(description); // Secondary message which gets displayed, should contain the debug info.
    self.ModelState = ko.observableArray(modelStateArray); // Contains an array of validation errors on the current form.
}