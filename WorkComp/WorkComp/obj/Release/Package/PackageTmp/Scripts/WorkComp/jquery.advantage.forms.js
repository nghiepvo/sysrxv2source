(function ($) {
    $.fn.Save = function (url, data, success, failure, createMode, modelName, showPopupForComboBox) {
        CreateLoading();
        if (createMode === undefined) {
            createMode = false;
        }
        if (showPopupForComboBox === undefined) {
            showPopupForComboBox = '';
        }
        if (modelName === undefined) {
            modelName = '';
        }
        var request = $.ajax({ cache: false, async: false, url: url, type: "POST", data: data, dataType: "json" });
        request.done(function (result) {
            if (result.Error === undefined || result.Error === '') {
                $('form').removeClass('dirty');
                EnableCreateFooterButton(false);
                if (!createMode) {
                    $('#LastModified').val(result.Data.LastModified);
                }
                if (showPopupForComboBox == 'ComboBoxForTreating') {
                    modelName = "TreatingPhysician";
                }
                if ((showPopupForComboBox == 'ComboBox' || showPopupForComboBox == 'ComboBoxForTreating') && modelName != '') {
                    var displayName = GetSharedViewModel().ComboBoxDisplayName();
                    if (modelName == "NpiNumber") {
                        displayName = GetSharedViewModel().ComboBoxDisplayNameForNpiNumber();
                    }
                    var keyID = 0;
                    if (createMode) {
                        keyID = result;
                    } else {
                        keyID = GetSharedViewModel().Id();
                    }
                    var returnValue = new ReturnViewModel(modelName, keyID, displayName);
                    $('form').Close(returnValue);
                } else {
                    success(result);
                }
            } else {
                failure(result);
            }
            DeleteLoading();
        });
    };

    $.fn.Delete = function (url, data, title, msg, no, yes, failure) {
        if (confirm(msg)) {
            var request = $.ajax({ cache: false, async: false, url: url, type: "POST", data: data, dataType: "json" });
            request.done(function (result) {
                if (result.Error === undefined || result.Error === '') {
                    $('form').removeClass('dirty');
                    Postbox.messages.notifySubscribers(null, Postbox.FOOTER_ACTION_CLOSE);
                } else {
                    failure(result);
                }
            });
        }
    };

    $.fn.Close = function (returnValue) {
       
        ClosePopup(returnValue);
    };
    $.fn.MakeReadOnly = function (isReadOnly) {
        if (isReadOnly == 'True') {
            $('input, select, textarea, button:not([class="win-command"])').attr('disabled', 'disabled');
        }
    };
}(jQuery));

// Handle ajaxError for authentication

$(document).ajaxError(function (xhr, props) {
    
    if (props.status === 403) {
        location.reload();
    }
});