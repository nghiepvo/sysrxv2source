﻿using System.Net;
using Framework.Service.Translation;
using Framework.Utility;

namespace WorkComp.Models.SendMailWithDualGrid
{
    public class SendMailWithDualGridViewModel:ViewModelBase
    {
        public int ParentId { get; set; }
        private string _emailTitle;

        public string EmailTitle
        {
            get { return string.IsNullOrEmpty(_emailTitle) || string.IsNullOrEmpty(ClaimNumber) ? "" : WebUtility.HtmlDecode(_emailTitle).RemoveHtml() + "- Claim Number: " + ClaimNumber; }
            set { _emailTitle = value; }
        }
        public string Claimant { get; set; }
        public string ClaimNumber { get; set; }
        private string _includeContent;
        public string IncludeContent {
            get
            {
                return string.IsNullOrEmpty(Claimant) ? _includeContent : string.Format(SystemMessageLookup.GetMessage("IncludeContentHtmlAttachment"), Claimant.Replace(" ", "_") + "_Result.pdf");
            }
            set { _includeContent = value; } 
        }

        public string FileName
        {
            get { return string.IsNullOrEmpty(Claimant) ? "" : Claimant.Replace(" ", "_") + "_Result.pdf"; }
        }

        public string ModelName { get; set; }
    }
}