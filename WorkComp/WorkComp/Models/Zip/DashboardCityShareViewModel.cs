﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Zip
{
    public class DashboardZipShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public string Extention { get; set; }
        public int? CityId { get; set; }
        public LookupItemVo CityDataSource { get; set; }
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
    }
}