﻿namespace WorkComp.Models.Zip
{
    public class DashboardZipDataViewModel:
        MasterfileViewModelBase<Framework.DomainModel.Entities.Zip>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardZipShareViewModel>(parameters);
        }
    }
}