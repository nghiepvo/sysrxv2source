﻿namespace WorkComp.Models
{
    public class EditableGridViewModel : ViewModelBase
    {
        public string GridInternalName { get; set; }
        public string GridId { get; set; }
        public string ModelName { get; set; }
    }
}