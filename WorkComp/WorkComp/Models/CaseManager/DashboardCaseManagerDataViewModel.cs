﻿namespace WorkComp.Models.CaseManager
{
    public class DashboardCaseManagerDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.CaseManager>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardCaseManagerShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Case Manager" : "Update Case Manager";
            }
        }
    }
}