﻿using System;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.CaseManager
{
    public class DashboardCaseManagerShareViewModel : DashboardSharedViewModel
    {
        public int? ClaimNumberId { get; set; }
        public LookupItemVo ClaimNumberDataSource { get; set; }

        public string Name { get; set; }
        public string Agency { get; set; }

        public string Phone { get; set; }

        
        public string Email { get; set; }
        public string Fax { get; set; }

        public string SpecialInstructions { get; set; }
        public string Address { get; set; }

        public int? CityId { get; set; }
        public LookupItemVo CityDataSource { get; set; }
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
        public int? ZipId { get; set; }
        public LookupItemVo ZipDataSource { get; set; }
    }
}