﻿namespace WorkComp.Models.PanelType
{
    public class DashboardPanelTypeIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.PanelType>
    {
        public override string PageTitle
        {
            get
            {
                return "Panel Type";
            }
        }
    }
}