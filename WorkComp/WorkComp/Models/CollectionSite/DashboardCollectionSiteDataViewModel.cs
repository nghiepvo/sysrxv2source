﻿namespace WorkComp.Models.CollectionSite
{
    public class DashboardCollectionSiteDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.CollectionSite>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardCollectionSiteShareViewModel>(parameters);
        }
        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Collection Site" : "Update Collection Site";
            }
        }
    }
}