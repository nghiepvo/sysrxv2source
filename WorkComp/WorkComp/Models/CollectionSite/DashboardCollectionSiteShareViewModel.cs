﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.CollectionSite
{
    public class DashboardCollectionSiteShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string CollectionHour { get; set; }
        public string LunchHour { get; set; }
        public string LocationIdentified { get; set; }
        public string CostInformation { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public LookupItemVo CityDataSource { get; set; }
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
        public int? ZipId { get; set; }
        public LookupItemVo ZipDataSource { get; set; }
        public bool Contracted { get; set; }
    }
}