﻿namespace WorkComp.Models.CollectionSite
{
    public class DashboardCollectionSiteIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.CollectionSite>
    {
        public override string PageTitle
        {
            get
            {
                return "Collection Site";
            }
        }
    }
    
}