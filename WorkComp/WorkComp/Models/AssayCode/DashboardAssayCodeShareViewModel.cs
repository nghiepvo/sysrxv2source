﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.AssayCode
{
    public class DashboardAssayCodeShareViewModel : DashboardSharedViewModel
    {
        public int? AssayCodeDescriptionId { get; set; }
        public LookupItemVo AssayCodeDescriptionDataSource { get; set; }
        public int? SampleTestingTypeId { get; set; }
        public LookupItemVo SampleTestingTypeDataSource { get; set; }
        public string Code { get; set; }
        
    }
}