﻿namespace WorkComp.Models.AssayCode
{
    public class DashboardAssayCodeIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.AssayCode>
    {
        public override string PageTitle
        {
            get
            {
                return "Assay Code";
            }
        }
    }
    
}