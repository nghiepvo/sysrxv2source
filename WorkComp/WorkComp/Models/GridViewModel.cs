﻿using System.Collections.Generic;
using Framework.DomainModel.Entities.Common;

namespace WorkComp.Models
{
    /// <summary>
    /// Grid view model
    /// </summary>
    public class GridViewModel : ViewModelBase
    {
        public GridViewModel()
        {
            CanDeleteMulti = true;
            CanCreateItem = true;
            CanCopyItem = true;
        }
        /// <summary>
        /// Grid id to distinguish grid in page
        /// </summary>
        public string GridId { get; set; }
        /// <summary>
        /// Model name to get data
        /// </summary>
        public string ModelName { get; set; }
        /// <summary>
        /// Grid internal name( use in grid in grid)
        /// </summary>
        public string GridInternalName { get; set; }
        /// <summary>
        /// Advance search url
        /// </summary>
        public string AdvancedSearchUrl { get; set; }
        /// <summary>
        /// Include/Exclude active item in grid
        /// </summary>
        public bool ExcludeFilterActiveRecords { get; set; }
        /// <summary>
        /// List column in grid
        /// </summary>
        public IList<ViewColumnViewModel> ViewColumns { get; set; }
        /// <summary>
        /// Grid use checkbox
        /// </summary>
        public bool UseCheckBoxColumn { get; set; }
        /// <summary>
        /// Grid use delete column
        /// </summary>
        public bool UseDeleteColumn { get; set; }
        /// <summary>
        /// Height for popup when click from grid
        /// </summary>
        public int HeightPopup { get; set; }
        /// <summary>
        /// Width for popup when click from grid
        /// </summary>
        public int WidthPopup { get; set; }
        /// <summary>
        /// Use for grid has child
        /// </summary>
        public string GridChildName { get; set; }
        public IList<ViewColumnViewModel> ViewColumnChilds { get; set; }
        /// <summary>
        /// Check grid have row which is read only( use for template task grid)
        /// </summary>
        public bool CheckIsReadOnlyRow { get; set; }

        public bool CanDeleteMulti { get; set; }
        public bool CanCreateItem { get; set; }
        public bool CanCopyItem { get; set; }

        public string UserCustomTemplate { get; set; }

        public TypeWithUserQueryEnum TypeWithUser { get; set; }
    }

    /// <summary>
    /// Column view model
    /// </summary>
    public class ViewColumnViewModel
    {
        public ViewColumnViewModel()
        {
            Sortable = true;
            IsHtmlEncode = false;
            IsBoolValue = false;
        }
        /// <summary>
        /// Column header text
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Column databind
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// This column will be link to navigator
        /// </summary>
        public bool Navigator { get; set; }
        /// <summary>
        /// Column width
        /// </summary>
        public int ColumnWidth { get; set; }
        /// <summary>
        /// Column justificate (Left, right)
        /// </summary>
        public GridColumnJustification ColumnJustification { get; set; }
        /// <summary>
        /// Column format( format with number, text, decimal...)
        /// </summary>
        public string ColumnFormat { get; set; }
        /// <summary>
        /// Hide or show column
        /// </summary>
        public bool HideColumn { get; set; }
        /// <summary>
        /// Position column in grid
        /// </summary>
        public int ColumnOrder { get; set; }
        /// <summary>
        /// This is madatory column or not( user cannot move, resize, show/hide)
        /// </summary>
        public bool Mandatory { get; set; }

        public bool Sortable { get; set; }

        public string CustomTemplate { get; set; }

        public bool IsHtmlEncode { get; set; }

        public bool IsBoolValue { get; set; }

    }
    public enum GridColumnJustification
    {
        Left,
        Right,
        Center
    }
}