﻿namespace WorkComp.Models.ProductType
{
    public class DashboardProductTypeIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.ProductType>
    {
        public override string PageTitle
        {
            get
            {
                return "Product Type";
            }
        }
    }
}