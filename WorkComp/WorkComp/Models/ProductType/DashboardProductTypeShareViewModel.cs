﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.ProductType
{
    public class DashboardProductTypeShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public int? PayerId { get; set; }
        public LookupItemVo PayerDataSource { get; set; }
    }
}