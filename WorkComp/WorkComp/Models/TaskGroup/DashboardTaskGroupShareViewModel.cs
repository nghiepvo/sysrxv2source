﻿using System.Collections.Generic;

namespace WorkComp.Models.TaskGroup
{
    public class DashboardTaskGroupShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public List<int> TaskTemplateIds { get; set; } 
    }
}