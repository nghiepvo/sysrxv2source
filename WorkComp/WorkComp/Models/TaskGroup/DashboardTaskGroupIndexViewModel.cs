﻿namespace WorkComp.Models.TaskGroup
{
    public class DashboardTaskGroupIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.TaskGroup>
    {
        public override string PageTitle
        {
            get
            {
                return "Task Group";
            }
        }
    }
}