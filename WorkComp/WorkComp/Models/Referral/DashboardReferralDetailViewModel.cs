﻿namespace WorkComp.Models.Referral
{
    public class DashboardReferralDetailViewModel : DashboardSharedViewModel
    {
        public DashboardReferralDetailViewModel()
        {
            DashboardReferralDetailReferralInfoViewModel = new DashboardReferralDetailReferralInfoViewModel();
            ReferralResultViewModel=new ReferralResultViewModel();
            DetailReferralTaskInfoViewModel = new DetailReferralTaskInfoViewModel();
        }

        public DashboardReferralDetailReferralInfoViewModel DashboardReferralDetailReferralInfoViewModel { get; set; }
        public ReferralResultViewModel ReferralResultViewModel { get; set; }
        public DetailReferralTaskInfoViewModel DetailReferralTaskInfoViewModel { get; set; }
        public RequisitionMappingModel RequisitionMappingModel { get; set; }
        public override string PageTitle
        {
            get
            {
                return "DETAIL REFERRALS";
            }
        }

        public string ControlNumber { get; set; }

        public string Patient { get; set; }
        public int ClaimantId { get; set; }
        public int PanelTypeId { get; set; }
        public int TotalAttachment { get; set; }

        public int PreviousId { get; set; }
        public int NextId { get; set; }
    }
    
}