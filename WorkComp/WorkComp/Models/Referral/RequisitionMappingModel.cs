﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkComp.Models.Referral
{

    public class RequisitionMappingModel
    {
        public string ClaimantName { get; set; }
        public string ClaimantSsn { get; set; }
        public string ClaimantAddress { get; set; }
        public string ClaimantCity { get; set; }
        public string ClaimantState { get; set; }
        public string ClaimantZip { get; set; }
        public DateTime? ClaimantDobDate { get; set; }
        public string ClaimantDob
        {
            get
            {
                return ClaimantDobDate != null ? ((DateTime)ClaimantDobDate).ToString("MM/dd/yyyy") : string.Empty;
            }
        }
        public string ClaimantHomePhone { get; set; }
        public string ClaimantGender { get; set; }
        public string BillTo { get; set; }
        public string PayerName { get; set; }
        public string ClaimNumber { get; set; }
        public string EmployerName { get; set; }
        public string Jurisdiction { get; set; }
        public DateTime? DoiDate { get; set; }
        public string Doi
        {
            get
            {
                return DoiDate != null ? ((DateTime)DoiDate).ToString("MM/dd/yyyy") : string.Empty;
            }
        }
        public string TreatingPhysicianName { get; set; }
        public string TreatingPhysicianAddress { get; set; }

        public DateTime? NextMdVisitDate { get; set; }
        public string NextMdVisit
        {
            get
            {
                return NextMdVisitDate != null ? ((DateTime)NextMdVisitDate).ToString("MM/dd/yyyy") : string.Empty;
            }
        }
        public TestingInfo TestingInfo { get; set; }
        public string Drug { get; set; }
        public string CustomPanel { get; set; }
    }
}