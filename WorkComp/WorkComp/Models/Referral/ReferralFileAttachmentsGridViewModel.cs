﻿using System;
using System.Collections.ObjectModel;
using Framework.DomainModel.Entities.Common;

namespace WorkComp.Models.Referral
{
    public class ReferralFileAttachmentsGridViewModel: EditableGridViewModel
    {
        public ReferralFileAttachmentsGridViewModel()
        {
            DocumentTypeId = (int)DocumentTypeKey.Referral;
        }
        public int ReferralId { get; set; }
        public Collection<ReferralFileAttachmentGridItemViewModel> FileAttachments { get; set; }
        public bool UseForReferralDetail { get; set; }
    }

    public class ReferralFileAttachmentGridItemViewModel
    {
        public Guid RowGuid { get; set; }
        public int Id { get; set; }
        public int? ReferralId { get; set; }
    }
}