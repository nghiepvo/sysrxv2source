﻿using System.Collections.Generic;

namespace WorkComp.Models.Referral
{
    public class ReferralCancelViewModel : ViewModelBase
    {
        public string Comment { get; set; }
        public int? ReasonId { get; set; }
    }
    public class ReferralAssignToViewModel : ViewModelBase
    {
        public List<int> ListReferralIdSelected { get; set; }
        public bool IsSelectAll { get; set; }
        public string ListReferralIdSelectedString { get; set; }
        public int? AssignToId { get; set; }
        public string TypeWithUser { get; set; } 
    }
    public class ReferralPrintViewModel : ViewModelBase
    {
        public string ListReferralIdSelectedString { get; set; }
        public int? CurrentUserId { get; set; }
    }
}