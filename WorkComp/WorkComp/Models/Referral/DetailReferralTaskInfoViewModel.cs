﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Referral
{
    public class DetailReferralTaskInfoViewModel
    {
        public LookupItemVo ReferralDataSource { get; set; }
        public LookupItemVo AssignToDataSource { get; set; }
    }
}