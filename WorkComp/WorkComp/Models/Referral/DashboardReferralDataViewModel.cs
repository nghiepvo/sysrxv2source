﻿using System;
using System.Collections.Generic;
using Framework.DomainModel.Entities;

namespace WorkComp.Models.Referral
{
    public class DashboardReferralDataViewModel :
        MasterfileViewModelBase<Framework.DomainModel.Entities.Referral>
    {
        public DashboardReferralDataViewModel()
        {
            ClaimInfo=new ClaimInfoPartialForReferral();
            ReasonReferrals=new List<ReferralReasonReferral>();
            ReferralInfo=new ReferralInfoPartialForReferral();
            TestingInfo=new TestingInfoPartialForReferral();
        }
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardReferralShareViewModel>(parameters);
        }
        public ClaimInfoPartialForReferral ClaimInfo { get; set; }
        public ReferralInfoPartialForReferral ReferralInfo { get; set; }
        public List<ReferralReasonReferral> ReasonReferrals { get; set; }
        public List<ReferralIcd> Icds { get; set; }
        public TestingInfoPartialForReferral TestingInfo { get; set; }
        public bool? NoMedicationHistory { get; set; }
        public List<ReferralMedicationHistory> MedicationHistories { get; set; }
        public List<Framework.DomainModel.Entities.ReferralTask> Tasks { get; set; }
        public List<ReferralAttachment> Attachments { get; set; }
        public int OldStatusId { get; set; }
    }

    public class ClaimInfoPartialForReferral
    {
        public int? ProductTypeId { get; set; }
        public int? ReferralSourceId { get; set; }
        public bool AdjusterIsReferral { get; set; }
        public int? ClaimNumberId { get; set; }
        public int? PayerId { get; set; }
        public int? BranchId { get; set; }
        public int? AdjusterId { get; set; }
        public int? ClaimantId { get; set; }
        public int? CaseManagerId { get; set; }
        public int? EmployerId { get; set; }
    }

    public class ReferralInfoPartialForReferral
    {
        
        public DateTime? EnteredDate { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public DateTime? DueDate { get; set; }
        public int? StatusId { get; set; }
        public int? AssignToId { get; set; }
        public int? ReferralMethodId { get; set; }
        public int? AttorneyId { get; set; }
        public string SpecialInstruction { get; set; }
        public string ControlNumber { get; set; }
        public bool Rush { get; set; }
    }

    public class TestingInfoPartialForReferral
    {
        public int? PanelTypeId { get; set; }
        public int? CollectionMethodId { get; set; }
        public int? CollectionSiteId { get; set; }
        public string CollectionSiteSpecialInstructions { get; set; }
        public DateTime? CollectionSiteDate { get; set; }
        public string CollectionSitePhone { get; set; }
        public string CollectionSiteFax { get; set; }
        public string CollectionSiteAddress { get; set; }
        public int? TreatingPhysicianId { get; set; }
        public string TreatingPhysicianSpecialHandling { get; set; }
        public DateTime? NextMdVisitDate { get; set; }
        public string TreatingPhysicianPhone { get; set; }
        public string TreatingPhysicianFax { get; set; }
        public string TreatingPhysicianAddress { get; set; }
        public string TreatingPhysicianEmail { get; set; }
        public bool CheckOralFluid { get; set; }
        public bool CheckBlood { get; set; }
        public bool CheckHair { get; set; }
        public bool CheckUrine { get; set; }
    }
}