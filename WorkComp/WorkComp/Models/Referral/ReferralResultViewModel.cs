﻿using Framework.DomainModel.ValueObject;
namespace WorkComp.Models.Referral
{
    public class ReferralResultViewModel
    {
        public ReferralResultDetailVo ReferralResultDetailVo { get; set; }
        public ReferralDetailExtension ReferralDetailExtention { get; set; }
        public ReferralResultViewModel()
        {
            ReferralResultDetailVo = new ReferralResultDetailVo();
            ReferralDetailExtention = new ReferralDetailExtension();
        }
    }

}