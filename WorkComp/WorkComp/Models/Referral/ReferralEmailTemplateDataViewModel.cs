﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkComp.Models.Referral
{
    public class ReferralEmailTemplateDataViewModel
    {
        public int ReferralId { get; set; }
        public int EmailTemplateId { get; set; }
        public string EmailContent { get; set; }
        public string EmailSubject { get; set; }
        public string EmailAddressSetup { get; set; }
        public DateTime? DateToSendMail { get; set; }
        public string FaxNumber { get; set; }
        public DateTime? DateToSendFax { get; set; }
        public string TypeSave { get; set; }
        public AttachmentSendMail[] FileNames { get; set; }
    }
}