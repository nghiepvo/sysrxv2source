﻿namespace WorkComp.Models.Referral
{
    public class ReferralParameter : MasterfileParameter
    {
        public string ClaimInfoParams { get; set; }
        public string ReasonReferralParams { get; set; }
        public string ReferralInfoParams { get; set; }
        public string IcdInfoParams { get; set; }
        public string TestingInfoParams { get; set; }
        public string MedicationHistoryInfoParams { get; set; }
        public string TaskGroupInfoParams { get; set; }
        public string AttachmentInfoParams { get; set; }
        public int TaskGroupIdSelected { get; set; }
    }
}