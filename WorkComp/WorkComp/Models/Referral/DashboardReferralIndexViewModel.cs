﻿using Framework.DomainModel.Entities.Common;

namespace WorkComp.Models.Referral
{
    public class DashboardReferralIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.Referral>
    {
        public ReferralFilterItem FilterType { get; set; }
        public override string PageTitle
        {
            get
            {
                switch (GridViewModel.TypeWithUser)
                {
                    case TypeWithUserQueryEnum.All:
                        return "ALL REFERRALS";
                    case TypeWithUserQueryEnum.Current:
                        return "MY REFERRALS";
                    case TypeWithUserQueryEnum.Other:
                        return "OTHER REFERRALS";
                    default:
                        return "";

                }
            }
            
        }
    }
}