﻿using System;

namespace WorkComp.Models.Referral
{
    public sealed class FileAttachment
    {
        public int FileSize { get; set; }
        public string AttachedFileName { get; set; }
        public Guid RowGuid { get; set; }
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}