﻿namespace WorkComp.Models.TaskTemplate
{
    public class DashboardTaskTemplateDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.TaskTemplate>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardTaskTemplateShareViewModel>(parameters);
        }
        public bool? IsSystem { get; set; }
        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Task Template" : "Update Task Template";
            }
        }
    }
}