﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkComp.Models.ReferralTask
{
    public class ReferralTaskSendEmailFaxViewModel
    {
        public int ReferralTaskId { get; set; }
        public string Type { get; set; }
        public string ContentHtml { get; set; }
        public string EmailTo { get; set; }
        public string EmailCc { get; set; }
        public string Subject { get; set; }
        public bool IsSecureEmail { get; set; }
        public string Fax { get; set; }
        public string DisplayName { get; set; }
    }
}