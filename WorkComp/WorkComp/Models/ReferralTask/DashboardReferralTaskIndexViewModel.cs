﻿using Framework.DomainModel.Entities.Common;

namespace WorkComp.Models.ReferralTask
{
    public class DashboardReferralTaskIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.ReferralTask>
    {
        public ReferralFilterItem FilterType { get; set; }
        public override string PageTitle
        {
            get
            {
                switch (GridViewModel.TypeWithUser)
                {
                    case TypeWithUserQueryEnum.All:
                        return "ALL TASKS";
                    case TypeWithUserQueryEnum.Current:
                        return "MY TASKS";
                    case TypeWithUserQueryEnum.Other:
                        return "OTHER TASKS";
                    default:
                        return "";

                }
            }
        }
    }
    
}