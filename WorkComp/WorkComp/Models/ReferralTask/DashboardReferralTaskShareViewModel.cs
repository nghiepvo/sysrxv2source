﻿using System;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.ReferralTask
{
    public class DashboardReferralTaskShareViewModel : DashboardSharedViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DueDate { get; set; }

        public int ReferralId { get; set; }
        public LookupItemVo ReferralDataSource { get; set; }

        public int? TaskTypeId { get; set; }

        public int? AssignToId { get; set; }
        public LookupItemVo AssignToDataSource { get; set; }

        public int StatusId { get; set; }
        public LookupItemVo StatusDataSource { get; set; }

        public int TaskTemplateId { get; set; }
        public LookupItemVo TaskTemplateDataSource { get; set; }

        public bool Rush { get; set; }
    }
}