﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace WorkComp.Models.Claimant
{
    public class DashboardClaimainItemDetail : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public string Suffix { get; set; }
        public string Ssn { get; set; }
        public string PatientAbbr { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ClaimantLanguage { get; set; }

        public DateTime? ExpirationDate { get; set; }
        public string Gender { get; set; }

        public string GenderName
        {
            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Gender.ToString(), Gender);
            }
        }

        public DateTime Birthday { get; set; }

        public string Email { get; set; }

        public string WorkPhone { get; set; }
        public string WorkPhoneExtension { get; set; }
        public string HomePhone { get; set; }
        public string HomePhoneExtension { get; set; }
        public string CellPhone { get; set; }

        public string BestContactNumber { get; set; }


        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
    }
}