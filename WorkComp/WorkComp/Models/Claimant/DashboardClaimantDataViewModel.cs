﻿namespace WorkComp.Models.Claimant
{
    public class DashboardClaimantDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Claimant>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardClaimantShareViewModel>(parameters);
        }
    }

    public class DashboardClaimainItemDetailDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Claimant>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardClaimainItemDetail>(parameters);
        }
    }
}