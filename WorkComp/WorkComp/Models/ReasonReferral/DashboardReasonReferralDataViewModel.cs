﻿namespace WorkComp.Models.ReasonReferral
{
    public class DashboardReasonReferralDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.ReasonReferral>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardReasonReferralShareViewModel>(parameters);
        }
    }
}