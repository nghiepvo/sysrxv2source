﻿namespace WorkComp.Models.State
{
    public class DashboardStateShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public string AbbreviationName { get; set; }
    }
} 