﻿using System.Collections.Generic;

namespace WorkComp.Models.SysRxReport
{
    public class DashboardMGMTUserProductivityViewModel
    {
        public DashboardMGMTUserProductivityViewModel()
        {
            MGMTUserProductivityViewModels = new List<MGMTUserProductivityViewModel>();
            CommonSysRxReportViewModel = new CommonSysRxReportViewModel();
        }
        public CommonSysRxReportViewModel CommonSysRxReportViewModel { get; set; }
        public List<MGMTUserProductivityViewModel> MGMTUserProductivityViewModels { get; set; }
    }
}