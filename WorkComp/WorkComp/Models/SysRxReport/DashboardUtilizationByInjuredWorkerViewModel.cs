﻿using System.Collections.Generic;

namespace WorkComp.Models.SysRxReport
{
    public class DashboardUtilizationByInjuredWorkerViewModel 
    {
        public DashboardUtilizationByInjuredWorkerViewModel()
        {
            UtilizationByInjuredWorkerViewModels = new List<UtilizationByInjuredWorkerViewModel>();
            CommonSysRxReportViewModel = new CommonSysRxReportViewModel();
        }
        public CommonSysRxReportViewModel CommonSysRxReportViewModel { get; set; }
        public List<UtilizationByInjuredWorkerViewModel> UtilizationByInjuredWorkerViewModels { get; set; }
    }
    
}