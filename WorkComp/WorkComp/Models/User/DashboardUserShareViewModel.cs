﻿
using Framework.DomainModel.ValueObject;
using Framework.Utility;

namespace WorkComp.Models.User
{
    public class DashboardUserShareViewModel : DashboardSharedViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        private string _phone;

        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value.ApplyFormatPhone();
            }
        }

        public string Avatar { get; set; }
        public string Address { get; set; }
        public int UserRoleId { get; set; }
        public int StateId { set; get; }
        public LookupItemVo StateDataSource { get; set; }
        public int CityId { set; get; }
        public LookupItemVo CityDataSource { get; set; }
        public int ZipId { set; get; }
        public LookupItemVo ZipDataSource { get; set; }

        public bool IsEnable
        {
            get { return true; }
        }
    }
}