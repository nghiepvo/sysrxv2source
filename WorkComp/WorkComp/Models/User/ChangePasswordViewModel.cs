﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkComp.Models.User
{
    public class ChangePasswordViewModel
    {
        public string PasswordOld { get; set; }
        public string PasswordNew { get; set; }
    }
}