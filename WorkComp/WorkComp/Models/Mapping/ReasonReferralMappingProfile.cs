﻿using AutoMapper;
using Framework.Mapping;
using WorkComp.Models.ReasonReferral;

namespace WorkComp.Models.Mapping
{
    public class ReasonReferralMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.ReasonReferral, DashboardReasonReferralShareViewModel>();
            Mapper.CreateMap<DashboardReasonReferralShareViewModel, Framework.DomainModel.Entities.ReasonReferral>().AfterMap(
                (s, d) =>
                {
                    d.ParentId = s.ReportsTo;
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.ReasonReferral, DashboardReasonReferralDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardReasonReferralShareViewModel>();
                 });

            Mapper.CreateMap<DashboardReasonReferralDataViewModel, Framework.DomainModel.Entities.ReasonReferral>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}