﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Framework.Mapping;
using Framework.Service.Translation;
using WorkComp.Models.AssayCodeDescription;
using WorkComp.Models.InformationConcerningWhenDelete1Function;


namespace WorkComp.Models.Mapping
{
    public class AssayCodeDescriptionDescriptionMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.AssayCodeDescription, DashboardInformationConcerningViewModel>().AfterMap(
                (s, d) =>
                {
                    var dic = new Dictionary<string, IList<string>> {{ "Assay Code with Code:", s.AssayCodes.Select(assayCode => assayCode.Code).ToList() }};
                    d.InformationConcernings = dic;
                });
            Mapper.CreateMap<Framework.DomainModel.Entities.AssayCodeDescription, DashboardAssayCodeDescriptionShareViewModel>();
            Mapper.CreateMap<DashboardAssayCodeDescriptionShareViewModel, Framework.DomainModel.Entities.AssayCodeDescription>();

            Mapper.CreateMap<Framework.DomainModel.Entities.AssayCodeDescription, DashboardAssayCodeDescriptionDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardAssayCodeDescriptionShareViewModel>();
                 });

            Mapper.CreateMap<DashboardAssayCodeDescriptionDataViewModel, Framework.DomainModel.Entities.AssayCodeDescription>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}