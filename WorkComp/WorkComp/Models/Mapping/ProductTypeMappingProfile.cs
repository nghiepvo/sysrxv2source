﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.ProductType;

namespace WorkComp.Models.Mapping
{
    public class ProductTypeMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.ProductType, DashboardProductTypeShareViewModel>()
                .ForMember(desc => desc.PayerDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.PayerId != 0)
                    {
                        d.PayerDataSource = new LookupItemVo
                        {
                            KeyId = s.PayerId.GetValueOrDefault(),
                            DisplayName = s.Payer == null ? "" : s.Payer.Name
                        };
                    }
                    else
                    {
                        d.PayerDataSource = null;
                    }
                });
            Mapper.CreateMap<DashboardProductTypeShareViewModel, Framework.DomainModel.Entities.ProductType>()
                .AfterMap((s, d) =>
                {
                    d.PayerId = s.PayerId;
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.ProductType, DashboardProductTypeDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardProductTypeShareViewModel>();
                });

            Mapper.CreateMap<DashboardProductTypeDataViewModel, Framework.DomainModel.Entities.ProductType>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });

        }
    }
}