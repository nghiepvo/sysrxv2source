﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.TaskTemplate;

namespace WorkComp.Models.Mapping
{
    public class TaskTemplateMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.TaskTemplate, DashboardTaskTemplateShareViewModel>()
                .ForMember(desc => desc.ShowPopupType, opt => opt.Ignore())
                .ForMember(desc => desc.AssignToDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.AssignToId.GetValueOrDefault() != 0)
                    {
                        d.AssignToDataSource = new LookupItemVo
                        {
                            KeyId = s.AssignToId.GetValueOrDefault(),
                            DisplayName = s.AssignTo.UserName
                        };
                    }
                    else
                    {
                        d.AssignToDataSource = null;
                    }
                }); 
            Mapper.CreateMap<DashboardTaskTemplateShareViewModel, Framework.DomainModel.Entities.TaskTemplate>()
                .AfterMap((s, d) =>
                {
                    d.TaskTypeId = s.TaskTypeId == 0 ? (int?) null : s.TaskTypeId;
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.TaskTemplate, DashboardTaskTemplateDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardTaskTemplateShareViewModel>();
                });

            Mapper.CreateMap<DashboardTaskTemplateDataViewModel, Framework.DomainModel.Entities.TaskTemplate>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}