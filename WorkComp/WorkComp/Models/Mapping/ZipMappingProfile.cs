﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.Zip;

namespace WorkComp.Models.Mapping
{
    public class ZipMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Zip, DashboardZipShareViewModel>()
                .ForMember(desc => desc.StateDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.CityDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.CityId != 0)
                    {
                        var objCity = s.City;
                        if (objCity != null)
                        {
                            d.CityDataSource = new LookupItemVo
                            {
                                KeyId = s.CityId,
                                DisplayName = objCity.Name
                            };
                            var objState = objCity.State;
                            if (objState != null)
                            {
                                d.StateId = objState.Id;
                                d.StateDataSource = new LookupItemVo
                                {
                                    KeyId = objState.Id,
                                    DisplayName = objState.Name
                                };
                            }
                            else
                            {
                                d.StateDataSource = null;
                            }
                        }
                        else
                        {
                            d.CityDataSource = null;
                            d.StateDataSource = null;
                        }
                    }
                    else
                    {
                        d.CityDataSource = null;
                        d.StateDataSource = null;
                    }
                });
            Mapper.CreateMap<DashboardZipShareViewModel, Framework.DomainModel.Entities.Zip>()
                .AfterMap((s, d) =>
                {
                    d.CityId = s.CityId ?? 0;
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.Zip, DashboardZipDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardZipShareViewModel>();
                });

            Mapper.CreateMap<DashboardZipDataViewModel, Framework.DomainModel.Entities.Zip>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });

        }
    }
}