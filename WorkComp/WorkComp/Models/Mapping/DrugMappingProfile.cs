﻿using AutoMapper;
using Framework.Mapping;
using WorkComp.Models.Drug;

namespace WorkComp.Models.Mapping
{
    public class DrugMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Drug, DashboardDrugShareViewModel>();
            Mapper.CreateMap<DashboardDrugShareViewModel, Framework.DomainModel.Entities.Drug>();

            Mapper.CreateMap<Framework.DomainModel.Entities.Drug, DashboardDrugDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardDrugShareViewModel>();
                 });

            Mapper.CreateMap<DashboardDrugDataViewModel, Framework.DomainModel.Entities.Drug>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}