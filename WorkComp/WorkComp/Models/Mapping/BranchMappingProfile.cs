﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Utility;
using WorkComp.Models.Branch;


namespace WorkComp.Models.Mapping
{
    public class BranchMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Branch, DashboardBranchShareViewModel>()
                .ForMember(desc => desc.StateDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.CityDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.ZipId != 0)
                    {
                        var objZip = s.Zip;
                        if (objZip!= null)
                        {
                            d.ZipDataSource = new LookupItemVo
                            {
                                KeyId = s.ZipId.GetValueOrDefault(),
                                DisplayName = objZip.Name
                            };
                            if (s.CityId != 0)
                            {
                                var objCity = s.City;
                                if (objCity != null)
                                {
                                    d.CityDataSource = new LookupItemVo
                                    {
                                        KeyId = s.CityId.GetValueOrDefault(),
                                        DisplayName = objCity.Name
                                    };
                                    var objState = objCity.State;
                                    if (objState != null)
                                    {
                                        d.StateId = objState.Id;
                                        d.StateDataSource = new LookupItemVo
                                        {
                                            KeyId = objState.Id,
                                            DisplayName = objState.Name
                                        };
                                    }
                                    else
                                    {
                                        d.StateDataSource = null;
                                    }
                                }
                                else
                                {
                                    d.CityDataSource = null;
                                    d.StateDataSource = null;
                                    d.ZipDataSource = null;
                                }
                            }
                            else
                            {
                                d.CityDataSource = null;
                                d.StateDataSource = null;
                                d.ZipDataSource = null;
                            }
                        }
                        else
                        {
                            d.CityDataSource = null;
                            d.StateDataSource = null;
                            d.ZipDataSource = null;
                        }
                        
                    }
                    else
                    {
                        d.CityDataSource = null;
                        d.StateDataSource = null;
                        d.ZipDataSource = null;
                    }

                    if (s.PayerId != 0)
                    {
                        var objPayer = s.Payer;
                        if (objPayer != null)
                        {
                            d.PayerId = objPayer.Id;
                            d.PayerDataSource = new LookupItemVo()
                            {
                                KeyId = objPayer.Id, DisplayName = objPayer.Name
                            };
                        }
                    }
                    
                });
            Mapper.CreateMap<DashboardBranchShareViewModel, Framework.DomainModel.Entities.Branch>()
                .AfterMap((s, d) =>
                {
                    d.StateId = s.StateId ?? 0;
                    d.CityId = s.CityId ?? 0;
                    d.ZipId = s.ZipId ?? 0;
                    d.PayerId = s.PayerId ?? 0;
                    d.Phone = s.Phone.RemoveFormatPhone();
                    d.Fax = s.Fax.RemoveFormatPhone();
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.Branch, DashboardBranchDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardBranchShareViewModel>();
                 });

            Mapper.CreateMap<DashboardBranchDataViewModel, Framework.DomainModel.Entities.Branch>()
                .AfterMap((s, d) => s.SharedViewModel.MapPropertiesToInstance(d));
        }
    }
}