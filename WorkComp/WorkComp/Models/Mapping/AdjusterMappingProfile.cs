﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Utility;
using WorkComp.Models.Adjuster;


namespace WorkComp.Models.Mapping
{
    public class AdjusterMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Adjuster, DashboardAdjusterShareViewModel>()
                .ForMember(desc => desc.StateDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.CityDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.ZipId != 0)
                    {
                        var objZip = s.Zip;
                        if (objZip!= null)
                        {
                            d.ZipDataSource = new LookupItemVo
                            {
                                KeyId = s.ZipId.GetValueOrDefault(),
                                DisplayName = objZip.Name
                            };
                            if (s.CityId != 0)
                            {
                                var objCity = s.City;
                                if (objCity != null)
                                {
                                    d.CityDataSource = new LookupItemVo
                                    {
                                        KeyId = s.CityId.GetValueOrDefault(),
                                        DisplayName = objCity.Name
                                    };
                                    var objState = objCity.State;
                                    if (objState != null)
                                    {
                                        d.StateId = objState.Id;
                                        d.StateDataSource = new LookupItemVo
                                        {
                                            KeyId = objState.Id,
                                            DisplayName = objState.Name
                                        };
                                    }
                                    else
                                    {
                                        d.StateDataSource = null;
                                    }
                                }
                                else
                                {
                                    d.CityDataSource = null;
                                    d.StateDataSource = null;
                                    d.ZipDataSource = null;
                                }
                            }
                            else
                            {
                                d.CityDataSource = null;
                                d.StateDataSource = null;
                                d.ZipDataSource = null;
                            }
                        }
                        else
                        {
                            d.CityDataSource = null;
                            d.StateDataSource = null;
                            d.ZipDataSource = null;
                        }
                        
                    }
                    else
                    {
                        d.CityDataSource = null;
                        d.StateDataSource = null;
                        d.ZipDataSource = null;
                    }
                    

                    if (s.BranchId != null)
                    {
                        var objBranch = s.Branch;
                        if (objBranch!= null)
                        {
                            d.BranchId = objBranch.Id;
                            d.BranchDataSource = new LookupItemVo
                            {
                                KeyId = objBranch.Id,
                                DisplayName = objBranch.Name
                            };
                        }
                    }
                    //TODO: Edit Allow Create Referral
                    //d.AllowCreateReferral = s.AllowCreateReferral == true;
                    d.OptedOutSendMail = s.OptedOutSendMail == true;


                });
            Mapper.CreateMap<DashboardAdjusterShareViewModel, Framework.DomainModel.Entities.Adjuster>()
                .AfterMap((s, d) =>
                {
                    d.ZipId = s.ZipId ?? 0;
                    d.CityId = s.CityId ?? 0;
                    d.ZipId = s.ZipId ?? 0;
                    d.Phone = s.Phone.RemoveFormatPhone();
                    d.AllowCreateReferral = s.AllowCreateReferral;
                    d.OptedOutSendMail = s.OptedOutSendMail;
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.Adjuster, DashboardAdjusterDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardAdjusterShareViewModel>();
                 });

            Mapper.CreateMap<DashboardAdjusterDataViewModel, Framework.DomainModel.Entities.Adjuster>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}