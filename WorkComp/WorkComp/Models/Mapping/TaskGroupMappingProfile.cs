﻿using AutoMapper;
using Framework.Mapping;
using WorkComp.Models.TaskGroup;

namespace WorkComp.Models.Mapping
{
    public class TaskGroupMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.TaskGroup, DashboardTaskGroupShareViewModel>();
            Mapper.CreateMap<DashboardTaskGroupShareViewModel, Framework.DomainModel.Entities.TaskGroup>();

            Mapper.CreateMap<Framework.DomainModel.Entities.TaskGroup, DashboardTaskGroupDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardTaskGroupShareViewModel>();
                });

            Mapper.CreateMap<DashboardTaskGroupDataViewModel, Framework.DomainModel.Entities.TaskGroup>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}