﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.User;

namespace WorkComp.Models.Mapping
{
    public class UserMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.User, DashboardUserShareViewModel>()
                .ForMember(desc => desc.StateDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.CityDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.ZipDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.StateId != 0)
                    {
                        d.StateDataSource = new LookupItemVo
                        {
                            KeyId = s.StateId.GetValueOrDefault(),
                            DisplayName = s.State.Name
                        };
                    }
                    else
                    {
                        d.StateDataSource = null;
                    }
                    if (s.CityId != 0)
                    {
                        d.CityDataSource = new LookupItemVo
                        {
                            KeyId = s.CityId.GetValueOrDefault(),
                            DisplayName = s.City.Name
                        };
                    }
                    else
                    {
                        d.CityDataSource = null;
                    }
                    if (s.ZipId != 0)
                    {
                        d.ZipDataSource = new LookupItemVo
                        {
                            KeyId = s.ZipId.GetValueOrDefault(),
                            DisplayName = s.Zip.Name
                        };
                    }
                    else
                    {
                        d.ZipDataSource = null;
                    }
                }); 
            Mapper.CreateMap<DashboardUserShareViewModel, Framework.DomainModel.Entities.User>();

            Mapper.CreateMap<Framework.DomainModel.Entities.User, DashboardUserDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardUserShareViewModel>();
                });

            Mapper.CreateMap<DashboardUserDataViewModel, Framework.DomainModel.Entities.User>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}