﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Utility;
using WorkComp.Models.ReferralSource;

namespace WorkComp.Models.Mapping
{
    public class ReferralSourceMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.ReferralSource, DashboardReferralSourceShareViewModel>()
                .ForMember(desc => desc.StateDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.CityDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.ZipDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.ZipId != 0)
                    {
                        var objZip = s.Zip;
                        if (objZip!= null)
                        {
                            d.ZipDataSource = new LookupItemVo
                            {
                                KeyId = s.ZipId.GetValueOrDefault(),
                                DisplayName = objZip.Name
                            };
                            if (s.CityId != 0)
                            {
                                var objCity = s.City;
                                if (objCity != null)
                                {
                                    d.CityDataSource = new LookupItemVo
                                    {
                                        KeyId = s.CityId.GetValueOrDefault(),
                                        DisplayName = objCity.Name
                                    };
                                    var objState = s.State;
                                    if (objState != null)
                                    {
                                        d.StateId = objState.Id;
                                        d.StateDataSource = new LookupItemVo
                                        {
                                            KeyId = objState.Id,
                                            DisplayName = objState.Name
                                        };
                                    }
                                    else
                                    {
                                        d.StateDataSource = null;
                                    }
                                }
                                else
                                {
                                    d.CityDataSource = null;
                                    d.StateDataSource = null;
                                }
                            }
                            else
                            {
                                d.CityDataSource = null;
                                d.StateDataSource = null;
                                d.ZipDataSource = null;
                            }
                        }
                        else
                        {
                            d.CityDataSource = null;
                            d.StateDataSource = null;
                            d.ZipDataSource = null;
                        }
                        
                    }
                    else
                    {
                        d.CityDataSource = null;
                        d.StateDataSource = null;
                        d.ZipDataSource = null;
                    }

                    if (s.ReferralTypeId == 0) return;
                    var objReferralType = s.ReferralType;
                    if (objReferralType == null) return;
                    d.ReferralTypeId = objReferralType.Id;
                    d.ReferralTypeDataSource = new LookupItemVo
                    {
                        KeyId = objReferralType.Id,
                        DisplayName = objReferralType.Name
                    };
                });
            Mapper.CreateMap<DashboardReferralSourceShareViewModel, Framework.DomainModel.Entities.ReferralSource>()
                .ForMember(desc => desc.ReferralTypeId, opt => opt.Ignore())
                .ForMember(desc => desc.StateId, opt => opt.Ignore())
                .ForMember(desc => desc.CityId, opt => opt.Ignore())
                .ForMember(desc => desc.ZipId, opt => opt.Ignore())
                .ForMember(desc => desc.Phone, opt => opt.Ignore())
                .ForMember(desc => desc.Fax, opt => opt.Ignore())
                .AfterMap((s, d) =>
            {
                d.ReferralTypeId = s.ReferralTypeId ?? 0;
                d.StateId = s.StateId ?? 0;
                d.CityId = s.CityId ?? 0;
                d.ZipId = s.ZipId ?? 0;
                d.Phone = s.Phone.RemoveFormatPhone();
                d.Fax = s.Fax.RemoveFormatPhone();
                if (!d.IsAuthorization)
                {
                    d.AuthorizationFrom = null;
                    d.AuthorizationTo = null;
                }
            });
            Mapper.CreateMap<Framework.DomainModel.Entities.ReferralSource, DashboardReferralSourceDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardReferralSourceShareViewModel>();
                 });

            Mapper.CreateMap<DashboardReferralSourceDataViewModel, Framework.DomainModel.Entities.ReferralSource>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}