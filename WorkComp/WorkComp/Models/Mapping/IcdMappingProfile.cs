﻿using AutoMapper;
using Framework.Mapping;
using WorkComp.Models.Icd;

namespace WorkComp.Models.Mapping
{
    public class IcdMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Icd, DashboardIcdShareViewModel>();
            Mapper.CreateMap<DashboardIcdShareViewModel, Framework.DomainModel.Entities.Icd>();

            Mapper.CreateMap<Framework.DomainModel.Entities.Icd, DashboardIcdDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardIcdShareViewModel>();
                });

            Mapper.CreateMap<DashboardIcdDataViewModel, Framework.DomainModel.Entities.Icd>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }

}