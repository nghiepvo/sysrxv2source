﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.Comment;

namespace WorkComp.Models.Mapping
{
    public class CommentMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Comment, DashboardCommentShareViewModel>();
            Mapper.CreateMap<DashboardCommentShareViewModel, Framework.DomainModel.Entities.Comment>();

            Mapper.CreateMap<Framework.DomainModel.Entities.Comment, DashboardCommentDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardCommentShareViewModel>();
                });

            Mapper.CreateMap<DashboardCommentDataViewModel, Framework.DomainModel.Entities.Comment>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);

                });
        }
    }
}