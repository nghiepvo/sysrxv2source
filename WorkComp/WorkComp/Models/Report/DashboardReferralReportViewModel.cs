﻿namespace WorkComp.Models.Report
{
    public class DashboardReferralReportViewModel:ViewModelBase
    {
        public GridViewModel GridViewModel { get; set; }
        public override string PageTitle
        {
            get
            {
                return ReferralSearchType.Equals("ClaimSearch")? "Claim# Report":"Referral Report";
            }
        }
        public string ReferralSearchType { get; set; }
    }
}