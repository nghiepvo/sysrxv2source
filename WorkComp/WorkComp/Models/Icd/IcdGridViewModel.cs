﻿using System;
using System.Collections.ObjectModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Icd
{
    public class IcdGridViewModel : EditableGridViewModel
    {
        public IcdGridViewModel()
        {
            DocumentTypeId = (int) DocumentTypeKey.Icd;
        }

        public Collection<IcdGridItemViewModel> Icds { get; set; }
        public int ReferralId { get; set; }

    }

    public class IcdGridItemViewModel
    {
        public Guid RowGuid { get; set; }
        public int Id { get; set; }
        public int? ReferralId { get; set; }
        public LookupInGridDataSourceViewModel IcdCode { get; set; }
        public LookupInGridDataSourceViewModel IcdDescription { get; set; }
    }
    
}