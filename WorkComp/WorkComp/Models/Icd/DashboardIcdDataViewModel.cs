﻿namespace WorkComp.Models.Icd
{
    public class DashboardIcdDataViewModel:MasterfileViewModelBase<Framework.DomainModel.Entities.Icd>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardIcdShareViewModel>(parameters);
        }
    }
}