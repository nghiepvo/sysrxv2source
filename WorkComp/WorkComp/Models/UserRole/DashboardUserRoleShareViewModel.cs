﻿namespace WorkComp.Models.UserRole
{
    public class DashboardUserRoleShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }

        public bool IsEnable
        {
            get { return true; }
        }
    }
}