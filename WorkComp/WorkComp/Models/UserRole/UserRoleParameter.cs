﻿namespace WorkComp.Models.UserRole
{
    public class UserRoleParameter : MasterfileParameter
    {
        public string UserRoleFunctionData { get; set; }
        public bool CheckAll { get; set; }
    }
}