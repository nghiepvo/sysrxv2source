﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using ServiceLayer.Common;
using ServiceLayer.Interfaces.Authentication;

namespace WorkComp.Models
{
    public abstract partial class ViewModelBase
    {
        public ViewModelBase()
        {
            if (HttpContext.Current != null && HttpContext.Current.Items.Contains("UserRoleFunctions"))
            {
                SecurityActionPermissions = HttpContext.Current.Items["UserRoleFunctions"] as List<UserRoleFunction>;
            }
        }
        public virtual string PageTitle { get;
            set;
        }
        public int Id { get; set; }
        public Guid RowGuid { get; set; }
        public bool IsDeleted { get; set; }
        private IWorkcompPrincipal _currentUser;

        public IWorkcompPrincipal CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    _currentUser = DependencyResolver.Current.GetService<IAuthenticationService>().GetCurrentUser();
                }
                return _currentUser;
            }
            set
            {
                _currentUser = value;
            }
        }

        public int DocumentTypeId { get; set; }
        public Collection<FooterAction> FooterActions { get; set; }
        public List<UserRoleFunction> SecurityActionPermissions { get; private set; }
        public object AddFooterAction(string text, string icon, string function, bool ignoreDirty = true, OperationAction securityMapAction = OperationAction.None)
        {
            var permission = true;

            if (FooterActions == null)
                FooterActions = new Collection<FooterAction>();

            if (securityMapAction != OperationAction.None && SecurityActionPermissions != null)
            {
                permission = SecurityActionPermissions.Any(s => s.SecurityOperationId == (int)securityMapAction);
            }

            FooterActions.Add(new FooterAction
            {
                Action = function,
                Icon = icon,
                Text = text.ToUpperInvariant(),
                IgnoreDirty = ignoreDirty,
                Permission = permission
            });
            return null;
        }

        public MenuViewModel MenuData
        {
            get
            {
                // Get current user
                if (CurrentUser == null || CurrentUser.User==null)
                {
                    return new MenuViewModel();
                }
                var idRole = CurrentUser.User.UserRoleId;
                return MenuExtractData.Instance.GetMenuViewModel(idRole);
            }
        }

    }
    public class FooterAction
    {
        public string Icon { get; set; }
        public string Text { get; set; }
        public string Action { get; set; }
        public bool IgnoreDirty { get; set; }
        public bool Permission { get; set; }
    }
}