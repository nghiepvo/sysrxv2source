﻿namespace WorkComp.Models.Diary
{
    public class DashboardDiaryDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Diary>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardDiaryShareViewModel>(parameters);
        }
    }
}