﻿using System;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.ReferralNote
{
    public class DashboardReferralNoteShareViewModel : DashboardSharedViewModel
    {
        public string Comment { get; set; }

        public int ReferralId { get; set; }
        public LookupItemVo ReferralDataSource { get; set; }

        public int? NoteType { get; set; }
        public LookupItemVo NoteTypeDataSource { get; set; }

        public int AssignToId { get; set; }
        public LookupItemVo AssignToDataSource { get; set; }
    }
}