﻿using System.Web.Routing;

namespace WorkComp.Models.Lookup
{
    public class DropdownListViewModel
    {
        public string ID { get; set; }
        public string ModelName { get; set; }
        public string Label { get; set; }
        public string UrlToReadData { get; set; }
        public object CurrentId { get; set; }
        public RouteValueDictionary HtmlAttributes { get; set; }
        public bool IsRequired { get; set; }
        public string StyleAttribute { get; set; }
        public string RequiredAttribute
        {
            get
            {
                return IsRequired ? "required=\"required\"" : "";
            }
        }

    }
}