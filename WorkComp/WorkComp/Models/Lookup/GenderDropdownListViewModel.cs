﻿using System.Web.Routing;

namespace WorkComp.Models.Lookup
{
    public class GenderDropdownListViewModel
    {
        public string ID { get; set; }
        public string Label { get; set; }
        public string ControllerName { get; set; }
        public string CurrentValue { get; set; }
        public RouteValueDictionary HtmlAttributes { get; set; }
        public bool IsRequired { get; set; }

        public string RequiredAttribute
        {
            get
            {
                return IsRequired ? "required=required" : "";
            }
        }

    }
}