﻿namespace WorkComp.Models.Comment
{
    public class DashboardCommentDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Comment>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardCommentShareViewModel>(parameters);
        }
    }
}