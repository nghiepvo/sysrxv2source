﻿namespace WorkComp.Models.Configuration
{
    public class DashboardConfigurationDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Configuration>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardConfigurationShareViewModel>(parameters);
        }
    }
}