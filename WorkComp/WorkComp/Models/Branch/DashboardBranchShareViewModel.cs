﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Branch
{
    public class DashboardBranchShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public string ManagerName { get; set; }
        public string Fax { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int? PayerId { get; set; }
        public LookupItemVo PayerDataSource { get; set; }
        public int? CityId { get; set; }
        public LookupItemVo CityDataSource { get; set; }
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
        public int? ZipId { get; set; }
        public LookupItemVo ZipDataSource { get; set; }

        public bool IsUserUpdate { get; set; }
    }
}