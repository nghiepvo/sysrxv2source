﻿using System.Web.Routing;

namespace WorkComp.Models.Input
{
    public class CheckBoxViewModel : ControlSharedViewModelBase
    {
        public string Class { get; set; }
        public string Style { get; set; }
        public RouteValueDictionary HtmlAttributes { get; set; }
    }
}