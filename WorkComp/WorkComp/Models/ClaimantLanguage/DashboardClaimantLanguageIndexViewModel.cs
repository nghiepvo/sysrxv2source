﻿namespace WorkComp.Models.ClaimantLanguage
{
    public class DashboardClaimantLanguageIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.ClaimantLanguage>
    {
        public override string PageTitle
        {
            get
            {
                return "Claimant Language";
            }
        }
    }
}