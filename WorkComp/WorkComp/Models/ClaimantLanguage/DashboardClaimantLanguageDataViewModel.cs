﻿namespace WorkComp.Models.ClaimantLanguage
{
    public class DashboardClaimantLanguageDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.ClaimantLanguage>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardClaimantLanguageShareViewModel>(parameters);
        }
        public override string PageTitle
        {
            get {
                return SharedViewModel.CreateMode ? "Create Claimant Language" : "Update Claimant Language";
            }
        }
    }
}