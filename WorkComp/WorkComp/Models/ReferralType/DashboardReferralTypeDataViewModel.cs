﻿namespace WorkComp.Models.ReferralType
{
    public class DashboardReferralTypeDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.ReferralType>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardReferralTypeShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Referral Type" : "Update Referral Type";
            }
        }
    }
}