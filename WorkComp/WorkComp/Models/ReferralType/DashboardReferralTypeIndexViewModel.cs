﻿namespace WorkComp.Models.ReferralType
{
    public class DashboardReferralTypeIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.ReferralType>
    {
        public override string PageTitle
        {
            get
            {
                return "Referral Type";
            }
        }
    }
}