﻿namespace WorkComp.Models.DualSelectListBox
{
    public class DualSelectListBoxViewModel : ViewModelBase
    {
        public string ControlId { get; set; }

        public string ModelName { get; set; }
        public string ParentSelectUrl { get; set; }

        public string ChildSelectUrl { get; set; }

        public string TreeViewUrl { get; set; }

        public int MasterfileId { get; set; }

        public string ParentSelectLabelText { get; set; }

        public string ChildSelectLabelText { get; set; }

        public string SelectedTreeLabelText { get; set; }

        public int? OffsetHeight { get; set; }

    }
}