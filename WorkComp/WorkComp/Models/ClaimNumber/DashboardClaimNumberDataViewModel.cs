﻿namespace WorkComp.Models.ClaimNumber
{
    public class DashboardClaimNumberDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.ClaimNumber>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardClaimNumberShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Claim Number" : "Update Claim Number";
            }
        }
    }
}