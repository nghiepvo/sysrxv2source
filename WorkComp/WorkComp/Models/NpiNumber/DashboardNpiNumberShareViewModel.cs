﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.NpiNumber
{
    public class DashboardNpiNumberShareViewModel : DashboardSharedViewModel
    {
        public string OrganizationName { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string MiddleName { set; get; }
        public string Npi { set; get; }
        public string Phone { set; get; }
        public string Fax { set; get; }
        public string LicenseNumber { set; get; }
        public string ProviderCredentialText { set; get; }
        public string Email { set; get; }
        public string Address { set; get; }

        public int? CityId { get; set; }
        public LookupItemVo CityDataSource { get; set; }
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
        public int? ZipId { get; set; }
        public LookupItemVo ZipDataSource { get; set; }

        public bool IsUserUpdate { set; get; }
    }
}