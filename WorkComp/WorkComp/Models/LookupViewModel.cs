﻿using System.Collections.Generic;
using System.Web.Routing;

namespace WorkComp.Models
{
    public class LookupViewModel
    {
        public string ID { get; set; }
        public string ModelName { get; set; }
        public string Label { get; set; }
        public string UrlToReadData { get; set; }
        public RouteValueDictionary HtmlAttributes { get; set; }
        public int CurrentId { get; set; }
        public string HierarchyGroupName { get; set; }
        public bool PopulatedByChildren { get; set; }
        public List<object> DataSource { get; set; }
        public int HeightLookup { get; set; }
    }
}