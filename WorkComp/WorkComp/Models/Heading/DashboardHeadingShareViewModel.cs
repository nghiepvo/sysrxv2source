﻿using System;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Heading
{
    public class DashboardHeadingShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
    }
}