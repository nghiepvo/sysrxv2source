﻿namespace WorkComp.Models.EmailTemplate
{
    public class DashboardEmailTemplateDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.EmailTemplate>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardEmailTemplateShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "" : "Update Email Template";
            }
        }
    }
}