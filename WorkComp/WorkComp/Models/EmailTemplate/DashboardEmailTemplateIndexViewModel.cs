﻿namespace WorkComp.Models.EmailTemplate
{
    public class DashboardEmailTemplateIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.EmailTemplate>
    {
        public override string PageTitle
        {
            get
            {
                return "Email Template";
            }
        }
    }
    
}