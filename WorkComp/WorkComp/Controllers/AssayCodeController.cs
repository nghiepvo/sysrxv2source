﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.AssayCode;

namespace WorkComp.Controllers
{
    public class AssayCodeController : ApplicationControllerGeneric<AssayCode, DashboardAssayCodeDataViewModel>
    {
        //
        // GET: /State/
        private readonly IAssayCodeService _assayCodeService;
        private readonly ISampleTestingTypeService _sampleTestingTypeService;
        public AssayCodeController(ISampleTestingTypeService sampleTestingTypeService, IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IAssayCodeService assayCodeService)
            : base(authenticationService, diagnosticService, assayCodeService)
        {
            _assayCodeService = assayCodeService;
            _sampleTestingTypeService = sampleTestingTypeService;
        }


        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardAssayCodeIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "AssayCodeGrid",
                ModelName = "AssayCode",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardAssayCodeDataViewModel
            {
                SharedViewModel = new DashboardAssayCodeShareViewModel
                {
                    CreateMode = true
                }
            };
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.Add)]
        public int Create(AssayCodeParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.Update)]
        public ActionResult Update(AssayCodeParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create",viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _assayCodeService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.View)]
        public JsonResult GetSampleTestingType()
        {
            var data = _sampleTestingTypeService.Get(o => true).Select(o => new LookupItemVo()
            {
                DisplayName = o.Name,
                KeyId = o.Id
            }).ToList();
            data.Insert(0, new LookupItemVo() {DisplayName = "", KeyId = 0});
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.AssayCode, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 300,
                    Name = "SampleTestingType",
                    Text = "Testing Type",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 400,
                    Name = "Description",
                    Text = "Description",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    Name = "Code",
                    Text = "Code",
                    ColumnWidth = 400,
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

    }
}
