﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Service.Diagnostics;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.City;

namespace WorkComp.Controllers
{
    public class HomeController : ApplicationControllerGeneric<City, DashboardCityDataViewModel>
    {
        //
        // GET: /Home/
        private readonly ICityService _cityService;
        public HomeController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, ICityService cityService)
            : base(authenticationService,diagnosticService, cityService)
        {
            _cityService = cityService;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.City, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardCityIndexViewModel();
            //var data = _cityService.ListAll();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "CityGrid",
                //AdvancedSearchUrl = "~/Views/City/CityAdvancedSearch.cshtml",
                //QueryUrl = "/Home/GetDataForGrid"
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }
        [HttpGet]
        public JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return null;
        }
        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 300,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }
    }
}
