﻿using Framework.DomainModel.Entities.Common;
using Framework.Mapping;
using WorkComp.Models.SysRxReport;

namespace WorkComp.Controllers
{
	public partial class SysRxReportController
	{
        private string HandleTestResultDetailReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            var data = _sysRxReportService.GetDataTestResultdetailReport(queryInfo, ref totalRow);
            queryInfo.TotalRow = totalRow;

            var viewModel = new DashboardTestResultDetailViewModel
            {
                CommonSysRxReportViewModel = queryInfo.MapTo<CommonSysRxReportViewModel>(),
                TestResultDetailViewModels = data.MapTo<TestResultDetailViewModel>()
            };
            return RenderRazorViewToString("~/Views/SysRxReport/TestResultsDetail.cshtml", viewModel);
        }

        private string HandleUtilizationByInjuredWorkerReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            var data = _sysRxReportService.GetDataUtilizationByInjuredWorkerReport(queryInfo, ref totalRow);
            queryInfo.TotalRow = totalRow;

            var viewModel = new DashboardUtilizationByInjuredWorkerViewModel
            {
                CommonSysRxReportViewModel = queryInfo.MapTo<CommonSysRxReportViewModel>(),
                UtilizationByInjuredWorkerViewModels = data.MapTo<UtilizationByInjuredWorkerViewModel>()
            };
            return RenderRazorViewToString("~/Views/SysRxReport/UtilizationByInjuredWorkerExport.cshtml", viewModel);
        }

        private string HandleCancellationDetailtReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            var data = _sysRxReportService.GetCancellationDetailtReport(queryInfo, ref totalRow);
            queryInfo.TotalRow = totalRow;

            var viewModel = new DashboardCancellationDetailViewModel
            {
                CommonSysRxReportViewModel = queryInfo.MapTo<CommonSysRxReportViewModel>(),
                CancellationDetailViewModels = data.MapTo<CancellationDetailViewModel>()
            };
            return RenderRazorViewToString("~/Views/SysRxReport/CancellationDetail.cshtml", viewModel);
        }

        private string HandleTestResultsSummaryReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            var data = _sysRxReportService.GetTestResultsSummaryReport(queryInfo, ref totalRow);
            queryInfo.TotalRow = totalRow;

            var viewModel = new DashboardTestResultSummaryViewModel
            {
                CommonSysRxReportViewModel = queryInfo.MapTo<CommonSysRxReportViewModel>(),
                TestResultSummaryViewModels = data.MapTo<TestResultSummaryViewModel>()
            };
            return RenderRazorViewToString("~/Views/SysRxReport/TestResultsSummary.cshtml", viewModel);
        }

        private string HandleMGMTStatusSummaryReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            var data = _sysRxReportService.GetMGMTStatusSummaryReport(queryInfo, ref totalRow);
            queryInfo.TotalRow = totalRow;

            var viewModel = new DashboardMGMTStatusSummaryViewModel
            {
                CommonSysRxReportViewModel = queryInfo.MapTo<CommonSysRxReportViewModel>(),
                MGMTStatusSummaryViewModels = data.MapTo<MGMTStatusSummaryViewModel>()
            };
            return RenderRazorViewToString("~/Views/SysRxReport/MGMTStatusSummaryReport.cshtml", viewModel);
        }

        private string HandleMGMTUserProductivityReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            var data = _sysRxReportService.GetMGMTUserProductivityReport(queryInfo, ref totalRow);
            queryInfo.TotalRow = totalRow;

            var viewModel = new DashboardMGMTUserProductivityViewModel
            {
                CommonSysRxReportViewModel = queryInfo.MapTo<CommonSysRxReportViewModel>(),
                MGMTUserProductivityViewModels = data.MapTo<MGMTUserProductivityViewModel>()
            };
            return RenderRazorViewToString("~/Views/SysRxReport/MGMTUserProductivity.cshtml", viewModel);
        }

	}
}