﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Framework.DomainModel.Entities;
using Newtonsoft.Json;
using WorkComp.Models.Referral;

namespace WorkComp.Controllers
{
    public partial class ReferralController
    {
        private static void ProcessForReasonReferralInfo(DashboardReferralDataViewModel viewModel, string reasonReferralInfo)
        {
            if (string.IsNullOrEmpty(reasonReferralInfo))
            {
                return;
            }
            var reasonReferralData = JsonConvert.DeserializeObject<Dictionary<string, object>>(reasonReferralInfo);
            var listAllJson = reasonReferralData.SingleOrDefault(g => g.Key.Equals("listAll")).Value.ToString();
            var listAll = JsonConvert.DeserializeObject<Collection<dynamic>>(listAllJson);
            var objResult = new List<ReferralReasonReferral>();
            if (listAll != null && listAll.Count != 0)
            {
                foreach (var item in listAll.Where(o => o.Id != 0))
                {
                    var objAdd = new ReferralReasonReferral
                    {
                        ReasonReferralId = item.Id
                    };
                    objResult.Add(objAdd);
                }
            }
            viewModel.ReasonReferrals = objResult;
        }

        private static bool CheckUserChangeReasonReferralWhenUpdate(IEnumerable<ReferralReasonReferral> listOld,
            IEnumerable<ReferralReasonReferral> listNew)
        {
            var listIdOld = listOld.OrderBy(o => o.ReasonReferralId.GetValueOrDefault()).Select(o => o.ReasonReferralId.GetValueOrDefault()).ToList();
            var listIdNew = listNew.OrderBy(o => o.ReasonReferralId.GetValueOrDefault()).Select(o => o.ReasonReferralId.GetValueOrDefault()).ToList();
            return !(listIdOld.Count == listIdNew.Count && new HashSet<int>(listIdOld).SetEquals(listIdNew));
        }
    }
}