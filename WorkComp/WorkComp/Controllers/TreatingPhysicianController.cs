﻿using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Service.Diagnostics;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.NpiNumber;

namespace WorkComp.Controllers
{
    public class TreatingPhysicianController : ApplicationControllerGeneric<NpiNumber, DashboardNpiNumberDataViewModel>
    {
        //
        // GET: /State/
        private readonly INpiNumberService _npiNumberService;
        public TreatingPhysicianController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, INpiNumberService npiNumberService)
            : base(authenticationService, diagnosticService, npiNumberService)
        {
            _npiNumberService = npiNumberService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.NpiNumber, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardNpiNumberDataViewModel
            {
                SharedViewModel = new DashboardNpiNumberShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBoxForTreating;
            }
            return View("../NpiNumber/Create",viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.NpiNumber, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBoxForTreating;
            }
            return View("../NpiNumber/Update", viewModel);
        }

    }
}