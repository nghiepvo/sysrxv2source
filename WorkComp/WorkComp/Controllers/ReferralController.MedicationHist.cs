﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Exceptions;
using Framework.Service.Translation;
using Framework.Utility;
using Newtonsoft.Json;
using WorkComp.Attributes;
using WorkComp.Models.Drug;
using WorkComp.Models.Referral;

namespace WorkComp.Controllers
{
    public partial class ReferralController
    {
        private static void ProcessForMedicationHistoryInfo(DashboardReferralDataViewModel viewModel, string medicationHistoryInfo)
        {
            if (string.IsNullOrEmpty(medicationHistoryInfo))
            {
                return;
            }
            var medicationHistoryInfoData = JsonConvert.DeserializeObject<Dictionary<string, object>>(medicationHistoryInfo);
            var noMedicationHistory = medicationHistoryInfoData.SingleOrDefault(g => g.Key.Equals("NoMedicationHistory")).Value;
            viewModel.NoMedicationHistory = noMedicationHistory as bool?;
            if (viewModel.NoMedicationHistory.GetValueOrDefault())
            {
                viewModel.MedicationHistories = new List<ReferralMedicationHistory>();
                return;
            }
            var medicationList = medicationHistoryInfoData.SingleOrDefault(g => g.Key.Equals("MedicationList")).Value.ToString();
            var medicationListData = JsonConvert.DeserializeObject<Dictionary<string, object>>(medicationList);
            var listAllJson = medicationListData.SingleOrDefault(g => g.Key.Equals("listAll")).Value.ToString();
            var listAll = JsonConvert.DeserializeObject<Collection<MedicationHistoryGridItemViewModel>>(listAllJson);
            var objResult = new List<ReferralMedicationHistory>();
            if (listAll != null && listAll.Count != 0)
            {
                CheckMedicationHistoryValid(listAll);
                foreach (var item in listAll.Where(o => o.Drug != null && o.Drug.KeyId != 0))
                {
                    var objAdd = new ReferralMedicationHistory
                    {
                        DrugId = item.Drug.KeyId,
                        DaysSupply = item.DaysSupply,
                        Dosage = item.Dosage,
                        FillDate = item.FillDate
                    };
                    if (item.DosageUnit == null || item.DosageUnit.KeyId == 0)
                    {
                        objAdd.DosageUnit = null;
                    }
                    else
                    {
                        objAdd.DosageUnit = item.DosageUnit.KeyId;
                    }
                    if (item.ProvidedBy == null || item.ProvidedBy.KeyId == 0)
                    {
                        objAdd.ProvidedById = null;
                    }
                    else
                    {
                        objAdd.ProvidedById = item.ProvidedBy.KeyId;
                    }
                    objResult.Add(objAdd);
                }
            }
            viewModel.MedicationHistories = objResult;
        }

        private static void CheckMedicationHistoryValid(Collection<MedicationHistoryGridItemViewModel> listMedication)
        {
            if (listMedication.Count == 0)
            {
                return;
            }
            var failed = false;
            var validationResult = new List<ValidationResult>();
            // Check if no select icd code
            if (listMedication.Any(o => o.Drug.KeyId <= 0))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Drug Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            //Check if there is >=2 Id
            var checkDupicate = listMedication.GroupBy(s => new { DrugId = s.Drug.KeyId, DosageUnitId = s.DosageUnit.KeyId, s.Dosage, s.DrugClass, ProvidedBy = s.ProvidedBy.KeyId, s.FillDate, s.DaysSupply }).Any(g => g.Count() > 1);
            if (checkDupicate)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DuplicateItemText"), "\"Drug Name\", \"Dosage\" and \"Dosage Unit\"");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", "ReferralMedicationHistory", 0, null, "ReferralMedicationHistoryRule") { ValidationResults = validationResult };
            if (failed)
            {
                // Give messages on every rule that failed
                throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
            }
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.View)]
        public JsonResult GetMedicationHistories(int? referralId)
        {
            var listMedicationReferral = _referralService.GetMedicationHistoriesByReferral(referralId.GetValueOrDefault());
            // create list grid 
            var objGrid = new List<MedicationHistoryGridItemViewModel>();
            foreach (var medicationItem in listMedicationReferral)
            {
                var objAdd = new MedicationHistoryGridItemViewModel
                {
                   RowGuid=Guid.NewGuid(),
                   ReferralId=referralId,
                   Drug=new LookupInGridDataSourceViewModel
                   {
                       KeyId = medicationItem.DrugId,
                       Name = medicationItem.Drug == null ? "" : medicationItem.Drug.Name
                   },
                   DaysSupply = medicationItem.DaysSupply,
                   Id = medicationItem.Id,
                   ProvidedBy = new LookupInGridDataSourceViewModel
                   {
                       KeyId = medicationItem.ProvidedById.GetValueOrDefault(),
                       Name = medicationItem.ProvidedBy==null?"":medicationItem.ProvidedBy.Name
                   },
                   Dosage = medicationItem.Dosage,
                   FillDate = medicationItem.FillDate,
                   DosageUnit = new LookupInGridDataSourceViewModel
                   {
                       KeyId = medicationItem.DosageUnit.GetValueOrDefault(),
                       Name = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.DosageUnit.ToString(), medicationItem.DosageUnit.GetValueOrDefault().ToString())
                   },
                   DrugClass = medicationItem.Drug == null ? "" : medicationItem.Drug.Class
                };
                objGrid.Add(objAdd);
            }
            dynamic dynamicResult = new { Data = objGrid, TotalRowCount = objGrid.Count };
            var clientsJson = Json(dynamicResult, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }

        private static bool CheckUserChangeMedicationHist(bool noMedicationHistOld, bool noMedicationHistNew,
            List<ReferralMedicationHistory> listMedicationOld, List<ReferralMedicationHistory> listMedicationNew)
        {
            if (noMedicationHistOld != noMedicationHistNew)
            {
                return true;
            }
            if (listMedicationOld.Count != listMedicationNew.Count)
            {
                return true;
            }
            listMedicationOld =
                listMedicationOld.OrderBy(o => o.DrugId)
                    .ThenBy(o => o.DosageUnit.GetValueOrDefault())
                    .ThenBy(o => o.Dosage)
                    .ToList();
            listMedicationNew =
                listMedicationNew.OrderBy(o => o.DrugId)
                    .ThenBy(o => o.DosageUnit.GetValueOrDefault())
                    .ThenBy(o => o.Dosage)
                    .ToList();
            for (var i = 0; i < listMedicationNew.Count; i++)
            {
                var objOld = listMedicationOld[i];
                var objNew = listMedicationNew[i];
                if (objOld.DrugId != objNew.DrugId || objOld.DaysSupply != objNew.DaysSupply ||
                    objOld.Dosage != objNew.Dosage || objOld.DosageUnit != objNew.DosageUnit ||
                    objOld.ProvidedById != objNew.ProvidedById || objOld.FillDate != objNew.FillDate)
                {
                    return true;
                }
            }
            return false;
        }
    }
}