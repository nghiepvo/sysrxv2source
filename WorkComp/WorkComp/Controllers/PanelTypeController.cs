﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using DotNetOpenAuth.Messaging;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Exceptions;
using Framework.Mapping;
using Framework.Service.Diagnostics;
using Framework.Service.Translation;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.PanelType;

namespace WorkComp.Controllers
{
    public class PanelTypeController : ApplicationControllerGeneric<PanelType, DashboardPanelTypeDataViewModel>
    {
        private readonly IPanelTypeService _panelTypeService;
        private readonly IPanelService _panelService;
        public PanelTypeController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService,
                                    IPanelTypeService panelTypeService, IPanelService panelService)
            : base(authenticationService, diagnosticService, panelTypeService)
        {
            _panelTypeService = panelTypeService;
            _panelService = panelService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardPanelTypeIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "PanelTypeGrid",
                ModelName = "PanelType",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 1250,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _panelTypeService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardPanelTypeDataViewModel
            {
                SharedViewModel = new DashboardPanelTypeShareViewModel
                {
                    CreateMode = true,
                    PanelCodeGrid = new PanelCodeGridViewModel
                    {
                        GridId = "PanelCodeGrid"
                    }
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create", viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.Add)]
        public int Create(PanelTypeParameter parameters)
        {
            var viewModel = MapFromClientParameters(parameters);
            var listPanelCodeUpdate = ProcessMappingFromPanelCodeGrid(parameters.PanelCodeParams);
            var objListPanelCodeForEntity = new List<PanelCode>();
            // Check if create from copy page
            if (viewModel.SharedViewModel.Id > 0)
            {
                var entityCopy = MasterFileService.GetById(viewModel.SharedViewModel.Id);
                var listPanelCodeOld = entityCopy.PanelCodes.ToList();
                // Delete all old item
                foreach (var oldItem in listPanelCodeOld)
                {
                    oldItem.IsDeleted = true;
                }
            }
            // Add list data in grid( which is edit)
            objListPanelCodeForEntity.AddRange(listPanelCodeUpdate);
            // Because we have to copy data for dual list box => we have to maintain the Id => we will remove id here
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            var entity = viewModel.MapTo<PanelType>();
            // Mapping data from grid in view
            entity.PanelCodes = objListPanelCodeForEntity;
            var savedEntity = MasterFileService.Add(entity);
            return savedEntity.Id;
        }

        private static void CheckPanelCodeValid(Collection<PanelCodeGridItemViewModel> listPanelCode)
        {
            
            var failed = false;
            var validationResult = new List<ValidationResult>();
            // Check if no select panel name
            if (listPanelCode.Count == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Panel Code List").Replace("field ", "");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (listPanelCode.Any(o => o.PanelId.GetValueOrDefault() <= 0))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Panel Code Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            //Check if there is >=2 PanelName
            var checkDupicate = listPanelCode.GroupBy(s => s.PanelId.GetValueOrDefault()).Any(g => g.Count() > 1);
            if (checkDupicate)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DuplicateItemText"), "Panel Code Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", "PanelType", 0, null, "PanelTypeRule") { ValidationResults = validationResult };
            if (failed)
            {
                // Give messages on every rule that failed
                throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
            }
        }
        /// <summary>
        /// Parse Json to list panel code
        /// </summary>
        /// <param name="panelCodeDataParam"></param>
        /// <returns></returns>
        private static IEnumerable<PanelCode> ProcessMappingFromPanelCodeGrid(string panelCodeDataParam)
        {
            var panelCodeData = JsonConvert.DeserializeObject<Dictionary<string, object>>(panelCodeDataParam);
            var listAllJson = panelCodeData.SingleOrDefault(g => g.Key.Equals("listAll")).Value.ToString();
            var listAll = JsonConvert.DeserializeObject<Collection<PanelCodeGridItemViewModel>>(listAllJson);
            // Check valid
            CheckPanelCodeValid(listAll);
            var objResult = new List<PanelCode>();
            if (listAll != null && listAll.Count != 0)
            {
                foreach (var panelCodeGridItemVo in listAll.Where(o => o.PanelId.GetValueOrDefault() != 0))
                {
                    // Create Urine
                    if (!string.IsNullOrEmpty(panelCodeGridItemVo.Urine))
                    {
                        var objUrineAdd = new PanelCode
                        {
                            CptCode = panelCodeGridItemVo.CptCode,
                            Code = panelCodeGridItemVo.Urine,
                            SampleTestingTypeId = (int)SampleTestingTypeEnum.Urine,
                            PanelId = panelCodeGridItemVo.PanelId.GetValueOrDefault()
                        };
                        objResult.Add(objUrineAdd);
                    }
                    // Create OralFluid
                    if (!string.IsNullOrEmpty(panelCodeGridItemVo.OralFluid))
                    {
                        var objOralFluidAdd = new PanelCode
                        {
                            CptCode = panelCodeGridItemVo.CptCode,
                            Code = panelCodeGridItemVo.OralFluid,
                            SampleTestingTypeId = (int)SampleTestingTypeEnum.OralFluid,
                            PanelId = panelCodeGridItemVo.PanelId.GetValueOrDefault()
                        };
                        objResult.Add(objOralFluidAdd);
                    }
                    // Create Blood
                    if (!string.IsNullOrEmpty(panelCodeGridItemVo.Blood))
                    {
                        var objBloodAdd = new PanelCode
                        {
                            CptCode = panelCodeGridItemVo.CptCode,
                            Code = panelCodeGridItemVo.Blood,
                            SampleTestingTypeId = (int)SampleTestingTypeEnum.Blood,
                            PanelId = panelCodeGridItemVo.PanelId.GetValueOrDefault()
                        };
                        objResult.Add(objBloodAdd);
                    }
                    // Create Hair
                    if (!string.IsNullOrEmpty(panelCodeGridItemVo.Hair))
                    {
                        var objHairAdd = new PanelCode
                        {
                            CptCode = panelCodeGridItemVo.CptCode,
                            Code = panelCodeGridItemVo.Hair,
                            SampleTestingTypeId = (int)SampleTestingTypeEnum.Hair,
                            PanelId = panelCodeGridItemVo.PanelId.GetValueOrDefault()
                        };
                        objResult.Add(objHairAdd);
                    }
                }
            }
            return objResult;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = false;
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.Update)]
        public ActionResult Update(PanelTypeParameter parameters)
        {
            var viewModel = MapFromClientParameters(parameters);
            byte[] lastModified = null;

            if (ModelState.IsValid)
            {
                var listPanelCodeUpdate = ProcessMappingFromPanelCodeGrid(parameters.PanelCodeParams);
                var entity = MasterFileService.GetById(viewModel.SharedViewModel.Id);
                var mappedEntity = viewModel.MapPropertiesToInstance(entity);
                var listPanelCodeOld = mappedEntity.PanelCodes;
                // Delete all panel old
                foreach (var oldItem in listPanelCodeOld)
                {
                    oldItem.IsDeleted = true;
                }
                // Add listUpdate
                mappedEntity.PanelCodes.AddRange(listPanelCodeUpdate);
                lastModified = MasterFileService.Update(mappedEntity).LastModified;
            }

            return Json(new { Error = string.Empty, Data = new { LastModified = lastModified } }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<PanelType, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.View)]
        public JsonResult GetLookupForReferral(LookupQuery queryInfo)
        {
            var selector = new Func<PanelType, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            var queryData = _panelTypeService.GetLookupForReferral(queryInfo, selector);
            var clientsJson = Json(queryData, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 200,
                    Name = "PayerName",
                    Text = "Payer",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 300,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    ColumnWidth = 300,
                    Name = "Code",
                    Text = "Code",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 4,
                    ColumnWidth = 200,
                    Name = "IsBasic",
                    Text = "Standard",
                    ColumnJustification = GridColumnJustification.Left
                } 
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.View)]
        public JsonResult GetLookupItem(LookupItem lookupItem)
        {
            var selector = new Func<PanelType, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupItemForEntity(lookupItem, selector);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.View)]
        public JsonResult ListPanelStandard()
        {
            var queryData = _panelTypeService.GetListPanelTypeStandard();
            queryData.Insert(0, new LookupItemVo
            {
                KeyId = 0,
                DisplayName = "-- Select standard --"
            });
            var clientsJson = Json(queryData, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.View)]
        public JsonResult GetPanelCodes(int panelTypeId)
        {
            // Get list panel code
            var listPanelCode = _panelTypeService.GetListPanelCode(panelTypeId);
            if (listPanelCode.Count == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            // create list grid PanelCodeGridItemViewModel
            var objGrid = new List<PanelCodeGridItemViewModel>();
            var panelCode = listPanelCode[0];
            var oldPanelId = panelCode.PanelId;
            var panelCodeCount = listPanelCode.Count;
            for (var i = 0; i < panelCodeCount; i++)
            {
                var objAdd = new PanelCodeGridItemViewModel
                {
                    OralFluid = "",
                    Urine = "",
                    Blood = "",
                    Hair = "",
                    RowGuid = Guid.NewGuid()
                };
                var newPanelId = panelCode.PanelId;
                while (oldPanelId == newPanelId && i < panelCodeCount)
                {
                    objAdd.PanelId = oldPanelId;
                    objAdd.Panel = new LookupInGridDataSourceViewModel { KeyId = oldPanelId, Name = panelCode.Panel.Name };
                    objAdd.CptCode = panelCode.CptCode;
                    switch (panelCode.SampleTestingTypeId)
                    {
                        case (int)SampleTestingTypeEnum.Urine:
                            objAdd.Urine = panelCode.Code;
                            break;
                        case (int)SampleTestingTypeEnum.OralFluid:
                            objAdd.OralFluid = panelCode.Code;
                            break;
                        case (int)SampleTestingTypeEnum.Blood:
                            objAdd.Blood = panelCode.Code;
                            break;
                        case (int)SampleTestingTypeEnum.Hair:
                            objAdd.Hair = panelCode.Code;
                            break;
                    }
                    i++;
                    if (i < panelCodeCount)
                    {
                        panelCode = listPanelCode[i];
                        oldPanelId = panelCode.PanelId;
                    }
                }
                if (oldPanelId != newPanelId || i == panelCodeCount)
                {
                    objGrid.Add(objAdd);
                    i--;
                }
            }
            dynamic dynamicResult = new { Data = objGrid, TotalRowCount = objGrid.Count };
            var clientsJson = Json(dynamicResult, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.View)]
        public JsonResult GetLookupPanel(LookupQuery queryInfo)
        {
            var selector = new Func<Panel, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            var queryData = _panelService.GetLookup(queryInfo, selector);

            var clientsJson = Json(queryData, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }
    }
}