﻿using Framework.DomainModel;
using Framework.Service.Diagnostics;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Models;

namespace WorkComp.Controllers
{
    public abstract class ApplicationControllerBase : ApplicationControllerGeneric<Entity, MasterfileViewModelBase<Entity>>
    {
        //
        // GET: /ApplicationControllerBase/

        protected ApplicationControllerBase(IAuthenticationService authenticationService, IDiagnosticService diagnosticService)
            : base(authenticationService, diagnosticService, null)
        {
        }

        protected ApplicationControllerBase(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IMasterFileService<Entity> masterfileService) 
            : base(authenticationService, diagnosticService, masterfileService)
        {
        }
        
    }
}
