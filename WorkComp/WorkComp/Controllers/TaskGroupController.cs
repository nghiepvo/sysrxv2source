﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Service.Diagnostics;
using Framework.Utility;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.TaskGroup;

namespace WorkComp.Controllers
{
    public class TaskGroupController : ApplicationControllerGeneric<TaskGroup, DashboardTaskGroupDataViewModel>
    {
        private readonly ITaskGroupService _taskGroupService;
        private readonly ITaskTemplateService _taskTemplateService;
        public TaskGroupController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService,
                                    ITaskGroupService taskGroupService, ITaskTemplateService taskTemplateService)
            : base(authenticationService, diagnosticService, taskGroupService)
        {
            _taskGroupService = taskGroupService;
            _taskTemplateService = taskTemplateService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardTaskGroupIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "TaskGroupGrid",
                ModelName = "TaskGroup",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600,
                CheckIsReadOnlyRow = true
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return base.DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _taskGroupService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardTaskGroupDataViewModel
            {
                SharedViewModel = new DashboardTaskGroupShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            // Because we have to copy data for dual list box => we have to maintain the Id
            //viewModel.SharedViewModel.Id = 0;
            //viewModel.Id = 0;
            return View("Create", viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.Add)]
        public int Create(TaskGroupParameter parameters)
        {
            var viewModel = MapFromClientParameters(parameters);
            // Because we have to copy data for dual list box => we have to maintain the Id => we will remove id here
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            var entity = viewModel.MapTo<TaskGroup>();
            // Add taskTemplateTaskGroup for entity
            var shareViewModel = viewModel.SharedViewModel as DashboardTaskGroupShareViewModel;
            if (shareViewModel != null && shareViewModel.TaskTemplateIds != null && shareViewModel.TaskTemplateIds.Count != 0)
            {
                foreach (var taskTemplateId in shareViewModel.TaskTemplateIds)
                {
                    var objTaskGroupTaskTemplate = new TaskGroupTaskTemplate
                    {
                        TaskTemplateId = taskTemplateId
                    };
                    entity.TaskGroupTaskTemplates.Add(objTaskGroupTaskTemplate);
                }
            }
            var savedEntity = MasterFileService.Add(entity);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = false;
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.Update)]
        public ActionResult Update(TaskGroupParameter parameters)
        {
            var viewModel = MapFromClientParameters(parameters);
            byte[] lastModified = null;

            if (ModelState.IsValid)
            {
                var entity = MasterFileService.GetById(viewModel.SharedViewModel.Id);
                var mappedEntity = viewModel.MapPropertiesToInstance(entity);
                // Delete all item for TaskGroupTaskTemplate
                foreach (var taskGroupTaskTemplate in mappedEntity.TaskGroupTaskTemplates)
                {
                    taskGroupTaskTemplate.IsDeleted = true;
                }
                // Add taskTemplateTaskGroup for entity
                var shareViewModel = viewModel.SharedViewModel as DashboardTaskGroupShareViewModel;
                if (shareViewModel != null && shareViewModel.TaskTemplateIds != null && shareViewModel.TaskTemplateIds.Count!=0)
                {
                    foreach (var taskTemplateId in shareViewModel.TaskTemplateIds)
                    {
                        var objTaskGroupTaskTemplate = new TaskGroupTaskTemplate
                        {
                            TaskTemplateId = taskTemplateId
                        };
                        entity.TaskGroupTaskTemplates.Add(objTaskGroupTaskTemplate);
                    }
                }

                lastModified = MasterFileService.Update(mappedEntity).LastModified;
            }

            return Json(new { Error = string.Empty, Data = new { LastModified = lastModified } }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }
        
        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 700,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    Name = "IsDefault",
                    Text = "Default",
                     ColumnWidth = 200,
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<TaskGroup, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskGroup, OperationAction = OperationAction.View)]
        public JsonResult GetDropdownList(LookupQuery queryInfo)
        {
            var selector = new Func<TaskGroup, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            var queryData = _taskGroupService.GetLookup(queryInfo, selector);
            queryData.Insert(0,new LookupItemVo
            {
                DisplayName = "-- Select Task Group --",
                KeyId = 0
            });
            var clientsJson = Json(queryData, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.View)]
        public JsonResult GetTaskTemplate(int taskGroupId, int? referralId)
        {
            var listTaskTemplate = _taskGroupService.GetListTaskTemplate(taskGroupId, referralId);
            if (listTaskTemplate.Count == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objResult = listTaskTemplate.Select(o => new TaskTemplateGridItemViewModel
            {
                Description = o.Description,
                Title = o.Title,
                AssignTo =
                    new LookupInGridDataSourceViewModel
                    {
                        KeyId = o.AssignToId.GetValueOrDefault(),
                        Name = o.AssignTo == null ? "" : o.AssignTo.FirstName + " " + (string.IsNullOrEmpty(o.AssignTo.MiddleName) ? "" : o.AssignTo.MiddleName + " ") + o.AssignTo.LastName
                    },
                StartDate = DateTime.Now,
                DueDate =
                    DateTime.Now.AddDays(o.NumDueDate.GetValueOrDefault()).AddHours(o.NumDueHour.GetValueOrDefault()),
                Id = o.Id,
                RowGuid = Guid.NewGuid(),
                TaskTemplate = new LookupInGridDataSourceViewModel {KeyId = o.Id, Name = o.Title},
                TaskType =
                    new LookupInGridDataSourceViewModel
                    {
                        KeyId = o.TaskTypeId.GetValueOrDefault(),
                        Name =
                            XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.TaskType.ToString(),
                                o.TaskTypeId.GetValueOrDefault().ToString())
                    }
            }).ToList();
            dynamic dynamicResult = new { Data = objResult, TotalRowCount = objResult.Count };
            var clientsJson = Json(dynamicResult, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }
    }
}