﻿using System;
using System.Text;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Service.Diagnostics;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.Diary;

namespace WorkComp.Controllers
{
    public class DiaryController : ApplicationControllerGeneric<Diary, DashboardDiaryDataViewModel>
    {
        private readonly IDiaryService _diaryService;
        public DiaryController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IDiaryService diaryService)
            : base(authenticationService, diagnosticService, diaryService)
        {
            _diaryService = diaryService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardDiaryDataViewModel
            {
                SharedViewModel = new DashboardDiaryShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.Grid;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Add)]
        public int Create(DiaryParameter parameters)
        {
            byte[] data = Convert.FromBase64String(parameters.SharedParameter);
            parameters.SharedParameter = Encoding.UTF8.GetString(data);
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }
        

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetDiaryWhenChangeReferral(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _diaryService.GetDiaryWhenChangeReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }
    }
}
