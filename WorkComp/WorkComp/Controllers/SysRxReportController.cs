﻿using System.Collections.Generic;
using System.Web.Mvc;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Mapping;
using Framework.Service.Diagnostics;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models.SysRxReport;


namespace WorkComp.Controllers
{
    public partial class SysRxReportController : ApplicationControllerBase
    {
        private readonly ISysRxReportService _sysRxReportService;
        private readonly IPayerService _payerService;
        public SysRxReportController(IAuthenticationService authenticationService,
                                    IDiagnosticService diagnosticService,
                                    ISysRxReportService sysRxReportService, IPayerService payerService)
            : base(authenticationService, diagnosticService)
        {
            _sysRxReportService = sysRxReportService;
            _payerService = payerService;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.SysRxReport, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {

            var viewModel = new DashboardSysRxRepotIndexViewModel();
            //Get defaul data source
            var objLookupItemPayer = _payerService.GetDefaulDataSource();
            if (objLookupItemPayer != null)
            {
                viewModel.DefaulPayerDataSource = objLookupItemPayer;
            }
            return View(viewModel);
        }

        [HttpPost]
        public JsonResult GetReport(SysRxReportQueryInfo queryInfo)
        {
            var totalRow = 0;
            var content = string.Empty;
            switch (queryInfo.ReportType)
            {
                case SysRxReportType.UtilizationByInjuredWorkerReport:
                    content = HandleUtilizationByInjuredWorkerReport(queryInfo, ref totalRow);
                    break;
                case SysRxReportType.TestResultsDetailReport:
                    content = HandleTestResultDetailReport(queryInfo, ref totalRow);
                    break;
                case SysRxReportType.CancellationDetailReport:
                    content = HandleCancellationDetailtReport(queryInfo, ref totalRow);
                    break;
                case SysRxReportType.TestResultsSummaryReport:
                    content = HandleTestResultsSummaryReport(queryInfo, ref totalRow);
                    break;
                case SysRxReportType.MGMTStatusSummaryReport:
                    content = HandleMGMTStatusSummaryReport(queryInfo, ref totalRow);
                    break;
                case SysRxReportType.MGMTUserProductivityReport:
                    content = HandleMGMTUserProductivityReport(queryInfo, ref totalRow);
                    break;
            }
            return Json(new { content, totalRow }, JsonRequestBehavior.AllowGet);
        }
    }
}
