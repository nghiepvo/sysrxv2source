﻿using System;
using System.Text;
using System.Web.Mvc;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Service.Diagnostics;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using SolrNet.DSL;
using WorkComp.Attributes;
using WorkComp.Models.Alert;


namespace WorkComp.Controllers
{
    public class AlertController : ApplicationControllerBase
    {
        //
        // GET: /State/
        private readonly IAlertService _alertService;
        public AlertController(IAuthenticationService authenticationService,
                                    IDiagnosticService diagnosticService,
                                    IAlertService alertService)
            : base(authenticationService, diagnosticService)
        {
            _alertService = alertService;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardAlertIndexViewModel();
            return View(viewModel);
        }
        
        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetDataForGrid(ReferralQueryInfo queryInfo)
        {
            if (!string.IsNullOrEmpty(queryInfo.SearchString))
            {
                queryInfo.SearchString = queryInfo.SearchString.Replace(' ', '+');
                queryInfo.SearchString = Encoding.UTF8.GetString(Convert.FromBase64String(queryInfo.SearchString));
                queryInfo.ParseParameters(queryInfo.SearchString);
            }

             var currentUserId = AuthenticationService.GetCurrentUser().User.Id;
            var isAdminRole = AuthenticationService.GetCurrentUser().User.UserRole.Name.ToLower() == "admin";
            var data = _alertService.GetDataAlertForGrid(currentUserId, isAdminRole, queryInfo.SearchTerms, queryInfo.DueDateFrom, queryInfo.DueDateTo, queryInfo.Skip, queryInfo.Take);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetAlertPartial()
        {
            var currentUserId = AuthenticationService.GetCurrentUser().User.Id;
            var isAdminRole = AuthenticationService.GetCurrentUser().User.UserRole.Name.ToLower() == "admin";
            var data = _alertService.GetAlertPartial(currentUserId, isAdminRole, 0 , 20);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
