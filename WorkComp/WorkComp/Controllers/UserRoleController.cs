﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using DotNetOpenAuth.Messaging;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Common;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.UserRole;

namespace WorkComp.Controllers
{
    public class UserRoleController : ApplicationControllerGeneric<UserRole, DashboardUserRoleDataViewModel>
    {
        //
        // GET: /UserRole/
        private readonly IUserRoleService _userRoleService;
        public UserRoleController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IUserRoleService userRoleService)
            : base(authenticationService, diagnosticService, userRoleService)
        {
            _userRoleService = userRoleService;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardUserRoleIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "UserRoleGrid",
                ModelName = "UserRole",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardUserRoleDataViewModel
            {
                SharedViewModel = new DashboardUserRoleShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.Add)]
        public int Create(UserRoleParameter parameters)
        {
            var viewModel = MapFromClientParameters(parameters);

            var listRoleFunctionUpdate = parameters.CheckAll ? GetAllRoleFunction() : ProcessMappingFromUserRoleGrid(parameters.UserRoleFunctionData);
            var listRoleFunctionOld = new List<UserRoleFunction>();
            var objListFunctionForEntity = new List<UserRoleFunction>();
            // Check if create from copy page
            if (viewModel.SharedViewModel.Id > 0)
            {
                var entityCopy = MasterFileService.GetById(viewModel.SharedViewModel.Id);
                listRoleFunctionOld = entityCopy.UserRoleFunctions.ToList();
                // Check user have edit some value in list role old => delete role old and add role new
                foreach (var oldItem in listRoleFunctionOld)
                {
                    if (listRoleFunctionUpdate.Any(o => o.DocumentTypeId == oldItem.DocumentTypeId))
                    {
                        oldItem.IsDeleted = true;
                    }
                }
            }

            //after check user removed, remove item of the list new with conditions has property IsDelete equal true;

            //Copy listRoleFunctionUpdate
            var listRoleFunctionUpdateRecheck = listRoleFunctionUpdate.ToList();
            foreach (var item in listRoleFunctionUpdateRecheck.Where(item => item.IsDeleted))
            {
                listRoleFunctionUpdate.Remove(item);
            }

            // Add list data in grid( which is edit)
            objListFunctionForEntity.AddRange(listRoleFunctionUpdate);
            // Add list data in old
            foreach (var itemOld in listRoleFunctionOld.Where(o=>!o.IsDeleted))
            {
                var objAdd = new UserRoleFunction
                {
                    DocumentTypeId = itemOld.DocumentTypeId,
                    SecurityOperationId = itemOld.SecurityOperationId
                };
                objListFunctionForEntity.Add(objAdd);
            }
            // Because we have to copy data for dual list box => we have to maintain the Id => we will remove id here
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            var entity = viewModel.MapTo<UserRole>();
            //Nghiep Fixed 
            entity.IsEnable = true;
            // Mapping data from grid in view
            entity.UserRoleFunctions = objListFunctionForEntity;
            var savedEntity = MasterFileService.Add(entity);
            return savedEntity.Id;
        }
        /// <summary>
        /// Mapping data from grid to list UserRoleFunction
        /// </summary>
        /// <param name="userRoleFunctionDataParam"></param>
        /// <returns></returns>
        private List<UserRoleFunction> ProcessMappingFromUserRoleGrid(string userRoleFunctionDataParam)
        {
            var userRoleFunctionData = JsonConvert.DeserializeObject<Dictionary<string, object>>(userRoleFunctionDataParam);
            var updatedListJson = userRoleFunctionData.SingleOrDefault(g => g.Key.Equals("updated")).Value.ToString();
            var listUpdate = JsonConvert.DeserializeObject<Collection<UserRoleFunctionGridVo>>(updatedListJson);
            var objResult = new List<UserRoleFunction>();
            if (listUpdate != null && listUpdate.Count != 0)
            {
                foreach (var userRoleFunctionGridVo in listUpdate)
                {
                    #region Nghiep
                    //Implement View
                    var objViewAdd = new UserRoleFunction
                    {
                        DocumentTypeId = userRoleFunctionGridVo.Id,
                        SecurityOperationId = (int)OperationAction.View,
                        IsDeleted = !userRoleFunctionGridVo.IsView
                    };
                    objResult.Add(objViewAdd);

                    //Implement View insert
                    var objInsertAdd = new UserRoleFunction
                    {
                        DocumentTypeId = userRoleFunctionGridVo.Id,
                        SecurityOperationId = (int)OperationAction.Add,
                        IsDeleted = !userRoleFunctionGridVo.IsInsert
                    };
                    objResult.Add(objInsertAdd);

                    //Implement View update
                    var objUpdateAdd = new UserRoleFunction
                    {
                        DocumentTypeId = userRoleFunctionGridVo.Id,
                        SecurityOperationId = (int)OperationAction.Update,
                        IsDeleted = !userRoleFunctionGridVo.IsUpdate
                    };
                    objResult.Add(objUpdateAdd);
                    //Implement View delete
                    var objDeleteAdd = new UserRoleFunction
                    {
                        DocumentTypeId = userRoleFunctionGridVo.Id,
                        SecurityOperationId = (int)OperationAction.Delete,
                        IsDeleted = !userRoleFunctionGridVo.IsDelete
                    };
                    objResult.Add(objDeleteAdd);
                    #endregion

                }
            }
            return objResult;
        }

        private List<UserRoleFunction> GetAllRoleFunction()
        {
            var objResult = new List<UserRoleFunction>();
            var objListDocumentType = _userRoleService.GetAllDocumentType();
            foreach (var item in objListDocumentType)
            {
                var objViewAdd = new UserRoleFunction
                {
                    DocumentTypeId = item.Id,
                    SecurityOperationId = (int)OperationAction.View
                };
                objResult.Add(objViewAdd);
                var objInsertAdd = new UserRoleFunction
                {
                    DocumentTypeId = item.Id,
                    SecurityOperationId = (int)OperationAction.Add
                };
                objResult.Add(objInsertAdd);
                var objUpdateAdd = new UserRoleFunction
                {
                    DocumentTypeId = item.Id,
                    SecurityOperationId = (int)OperationAction.Update
                };
                objResult.Add(objUpdateAdd);
                var objDeleteAdd = new UserRoleFunction
                {
                    DocumentTypeId = item.Id,
                    SecurityOperationId = (int)OperationAction.Delete
                };
                objResult.Add(objDeleteAdd);
            }
            return objResult;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.Update)]
        public ActionResult Update(UserRoleParameter parameters)
        {
            var viewModel = MapFromClientParameters(parameters);
            byte[] lastModified = null;

            if (ModelState.IsValid)
            {
                var listRoleFunctionUpdate = parameters.CheckAll ? GetAllRoleFunction() : ProcessMappingFromUserRoleGrid(parameters.UserRoleFunctionData);
                var entity = MasterFileService.GetById(viewModel.SharedViewModel.Id);
                var mappedEntity = viewModel.MapPropertiesToInstance(entity);
                var listRoleFunctionOld = mappedEntity.UserRoleFunctions;
                // Check user have edit some value in list role old => delete role old and add role new
                foreach (var oldItem in listRoleFunctionOld)
                {
                    if (listRoleFunctionUpdate.Any(o => o.DocumentTypeId == oldItem.DocumentTypeId))
                    {
                        oldItem.IsDeleted = true;
                    }
                }
                //after check user removed, remove item of the list new with conditions has property IsDelete equal true;

                //Copy listRoleFunctionUpdate
                var listRoleFunctionUpdateRecheck = listRoleFunctionUpdate.ToList();
                foreach (var item in listRoleFunctionUpdateRecheck.Where(item => item.IsDeleted))
                {
                    listRoleFunctionUpdate.Remove(item);
                }
                // Add listUpdate
                mappedEntity.UserRoleFunctions.AddRange(listRoleFunctionUpdate);
                lastModified = MasterFileService.Update(mappedEntity).LastModified;
            }

            return Json(new { Error = string.Empty, Data = new { LastModified = lastModified } }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            // Because we have to copy data for grid edit role => we have to maintain the Id
            //viewModel.SharedViewModel.Id = 0;
            //viewModel.Id = 0;
            return View("Create", viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _userRoleService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    Name = "Name",
                    Text = "Name",
                     ColumnWidth = 700,
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<UserRole, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.View)]
        public JsonResult GetLookupItem(LookupItem lookupItem)
        {
            var selector = new Func<UserRole, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupItemForEntity(lookupItem, selector);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.View)]
        public JsonResult GetRoleFunction(int id)
        {
            var queryData = _userRoleService.GetRoleFunction(id);
            var clientsJson = Json(queryData, JsonRequestBehavior.AllowGet);

            return clientsJson; 
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.UserRole, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }
    }
}