﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Web.Mvc;
using Common;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Service.Diagnostics;
using Framework.Service.Translation;
using Framework.Utility;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.User;

namespace WorkComp.Controllers
{
    public class UserController : ApplicationControllerGeneric<User, DashboardUserDataViewModel>
    {
        private readonly IUserService _userService;
        private readonly List<string> _imageExtensions = new List<string> { ".JPG", ".JPE", ".BMP", ".GIF", ".PNG" };

        public UserController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IUserService userService)
            : base(authenticationService, diagnosticService, userService)
        {
            _userService = userService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardUserIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "UserGrid",
                ModelName = "User",
                UseCheckBoxColumn = true,
                UseDeleteColumn = false,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 90,
                    Name = "FullName",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 70,
                    Name = "UserName",
                    Text = "UserName",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    ColumnWidth = 73,
                    Name = "Role",
                    Text = "Role",
                    ColumnJustification = GridColumnJustification.Left
                }
                ,
                new ViewColumnViewModel
                {
                    ColumnOrder = 4,
                    ColumnWidth = 150,
                    Name = "Email",
                    Text = "Email",
                    ColumnJustification = GridColumnJustification.Left
                }
                ,
                new ViewColumnViewModel
                {
                    ColumnOrder = 5,
                    ColumnWidth = 100,
                    Name = "Phone",
                    Text = "Phone",
                    ColumnJustification = GridColumnJustification.Left
                }
                ,
                new ViewColumnViewModel
                {
                    ColumnOrder = 6,
                    ColumnWidth = 160,
                    Name = "Address",
                    Text = "Address",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 7,
                    ColumnWidth = 70,
                    Name = "State",
                    Text = "State",
                    ColumnJustification = GridColumnJustification.Left
                }
                ,
                new ViewColumnViewModel
                {
                    ColumnOrder = 8,
                    ColumnWidth = 70,
                    Name = "City",
                    Text = "City",
                    ColumnJustification = GridColumnJustification.Left
                }
                ,
                new ViewColumnViewModel
                {
                    ColumnOrder = 9,
                    ColumnWidth = 60,
                    Name = "Zip",
                    Text = "Zip",
                    ColumnJustification = GridColumnJustification.Left
                }
                ,
                new ViewColumnViewModel
                {
                    ColumnOrder = 10,
                    ColumnWidth = 280,
                    Name = "IsActive",
                    Text = "IsActive",
                    ColumnJustification = GridColumnJustification.Left,
                    CustomTemplate = "userCommandTemplate",
                }
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardUserDataViewModel
            {
                SharedViewModel = new DashboardUserShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.Add)]
        public int Create(UserParameter parameters)
        {
            var viewModel = MapFromClientParameters(parameters);
            var entity = viewModel.MapTo<User>();
            // Generate password
            var randomPassword = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 6);
            entity.Password = PasswordHelper.HashString(randomPassword, entity.UserName);
            entity.IsActive = true;
            entity.IsEnable = true;
            var savedEntity = MasterFileService.Add(entity);

            if (savedEntity.Id > 0 && !string.IsNullOrEmpty(savedEntity.Avatar))
            {
                var tempFile = Path.GetTempPath() + savedEntity.Avatar;
                if (System.IO.File.Exists(tempFile) && _imageExtensions.Contains(Path.GetExtension(tempFile).ToUpperInvariant()))
                {
                    //Upload file to folder
                    var path = HttpContext.Server.MapPath(UserUploadPathFile.UserAvatar);
                    if (Directory.Exists(path))
                    {
                        var fileName = path + savedEntity.Avatar;
                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }

                        // Move file from temp folder
                        System.IO.File.Move(tempFile,fileName);
                    }
                }
            }
            // Send the email to user to notify
            if (savedEntity.Id > 0)
            {
                var webLink = AppSettingsReader.GetValue("Url", typeof (String)) as string;
                var imgSrc =webLink+ "/Content/images/logo.png";
                var urlChangePass = webLink + "/user/changepass/"+savedEntity.Id;
                var fromEmail=AppSettingsReader.GetValue("EmailFrom", typeof (String)) as string;
                var displayName = AppSettingsReader.GetValue("EmailFromDisplayName", typeof(String)) as string;
                var emailContent = TemplateHelpper.FormatTemplateWithContentTemplate(
                    TemplateHelpper.ReadContentFromFile(TemplateConfigFile.CreateUserEmailTemplate, true),
                    new
                    {
                        img_src = imgSrc,
                        full_name = savedEntity.FirstName + " " + (savedEntity.MiddleName ?? "") + savedEntity.LastName,
                        web_link = webLink,
                        user_name = entity.UserName,
                        password = randomPassword,
                        url_change_pass = urlChangePass
                    });
                // send email
                EmailHelper.SendEmail(fromEmail, new[] { savedEntity.Email }, SystemMessageLookup.GetMessage("SubjectToSendEmailForCreateUser"),
                                        emailContent,true, displayName);
            }
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);

            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.Update)]
        public ActionResult Update(UserParameter parameters)
        {
            var viewModel = MapFromClientParameters(parameters);


            byte[] lastModified = null;

            if (ModelState.IsValid)
            {
                var entity = MasterFileService.GetById(viewModel.SharedViewModel.Id);
                var mappedEntity = viewModel.MapPropertiesToInstance(entity);
                lastModified = MasterFileService.Update(mappedEntity).LastModified;

                if (!string.IsNullOrEmpty(mappedEntity.Avatar))
                {
                    var tempFile = Path.GetTempPath() + mappedEntity.Avatar;
                    if (System.IO.File.Exists(tempFile) && _imageExtensions.Contains(Path.GetExtension(tempFile).ToUpperInvariant()))
                    {
                        //Upload file to folder
                        var path = HttpContext.Server.MapPath(UserUploadPathFile.UserAvatar);
                        if (Directory.Exists(path))
                        {
                            var fileName = path + mappedEntity.Avatar;
                            if (System.IO.File.Exists(fileName))
                            {
                                System.IO.File.Delete(fileName);
                            }
                            // Move file from temp folder
                            System.IO.File.Move(tempFile, fileName);
                        }
                    }
                }
            }

            return Json(new { Error = string.Empty, Data = new { LastModified = lastModified } }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create", viewModel);
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.Update)]
        public JsonResult ActiveUser(int id)
        {
            _userService.Active(id);
            return Json(new { Error = string.Empty }, JsonRequestBehavior.AllowGet);
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<User, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = (o.FirstName ?? "") + " " + (o.MiddleName ?? "") + " " + (o.LastName ?? "")
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _userService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.Update)]
        public JsonResult SetPassword(int userId)
        {
            var user = _userService.GetById(userId);
            if (user!= null)
            {
                var randomPassword = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 6);
                user.Password = PasswordHelper.HashString(randomPassword, user.UserName);
                _userService.Update(user);

                var webLink = AppSettingsReader.GetValue("Url", typeof(String)) as string;
                var imgSrc = webLink + "/Content/images/logo.png";
                var urlChangePass = webLink + "/user/changepass/" + user.Id;
                var fromEmail = AppSettingsReader.GetValue("EmailFrom", typeof(String)) as string;
                var displayName = AppSettingsReader.GetValue("EmailFromDisplayName", typeof(String)) as string;
                var emailContent = TemplateHelpper.FormatTemplateWithContentTemplate(
                    TemplateHelpper.ReadContentFromFile(TemplateConfigFile.RestorePasswordTemplate, true),
                    new
                    {
                        img_src = imgSrc,
                        full_name = user.FirstName + " " + (user.MiddleName ?? "") + user.LastName,
                        web_link = webLink,
                        user_name = user.UserName,
                        password = randomPassword,
                        url_change_pass = urlChangePass
                    });
                // send email
                var isSendMail = EmailHelper.SendEmail(fromEmail, new[] { user.Email }, SystemMessageLookup.GetMessage("SubjectToSendEmailForCreateUser"),
                                        emailContent, true, displayName);
                return Json(isSendMail, JsonRequestBehavior.AllowGet);
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.None)]
        public ActionResult ChangePassword()
        {
            var viewModel = new Dictionary<string, string>
            {
                {"PasswordNotMatch", SystemMessageLookup.GetMessage("PasswordNotMatch")}
            };
            return View("_ChangePassword", viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.None)]
        public JsonResult SaveChangePassword(string changePasswrodModle)
        {
            var data = JsonConvert.DeserializeObject<ChangePasswordViewModel>(changePasswrodModle);
            if (data != null)
            {
                var id = AuthenticationService.GetCurrentUser().User.Id;
                var user = _userService.GetById(id);
                if (user != null)
                {
                    var oldPassword = PasswordHelper.HashString(data.PasswordOld, user.UserName);
                    if (user.Password.Equals(oldPassword))
                    {
                        var newPassword = PasswordHelper.HashString(data.PasswordNew, user.UserName);
                        user.Password = newPassword;
                        _userService.Update(user);
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { Error = SystemMessageLookup.GetMessage("PasswordOldInvalid") }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { Error = SystemMessageLookup.GetMessage("ChangePasswordError") }, JsonRequestBehavior.AllowGet);
        }
        
    }
}