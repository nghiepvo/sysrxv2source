﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AutoMapper;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Service.Translation;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using WorkComp.Attributes;
using WorkComp.Models.Referral;
using WorkComp.Models.SendMailWithDualGrid;

namespace WorkComp.Controllers
{
    public partial class ReferralController
    {
        private readonly IConfigurationService _configurationService;
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult Detail(int id)
        {
            
            var viewModel = new DashboardReferralDetailViewModel();
            var data = _referralService.GetReferralDeltailById(id);
            if (data != null)
            {
                viewModel = data.MapTo<DashboardReferralDetailViewModel>();
                var reasonReferralInfors = _referralService.GetReasonReferralInfos(id);
                if (reasonReferralInfors != null && reasonReferralInfors.Count > 0)
                {
                    viewModel.DashboardReferralDetailReferralInfoViewModel.ReasonReferralInReferralDetailVos = reasonReferralInfors;
                    viewModel.DashboardReferralDetailReferralInfoViewModel.SetReasonReferralInfos();
                }

                var dataSampleTestingTypes = _referralSampleTestingTypeService.GetListTestingTypeName(id);
                if (dataSampleTestingTypes != null && dataSampleTestingTypes.Count > 0)
                {
                    var testingInfo = new TestingInfo
                    {
                        CheckBlood = !string.IsNullOrEmpty(dataSampleTestingTypes.FirstOrDefault(o => o.ToLower().Equals("blood"))),
                        CheckHair = !string.IsNullOrEmpty(dataSampleTestingTypes.FirstOrDefault(o => o.ToLower().Equals("hair"))),
                        CheckOralFluid = !string.IsNullOrEmpty(dataSampleTestingTypes.FirstOrDefault(o => o.ToLower().Equals("oral fluid"))),
                        CheckUrine = !string.IsNullOrEmpty(dataSampleTestingTypes.FirstOrDefault(o => o.ToLower().Equals("urine"))),
                    };
                    viewModel.DashboardReferralDetailReferralInfoViewModel.TestingInfo = testingInfo;
                    viewModel.RequisitionMappingModel.TestingInfo = testingInfo;
                }
                viewModel.RequisitionMappingModel.Drug = _drugService.GetDrugs(id);
                viewModel.PreviousId = _referralService.GetPreviousReferralId(id);
                viewModel.NextId = _referralService.GetNextReferralId(id);
                var referralResultViewModel = _referralService.GetReferralResultInfo(id);
                var referralDetailExtension = referralResultViewModel.MapTo<ReferralDetailExtension>();
                viewModel.ReferralResultViewModel.ReferralResultDetailVo = referralResultViewModel;
                viewModel.ReferralResultViewModel.ReferralDetailExtention = referralDetailExtension;
            }
            
            return View(viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult SendMailResult(string dataJson)
        {
            var result = false;
            byte[] dataEncode = Convert.FromBase64String(dataJson);
            dataJson = Encoding.UTF8.GetString(dataEncode);

            var data = JsonConvert.DeserializeObject<ReferralResultSendMailViewModel>(dataJson);
            if (data!= null)
            {
                Dictionary<string, string> pathEmailsList = null;
                if (data.ReferralId != 0)
                {
                    var objReferral =
                        _referralService.FirstOrDefault(
                            o => o.Id == data.ReferralId && !string.IsNullOrEmpty(o.FileResult));
                    if (objReferral != null)
                    {
                        pathEmailsList = new Dictionary<string, string>
                        {
                            {data.FileName, Path.Combine(Server.MapPath("~/FileUpload/ResultPdf"), objReferral.FileResult)}
                        };
                    }
                }
                var fromEmail = AppSettingsReader.GetValue("EmailFrom", typeof(String)) as string;
                var displayName = AppSettingsReader.GetValue("EmailFromDisplayName", typeof(String)) as string;
                var listTo = data.To.Split(';').Where(o => !string.IsNullOrEmpty(o)).ToArray();
                var listCc= data.Cc.Split(';').Where(o => !string.IsNullOrEmpty(o)).ToArray();
                if (data.ReferralId == 0)
                {
                    data.Message = data.Message + "<br>" + data.IncludeContent;
                }

                if (data.SecureEmail)
                {
                    data.Title = SystemMessageLookup.GetMessage("SecureEmail") + " " + data.Title;
                }
                result = Common.EmailHelper.SendEmail(fromEmail, listTo, data.Title, data.Message, true, displayName, pathEmailsList, listCc);

                return result ? Json(new { Data = SystemMessageLookup.GetMessage("SendEmailSuccess") }, JsonRequestBehavior.AllowGet) : Json(new { Error = SystemMessageLookup.GetMessage("SendEmailFailed") }, JsonRequestBehavior.AllowGet);
                
            }
            return Json(new { Error = SystemMessageLookup.GetMessage("SendEmailFailed") }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetListEmailUser(QueryInfo queryInfo)
        {
            var data = _userService.GetListEmailUser(queryInfo);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult ShowPopupSendEmail(string selectedRowIdArray, int? referralId)
        {
            var includeContentHtml = string.Empty;
            var urlWeb = ConfigurationManager.AppSettings["Url"];
            if (!string.IsNullOrEmpty(selectedRowIdArray))
            {
                var strArrayId = selectedRowIdArray.Split(',');
                includeContentHtml += SystemMessageLookup.GetMessage("IncludedMessageHtml");
                includeContentHtml += "<ul>";
                includeContentHtml = strArrayId.Aggregate(includeContentHtml, (current, idStr) => current + ("<li> Rerferral ID: <a href='" + urlWeb + "Referral/Detail/" + idStr + "' >" + idStr + "</a></li>"));
                includeContentHtml += "</ul>";
            }
            var viewModel = new SendMailWithDualGridViewModel()
            {
                Claimant = string.Empty,
                EmailTitle = string.Empty,
                ParentId = 0,
                IncludeContent = includeContentHtml,
                ModelName = "Referral",
            };

            if (referralId.GetValueOrDefault() != 0)
            {
                var data = _referralService.GetSendMailInfo(referralId.GetValueOrDefault());
                
                if (data!= null)
                {
                    viewModel.Claimant = data.Claimant;
                    viewModel.ParentId = data.ReferralId;
                    viewModel.ClaimNumber = data.ClaimNumber;
                }
                var configuration = _configurationService.Get(o => o.Id == 14).FirstOrDefault();
                if (configuration != null)
                {
                    viewModel.EmailTitle = configuration.Value;
                }
            }

            return View("SendMailWithDualGrid/_SendMailWithDualGridViewModel", viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult ShowPopupSelectEmail(int? referralId)
        {
            return View("DualGrid/_DualGridViewModel", referralId);
        }
    }
}