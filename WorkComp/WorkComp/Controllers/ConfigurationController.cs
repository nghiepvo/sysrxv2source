﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.Configuration;

namespace WorkComp.Controllers
{
    public class ConfigurationController : ApplicationControllerGeneric<Configuration, DashboardConfigurationDataViewModel>
    {
        //
        // GET: /Configuration/
        private readonly IConfigurationService _configurationService;


        public ConfigurationController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IConfigurationService configurationService) : base(authenticationService, diagnosticService, configurationService)
        {
            _configurationService = configurationService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Configuration, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardConfigurationIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "ConfigurationGrid",
                ModelName = "Configuration",
                UseCheckBoxColumn = false,
                UseDeleteColumn = false,
                WidthPopup = 1000,
                HeightPopup = 650,
                CanCopyItem = false,
                CanDeleteMulti = false,
                CanCreateItem = false
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }
        
        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Configuration, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Configuration, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Configuration, OperationAction = OperationAction.Update)]
        public ActionResult Update(ConfigurationParameter parameters)
        {
            byte[] data = Convert.FromBase64String(parameters.SharedParameter);
            parameters.SharedParameter = Encoding.UTF8.GetString(data);
            return UpdateMasterFile(parameters);
        }
        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 75,
                    Name = "TypeName",
                    Text = "Type",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 200,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 425,
                    Name = "Value",
                    Text = "Value",
                    ColumnJustification = GridColumnJustification.Left,
                    IsHtmlEncode = true
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    ColumnWidth = 75,
                    Name = "Configurable",
                    Text = "Configurable",
                    ColumnJustification = GridColumnJustification.Center,
                    IsBoolValue = true
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 4,
                    ColumnWidth = 50,
                    Name = "IsHtml",
                    Text = "Is Html",
                    ColumnJustification = GridColumnJustification.Center,
                    IsBoolValue = true
                }
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Configuration, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }
    }
}
