﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.ReferralType;

namespace WorkComp.Controllers
{
    public class ReferralTypeController : ApplicationControllerGeneric<ReferralType, DashboardReferralTypeDataViewModel>
    {
        //
        // GET: /State/
        private readonly IReferralTypeService _referralTypeService;

        public ReferralTypeController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IReferralTypeService referralTypeService)
            : base(authenticationService, diagnosticService, referralTypeService)
        {
            _referralTypeService = referralTypeService;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardReferralTypeIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "ReferralTypeGrid",
                ModelName = "ReferralType",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardReferralTypeDataViewModel
            {
                SharedViewModel = new DashboardReferralTypeShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.Add)]
        public int Create(ReferralTypeParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.Update)]
        public ActionResult Update(ReferralTypeParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create",viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _referralTypeService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<ReferralType, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralType, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 1100,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

    }
}
