﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using Framework.Service.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Net.Mime;

namespace Common
{
    public static class EmailHelper
    {
        public static bool SendEmail(string fromEmail, string[] toEmail, string emailSubject, string emailContent, bool isHtmlFormat, string displayName, Dictionary<string, string> listAttachmentFilename = null, string[] ccEmail = null)
        {

            var message = new MailMessage();
            var settingsReader = new AppSettingsReader();
            var smtp = new SmtpClient
            {
                Host = (string)settingsReader.GetValue("Host", typeof(String)),
                //Port = Protector.Int(ConfigurationSettings.AppSettings["SMTP_PORT"], 587),
                EnableSsl = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential((string)settingsReader.GetValue("EmailFrom", typeof(String)), (string)settingsReader.GetValue("Password", typeof(String)))

            };
            message.From = new MailAddress(fromEmail, displayName);
            if (toEmail!= null && toEmail.Length > 0)
            {
                foreach (var item in toEmail.Where(item => !string.IsNullOrEmpty(item)))
                {
                    message.To.Add(new MailAddress(item));
                }
            }
            if (ccEmail!= null && ccEmail.Length > 0)
            {
                foreach (var item in ccEmail.Where(item => !string.IsNullOrEmpty(item)))
                {
                    message.CC.Add(new MailAddress(item));
                }
            }
            message.Subject = emailSubject;
            message.Body = emailContent;
            message.IsBodyHtml = isHtmlFormat;
            message.BodyEncoding = Encoding.UTF8;
            message.SubjectEncoding = Encoding.UTF8;
            if (listAttachmentFilename != null && listAttachmentFilename.Count > 0)
            {
                foreach (KeyValuePair<string, string> attachmentFilename in listAttachmentFilename)
                {
                    var attachment = new Attachment(attachmentFilename.Value, MediaTypeNames.Application.Octet);
                    var disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(attachmentFilename.Value);
                    disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename.Value);
                    disposition.ReadDate = File.GetLastAccessTime(attachmentFilename.Value);
                    disposition.FileName = attachmentFilename.Key;
                    disposition.Size = new FileInfo(attachmentFilename.Value).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    message.Attachments.Add(attachment);
                }
            }
            var diagnosticService = DependencyResolver.Current.GetService<IDiagnosticService>();

            try
            {
                if (diagnosticService != null)
                {
                    diagnosticService.Info("Send email");
                }
                smtp.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                if (diagnosticService != null)
                {
                    diagnosticService.Info("Send email have problem:" + ex);
                }
                return false;
            }

        }

        public static bool SendFax(string subject, string senderName, string recipientName, string faxNumber, string contentOrFilePath, ref string errorMess, bool isFilePath = false)
        {
            var diagnosticService = DependencyResolver.Current.GetService<IDiagnosticService>();
            faxNumber = Regex.Replace(faxNumber, "[^0-9]+", "");
            var settingsReader = new AppSettingsReader();
            var api = new wfApi();
            var url = (string)settingsReader.GetValue("FaxServer", typeof(String));
            api.Url = url;
            Int64 uid = 0;
            var user = (string)settingsReader.GetValue("FaxUserName", typeof(String));
            var password = (string)settingsReader.GetValue("FaxPassword", typeof(String));
            uid = api.AuthenticateUser(user, password);
            if (uid != 0 && uid != 1 && uid != 2)
            {
                // create file temp to store content
                string fullPath = string.Empty;
                if (!isFilePath)
                {
                    string filePath = Path.GetTempPath();
                    string fileName = Guid.NewGuid().ToString().Replace("-", "") + ".pdf";
                    var exportFileExportToPdf = new HtmToPdfProvider();
                    fullPath = exportFileExportToPdf.Export(filePath, fileName, contentOrFilePath, null);
                }
                else
                {
                    fullPath = contentOrFilePath;
                }
                if (string.IsNullOrEmpty(fullPath) || !File.Exists(fullPath))
                {
                    if (diagnosticService != null)
                    {
                        diagnosticService.Info("Cannot create pdf file to fax.");
                    }
                    return false;
                }

                string result = SendFax(api, uid, senderName, subject, recipientName, faxNumber, fullPath);

                Int64 msgNo = 0;
                if (Int64.TryParse(result, out msgNo))
                {
                    //message successfully send to server for processing, return value Message ID
                    result = msgNo.ToString();
                    if (diagnosticService != null)
                    {
                        diagnosticService.Info("Message sent: " + result);
                    }
                    return true;
                }
                //message fail to send to server for processing, return value = 0
                if (diagnosticService != null)
                {
                    diagnosticService.Info("Fail to send.");
                }
                return false;
            } 
            if (diagnosticService != null)
            {
                diagnosticService.Info("Authentication Failed.");
            }
            return false;
        }

        private static string SendFax(wfApi api, Int64 uid, string senderName, string subject, string recipientNameValue, string recipientFaxNumber, string filename)
        {
            var trackingKey = new string[2];
            var trackingValue = new string[2];
            trackingKey[0] = "Tracking Key 1";
            trackingValue[0] = "";
            trackingKey[1] = "Tracking Key 2";
            trackingValue[1] = "";
            //recipient information. sample below shows how to insert one recipient to the message.
            //to send to multiple fax recipients, just extend the list for information below.
            //recipient notify address, notification on send success and notification on send fail are optional.
            var recipientName = new string[1];
            var recipientCompany = new string[1];
            var recipientFax = new string[1];              //recipient fax number
            var isRawFax = new bool[1];                      //specify fax number as raw format
            var recipientNotifyAddress = new string[1];    //recipient email notification address
            var notifyRecipientOnSendSuccess = new bool[1];  //notify recipient on message send success
            var notifyRecipientOnSendFail = new bool[1];     //notify recipient on message send fail
            recipientName[0] = recipientNameValue;
            recipientCompany[0] = "";
            recipientFax[0] = recipientFaxNumber;
            isRawFax[0] = false;
            recipientNotifyAddress[0] = "";
            notifyRecipientOnSendSuccess[0] = false;
            notifyRecipientOnSendFail[0] = false;

            //attachment information

            FileStream fs = null;
            byte[] doc1;
            try
            {
                fs = File.OpenRead(filename);
                doc1 = new byte[fs.Length];
                fs.Read(doc1, 0, Convert.ToInt32(fs.Length));
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
            }


            //attach document from Bytes/Upload (multiple documents)
            var documentName = new string[1];
            var documentPath = new string[1];
            var documentBytes = new object[1];
            var isMergeDocument = new bool[1];

            documentName[0] = Path.GetFileName(filename);
            documentPath[0] = string.Empty;
            documentBytes[0] = doc1;
            isMergeDocument[0] = false;

            string result = api.SendMessage(uid
                , senderName
                , ""
                , subject
                , ""
                , ""
                , ""
                , 50
                , false
                , false
                , false
                , trackingKey
                , trackingValue
                , recipientName
                , recipientCompany
                , recipientFax
                , isRawFax
                , recipientNotifyAddress
                , notifyRecipientOnSendFail
                , notifyRecipientOnSendSuccess
                , documentName
                , documentPath
                , documentBytes
                , isMergeDocument);


            return result;

        }
    }
}
